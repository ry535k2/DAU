from base import *
from DAUConv2d.original import *
from DAUConv2d.mu_i import *
from DAUConv2d.mu_j import *
from DAUConv2d.one_mu import *
from DAUConv2d.zero_mu import *

from run import loop_parameters


class Net(nn.Module):
    def __init__(self, layer_type, *, no_units=None, sigma=None):
        super(Net, self).__init__()

        if layer_type == 'Conv2d':
            self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3, padding=1)
            self.conv2 = nn.Conv2d(in_channels=64, out_channels=8, kernel_size=3, padding=1)
        else:
            klass = eval(layer_type)
            self.conv1 = klass(in_channels=3, out_channels=64, no_units=no_units, height=32, width=32, sigma=sigma)
            self.conv2 = klass(in_channels=64, out_channels=8, no_units=no_units, height=16, width=16, sigma=sigma)

        self.norm1 = nn.BatchNorm2d(64)
        self.norm2 = nn.BatchNorm2d(8)

        self.pool2 = nn.MaxPool2d(kernel_size=2)
        self.pool4 = nn.MaxPool2d(kernel_size=4)

        self.fc1 = nn.Linear(8 * 4 * 4, 10)

    def forward(self, x):
        x = self.pool2(F.relu(self.norm1(self.conv1(x))))
        x = self.pool4(F.relu(self.norm2(self.conv2(x))))

        x = x.view(-1, 8 * 4 * 4)
        x = self.fc1(x)
        return x


if __name__ == '__main__':
    for title, layer_type, no_units, sigma in loop_parameters():
        net = Net(layer_type, no_units=no_units, sigma=sigma)
        print('\n' + title)
        print(net)
        summary(net, (3, 32, 32), device=device)
