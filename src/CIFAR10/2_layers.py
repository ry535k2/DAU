from base import *
from DAUConv2d.original import *
from DAUConv2d.mu_i import *
from DAUConv2d.mu_j import *
from DAUConv2d.one_mu import *
from DAUConv2d.zero_mu import *

from run import loop_parameters


class Net(nn.Module):
    def __init__(self, layer_type, *, no_units=None, sigma=None):
        super(Net, self).__init__()

        if layer_type == 'Conv2d':
            self.conv1 = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=3, padding=1)
            self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, padding=1)
        else:
            klass = eval(layer_type)
            self.conv1 = klass(in_channels=3, out_channels=16, no_units=no_units, height=32, width=32, sigma=sigma)
            self.conv2 = klass(in_channels=16, out_channels=32, no_units=no_units, height=16, width=16, sigma=sigma)

        self.norm1 = nn.BatchNorm2d(16)
        self.norm2 = nn.BatchNorm2d(32)

        self.pool = nn.MaxPool2d(kernel_size=2)

        self.fc = nn.Linear(in_features=32 * 8 * 8, out_features=10)

    def forward(self, x):
        x = self.pool(F.relu(self.norm1(self.conv1(x))))
        x = self.pool(F.relu(self.norm2(self.conv2(x))))

        x = x.view(-1, 32 * 8 * 8)
        x = self.fc(x)
        return x


if __name__ == '__main__':
    for title, layer_type, no_units, sigma in loop_parameters():
        net = Net(layer_type, no_units=no_units, sigma=sigma)
        print('\n' + title)
        print(net)
        summary(net, (3, 32, 32), device=device)
