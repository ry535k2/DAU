from base import *
from DAUConv2d.original import *
from DAUConv2d.mu_i import *
from DAUConv2d.mu_j import *
from DAUConv2d.one_mu import *
from DAUConv2d.zero_mu import *

from run import loop_parameters


class Net(nn.Module):
    def __init__(self, layer_type, *, no_units=None, sigma=None):
        super(Net, self).__init__()

        out_ch1 = 64
        out_ch2 = 128
        out_ch3 = 256
        in_ch2 = 32
        in_ch3 = 64
        self.in_ch4 = in_ch4 = 64

        assert layer_type == 'DAUConv2di'
        self.conv1 = DAUConv2di(in_channels=3, out_channels=out_ch1, no_units=no_units, height=32, width=32, sigma=sigma)
        self.conv2 = DAUConv2di(in_channels=in_ch2, out_channels=out_ch2, no_units=no_units, height=16, width=16, sigma=sigma)
        self.conv3 = DAUConv2di(in_channels=in_ch3, out_channels=out_ch3, no_units=no_units, height=8, width=8, sigma=sigma)
        self.conv1s = nn.Conv2d(in_channels=out_ch1, out_channels=in_ch2, kernel_size=1)
        self.conv2s = nn.Conv2d(in_channels=out_ch2, out_channels=in_ch3, kernel_size=1)
        self.conv3s = nn.Conv2d(in_channels=out_ch3, out_channels=in_ch4, kernel_size=1)

        self.norm1 = nn.BatchNorm2d(out_ch1)
        self.norm2 = nn.BatchNorm2d(out_ch2)
        self.norm3 = nn.BatchNorm2d(out_ch3)
        self.norm1s = nn.BatchNorm2d(in_ch2)
        self.norm2s = nn.BatchNorm2d(in_ch3)
        self.norm3s = nn.BatchNorm2d(in_ch4)

        self.pool = nn.MaxPool2d(kernel_size=2)

        self.fc = nn.Linear(in_features=in_ch4 * 4 * 4, out_features=10)

    def forward(self, x):
        x = self.pool(F.relu(self.norm1(self.conv1(x))))
        x = F.relu(self.norm1s(self.conv1s(x)))

        x = self.pool(F.relu(self.norm2(self.conv2(x))))
        x = F.relu(self.norm2s(self.conv2s(x)))

        x = self.pool(F.relu(self.norm3(self.conv3(x))))
        x = F.relu(self.norm3s(self.conv3s(x)))

        x = x.view(-1, self.in_ch4 * 4 * 4)
        x = self.fc(x)
        return x


if __name__ == '__main__':
    for title, layer_type, no_units, sigma in loop_parameters():
        net = Net(layer_type, no_units=no_units, sigma=sigma)
        print('\n' + title)
        print(net)
        summary(net, (3, 32, 32), device=device)
