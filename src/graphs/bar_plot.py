from graphs.helpers import *


def bar_plot(data):
    for title, group in groups(data):
        group = sorted(group, key=lambda x: -x['max_accuracy'])
        titles = [x['short_title'] for x in group]
        means = [x['max_accuracy'] for x in group]

        ind = np.arange(len(group))
        fig, ax = plt.subplots(figsize=(6, 12), dpi=200)
        ax.bar(ind, means,
               # yerr=stds,
               )

        ax.set_ylabel('Accuracy')
        ax.set_title(title)
        ax.set_xticks(ind)
        ax.set_xticklabels(titles, rotation='vertical')
        # ax.set_ylim(.6, None)

        show_or_save(title)


if __name__ == '__main__':
    data = get_data(input('md file: '))
    # data = get_data('/home/v/PycharmProjects/tensorflow/results/all/stdout.md')

    bar_plot(data)
