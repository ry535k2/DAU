from graphs.helpers import *


def box_plot(data):
    for title, group in groups(data):
        group = sorted(group, key=lambda x: -x['mean_accuracy'])
        titles = [x['short_title'] for x in group]
        means = [x['max_accuracies'] for x in group]

        fig, ax = plt.subplots(figsize=(6, 7), dpi=200)
        ax.set_ylabel('Accuracy')
        ax.set_title(title)
        ax.set_xticklabels(titles, rotation='vertical')
        ax.boxplot(means,
                   # sym='',
                   # whis=2,
                   )

        plt.show()


def print_means(data):
    for title, group in groups(data):
        print(title)
        group = sorted(group, key=lambda x: -x['mean_accuracy'])
        for d in group:
            print(round(d['mean_accuracy'], 3), '\t\t', d['short_title'])


def merge():
    files = [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    merged = []
    for file in files:
        data = get_data('/home/v/PycharmProjects/tensorflow/results/7_gpu_cifar/' + str(file) + '.md')

        for d in data:
            added = False
            for m in merged:
                if d['title'] == m['title']:
                    m['max_accuracies'].append(d['max_accuracy'])
                    added = True
                    break
            if not added:
                merged.append({
                    'title': d['title'],
                    'max_accuracies': [d['max_accuracy']],
                    'dataset': d['dataset'],
                    'architecture': d['architecture'],
                    'layer_type': d['layer_type'],
                })
                keys = ['units_str', 'sigma_str', 'units', 'sigma']
                for key in keys:
                    if key in d:
                        merged[-1][key] = d[key]

    # calculating mean_accuracy
    for m in merged:
        m['mean_accuracy'] = np.mean(m['max_accuracies'])

    return merged


if __name__ == '__main__':
    data = merge()
    print_means(data)
    box_plot(data)
