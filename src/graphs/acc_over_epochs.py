from graphs.helpers import *


def acc_and_loss(data, title):
    for experiment in data:
        if title not in experiment['title']:
            continue

        x = list(range(1, len(experiment['loss']) + 1))

        fig, ax1 = plt.subplots()
        ax1.set_xlabel('epoch')
        ax1.set_ylabel('loss')
        loss, = ax1.plot(x, experiment['loss'], color='orange', label='loss')
        ax1.set_title(experiment['title'])

        ax2 = ax1.twinx()
        ax2.set_ylabel('accuracy')
        acc, = ax2.plot(x, experiment['accuracy'], color='green', label='accuracy')
        ax2.set_ylim([0, 1])

        plt.legend(handles=(acc, loss))
        show_or_save(experiment['title'])


def acc(data):
    for title, group in groups(data):
    # for title, group in groups(data, group_by=('dataset',)):
        lines = []
        cmap = get_colormap(len(group))

        fig, ax = plt.subplots(figsize=(12, 6), dpi=200)
        ax.set_xlabel('epoch')
        ax.set_ylabel('accuracy')
        ax.set_title(title)
        # ax.set_ylim([.88, 1])

        for i, experiment in enumerate(group):
            y = experiment['accuracy']
            # y = experiment['accuracy'][80:]
            x = list(range(1, len(y) + 1))
            acc, = ax.plot(x, y, color=cmap(i), label=experiment['short_title'], linestyle=get_linestyle(i))
            lines.append(acc)

        plt.legend(handles=lines, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)
        show_or_save(title)


if __name__ == '__main__':
    data = get_data(input('md file: '))
    # data = get_data('/home/v/PycharmProjects/tensorflow/results/1st_big_run/stdout.md')

    acc(data)
    # acc_and_loss(data, 'CIFAR10, small_fc, DAUConv2dZ')
