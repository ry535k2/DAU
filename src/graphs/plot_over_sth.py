from graphs.helpers import *


def plot(data):
    keys = ['units_str', 'sigma_str']
    for i, key in enumerate(keys):
        for title, group in groups(data, group_by=[key]):
            lines = []
            cmap = get_colormap(len(group))
            other = keys[(i + 1) % 2]

            draw = False
            j = -1
            for title2, group2 in groups(group, group_by=['dataset'], properties=('architecture', other)):
                j += 1
                group2 = [x for x in group2 if other in x]
                group2 = sorted(group2, key=lambda x: x[other])
                y = [experiment['max_accuracy'] for experiment in group2]
                if len(y) <= 1:
                    break

                if not draw:
                    draw = True
                    fig, ax = plt.subplots(figsize=(12, 6), dpi=200)

                    ax.set_xlabel(other[:-4])
                    ax.set_ylabel('accuracy')
                    ax.set_title(title)
                    # ax.set_xlim([80, 100])
                    # ax.set_ylim([.88, 1])

                x = [experiment[other[:-4]] for experiment in group2]
                acc, = ax.plot(x, y, color=cmap(j), label=title2, linestyle=get_linestyle(j))
                lines.append(acc)

            if not draw:
                continue

            plt.legend(handles=lines, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)
            show_or_save(title)


if __name__ == '__main__':
    # data = get_data(input('md file: '))
    data = get_data('/home/v/PycharmProjects/tensorflow/results/all/stdout.md')

    plot(data)
