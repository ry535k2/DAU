import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict


def get_colormap(n):
    return plt.cm.get_cmap('Paired_r', n + 1)  # ['Paired_r', 'Dark2', 'Accent', 'tab10']


def get_linestyle(i):
    linestyles = ['-', '--', '-.', ':']
    return linestyles[i % len(linestyles)]


def show_or_save(name):
    plt.show()
    # plt.savefig('/home/v/PycharmProjects/tensorflow/src/graphs/out/' + str(name) + '.png', bbox_inches='tight')


def extract1(text: str):
    text = text.strip()
    return float(text.split(':')[1].strip())


def add_property(experiment, title, part):
    parts = title.split(', ')
    if len(parts) > part:
        part = parts[part].strip()
        key, value = part.split(':')
        experiment[key + '_str'] = part
        if key == 'units':
            experiment[key.strip()] = int(value.strip())
        else:
            experiment[key.strip()] = float(value.strip())


def get_data(src):
    data = []
    params = ['total params: ', 'DAU params: ', 'other params: ']

    with open(src) as file:
        for line in file.readlines():
            if line.startswith('##'):
                title = line[2:].strip()
                parts = title.split(', ')
                data.append({
                    'title': title,
                    'loss': [],
                    'accuracy': [],
                    'dataset': parts[0],
                    'architecture': parts[1],
                    'layer_type': parts[2],
                })
                add_property(data[-1], title, 3)
                add_property(data[-1], title, 4)

            elif line.startswith('epoch:'):
                parts = line.split(',')
                data[-1]['loss'].append(extract1(parts[1]))
                data[-1]['accuracy'].append(extract1(parts[2]))

            for param in params:
                if line.startswith(param):
                    data[-1][param] = int(line[len(param):].replace(',', ''))

    # removing runs with <= 80 finished epochs
    data2 = []
    for d in data:
        if len(d['loss']) > 80:
            data2.append(d)
    data = data2

    # normalizing
    for experiment in data:
        experiment['accuracy'] = np.array(experiment['accuracy'])
        experiment['loss'] = np.array(experiment['loss'])
        experiment['loss'] *= 100 / max(experiment['loss'])

    # max acc
    for experiment in data:
        experiment['max_accuracy'] = np.max(experiment['accuracy'])

    return data


def global_condition(experiment):
    return True
    return experiment['dataset'] == 'CIFAR10'


def condition(experiment):
    return True

    draw = [
        # 'DAUConv2dZeroMu',
        # 'DAUConv2dOneMu'
    ]
    skip = [
        'DAUConv2dZeroMu',
        'DAUConv2dOneMu'
    ]

    for d in draw:
        if d in experiment['title']:
            return True
    for s in skip:
        if s in experiment['title']:
            return False
    return True
    # return False


def groups(data, *, group_by=('dataset', 'architecture'), properties = ('dataset', 'architecture', 'layer_type', 'units_str', 'sigma_str')):
    data = [experiment for experiment in data if global_condition(experiment)]

    groups = defaultdict(list)
    for experiment in data:
        group = ', '.join([experiment[property] for property in group_by if property in experiment])
        if len(group) == 0:
            continue
        if condition(experiment):
            groups[group].append(experiment)
            experiment['short_title'] = ', '.join(
                [experiment[property] for property in properties if property not in group_by and property in experiment])

    for title in sorted(groups.keys()):
        yield title, groups[title]
