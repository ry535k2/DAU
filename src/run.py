from base import *

dataset_params = {
    'MNIST': {
        'input_size': (1, 28, 28),
        'transform': transforms.Compose([
            # transforms.RandomCrop(28, padding=(2, 2)),
            transforms.ToTensor(),
            transforms.Normalize([0.5], [0.5]),
        ]),
        'batch_size': 64
    },
    'CIFAR10': {
        'input_size': (3, 32, 32),
        'transform': transforms.Compose([
            # transforms.RandomCrop(32, padding=(4, 4), padding_mode='reflect'),
            transforms.RandomOrder((
                # transforms.RandomHorizontalFlip(),
                # transforms.ColorJitter(.2, .1),
                # transforms.RandomAffine(5),
            )),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ]),
        'batch_size': 64
    },
}

layer_types = [
    {'layer_type': 'Conv2d', 'no_units': [None], 'sigma': [None]},
    {'layer_type': 'DAUConv2d', 'no_units': [2], 'sigma': [None]},
    {'layer_type': 'DAUConv2di', 'no_units': [2, 3, 4], 'sigma': [None]},
    {'layer_type': 'DAUConv2dj', 'no_units': [2, 3, 4], 'sigma': [None]},
    {'layer_type': 'DAUConv2dOneMu', 'no_units': [2, 3, 4], 'sigma': [None]},
    {'layer_type': 'DAUConv2dZeroMu', 'no_units': [1], 'sigma': [None]},
]
architectures = [
    # 'paper',
    '62k_params',
    # 'small_fc',
    # 'i_test',
    # '2_layers',
    # '3_layers',
]
datasets = [
    'CIFAR10',
    # 'FashionMNIST',
    # 'MNIST',
]


def loop_parameters():
    for layer in layer_types:
        for no_units in layer['no_units']:
            for sigma in layer['sigma']:
                title = '{layer_type}{no_units}{sigma}'.format(
                    layer_type=layer['layer_type'],
                    no_units=', units: ' + str(no_units) if no_units is not None else '',
                    sigma=', sigma: ' + str(sigma) if sigma is not None else ''
                )
                yield title, layer['layer_type'], no_units, sigma


def loop_all():
    for dataset in datasets:
        for architecture in architectures:
            for title, layer_type, no_units, sigma in loop_parameters():
                title = '{data}, {arch}, {other}'.format(
                    data=dataset,
                    arch=architecture,
                    other=title
                )
                yield title, dataset, architecture, layer_type, no_units, sigma


if __name__ == '__main__':
    print('start:', datetime.now())
    for x in loop_all():
        print('*', x[0])

    for title, dataset, architecture, layer_type, no_units, sigma in loop_all():
        print('\n##', title)
        print('```text')

        try:
            temp = (dataset if dataset != 'FashionMNIST' else 'MNIST')
            Net = get_class(temp + '.' + architecture, 'Net')
            params = dataset_params[temp]

            net = Base(
                dataset=dataset,
                net=Net(layer_type, no_units=no_units, sigma=sigma),
                input_size=params['input_size'],
                transform=params['transform'],
                batch_size=params['batch_size'],
                title=title,
            )
            net.train(epochs=100)
            net.acc_over_classes()
        except:
            traceback.print_exc()

        print('```')

    print('\nfinish:', datetime.now())
