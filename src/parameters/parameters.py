from helpers.constants import *


def draw(mu):
    mu = mu.view(-1, 2).detach().cpu().numpy()
    plt.plot(mu[:, 1], mu[:, 0], 'ro')
    show_or_save('mu')


# src = '/home/v/PycharmProjects/tensorflow/results/all/FashionMNIST, 3_layers, DAUConv2d, units: 2.pt'
src = input('.pt file: ')

data = torch.load(src, map_location='cpu')

draw(data[-1]['conv1.mu'])
