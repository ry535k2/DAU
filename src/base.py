from helpers.constants import *


class Base:
    def __init__(self, *, batch_size, dataset, net, input_size, title, transform=None):
        self.batch_size = batch_size
        self.net = net
        self.input_size = input_size
        self.title = title

        # getting data
        dataset = get_class('torchvision.datasets', dataset)
        if transform is None:
            transform = transforms.ToTensor()
        try:
            self.get_dataset(dataset, transform, download=False)
        except:
            self.get_dataset(dataset, transform, download=True)

        self.no_train = len(self.trainloader.dataset.data)
        self.no_test = len(self.testloader.dataset.data)
        self.no_batches = self.no_train // self.batch_size

        # printing
        print('Config:', config)
        print('Device:', device)
        print()
        print('Network:', self.net)
        self.summary()

    def get_dataset(self, dataset, transform, download):
        trainset = dataset(root=config['data_folder'], train=True, download=download, transform=transform)
        self.trainloader = torch.utils.data.DataLoader(trainset, batch_size=self.batch_size, shuffle=True,
                                                       num_workers=config['num_workers'])

        testset = dataset(root=config['data_folder'], train=False, download=download, transform=transform)
        self.testloader = torch.utils.data.DataLoader(testset, batch_size=self.batch_size, shuffle=False, num_workers=config['num_workers'])

    def imshow(self, img):
        img = img / 2 + 0.5
        npimg = img.cpu().numpy()
        if len(npimg.shape) == 3:
            npimg = np.transpose(npimg, (1, 2, 0))
        plt.imshow(npimg)
        show_or_save('image')

    def show_batch(self, images, labels):
        self.imshow(torchvision.utils.make_grid(images))
        print(' '.join('%5s' % labels[j] for j in range(self.batch_size)))

    def show_first_batch(self):
        dataiter = iter(self.trainloader)
        images, labels = dataiter.next()
        self.show_batch(images, labels)

    def acc(self):
        correct = 0
        total = 0
        self.net.eval()
        with torch.no_grad():
            for data in self.testloader:
                inputs, labels = data[0].to(device), data[1].to(device)
                outputs = self.net(inputs)
                predicted = torch.argmax(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
        self.net.train()
        return correct / total

    def show_F(self):
        with torch.no_grad():
            for data in self.testloader:
                inputs, labels = data[0].to(device), data[1].to(device)
                outputs = self.net(inputs)
                predicted = torch.argmax(outputs.data, 1)

                for i in range(labels.size(0)):
                    if predicted[i] != labels[i]:
                        print("true: {}, predicted: {}".format(labels[i], predicted[i]))
                        self.imshow(torch.squeeze(inputs[i]))
                        input()

    def acc_over_classes(self):
        class_correct = list(0. for i in range(10))
        class_total = list(0. for i in range(10))
        with torch.no_grad():
            for data in self.testloader:
                inputs, labels = data[0].to(device), data[1].to(device)
                outputs = self.net(inputs)
                predicted = torch.argmax(outputs, 1)
                c = (predicted == labels).squeeze()
                for i in range(4):
                    label = labels[i]
                    class_correct[label] += c[i].item()
                    class_total[label] += 1

        for i in range(10):
            print('Accuracy of %5s : %2d %%' % (
                i, 100 * class_correct[i] / class_total[i]))

    def summary(self):
        summary(self.net, input_size=self.input_size, device=device)

    def save_params(self):
        torch.save(self.net.state_dict(), '{folder}{title}.pt'.format(
            folder=config['out_folder'],
            title=self.title,
        ))

    def train(self, *, epochs):
        time.start('train')

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.Adam(self.net.parameters(), lr=.005, weight_decay=1e-8)

        scheduler1 = optim.lr_scheduler.ReduceLROnPlateau(
            optimizer,
            factor=.5,
            min_lr=0.0001,
            patience=5,
            cooldown=5,
            verbose=True,
            threshold=1e-3
        )
        scheduler2 = optim.lr_scheduler.MultiStepLR(
            optimizer,
            milestones=[70, 95],
            gamma=.2
        )

        self.net.to(device)

        for epoch in range(epochs):  # loop over the dataset multiple times
            loss_sum = 0

            for i, data in enumerate(self.trainloader):
                # get the inputs; data is a list of [inputs, labels]
                # inputs, labels = data
                inputs, labels = data[0].to(device), data[1].to(device)

                # calculate error
                outputs = self.net(inputs)
                loss = criterion(outputs, labels)

                # change weights
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                loss_sum += loss.item()

                # print(i, loss.item())
                # time.print_()

            # changing lr
            scheduler1.step(loss_sum)
            scheduler2.step()

            print('epoch: {epoch}, loss: {loss}, accuracy: {acc}'.format(
                epoch=epoch,
                acc=self.acc(),
                loss=round(loss_sum, 2)
            ))
            # time.print_()

        self.save_params()
        time.end('train')
        time.print_()
