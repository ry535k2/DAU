from DAUConv2d_grid.original import *


class DAUConv2dOneMu(DAUConv2d):
    """
    ...
    """

    def __init__(self, in_channels, out_channels, no_units, height, width, gauss_kernel=None, sigma=None):
        super(DAUConv2dOneMu, self).__init__(in_channels, out_channels, no_units, height, width,
                                             gauss_kernel=gauss_kernel, sigma=sigma, init=False)

        self.mu = Parameter(torch.randn(no_units, 2, device=device) * .1)
        self.mu_flat = self.mu.view(-1, 2)
        self.grid = self.grid.repeat(in_channels * out_channels * no_units, 1, 1, 1)

    def interpolate(self, x):
        batch_size, in_channels, height, width = x.shape

        # repeating input
        x.transpose_(0, 1)
        x = x.repeat_interleave(self.out_channels * self.no_units, dim=0)

        # making grid
        grid = self.grid.permute(1, 2, 0, 3)
        grid = grid + (self.mu_flat * self.px_to_unit).repeat(self.in_channels * self.out_channels, 1)
        grid = grid.permute(2, 0, 1, 3)

        # actual interpolation
        interpolated = F.grid_sample(x, grid)

        # reshaping and applying weight
        interpolated = interpolated.view(in_channels, self.out_channels, self.no_units, batch_size, height, width)
        interpolated = interpolated.permute(3, 4, 5, 0, 1, 2)  # batch_size, height, width, in_channels, out_channels, no_units
        interpolated *= self.w
        interpolated = interpolated.permute(0, 3, 4, 1, 2, 5)  # batch_size, in_channels, out_channels, height, width, no_units

        # sum
        y = interpolated.sum(dim=(1, 5))

        return y


if __name__ == '__main__':
    height, width = 2, 2
    in_channels, out_channels, no_units = 2, 3, 4
    batches = 2

    layer = DAUConv2dOneMu(in_channels, out_channels, no_units, height, width,
                           gauss_kernel=tensor([1], dtype=float, device=device))

    input = torch.randn(batches, in_channels, height, width)
    # input[0][0][0][0] = 1

    pr(input, 'input')
    out = layer.forward(input)
    pr(out, 'out')

    # layer.draw_mu()

    # loss = out.sum()
    # loss.backward()
    # draw_grad_graph(loss)
