from DAUConv2d_grid.original import *


class DAUConv2dZeroMu(DAUConv2d):
    """
    ...
    """

    def __init__(self, in_channels, out_channels, no_units, height, width, gauss_kernel=None, sigma=None):
        super(DAUConv2dZeroMu, self).__init__(in_channels, out_channels, no_units, height, width,
                                              gauss_kernel=gauss_kernel, sigma=sigma, init=False)

    def interpolate(self, x):
        w = self.w.permute(2, 1, 0)  # no_units, out_channels, in_channels
        x = x.permute(2, 3, 1, 0).unsqueeze(2)  # height, width, 1, in_channels, batch_size
        y = w.matmul(x)  # height, width, no_units, out_channels, batch_size
        y = y.permute(4, 3, 0, 1, 2)  # batch_size, out_channels, height, width, no_units

        # sum
        y = y.sum(dim=4)

        return y


if __name__ == '__main__':
    height, width = 2, 2
    in_channels, out_channels, no_units = 2, 3, 4
    batches = 2

    layer = DAUConv2dZeroMu(in_channels, out_channels, no_units, height, width,
                            gauss_kernel=tensor([1], dtype=float, device=device))

    input = torch.randn(batches, in_channels, height, width)
    # input[0][0][0][0] = 1

    pr(input, 'input')
    out = layer.forward(input)
    pr(out, 'out')

    # layer.draw_mu()

    # loss = out.sum()
    # loss.backward()
    # draw_grad_graph(loss)
