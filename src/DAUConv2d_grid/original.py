from base import *


class DAUConv2d(nn.Module):
    """
    `y` is associated with height, but is stored second when storing in dim of size 2 (`mu`, `px_to_unit`).
    """

    def __init__(self, in_channels, out_channels, no_units, height, width, *, gauss_kernel=None, sigma=None, init=True):
        super(DAUConv2d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.no_units = no_units
        self.height = height
        self.width = width
        self.sigma = sigma if sigma is not None else .5

        assert width > 1
        assert height > 1

        # parameters
        if init:
            self.mu = Parameter(torch.randn(in_channels, out_channels, no_units, 2, device=device) * .1)
            self.mu_flat = self.mu.view(-1, 2)
        self.w = Parameter(torch.empty(in_channels, out_channels, no_units, device=device))
        self.bias = Parameter(torch.zeros(out_channels, device=device))

        # initialization of weights
        nn.init.xavier_normal_(self.w, gain=nn.init.calculate_gain('relu'))

        # Gauss
        if gauss_kernel is None:
            gauss_kernel = gauss(self.sigma)
        self.gauss_horizontal = gauss_kernel.expand(height, 1, -1)
        self.gauss_vertical = gauss_kernel.expand(width, 1, -1)
        self.gauss_padding = gauss_kernel.size(0) // 2

        # grid
        self.px_to_unit = tensor([2 / (self.width - 1), 2 / (self.height - 1)], device=device)
        grid_y, grid_x = torch.meshgrid([
            torch.linspace(-1, 1, self.height, device=device),
            torch.linspace(-1, 1, self.width, device=device)
        ])
        self.grid = torch.stack((grid_x, grid_y), dim=2)
        if init:
            self.grid = self.grid.repeat(in_channels * out_channels * no_units, 1, 1, 1)

    def forward(self, x):
        batch_size, in_channels, height, width = x.shape
        assert in_channels == self.in_channels
        assert height == self.height, 'should be ' + str(height)
        assert width == self.width

        x = self.blur(x)
        y = self.interpolate(x)  # N, out, H, W

        # bias
        y = y.permute(0, 2, 3, 1)  # N, H, W, out
        y += self.bias
        y = y.permute(0, 3, 1, 2)

        return y

    def blur(self, x):
        batch_size, in_channels, height, width = x.shape

        x = x.view(-1, height, width)

        # Gaussian over horizontal axis
        x = F.conv1d(x, self.gauss_horizontal, padding=self.gauss_padding, groups=self.height)

        # Gaussian over vertical axis
        x.transpose_(1, 2)
        x = F.conv1d(x, self.gauss_vertical, padding=self.gauss_padding, groups=self.width)
        x.transpose_(1, 2)

        return x.view(batch_size, in_channels, height, width)

    def interpolate(self, x):
        batch_size, in_channels, height, width = x.shape

        # repeating input
        x.transpose_(0, 1)
        x = x.repeat_interleave(self.out_channels * self.no_units, dim=0)

        # making grid
        grid = self.grid.permute(1, 2, 0, 3)
        grid = grid + self.mu_flat * self.px_to_unit
        grid = grid.permute(2, 0, 1, 3)

        # actual interpolation
        interpolated = F.grid_sample(x, grid)

        # reshaping and applying weight
        interpolated = interpolated.view(in_channels, self.out_channels, self.no_units, batch_size, height, width)
        interpolated = interpolated.permute(3, 4, 5, 0, 1, 2)  # batch_size, height, width, in_channels, out_channels, no_units
        interpolated *= self.w
        interpolated = interpolated.permute(0, 3, 4, 1, 2, 5)  # batch_size, in_channels, out_channels, height, width, no_units

        # sum
        y = interpolated.sum(dim=(1, 5))

        return y

    def extra_repr(self):
        return 'in_channels={}, out_channels={}, no_units={}, sigma={}'.format(
            self.in_channels, self.out_channels, self.no_units, self.sigma
        )

    def draw_mu(self):
        mu = self.mu.view(-1, 2).detach().cpu().numpy()
        plt.plot(mu[:, 0], mu[:, 1], 'ro')
        show_or_save('mu')


if __name__ == '__main__':
    height, width = 2, 2
    in_channels, out_channels, no_units = 2, 3, 4
    batches = 2

    layer = DAUConv2d(in_channels, out_channels, no_units, height, width,
                      gauss_kernel=tensor([1], dtype=float, device=device))

    input = torch.randn(batches, in_channels, height, width)
    # input[0][0][0][0] = 1

    pr(input, 'input')
    out = layer.forward(input)
    pr(out, 'out')

    # layer.draw_mu()

    # loss = out.sum()
    # loss.backward()
    # draw_grad_graph(loss)
