from collections import defaultdict
from time import time

starts = {}
times = defaultdict(float)


def start(key):
    starts[key] = time()


def end(key):
    if key in starts:
        times[key] += time() - starts[key]


def print_():
    end('all')
    if len(times) > 0:
        print('time analysis:')
        for key, t in times.items():
            print('   ', key, round(t, 4), 's')
    times.clear()
    start('all')
