import torch
import torch
from math import pi, sqrt
from torch import float
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import helpers.time_analysis as time
import yaml
import torch.nn.functional as F
from torch import tensor, unsqueeze
from torch.nn import Parameter
from helpers.summary import summary
import os
from datetime import datetime
import sys
from pprint import pprint
from copy import deepcopy
import math
import traceback

# torch.manual_seed(0)
cuda_core = 2

device = torch.device('cuda:' + str(cuda_core) if torch.cuda.is_available() else 'cpu')


def get_config(depth=0):
    depth += 1
    assert depth < 5

    try:
        with open('../config.yaml', 'r') as file:
            return yaml.safe_load(file)
    except:
        os.chdir('..')
        return get_config(depth)


config = get_config()


def draw_grad_graph(grad_fn, depth=0):
    if type(grad_fn) in [torch.nn.parameter.Parameter, torch.Tensor]:
        grad_fn = grad_fn.grad_fn
    if depth == 0:
        print('--- drawing autograd graph ---')

    tab = "".join(["  "] * depth)
    print(tab, grad_fn, sep="")

    depth += 1
    if grad_fn is not None:
        tab = "".join(["  "] * depth)
        for n in grad_fn.next_functions:
            if n[0]:
                if type(n[0]) == dict and 'variable' in n[0]:
                    print(tab, n[0], sep="")
                else:
                    draw_grad_graph(n[0], depth)

    if depth == 1:
        print()


def get_class(module, name):
    imp = __import__(module, fromlist=[name])
    return getattr(imp, name)


def gauss(sigma):
    r = round(3 * sigma)
    x = torch.tensor(range(-r, r + 1), dtype=float, device=device)
    g = 1 / sqrt(2 * pi) / sigma * torch.exp(-x.pow(2) / 2 / sigma ** 2)
    return g / g.sum()


def pr(t, bes):
    print(bes)
    print(t)
    print()


def show_or_save(name):
    if not torch.cuda.is_available():
        plt.show()
    else:
        plt.savefig(config['out_folder'] + str(name) + '.png')
