from base import *
from DAUConv2d.helpers.shift import Shift


class DAUConv2d(nn.Module):
    """
    `y` is associated with height and is stored first.
    """

    def __init__(self, in_channels, out_channels, no_units, height, width, *, gauss_kernel=None, sigma=None, init=True):
        super(DAUConv2d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.no_units = no_units
        self.height = height
        self.width = width
        self.sigma = sigma if sigma is not None else .5

        assert width > 1
        assert height > 1

        # parameters
        if init:
            self.mu = Parameter(torch.randn(in_channels, out_channels, no_units, 2, device=device) * .1)
            self.mu_flat = self.mu.view(-1, 2)
        self.w = Parameter(torch.empty(in_channels, out_channels, no_units, device=device))
        self.w_flat = self.w.view(-1)
        self.bias = Parameter(torch.zeros(out_channels, device=device))

        # initialization of weights
        nn.init.xavier_normal_(self.w, gain=nn.init.calculate_gain('relu'))

        # Gauss
        if gauss_kernel is None:
            gauss_kernel = gauss(self.sigma)
        self.gauss_horizontal = gauss_kernel.expand(height, 1, -1)
        self.gauss_vertical = gauss_kernel.expand(width, 1, -1)
        self.gauss_padding = gauss_kernel.size(0) // 2

    def forward(self, x):
        batch_size, in_channels, height, width = x.shape
        assert in_channels == self.in_channels
        assert height == self.height, 'should be ' + str(height)
        assert width == self.width

        x = self.blur(x)
        y = self.interpolate(x)  # N, out, H, W

        # bias
        y = y.permute(0, 2, 3, 1)  # N, H, W, out
        y += self.bias
        y = y.permute(0, 3, 1, 2)

        return y

    def blur(self, x):
        batch_size, in_channels, height, width = x.shape

        x = x.view(-1, height, width)

        # Gaussian over horizontal axis
        x = F.conv1d(x, self.gauss_horizontal, padding=self.gauss_padding, groups=self.height)

        # Gaussian over vertical axis
        x.transpose_(1, 2)
        x = F.conv1d(x, self.gauss_vertical, padding=self.gauss_padding, groups=self.width)
        x.transpose_(1, 2)

        return x.view(batch_size, in_channels, height, width)

    def interpolate(self, inp):
        batch_size, in_channels, height, width = inp.shape

        # constructing weight for convolution
        mu = self.mu_flat.fmod(1)
        y = mu[:, 0]
        y = torch.stack((-y, 1 - y.abs(), y), dim=1).max(tensor(0., device=device))
        x = mu[:, 1]
        x = torch.stack((-x, 1 - x.abs(), x), dim=1).max(tensor(0., device=device))
        w = torch.matmul(y.unsqueeze(2), x.unsqueeze(1))  # n, h, w

        # applying weight
        w = w.permute(1, 2, 0)  # h, w, n
        w *= self.w_flat
        w = w.permute(2, 0, 1)

        # convolution
        w.unsqueeze_(1)
        interpolated = F.conv2d(inp, w, padding=1, groups=in_channels)

        # shifting
        out = Shift.apply(interpolated, self.mu_flat)

        # sum
        out = out.view(batch_size, in_channels, self.out_channels, self.no_units, height, width)
        out = out.sum(dim=(1, 3))

        return out

    def extra_repr(self):
        return 'in_channels={}, out_channels={}, no_units={}, sigma={}'.format(
            self.in_channels, self.out_channels, self.no_units, self.sigma
        )

    def draw_mu(self):
        mu = self.mu.view(-1, 2).detach().cpu().numpy()
        plt.plot(mu[:, 1], mu[:, 0], 'ro')
        show_or_save('mu')


if __name__ == '__main__':
    height, width = 2, 2
    in_channels, out_channels, no_units = 2, 3, 4
    batches = 2

    layer = DAUConv2d(in_channels, out_channels, no_units, height, width,
                      gauss_kernel=tensor([1], dtype=float, device=device))

    input = torch.randn(batches, in_channels, height, width, requires_grad=True)
    # input[0][0][0][0] = 1

    pr(input, 'input')
    out = layer.forward(input)
    pr(out, 'out')

    # layer.draw_mu()

    # loss = out.sum()
    # loss.backward()
    # draw_grad_graph(loss)

'''

/home/v/PycharmProjects/tensorflow/venv/bin/python /home/v/PycharmProjects/tensorflow/src/DAUConv2d/original.py
input
tensor([[[[-0.0033, -0.5344],
          [ 1.1687,  0.3945]],

         [[ 1.9415,  0.7915],
          [-0.0203, -0.4372]]],


        [[[-1.5353, -0.4127],
          [ 0.9663,  1.6248]],

         [[-0.3656, -1.3024],
          [ 0.0994,  0.4418]]]])

out
tensor([[[[-1.8865, -1.0778],
          [ 0.7469,  0.6639]],

         [[-1.6976, -0.5645],
          [-0.2077,  0.2687]],

         [[ 0.3340, -1.2067],
          [ 3.3452,  1.2294]]],


        [[[-0.4511,  1.0334],
          [ 0.5226,  0.5493]],

         [[ 0.5888,  1.1524],
          [-0.2350, -0.6952]],

         [[-4.3273, -1.5501],
          [ 2.9240,  4.6198]]]], grad_fn=<PermuteBackward>)


Process finished with exit code 0


'''
