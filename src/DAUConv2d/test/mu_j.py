from DAUConv2d.mu_j import *

# torch.manual_seed(0)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        self.conv = DAUConv2dj(1, 2, 3, 11, 11)
        self.fc = nn.Linear(11 * 11 * 2, 10)

    def forward(self, x):
        x = F.relu(self.conv(x))
        x = x.view(-1, 11 * 11 * 2)
        x = self.fc(x)
        return x


x = torch.randn(5, 1, 11, 11)
y = torch.randn(5, 10)

net = Net()

criterion = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(net.parameters(), lr=1e-3)

for t in range(100):
    y_pred = net(x)

    loss = criterion(y_pred, y)
    print(t, loss.item())

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

time.print_()
net.conv.draw_mu()
