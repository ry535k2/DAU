from DAUConv2d.original import *


class DAUConv2dj(DAUConv2d):
    """
    ...
    """

    def __init__(self, in_channels, out_channels, no_units, height, width, *, gauss_kernel=None, sigma=None):
        super(DAUConv2dj, self).__init__(in_channels, out_channels, no_units, height, width,
                                         gauss_kernel=gauss_kernel, sigma=sigma, init=False)

        self.mu = Parameter(torch.randn(out_channels, no_units, 2, device=device) * .1)
        self.mu_flat = self.mu.view(-1, 2)

    def interpolate(self, inp):
        batch_size, in_channels, height, width = inp.shape

        # constructing weight for convolution
        mu_repeated = self.mu_flat.repeat(self.in_channels, 1)
        mu = mu_repeated.fmod(1)
        y = mu[:, 0]
        y = torch.stack((-y, 1 - y.abs(), y), dim=1).max(tensor(0., device=device))
        x = mu[:, 1]
        x = torch.stack((-x, 1 - x.abs(), x), dim=1).max(tensor(0., device=device))
        w = torch.matmul(y.unsqueeze(2), x.unsqueeze(1))  # n, h, w

        # applying weight
        w = w.permute(1, 2, 0)  # h, w, n
        w *= self.w_flat
        w = w.permute(2, 0, 1)

        # convolution
        w.unsqueeze_(1)
        interpolated = F.conv2d(inp, w, padding=1, groups=in_channels)

        # shifting
        out = Shift.apply(interpolated, mu_repeated)

        # sum
        out = out.view(batch_size, in_channels, self.out_channels, self.no_units, height, width)
        out = out.sum(dim=(1, 3))

        return out


if __name__ == '__main__':
    height, width = 2, 2
    in_channels, out_channels, no_units = 2, 3, 4
    batches = 2

    layer = DAUConv2dj(in_channels, out_channels, no_units, height, width,
                       gauss_kernel=tensor([1], dtype=float, device=device))

    input = torch.randn(batches, in_channels, height, width)
    # input[0][0][0][0] = 1

    pr(input, 'input')
    out = layer.forward(input)
    pr(out, 'out')

    # layer.draw_mu()

    # loss = out.sum()
    # loss.backward()
    # draw_grad_graph(loss)
