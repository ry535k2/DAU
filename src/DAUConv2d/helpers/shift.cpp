#include <iostream>
#include <vector>
#include <math.h>

#include <torch/extension.h>

using namespace std;

torch::Tensor shift(torch::Tensor input, torch::Tensor mu) {
    int b = input.size(0), n = input.size(1), h = input.size(2), w = input.size(3);
    auto output = torch::zeros({b, n, h, w}, torch::device(input.device()));  // torch::device(torch::kCUDA, 1).requires_grad(true)

    auto input_a = input.accessor<float,4>();
    auto output_a = output.accessor<float,4>();
    auto mu_a = mu.accessor<int,2>();

    for (int i = 0; i < n; i++) {
        int dy = mu_a[i][0], dx = mu_a[i][1];

        if (abs(dy) >= h || abs(dx) >= w)
            continue;

        int yf1 = 0, yt1 = h, xf1 = 0, xt1 = w;
        int yf2 = dy, xf2 = dx;

        if (dy < 0) {
            yf1 = -dy;
            yf2 = 0;
        }
        if (dy > 0) {
            yt1 = h - dy;
        }
        if (dx < 0) {
            xf1 = -dx;
            xf2 = 0;
        }
        if (dx > 0) {
            xt1 = h - dx;
        }

        dy = yf2 - yf1;
        dx = xf2 - xf1;

        for (int bi = 0; bi < b; bi++) {
            for (int y = yf1; y < yt1; y++) {
                for (int x = xf1; x < xt1; x++) {
                    output_a[bi][i][y][x] = input_a[bi][i][y + dy][x + dx];
                }
            }
        }
    }

    return output;
}

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
    m.def("shift", &shift, "cpp shift");
}
