from helpers.constants import *


def shift(x, vertical, horizontal, padding):
    b, n, h, w = x.shape

    # vertical convolution
    x = x.permute(0, 3, 1, 2)  # b, w, n, h
    x = x.contiguous().view(b * w, n, h)
    x = F.conv1d(x, vertical, padding=int(padding), groups=n)  # b * w, n, h
    x = x.view(b, w, n, h)

    # horizontal convolution
    x = x.permute(0, 3, 2, 1)  # b, h, n, w
    x = x.contiguous().view(b * h, n, w)
    x = F.conv1d(x, horizontal, padding=int(padding), groups=n)  # b * h, n, w
    x = x.view(b, h, n, w)

    return x.permute(0, 2, 1, 3)  # b, n, h, w


class Shift(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, mu):
        b, n, h, w = x.shape

        mu = mu.short().float().transpose(0, 1).unsqueeze_(2)
        padding = min(mu.abs().max(), max(h - 1, w - 1))
        vertical = torch.arange(-padding, padding + 1, device=device, dtype=float).repeat(n, 1)
        horizontal = vertical.clone()

        vertical.eq_(mu[0]).unsqueeze_(1)
        horizontal.eq_(mu[1]).unsqueeze_(1)

        ctx.save_for_backward(vertical, horizontal, padding)
        return shift(x, vertical, horizontal, padding).clone()

    @staticmethod
    def backward(ctx, grad_output):
        vertical, horizontal, padding = ctx.saved_tensors
        return shift(grad_output, vertical.flip(2), horizontal.flip(2), padding), None


if __name__ == '__main__':
    x = torch.arange(1, 10, dtype=float,requires_grad=True).view(1, 1, 3, 3)
    mu = torch.zeros(1, 2).short().float()
    mu[0][0] = 2
    mu[0][1] = 2

    print(Shift.apply(x, mu))
