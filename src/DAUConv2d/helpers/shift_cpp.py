from helpers.constants import *
from torch.utils.cpp_extension import load

shift_cpp = load(name="shift_cpp", sources=["DAUConv2d/helpers/shift.cpp"], verbose=True)


def shift(input, mu):
    b, n, h, w = input.shape
    y = torch.zeros_like(input)
    for i in range(n):
        dy, dx = mu[i]
        if abs(dy) >= h or abs(dx) >= w: continue

        yf1, yt1, xf1, xt1 = 0, h, 0, w
        yf2, yt2, xf2, xt2 = dy, h + dy, dx, w + dx
        if dy < 0:
            yf1 = -dy
            yf2 = 0
        if dy > 0:
            yt1 = h - dy
            yt2 = h
        if dx < 0:
            xf1 = -dx
            xf2 = 0
        if dx > 0:
            xt1 = h - dx
            xt2 = h

        y[:, i, yf1:yt1, xf1:xt1] = input[:, i, yf2:yt2, xf2:xt2]

    return y


class Shift(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input, mu):
        mu = mu.int()
        ctx.save_for_backward(mu)
        return shift(input, mu)
        # return shift_cpp.shift(input, mu)

    @staticmethod
    def backward(ctx, grad_output):
        mu, = ctx.saved_tensors
        return shift(grad_output, -mu), None
        # return shift_cpp.shift(grad_output, -mu), None
