#!/usr/bin/env bash

echo "Downloading 'out' folder ..."
rm -r out/*
sftp gpu << !
    cd /home/marko/Desktop/hdd
    get -r out
!
