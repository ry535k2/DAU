#!/usr/bin/env bash

echo "Checking folder size ..."
SIZE=$(du -s src)
SIZE=( $SIZE )
SIZE=${SIZE[0]}
if [[ $SIZE -gt 1000 ]]; then
    echo "'src' folder is too large."
    exit
fi

echo "Copying src to server ..."
ssh gpu "cd ~/Desktop/hdd ; rm -r out/* ; cd ~/Desktop/ssd; rm -r src/*"
sftp gpu << !
    cd /home/marko/Desktop/ssd
    put -r src
!
