#!/usr/bin/env bash

ssh gpu "cd ~/Desktop/hdd; rm -rf data"
sftp gpu << !
    cd /home/marko/Desktop/hdd
    put -r data
!

echo "Done."
