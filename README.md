# DAU in PyTorch

This project implements different versions of [DAU CNN architecture](https://www.vicos.si/Research/DeepCompositionalNet) 
and evaluates them.

All models have less parameters than original DAU architecture, which is accomplished by sharing DAU offsets within every layer. 
Sharing can be done in multiple ways, here we tested four different types.

### Code Organization

Project is very modular, since many ideas were tested. [`src`](./src) is source folder containing all Python scripts, here are brief
explanations of its content:

* [`src/CIFAR10`](./src/CIFAR10) and [`src/MNIST`](./src/MNIST) are folders containing network architectures.
* [`src/DAUConv2d`](./src/DAUConv2d) and [`src/DAUConv2d`](./src/DAUConv2d) implement all modifications of DAU CNN in two different ways.
* [`src/graphs`](./src/graphs) is folder for drawing accuracy graphs.
* [`src/helpers`](./src/helpers) contains scripts that are shared over all other scrips.
* [`src/parameters`](./src/parameters) is for drawing offsets of DAUs.
* [`src/base.py`](./src/base.py) trains and evaluates given model.
* [`src/run.py`](./src/run.py) is main script that controls everything.
* [`src/run.sh`](./src/run.sh) is script for calling run.py.
