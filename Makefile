
upload-data:
	./bash/upload_data.sh

download:
	./bash/download.sh

deploy:
	./bash/deploy.sh

ssh: deploy
	ssh -t gpu "cd Desktop/src ; export PYTHONPATH=$PYTHONPATH:~/Desktop/src ; bash"

state:
	ssh gpu "cd /usr/local/nvidia/bin ; ./nvidia-smi"

run: deploy
	ssh -t gpu "cd ~/Desktop/src ; ./run.sh"
	./bash/download.sh
