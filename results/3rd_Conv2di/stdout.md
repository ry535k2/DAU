start: 2019-07-03 08:44:53.746573
* CIFAR10, 62k_params, DAUConv2di, units: 2

## CIFAR10, 62k_params, DAUConv2di, units: 2
```text
Config: {'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=64, no_units=2, sigma=0.5)
  (conv3): DAUConv2di(in_channels=64, out_channels=128, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 32, 32]             236
       BatchNorm2d-2           [-1, 32, 32, 32]              64
         MaxPool2d-3           [-1, 32, 16, 16]               0
        DAUConv2di-4           [-1, 64, 16, 16]           4,288
       BatchNorm2d-5           [-1, 64, 16, 16]             128
         MaxPool2d-6             [-1, 64, 8, 8]               0
        DAUConv2di-7            [-1, 128, 8, 8]          16,768
       BatchNorm2d-8            [-1, 128, 8, 8]             256
         MaxPool2d-9            [-1, 128, 4, 4]               0
           Linear-10                   [-1, 10]          20,490
================================================================
total params: 42,230
DAU params: 21,292
other params: 20,938
----------------------------------------------------------------
epoch: 0, loss: 1100.11, accuracy: 0.5749
epoch: 1, loss: 849.31, accuracy: 0.5859
epoch: 2, loss: 746.52, accuracy: 0.6358
epoch: 3, loss: 688.11, accuracy: 0.6564
epoch: 4, loss: 644.47, accuracy: 0.6827
epoch: 5, loss: 610.57, accuracy: 0.6686
epoch: 6, loss: 589.38, accuracy: 0.698
epoch: 7, loss: 565.28, accuracy: 0.6894
epoch: 8, loss: 542.09, accuracy: 0.6984
epoch: 9, loss: 531.48, accuracy: 0.693
epoch: 10, loss: 510.26, accuracy: 0.7
epoch: 11, loss: 501.16, accuracy: 0.6993
epoch: 12, loss: 485.27, accuracy: 0.7067
epoch: 13, loss: 475.34, accuracy: 0.6928
epoch: 14, loss: 462.51, accuracy: 0.723
epoch: 15, loss: 455.14, accuracy: 0.721
epoch: 16, loss: 442.24, accuracy: 0.7149
epoch: 17, loss: 434.35, accuracy: 0.711
epoch: 18, loss: 426.83, accuracy: 0.7173
epoch: 19, loss: 419.06, accuracy: 0.712
epoch: 20, loss: 412.11, accuracy: 0.7254
epoch: 21, loss: 407.84, accuracy: 0.719
epoch: 22, loss: 399.81, accuracy: 0.7119
epoch: 23, loss: 393.6, accuracy: 0.7288
epoch: 24, loss: 389.31, accuracy: 0.7094
epoch: 25, loss: 383.2, accuracy: 0.7061
epoch: 26, loss: 378.77, accuracy: 0.7204
epoch: 27, loss: 371.27, accuracy: 0.7228
epoch: 28, loss: 366.11, accuracy: 0.7231
epoch: 29, loss: 358.47, accuracy: 0.726
epoch: 30, loss: 357.84, accuracy: 0.7263
epoch: 31, loss: 356.27, accuracy: 0.7218
epoch: 32, loss: 349.54, accuracy: 0.714
epoch: 33, loss: 345.13, accuracy: 0.7239
epoch: 34, loss: 344.77, accuracy: 0.712
epoch: 35, loss: 339.27, accuracy: 0.7182
epoch: 36, loss: 336.83, accuracy: 0.7151
epoch: 37, loss: 333.65, accuracy: 0.7187
epoch: 38, loss: 329.59, accuracy: 0.7203
epoch: 39, loss: 326.73, accuracy: 0.7194
epoch: 40, loss: 322.58, accuracy: 0.7198
epoch: 41, loss: 319.43, accuracy: 0.7167
epoch: 42, loss: 317.34, accuracy: 0.7242
epoch: 43, loss: 311.52, accuracy: 0.7163
epoch: 44, loss: 309.87, accuracy: 0.7153
epoch: 45, loss: 307.63, accuracy: 0.7206
epoch: 46, loss: 306.55, accuracy: 0.7212
epoch: 47, loss: 299.97, accuracy: 0.7031
epoch: 48, loss: 299.95, accuracy: 0.7173
epoch: 49, loss: 301.62, accuracy: 0.7267
epoch: 50, loss: 294.24, accuracy: 0.7228
epoch: 51, loss: 290.69, accuracy: 0.7157
epoch: 52, loss: 289.23, accuracy: 0.7178
epoch: 53, loss: 288.0, accuracy: 0.7165
epoch: 54, loss: 286.49, accuracy: 0.7204
epoch: 55, loss: 285.35, accuracy: 0.7175
epoch: 56, loss: 280.21, accuracy: 0.7132
epoch: 57, loss: 279.29, accuracy: 0.7177
epoch: 58, loss: 279.67, accuracy: 0.717
epoch: 59, loss: 270.45, accuracy: 0.7223
epoch: 60, loss: 273.56, accuracy: 0.7081
epoch: 61, loss: 269.75, accuracy: 0.7171
epoch: 62, loss: 267.05, accuracy: 0.7133
epoch: 63, loss: 267.93, accuracy: 0.7187
epoch: 64, loss: 263.85, accuracy: 0.7065
epoch: 65, loss: 269.14, accuracy: 0.715
epoch: 66, loss: 263.44, accuracy: 0.718
epoch: 67, loss: 261.48, accuracy: 0.6976
epoch: 68, loss: 261.12, accuracy: 0.7196
epoch: 69, loss: 256.19, accuracy: 0.71
epoch: 70, loss: 174.17, accuracy: 0.7281
epoch: 71, loss: 154.89, accuracy: 0.7317
epoch: 72, loss: 150.54, accuracy: 0.7304
epoch: 73, loss: 148.53, accuracy: 0.7296
epoch: 74, loss: 146.83, accuracy: 0.7279
epoch: 75, loss: 142.0, accuracy: 0.7284
epoch: 76, loss: 140.86, accuracy: 0.7279
epoch: 77, loss: 138.81, accuracy: 0.7257
epoch: 78, loss: 137.33, accuracy: 0.7298
epoch: 79, loss: 137.43, accuracy: 0.7284
epoch: 80, loss: 135.6, accuracy: 0.7223
epoch: 81, loss: 131.83, accuracy: 0.7265
epoch: 82, loss: 131.29, accuracy: 0.726
epoch: 83, loss: 131.44, accuracy: 0.726
epoch: 84, loss: 130.88, accuracy: 0.7254
epoch: 85, loss: 128.88, accuracy: 0.724
epoch: 86, loss: 126.91, accuracy: 0.7221
epoch: 87, loss: 126.08, accuracy: 0.7248
epoch: 88, loss: 123.32, accuracy: 0.7235
epoch: 89, loss: 124.01, accuracy: 0.7245
epoch: 90, loss: 123.6, accuracy: 0.7229
epoch: 91, loss: 122.78, accuracy: 0.7242
epoch: 92, loss: 122.22, accuracy: 0.7231
epoch: 93, loss: 119.97, accuracy: 0.7239
epoch: 94, loss: 120.08, accuracy: 0.7182
epoch: 95, loss: 102.61, accuracy: 0.7258
epoch: 96, loss: 103.25, accuracy: 0.7255
epoch: 97, loss: 100.31, accuracy: 0.7246
epoch: 98, loss: 99.24, accuracy: 0.7259
epoch: 99, loss: 100.94, accuracy: 0.7249
time analysis:
    train 2028.6451 s
Accuracy of     0 : 76 %
Accuracy of     1 : 84 %
Accuracy of     2 : 59 %
Accuracy of     3 : 52 %
Accuracy of     4 : 60 %
Accuracy of     5 : 67 %
Accuracy of     6 : 78 %
Accuracy of     7 : 75 %
Accuracy of     8 : 87 %
Accuracy of     9 : 80 %
```

finish: 2019-07-03 09:18:47.581385
