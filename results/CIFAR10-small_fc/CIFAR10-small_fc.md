start: 2019-06-26 16:08:20.108065

## Conv2d 
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(64, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
            Conv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 0
other params: 11,682
----------------------------------------------------------------
epoch: 0, loss: 1061.48, accuracy: 0.5742
epoch: 1, loss: 881.87, accuracy: 0.6259
epoch: 2, loss: 821.69, accuracy: 0.6218
epoch: 3, loss: 800.43, accuracy: 0.6328
epoch: 4, loss: 773.8, accuracy: 0.6408
epoch: 5, loss: 762.2, accuracy: 0.6516
epoch: 6, loss: 748.34, accuracy: 0.652
epoch: 7, loss: 736.37, accuracy: 0.6698
epoch: 8, loss: 725.54, accuracy: 0.663
epoch: 9, loss: 719.85, accuracy: 0.6462
epoch: 10, loss: 711.73, accuracy: 0.656
epoch: 11, loss: 701.89, accuracy: 0.6545
epoch: 12, loss: 702.54, accuracy: 0.6444
epoch: 13, loss: 693.67, accuracy: 0.6658
epoch: 14, loss: 686.87, accuracy: 0.6591
epoch: 15, loss: 682.0, accuracy: 0.6622
epoch: 16, loss: 680.56, accuracy: 0.6676
epoch: 17, loss: 674.57, accuracy: 0.6689
epoch: 18, loss: 673.48, accuracy: 0.675
epoch: 19, loss: 668.56, accuracy: 0.6654
epoch: 20, loss: 666.81, accuracy: 0.6757
epoch: 21, loss: 663.83, accuracy: 0.6773
epoch: 22, loss: 658.97, accuracy: 0.6498
epoch: 23, loss: 655.57, accuracy: 0.6645
epoch: 24, loss: 653.27, accuracy: 0.6474
epoch: 25, loss: 652.18, accuracy: 0.6683
epoch: 26, loss: 652.99, accuracy: 0.6761
epoch: 27, loss: 645.83, accuracy: 0.6804
epoch: 28, loss: 642.89, accuracy: 0.6876
epoch: 29, loss: 641.35, accuracy: 0.68
epoch: 30, loss: 638.36, accuracy: 0.6764
epoch: 31, loss: 639.42, accuracy: 0.6714
epoch: 32, loss: 640.16, accuracy: 0.678
epoch: 33, loss: 636.36, accuracy: 0.6667
epoch: 34, loss: 635.81, accuracy: 0.6667
epoch: 35, loss: 632.53, accuracy: 0.6909
epoch: 36, loss: 632.28, accuracy: 0.6803
epoch: 37, loss: 628.16, accuracy: 0.6771
epoch: 38, loss: 627.14, accuracy: 0.6806
epoch: 39, loss: 625.03, accuracy: 0.672
epoch: 40, loss: 624.95, accuracy: 0.6801
epoch: 41, loss: 621.91, accuracy: 0.6764
epoch: 42, loss: 620.37, accuracy: 0.6815
epoch: 43, loss: 623.45, accuracy: 0.6748
epoch: 44, loss: 622.79, accuracy: 0.6741
epoch: 45, loss: 618.09, accuracy: 0.6749
epoch: 46, loss: 615.69, accuracy: 0.6818
epoch: 47, loss: 616.41, accuracy: 0.6676
epoch: 48, loss: 615.58, accuracy: 0.6757
epoch: 49, loss: 611.49, accuracy: 0.6756
epoch: 50, loss: 617.49, accuracy: 0.6741
epoch: 51, loss: 609.41, accuracy: 0.6721
epoch: 52, loss: 610.86, accuracy: 0.6789
epoch: 53, loss: 609.46, accuracy: 0.6759
epoch: 54, loss: 611.69, accuracy: 0.6704
epoch: 55, loss: 609.52, accuracy: 0.6757
epoch: 56, loss: 605.39, accuracy: 0.676
epoch: 57, loss: 604.68, accuracy: 0.6594
epoch: 58, loss: 608.17, accuracy: 0.6763
epoch: 59, loss: 602.31, accuracy: 0.6761
epoch: 60, loss: 604.31, accuracy: 0.6866
epoch: 61, loss: 601.64, accuracy: 0.6782
epoch: 62, loss: 602.84, accuracy: 0.6682
epoch: 63, loss: 600.0, accuracy: 0.6829
epoch: 64, loss: 597.08, accuracy: 0.6766
epoch: 65, loss: 598.25, accuracy: 0.6668
epoch: 66, loss: 601.41, accuracy: 0.6745
epoch: 67, loss: 600.82, accuracy: 0.6722
epoch: 68, loss: 599.85, accuracy: 0.6791
epoch: 69, loss: 595.63, accuracy: 0.68
epoch: 70, loss: 592.18, accuracy: 0.6802
epoch: 71, loss: 595.05, accuracy: 0.6807
epoch: 72, loss: 596.76, accuracy: 0.6741
epoch: 73, loss: 591.56, accuracy: 0.6671
epoch: 74, loss: 594.74, accuracy: 0.6731
epoch: 75, loss: 593.59, accuracy: 0.6784
epoch: 76, loss: 591.89, accuracy: 0.664
epoch: 77, loss: 591.2, accuracy: 0.675
epoch: 78, loss: 588.93, accuracy: 0.666
epoch: 79, loss: 591.01, accuracy: 0.6756
epoch: 80, loss: 528.99, accuracy: 0.6903
epoch: 81, loss: 521.08, accuracy: 0.6931
epoch: 82, loss: 517.97, accuracy: 0.6917
epoch: 83, loss: 517.75, accuracy: 0.6944
epoch: 84, loss: 516.9, accuracy: 0.694
epoch: 85, loss: 516.44, accuracy: 0.6924
epoch: 86, loss: 516.24, accuracy: 0.6945
epoch: 87, loss: 515.22, accuracy: 0.6944
epoch: 88, loss: 515.35, accuracy: 0.6923
epoch: 89, loss: 514.5, accuracy: 0.6924
epoch: 90, loss: 513.4, accuracy: 0.6929
epoch: 91, loss: 513.09, accuracy: 0.6912
epoch: 92, loss: 513.05, accuracy: 0.695
epoch: 93, loss: 513.77, accuracy: 0.6902
epoch: 94, loss: 513.24, accuracy: 0.6933
epoch: 95, loss: 512.16, accuracy: 0.6916
epoch: 96, loss: 511.68, accuracy: 0.6931
epoch: 97, loss: 512.08, accuracy: 0.6939
epoch: 98, loss: 511.87, accuracy: 0.6904
epoch: 99, loss: 511.6, accuracy: 0.6894
epoch: 100, loss: 511.32, accuracy: 0.6904
epoch: 101, loss: 509.49, accuracy: 0.6902
epoch: 102, loss: 511.1, accuracy: 0.6936
epoch: 103, loss: 509.28, accuracy: 0.6918
epoch: 104, loss: 510.98, accuracy: 0.6955
epoch: 105, loss: 510.49, accuracy: 0.6909
Epoch   106: reducing learning rate of group 0 to 5.0000e-04.
epoch: 106, loss: 509.93, accuracy: 0.6912
epoch: 107, loss: 505.25, accuracy: 0.6909
epoch: 108, loss: 505.72, accuracy: 0.6913
epoch: 109, loss: 505.38, accuracy: 0.6928
epoch: 110, loss: 505.19, accuracy: 0.6937
epoch: 111, loss: 504.4, accuracy: 0.6933
epoch: 112, loss: 504.17, accuracy: 0.6938
epoch: 113, loss: 504.26, accuracy: 0.6946
epoch: 114, loss: 503.95, accuracy: 0.6947
epoch: 115, loss: 503.86, accuracy: 0.693
epoch: 116, loss: 504.25, accuracy: 0.694
epoch: 117, loss: 503.92, accuracy: 0.6923
epoch: 118, loss: 504.31, accuracy: 0.6943
epoch: 119, loss: 503.61, accuracy: 0.6923
Epoch   120: reducing learning rate of group 0 to 2.5000e-04.
epoch: 120, loss: 503.5, accuracy: 0.6928
epoch: 121, loss: 500.09, accuracy: 0.6937
epoch: 122, loss: 499.83, accuracy: 0.6949
epoch: 123, loss: 500.05, accuracy: 0.6938
epoch: 124, loss: 501.05, accuracy: 0.6952
epoch: 125, loss: 500.79, accuracy: 0.694
epoch: 126, loss: 500.23, accuracy: 0.6949
epoch: 127, loss: 500.58, accuracy: 0.6956
epoch: 128, loss: 500.17, accuracy: 0.6927
epoch: 129, loss: 500.32, accuracy: 0.6937
epoch: 130, loss: 499.25, accuracy: 0.6951
epoch: 131, loss: 498.55, accuracy: 0.6954
epoch: 132, loss: 497.87, accuracy: 0.6939
epoch: 133, loss: 497.92, accuracy: 0.6942
epoch: 134, loss: 497.66, accuracy: 0.6926
epoch: 135, loss: 497.45, accuracy: 0.6943
epoch: 136, loss: 498.7, accuracy: 0.6932
epoch: 137, loss: 496.95, accuracy: 0.6942
epoch: 138, loss: 499.11, accuracy: 0.694
epoch: 139, loss: 498.49, accuracy: 0.6942
epoch: 140, loss: 497.06, accuracy: 0.6936
epoch: 141, loss: 498.69, accuracy: 0.6923
epoch: 142, loss: 497.33, accuracy: 0.6936
epoch: 143, loss: 497.86, accuracy: 0.6954
epoch: 144, loss: 497.64, accuracy: 0.6944
epoch: 145, loss: 497.71, accuracy: 0.6946
epoch: 146, loss: 497.67, accuracy: 0.6942
epoch: 147, loss: 497.63, accuracy: 0.6935
epoch: 148, loss: 498.17, accuracy: 0.6933
epoch: 149, loss: 497.51, accuracy: 0.694
time analysis:
    train 864.6671 s
Accuracy of     0 : 73 %
Accuracy of     1 : 86 %
Accuracy of     2 : 63 %
Accuracy of     3 : 46 %
Accuracy of     4 : 61 %
Accuracy of     5 : 62 %
Accuracy of     6 : 80 %
Accuracy of     7 : 71 %
Accuracy of     8 : 72 %
Accuracy of     9 : 73 %
```

## DAUConv2d 2 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=2)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,216
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           3,080
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 9,570
DAU params: 4,296
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1120.1, accuracy: 0.5433
epoch: 1, loss: 923.64, accuracy: 0.5925
epoch: 2, loss: 863.99, accuracy: 0.5545
epoch: 3, loss: 828.68, accuracy: 0.6026
epoch: 4, loss: 805.3, accuracy: 0.6422
epoch: 5, loss: 784.08, accuracy: 0.6333
epoch: 6, loss: 769.33, accuracy: 0.6449
epoch: 7, loss: 759.26, accuracy: 0.6462
epoch: 8, loss: 747.23, accuracy: 0.6482
epoch: 9, loss: 741.73, accuracy: 0.6477
epoch: 10, loss: 731.7, accuracy: 0.661
epoch: 11, loss: 721.45, accuracy: 0.6549
epoch: 12, loss: 716.31, accuracy: 0.6453
epoch: 13, loss: 711.82, accuracy: 0.6646
epoch: 14, loss: 705.87, accuracy: 0.6627
epoch: 15, loss: 700.58, accuracy: 0.6713
epoch: 16, loss: 701.74, accuracy: 0.6725
epoch: 17, loss: 692.57, accuracy: 0.673
epoch: 18, loss: 691.1, accuracy: 0.6649
epoch: 19, loss: 687.8, accuracy: 0.6726
epoch: 20, loss: 685.84, accuracy: 0.6633
epoch: 21, loss: 681.57, accuracy: 0.6447
epoch: 22, loss: 680.09, accuracy: 0.6604
epoch: 23, loss: 677.49, accuracy: 0.6739
epoch: 24, loss: 678.35, accuracy: 0.6848
epoch: 25, loss: 677.19, accuracy: 0.6695
epoch: 26, loss: 669.94, accuracy: 0.6655
epoch: 27, loss: 668.85, accuracy: 0.6679
epoch: 28, loss: 667.94, accuracy: 0.681
epoch: 29, loss: 666.24, accuracy: 0.6755
epoch: 30, loss: 668.22, accuracy: 0.6732
epoch: 31, loss: 662.29, accuracy: 0.6564
epoch: 32, loss: 662.37, accuracy: 0.6842
epoch: 33, loss: 659.97, accuracy: 0.6657
epoch: 34, loss: 659.0, accuracy: 0.6668
epoch: 35, loss: 657.8, accuracy: 0.6767
epoch: 36, loss: 659.66, accuracy: 0.6687
epoch: 37, loss: 661.21, accuracy: 0.6857
epoch: 38, loss: 652.44, accuracy: 0.6732
epoch: 39, loss: 651.71, accuracy: 0.6799
epoch: 40, loss: 653.69, accuracy: 0.667
epoch: 41, loss: 648.74, accuracy: 0.6693
epoch: 42, loss: 651.71, accuracy: 0.6782
epoch: 43, loss: 648.97, accuracy: 0.6762
epoch: 44, loss: 649.29, accuracy: 0.6826
epoch: 45, loss: 650.45, accuracy: 0.6804
epoch: 46, loss: 645.73, accuracy: 0.6547
epoch: 47, loss: 648.0, accuracy: 0.673
epoch: 48, loss: 641.34, accuracy: 0.6828
epoch: 49, loss: 644.85, accuracy: 0.6685
epoch: 50, loss: 643.13, accuracy: 0.6723
epoch: 51, loss: 641.74, accuracy: 0.6767
epoch: 52, loss: 644.74, accuracy: 0.6666
Epoch    53: reducing learning rate of group 0 to 5.0000e-03.
epoch: 53, loss: 641.62, accuracy: 0.684
epoch: 54, loss: 599.4, accuracy: 0.6849
epoch: 55, loss: 594.72, accuracy: 0.6972
epoch: 56, loss: 591.68, accuracy: 0.6955
epoch: 57, loss: 591.47, accuracy: 0.6971
epoch: 58, loss: 592.84, accuracy: 0.6885
epoch: 59, loss: 591.46, accuracy: 0.6887
epoch: 60, loss: 589.25, accuracy: 0.6897
epoch: 61, loss: 588.47, accuracy: 0.6868
epoch: 62, loss: 587.47, accuracy: 0.6954
epoch: 63, loss: 587.95, accuracy: 0.6928
epoch: 64, loss: 585.63, accuracy: 0.6964
epoch: 65, loss: 586.55, accuracy: 0.6955
epoch: 66, loss: 588.27, accuracy: 0.6893
epoch: 67, loss: 586.72, accuracy: 0.6995
epoch: 68, loss: 584.37, accuracy: 0.6851
epoch: 69, loss: 585.92, accuracy: 0.6897
epoch: 70, loss: 583.18, accuracy: 0.7026
epoch: 71, loss: 585.34, accuracy: 0.697
epoch: 72, loss: 583.67, accuracy: 0.7021
epoch: 73, loss: 582.58, accuracy: 0.6839
epoch: 74, loss: 583.33, accuracy: 0.6961
epoch: 75, loss: 582.6, accuracy: 0.6912
epoch: 76, loss: 580.59, accuracy: 0.6922
epoch: 77, loss: 582.32, accuracy: 0.6884
epoch: 78, loss: 580.91, accuracy: 0.6913
epoch: 79, loss: 580.85, accuracy: 0.6948
epoch: 80, loss: 540.13, accuracy: 0.7087
epoch: 81, loss: 535.58, accuracy: 0.7086
epoch: 82, loss: 534.84, accuracy: 0.7076
epoch: 83, loss: 534.06, accuracy: 0.7074
epoch: 84, loss: 533.15, accuracy: 0.7063
epoch: 85, loss: 531.51, accuracy: 0.7049
epoch: 86, loss: 532.28, accuracy: 0.7067
epoch: 87, loss: 531.58, accuracy: 0.7028
epoch: 88, loss: 531.39, accuracy: 0.7043
epoch: 89, loss: 530.5, accuracy: 0.7062
epoch: 90, loss: 531.09, accuracy: 0.7074
epoch: 91, loss: 530.17, accuracy: 0.7061
epoch: 92, loss: 530.49, accuracy: 0.706
epoch: 93, loss: 529.75, accuracy: 0.7049
epoch: 94, loss: 529.35, accuracy: 0.7065
epoch: 95, loss: 529.07, accuracy: 0.7032
epoch: 96, loss: 530.64, accuracy: 0.706
epoch: 97, loss: 530.45, accuracy: 0.7065
epoch: 98, loss: 530.41, accuracy: 0.7054
epoch: 99, loss: 530.07, accuracy: 0.7047
Epoch   100: reducing learning rate of group 0 to 2.5000e-04.
epoch: 100, loss: 529.61, accuracy: 0.7088
epoch: 101, loss: 525.83, accuracy: 0.705
epoch: 102, loss: 525.67, accuracy: 0.7077
epoch: 103, loss: 526.86, accuracy: 0.7074
epoch: 104, loss: 525.87, accuracy: 0.7083
epoch: 105, loss: 525.83, accuracy: 0.7092
epoch: 106, loss: 526.5, accuracy: 0.7059
epoch: 107, loss: 525.83, accuracy: 0.7068
epoch: 108, loss: 525.57, accuracy: 0.7077
epoch: 109, loss: 526.37, accuracy: 0.707
Epoch   110: reducing learning rate of group 0 to 1.2500e-04.
epoch: 110, loss: 525.32, accuracy: 0.7066
epoch: 111, loss: 524.87, accuracy: 0.7062
epoch: 112, loss: 524.33, accuracy: 0.7073
epoch: 113, loss: 524.47, accuracy: 0.7072
epoch: 114, loss: 523.16, accuracy: 0.7083
epoch: 115, loss: 522.29, accuracy: 0.7062
epoch: 116, loss: 523.88, accuracy: 0.7064
epoch: 117, loss: 523.42, accuracy: 0.7076
epoch: 118, loss: 523.59, accuracy: 0.7074
epoch: 119, loss: 523.7, accuracy: 0.706
Epoch   120: reducing learning rate of group 0 to 1.0000e-04.
epoch: 120, loss: 524.06, accuracy: 0.7055
epoch: 121, loss: 523.19, accuracy: 0.7069
epoch: 122, loss: 523.08, accuracy: 0.7055
epoch: 123, loss: 523.17, accuracy: 0.7058
epoch: 124, loss: 521.32, accuracy: 0.7068
epoch: 125, loss: 522.67, accuracy: 0.7059
epoch: 126, loss: 522.1, accuracy: 0.7075
epoch: 127, loss: 523.16, accuracy: 0.7063
epoch: 128, loss: 522.44, accuracy: 0.7063
epoch: 129, loss: 523.39, accuracy: 0.708
epoch: 130, loss: 521.89, accuracy: 0.7066
epoch: 131, loss: 521.4, accuracy: 0.7054
epoch: 132, loss: 522.03, accuracy: 0.7054
epoch: 133, loss: 521.77, accuracy: 0.7066
epoch: 134, loss: 520.33, accuracy: 0.7066
epoch: 135, loss: 520.36, accuracy: 0.7063
epoch: 136, loss: 521.98, accuracy: 0.7067
epoch: 137, loss: 520.92, accuracy: 0.7062
epoch: 138, loss: 521.81, accuracy: 0.7064
epoch: 139, loss: 522.42, accuracy: 0.7048
epoch: 140, loss: 521.06, accuracy: 0.707
epoch: 141, loss: 522.12, accuracy: 0.7069
epoch: 142, loss: 522.7, accuracy: 0.7066
epoch: 143, loss: 521.03, accuracy: 0.7072
epoch: 144, loss: 521.62, accuracy: 0.708
epoch: 145, loss: 522.79, accuracy: 0.7068
epoch: 146, loss: 521.68, accuracy: 0.7066
epoch: 147, loss: 520.85, accuracy: 0.7068
epoch: 148, loss: 521.58, accuracy: 0.7073
epoch: 149, loss: 522.05, accuracy: 0.7071
time analysis:
    train 8262.9964 s
    all 8264.8179 s
Accuracy of     0 : 76 %
Accuracy of     1 : 88 %
Accuracy of     2 : 58 %
Accuracy of     3 : 52 %
Accuracy of     4 : 63 %
Accuracy of     5 : 62 %
Accuracy of     6 : 75 %
Accuracy of     7 : 70 %
Accuracy of     8 : 82 %
Accuracy of     9 : 82 %
```

## DAUConv2d 3 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=3)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 6,408
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1151.15, accuracy: 0.5439
epoch: 1, loss: 968.72, accuracy: 0.5498
epoch: 2, loss: 908.87, accuracy: 0.5716
epoch: 3, loss: 871.39, accuracy: 0.601
epoch: 4, loss: 846.89, accuracy: 0.5967
epoch: 5, loss: 826.73, accuracy: 0.585
epoch: 6, loss: 809.69, accuracy: 0.5989
epoch: 7, loss: 795.97, accuracy: 0.626
epoch: 8, loss: 781.46, accuracy: 0.59
epoch: 9, loss: 774.72, accuracy: 0.6442
epoch: 10, loss: 762.78, accuracy: 0.6345
epoch: 11, loss: 756.27, accuracy: 0.6389
epoch: 12, loss: 747.76, accuracy: 0.6288
epoch: 13, loss: 739.92, accuracy: 0.623
epoch: 14, loss: 738.38, accuracy: 0.6466
epoch: 15, loss: 729.25, accuracy: 0.6647
epoch: 16, loss: 724.21, accuracy: 0.6496
epoch: 17, loss: 718.48, accuracy: 0.652
epoch: 18, loss: 713.94, accuracy: 0.6558
epoch: 19, loss: 709.59, accuracy: 0.66
epoch: 20, loss: 707.96, accuracy: 0.6726
epoch: 21, loss: 702.33, accuracy: 0.6672
epoch: 22, loss: 698.65, accuracy: 0.6573
epoch: 23, loss: 694.99, accuracy: 0.6556
epoch: 24, loss: 690.48, accuracy: 0.6548
epoch: 25, loss: 692.94, accuracy: 0.674
epoch: 26, loss: 686.57, accuracy: 0.6699
epoch: 27, loss: 679.87, accuracy: 0.6508
epoch: 28, loss: 684.95, accuracy: 0.6549
epoch: 29, loss: 681.6, accuracy: 0.6682
epoch: 30, loss: 672.41, accuracy: 0.6708
epoch: 31, loss: 675.03, accuracy: 0.6468
epoch: 32, loss: 674.02, accuracy: 0.675
epoch: 33, loss: 670.38, accuracy: 0.664
epoch: 34, loss: 669.52, accuracy: 0.6646
epoch: 35, loss: 667.07, accuracy: 0.6706
epoch: 36, loss: 664.58, accuracy: 0.667
epoch: 37, loss: 658.45, accuracy: 0.6788
epoch: 38, loss: 660.52, accuracy: 0.6761
epoch: 39, loss: 661.69, accuracy: 0.675
epoch: 40, loss: 660.11, accuracy: 0.6748
epoch: 41, loss: 657.31, accuracy: 0.6867
epoch: 42, loss: 656.9, accuracy: 0.6704
epoch: 43, loss: 657.12, accuracy: 0.6897
epoch: 44, loss: 650.52, accuracy: 0.6646
epoch: 45, loss: 649.77, accuracy: 0.6886
epoch: 46, loss: 654.13, accuracy: 0.6783
epoch: 47, loss: 646.19, accuracy: 0.6769
epoch: 48, loss: 645.18, accuracy: 0.6659
epoch: 49, loss: 646.56, accuracy: 0.6529
epoch: 50, loss: 645.54, accuracy: 0.6783
epoch: 51, loss: 646.79, accuracy: 0.6708
epoch: 52, loss: 644.05, accuracy: 0.6752
epoch: 53, loss: 640.64, accuracy: 0.6646
epoch: 54, loss: 638.89, accuracy: 0.6745
epoch: 55, loss: 639.09, accuracy: 0.6692
epoch: 56, loss: 639.07, accuracy: 0.6588
epoch: 57, loss: 637.8, accuracy: 0.6827
epoch: 58, loss: 633.53, accuracy: 0.6649
epoch: 59, loss: 635.97, accuracy: 0.6628
epoch: 60, loss: 635.56, accuracy: 0.6712
epoch: 61, loss: 636.92, accuracy: 0.677
epoch: 62, loss: 632.75, accuracy: 0.6784
epoch: 63, loss: 636.86, accuracy: 0.6743
epoch: 64, loss: 635.62, accuracy: 0.6764
epoch: 65, loss: 632.08, accuracy: 0.6883
epoch: 66, loss: 633.44, accuracy: 0.6695
epoch: 67, loss: 631.58, accuracy: 0.676
epoch: 68, loss: 633.12, accuracy: 0.6766
epoch: 69, loss: 625.22, accuracy: 0.6726
epoch: 70, loss: 629.43, accuracy: 0.666
epoch: 71, loss: 625.41, accuracy: 0.6707
epoch: 72, loss: 625.45, accuracy: 0.6845
epoch: 73, loss: 629.79, accuracy: 0.6886
Epoch    74: reducing learning rate of group 0 to 5.0000e-03.
epoch: 74, loss: 625.06, accuracy: 0.6783
epoch: 75, loss: 582.85, accuracy: 0.6836
epoch: 76, loss: 580.43, accuracy: 0.6908
epoch: 77, loss: 578.76, accuracy: 0.6938
epoch: 78, loss: 580.44, accuracy: 0.6888
epoch: 79, loss: 575.51, accuracy: 0.6886
epoch: 80, loss: 538.66, accuracy: 0.6998
epoch: 81, loss: 533.03, accuracy: 0.7004
epoch: 82, loss: 532.28, accuracy: 0.7008
epoch: 83, loss: 531.46, accuracy: 0.7024
epoch: 84, loss: 530.78, accuracy: 0.7014
epoch: 85, loss: 529.95, accuracy: 0.7008
epoch: 86, loss: 529.3, accuracy: 0.6983
epoch: 87, loss: 528.57, accuracy: 0.6995
epoch: 88, loss: 527.86, accuracy: 0.7013
epoch: 89, loss: 528.21, accuracy: 0.7013
epoch: 90, loss: 527.18, accuracy: 0.7022
epoch: 91, loss: 529.26, accuracy: 0.6991
epoch: 92, loss: 528.56, accuracy: 0.7001
epoch: 93, loss: 528.06, accuracy: 0.6974
epoch: 94, loss: 526.7, accuracy: 0.7011
Epoch    95: reducing learning rate of group 0 to 2.5000e-04.
epoch: 95, loss: 527.09, accuracy: 0.7012
epoch: 96, loss: 525.49, accuracy: 0.699
epoch: 97, loss: 523.14, accuracy: 0.6999
epoch: 98, loss: 524.41, accuracy: 0.7006
epoch: 99, loss: 523.78, accuracy: 0.6992
epoch: 100, loss: 523.95, accuracy: 0.7011
epoch: 101, loss: 521.62, accuracy: 0.6996
epoch: 102, loss: 523.81, accuracy: 0.7014
epoch: 103, loss: 523.82, accuracy: 0.6992
epoch: 104, loss: 522.5, accuracy: 0.6997
epoch: 105, loss: 522.14, accuracy: 0.7011
Epoch   106: reducing learning rate of group 0 to 1.2500e-04.
epoch: 106, loss: 522.16, accuracy: 0.6988
epoch: 107, loss: 521.64, accuracy: 0.7008
epoch: 108, loss: 520.83, accuracy: 0.701
epoch: 109, loss: 521.05, accuracy: 0.7011
epoch: 110, loss: 520.88, accuracy: 0.7002
epoch: 111, loss: 521.84, accuracy: 0.7007
epoch: 112, loss: 520.53, accuracy: 0.7014
epoch: 113, loss: 520.28, accuracy: 0.7024
epoch: 114, loss: 521.57, accuracy: 0.6988
epoch: 115, loss: 521.26, accuracy: 0.7022
epoch: 116, loss: 520.09, accuracy: 0.6994
epoch: 117, loss: 520.1, accuracy: 0.7007
Epoch   118: reducing learning rate of group 0 to 1.0000e-04.
epoch: 118, loss: 520.68, accuracy: 0.7018
epoch: 119, loss: 520.62, accuracy: 0.6997
epoch: 120, loss: 520.07, accuracy: 0.7003
epoch: 121, loss: 520.61, accuracy: 0.7002
epoch: 122, loss: 519.63, accuracy: 0.7009
epoch: 123, loss: 520.44, accuracy: 0.7001
epoch: 124, loss: 519.43, accuracy: 0.7032
epoch: 125, loss: 519.17, accuracy: 0.7018
epoch: 126, loss: 520.33, accuracy: 0.7008
epoch: 127, loss: 520.3, accuracy: 0.7001
epoch: 128, loss: 520.31, accuracy: 0.701
epoch: 129, loss: 520.18, accuracy: 0.7008
epoch: 130, loss: 518.27, accuracy: 0.7005
epoch: 131, loss: 519.06, accuracy: 0.7002
epoch: 132, loss: 519.1, accuracy: 0.6994
epoch: 133, loss: 518.95, accuracy: 0.7006
epoch: 134, loss: 518.68, accuracy: 0.7
epoch: 135, loss: 518.36, accuracy: 0.7001
epoch: 136, loss: 518.16, accuracy: 0.7006
epoch: 137, loss: 518.98, accuracy: 0.6998
epoch: 138, loss: 519.52, accuracy: 0.7006
epoch: 139, loss: 517.88, accuracy: 0.6996
epoch: 140, loss: 519.21, accuracy: 0.6998
epoch: 141, loss: 518.1, accuracy: 0.701
epoch: 142, loss: 518.6, accuracy: 0.7018
epoch: 143, loss: 519.0, accuracy: 0.6997
epoch: 144, loss: 518.15, accuracy: 0.7002
epoch: 145, loss: 519.15, accuracy: 0.7001
epoch: 146, loss: 518.38, accuracy: 0.7007
epoch: 147, loss: 520.44, accuracy: 0.7011
epoch: 148, loss: 517.95, accuracy: 0.7013
epoch: 149, loss: 518.72, accuracy: 0.7012
time analysis:
    train 12468.904 s
    all 12472.5462 s
Accuracy of     0 : 64 %
Accuracy of     1 : 80 %
Accuracy of     2 : 62 %
Accuracy of     3 : 46 %
Accuracy of     4 : 61 %
Accuracy of     5 : 57 %
Accuracy of     6 : 75 %
Accuracy of     7 : 70 %
Accuracy of     8 : 75 %
Accuracy of     9 : 79 %
```

## DAUConv2di 2 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=2)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             460
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,288
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,022
DAU params: 1,748
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1197.59, accuracy: 0.4901
epoch: 1, loss: 1051.46, accuracy: 0.5347
epoch: 2, loss: 1009.9, accuracy: 0.5448
epoch: 3, loss: 985.65, accuracy: 0.5415
epoch: 4, loss: 962.7, accuracy: 0.5628
epoch: 5, loss: 954.71, accuracy: 0.5726
epoch: 6, loss: 939.82, accuracy: 0.579
epoch: 7, loss: 930.42, accuracy: 0.5411
epoch: 8, loss: 924.9, accuracy: 0.5815
epoch: 9, loss: 918.16, accuracy: 0.583
epoch: 10, loss: 908.51, accuracy: 0.5853
epoch: 11, loss: 906.15, accuracy: 0.5719
epoch: 12, loss: 902.18, accuracy: 0.5785
epoch: 13, loss: 898.45, accuracy: 0.5857
epoch: 14, loss: 902.47, accuracy: 0.5889
epoch: 15, loss: 891.14, accuracy: 0.5876
epoch: 16, loss: 890.11, accuracy: 0.5885
epoch: 17, loss: 887.75, accuracy: 0.5781
epoch: 18, loss: 885.43, accuracy: 0.5664
epoch: 19, loss: 882.81, accuracy: 0.595
epoch: 20, loss: 881.31, accuracy: 0.5746
epoch: 21, loss: 879.65, accuracy: 0.5887
epoch: 22, loss: 878.8, accuracy: 0.5858
epoch: 23, loss: 874.42, accuracy: 0.5925
epoch: 24, loss: 872.78, accuracy: 0.5859
epoch: 25, loss: 871.5, accuracy: 0.5822
epoch: 26, loss: 867.9, accuracy: 0.5943
epoch: 27, loss: 866.05, accuracy: 0.5869
epoch: 28, loss: 867.04, accuracy: 0.5747
epoch: 29, loss: 865.92, accuracy: 0.5824
epoch: 30, loss: 861.61, accuracy: 0.58
epoch: 31, loss: 863.35, accuracy: 0.5818
epoch: 32, loss: 860.11, accuracy: 0.5861
epoch: 33, loss: 861.03, accuracy: 0.5955
epoch: 34, loss: 861.28, accuracy: 0.5931
epoch: 35, loss: 856.97, accuracy: 0.5991
epoch: 36, loss: 854.72, accuracy: 0.5904
epoch: 37, loss: 856.05, accuracy: 0.59
epoch: 38, loss: 858.93, accuracy: 0.5953
epoch: 39, loss: 852.23, accuracy: 0.585
epoch: 40, loss: 853.07, accuracy: 0.5918
epoch: 41, loss: 853.16, accuracy: 0.5937
epoch: 42, loss: 850.0, accuracy: 0.5727
epoch: 43, loss: 852.49, accuracy: 0.6004
epoch: 44, loss: 847.73, accuracy: 0.5947
epoch: 45, loss: 850.95, accuracy: 0.5999
epoch: 46, loss: 848.27, accuracy: 0.5918
epoch: 47, loss: 848.26, accuracy: 0.6061
epoch: 48, loss: 843.67, accuracy: 0.6023
epoch: 49, loss: 845.6, accuracy: 0.5858
epoch: 50, loss: 844.22, accuracy: 0.5892
epoch: 51, loss: 844.63, accuracy: 0.5982
epoch: 52, loss: 845.77, accuracy: 0.5759
epoch: 53, loss: 838.68, accuracy: 0.5842
epoch: 54, loss: 842.13, accuracy: 0.5999
epoch: 55, loss: 843.08, accuracy: 0.6025
epoch: 56, loss: 839.57, accuracy: 0.5923
epoch: 57, loss: 839.41, accuracy: 0.5983
epoch: 58, loss: 836.54, accuracy: 0.5972
epoch: 59, loss: 837.48, accuracy: 0.5967
epoch: 60, loss: 835.54, accuracy: 0.6064
epoch: 61, loss: 837.87, accuracy: 0.6036
epoch: 62, loss: 836.54, accuracy: 0.5828
epoch: 63, loss: 833.32, accuracy: 0.6082
epoch: 64, loss: 833.12, accuracy: 0.5966
epoch: 65, loss: 831.11, accuracy: 0.595
epoch: 66, loss: 829.72, accuracy: 0.5801
epoch: 67, loss: 827.72, accuracy: 0.5879
epoch: 68, loss: 825.09, accuracy: 0.6096
epoch: 69, loss: 826.91, accuracy: 0.6017
epoch: 70, loss: 823.93, accuracy: 0.604
epoch: 71, loss: 822.86, accuracy: 0.6031
epoch: 72, loss: 824.24, accuracy: 0.609
epoch: 73, loss: 820.61, accuracy: 0.5955
epoch: 74, loss: 822.38, accuracy: 0.6068
epoch: 75, loss: 818.57, accuracy: 0.6142
epoch: 76, loss: 820.45, accuracy: 0.5953
epoch: 77, loss: 820.68, accuracy: 0.6089
epoch: 78, loss: 816.63, accuracy: 0.6075
epoch: 79, loss: 815.08, accuracy: 0.6046
epoch: 80, loss: 761.57, accuracy: 0.6333
epoch: 81, loss: 755.94, accuracy: 0.624
epoch: 82, loss: 755.11, accuracy: 0.6307
epoch: 83, loss: 754.06, accuracy: 0.6302
epoch: 84, loss: 752.02, accuracy: 0.63
epoch: 85, loss: 751.23, accuracy: 0.6318
epoch: 86, loss: 750.87, accuracy: 0.631
epoch: 87, loss: 751.05, accuracy: 0.6292
epoch: 88, loss: 750.03, accuracy: 0.6303
epoch: 89, loss: 749.58, accuracy: 0.6301
epoch: 90, loss: 748.61, accuracy: 0.6314
epoch: 91, loss: 748.76, accuracy: 0.6293
epoch: 92, loss: 748.46, accuracy: 0.6322
epoch: 93, loss: 747.84, accuracy: 0.63
epoch: 94, loss: 749.0, accuracy: 0.6275
epoch: 95, loss: 746.78, accuracy: 0.6301
epoch: 96, loss: 747.61, accuracy: 0.6317
epoch: 97, loss: 747.23, accuracy: 0.6294
epoch: 98, loss: 746.27, accuracy: 0.6277
epoch: 99, loss: 745.55, accuracy: 0.6302
epoch: 100, loss: 746.92, accuracy: 0.6268
epoch: 101, loss: 745.29, accuracy: 0.6302
epoch: 102, loss: 745.89, accuracy: 0.6286
epoch: 103, loss: 745.3, accuracy: 0.6293
epoch: 104, loss: 744.45, accuracy: 0.6282
epoch: 105, loss: 746.53, accuracy: 0.6304
epoch: 106, loss: 744.67, accuracy: 0.6286
epoch: 107, loss: 745.14, accuracy: 0.6312
epoch: 108, loss: 743.83, accuracy: 0.6316
Epoch   109: reducing learning rate of group 0 to 5.0000e-04.
epoch: 109, loss: 744.67, accuracy: 0.6301
epoch: 110, loss: 739.49, accuracy: 0.6319
epoch: 111, loss: 739.44, accuracy: 0.6323
epoch: 112, loss: 739.52, accuracy: 0.6317
epoch: 113, loss: 740.55, accuracy: 0.6359
epoch: 114, loss: 739.53, accuracy: 0.6328
epoch: 115, loss: 738.13, accuracy: 0.6327
epoch: 116, loss: 738.71, accuracy: 0.6328
epoch: 117, loss: 739.07, accuracy: 0.6337
epoch: 118, loss: 738.86, accuracy: 0.6321
epoch: 119, loss: 738.89, accuracy: 0.6334
Epoch   120: reducing learning rate of group 0 to 2.5000e-04.
epoch: 120, loss: 738.76, accuracy: 0.6321
epoch: 121, loss: 737.62, accuracy: 0.6339
epoch: 122, loss: 737.01, accuracy: 0.6351
epoch: 123, loss: 735.58, accuracy: 0.634
epoch: 124, loss: 735.93, accuracy: 0.6334
epoch: 125, loss: 736.48, accuracy: 0.6336
epoch: 126, loss: 736.5, accuracy: 0.6329
epoch: 127, loss: 735.07, accuracy: 0.6331
epoch: 128, loss: 736.62, accuracy: 0.6346
epoch: 129, loss: 736.77, accuracy: 0.6343
epoch: 130, loss: 733.05, accuracy: 0.6334
epoch: 131, loss: 733.09, accuracy: 0.6349
epoch: 132, loss: 733.2, accuracy: 0.6357
epoch: 133, loss: 733.56, accuracy: 0.6339
epoch: 134, loss: 733.76, accuracy: 0.6356
epoch: 135, loss: 732.74, accuracy: 0.6357
epoch: 136, loss: 733.53, accuracy: 0.6345
epoch: 137, loss: 734.35, accuracy: 0.6352
epoch: 138, loss: 733.74, accuracy: 0.6357
epoch: 139, loss: 732.8, accuracy: 0.6338
epoch: 140, loss: 734.12, accuracy: 0.6335
epoch: 141, loss: 733.53, accuracy: 0.6337
epoch: 142, loss: 732.89, accuracy: 0.6333
epoch: 143, loss: 733.45, accuracy: 0.6353
epoch: 144, loss: 734.37, accuracy: 0.6358
epoch: 145, loss: 732.85, accuracy: 0.635
epoch: 146, loss: 732.41, accuracy: 0.636
epoch: 147, loss: 734.8, accuracy: 0.6343
epoch: 148, loss: 734.29, accuracy: 0.6353
epoch: 149, loss: 733.01, accuracy: 0.6336
time analysis:
    train 2591.8135 s
    all 2596.2388 s
Accuracy of     0 : 69 %
Accuracy of     1 : 74 %
Accuracy of     2 : 50 %
Accuracy of     3 : 39 %
Accuracy of     4 : 50 %
Accuracy of     5 : 44 %
Accuracy of     6 : 75 %
Accuracy of     7 : 60 %
Accuracy of     8 : 74 %
Accuracy of     9 : 65 %
```

## DAUConv2di 3 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=3)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             658
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,928
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,860
DAU params: 2,586
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1205.34, accuracy: 0.5141
epoch: 1, loss: 1027.66, accuracy: 0.5306
epoch: 2, loss: 965.99, accuracy: 0.5621
epoch: 3, loss: 927.15, accuracy: 0.5588
epoch: 4, loss: 903.71, accuracy: 0.59
epoch: 5, loss: 883.83, accuracy: 0.59
epoch: 6, loss: 873.18, accuracy: 0.596
epoch: 7, loss: 859.48, accuracy: 0.6068
epoch: 8, loss: 851.4, accuracy: 0.6081
epoch: 9, loss: 842.75, accuracy: 0.6196
epoch: 10, loss: 834.44, accuracy: 0.6163
epoch: 11, loss: 830.25, accuracy: 0.6085
epoch: 12, loss: 830.22, accuracy: 0.6085
epoch: 13, loss: 822.52, accuracy: 0.5931
epoch: 14, loss: 812.34, accuracy: 0.5855
epoch: 15, loss: 809.53, accuracy: 0.6339
epoch: 16, loss: 805.98, accuracy: 0.6076
epoch: 17, loss: 805.71, accuracy: 0.6089
epoch: 18, loss: 800.94, accuracy: 0.6314
epoch: 19, loss: 798.3, accuracy: 0.6204
epoch: 20, loss: 795.84, accuracy: 0.6323
epoch: 21, loss: 792.24, accuracy: 0.6359
epoch: 22, loss: 791.67, accuracy: 0.6352
epoch: 23, loss: 787.91, accuracy: 0.635
epoch: 24, loss: 782.12, accuracy: 0.6215
epoch: 25, loss: 787.95, accuracy: 0.6286
epoch: 26, loss: 785.32, accuracy: 0.6325
epoch: 27, loss: 780.85, accuracy: 0.6265
epoch: 28, loss: 779.14, accuracy: 0.6162
epoch: 29, loss: 775.89, accuracy: 0.6265
epoch: 30, loss: 772.12, accuracy: 0.6238
epoch: 31, loss: 775.34, accuracy: 0.6254
epoch: 32, loss: 774.42, accuracy: 0.6223
epoch: 33, loss: 773.0, accuracy: 0.6371
epoch: 34, loss: 772.21, accuracy: 0.6356
epoch: 35, loss: 766.28, accuracy: 0.6223
epoch: 36, loss: 764.56, accuracy: 0.6193
epoch: 37, loss: 767.33, accuracy: 0.6319
epoch: 38, loss: 763.01, accuracy: 0.6341
epoch: 39, loss: 761.52, accuracy: 0.6306
epoch: 40, loss: 762.09, accuracy: 0.6273
epoch: 41, loss: 759.83, accuracy: 0.6342
epoch: 42, loss: 756.75, accuracy: 0.6336
epoch: 43, loss: 758.37, accuracy: 0.632
epoch: 44, loss: 755.87, accuracy: 0.6348
epoch: 45, loss: 754.43, accuracy: 0.6386
epoch: 46, loss: 754.76, accuracy: 0.6319
epoch: 47, loss: 752.59, accuracy: 0.6294
epoch: 48, loss: 750.75, accuracy: 0.6098
epoch: 49, loss: 753.41, accuracy: 0.6244
epoch: 50, loss: 749.82, accuracy: 0.6408
epoch: 51, loss: 750.64, accuracy: 0.6211
epoch: 52, loss: 752.06, accuracy: 0.6428
epoch: 53, loss: 748.24, accuracy: 0.6227
epoch: 54, loss: 750.29, accuracy: 0.6312
epoch: 55, loss: 747.05, accuracy: 0.6258
epoch: 56, loss: 748.18, accuracy: 0.6233
epoch: 57, loss: 747.5, accuracy: 0.643
epoch: 58, loss: 746.04, accuracy: 0.6409
epoch: 59, loss: 747.75, accuracy: 0.6381
epoch: 60, loss: 747.83, accuracy: 0.6418
epoch: 61, loss: 745.19, accuracy: 0.639
epoch: 62, loss: 745.05, accuracy: 0.6367
epoch: 63, loss: 741.99, accuracy: 0.6319
epoch: 64, loss: 743.05, accuracy: 0.6431
epoch: 65, loss: 743.37, accuracy: 0.6451
epoch: 66, loss: 744.61, accuracy: 0.6348
epoch: 67, loss: 741.73, accuracy: 0.6449
epoch: 68, loss: 740.81, accuracy: 0.6515
epoch: 69, loss: 741.59, accuracy: 0.6384
epoch: 70, loss: 739.44, accuracy: 0.6404
epoch: 71, loss: 737.52, accuracy: 0.6243
epoch: 72, loss: 739.55, accuracy: 0.6413
epoch: 73, loss: 738.79, accuracy: 0.6361
epoch: 74, loss: 739.86, accuracy: 0.6277
epoch: 75, loss: 737.55, accuracy: 0.6469
epoch: 76, loss: 736.51, accuracy: 0.6435
epoch: 77, loss: 735.99, accuracy: 0.6202
epoch: 78, loss: 737.46, accuracy: 0.6358
epoch: 79, loss: 732.47, accuracy: 0.6416
epoch: 80, loss: 681.51, accuracy: 0.658
epoch: 81, loss: 675.0, accuracy: 0.661
epoch: 82, loss: 673.24, accuracy: 0.6565
epoch: 83, loss: 672.25, accuracy: 0.6614
epoch: 84, loss: 671.39, accuracy: 0.6584
epoch: 85, loss: 670.68, accuracy: 0.6583
epoch: 86, loss: 670.23, accuracy: 0.6605
epoch: 87, loss: 670.83, accuracy: 0.6592
epoch: 88, loss: 670.31, accuracy: 0.6565
epoch: 89, loss: 669.34, accuracy: 0.6615
epoch: 90, loss: 668.83, accuracy: 0.66
epoch: 91, loss: 667.86, accuracy: 0.6593
epoch: 92, loss: 668.57, accuracy: 0.6604
epoch: 93, loss: 667.19, accuracy: 0.6578
epoch: 94, loss: 667.91, accuracy: 0.6609
epoch: 95, loss: 667.34, accuracy: 0.6604
epoch: 96, loss: 667.08, accuracy: 0.6607
epoch: 97, loss: 667.32, accuracy: 0.6607
epoch: 98, loss: 666.46, accuracy: 0.6605
epoch: 99, loss: 665.51, accuracy: 0.6596
epoch: 100, loss: 665.54, accuracy: 0.6584
epoch: 101, loss: 665.64, accuracy: 0.6603
epoch: 102, loss: 665.86, accuracy: 0.6607
epoch: 103, loss: 666.17, accuracy: 0.6638
epoch: 104, loss: 663.61, accuracy: 0.6597
epoch: 105, loss: 664.92, accuracy: 0.6608
epoch: 106, loss: 665.85, accuracy: 0.6603
epoch: 107, loss: 665.45, accuracy: 0.6625
epoch: 108, loss: 663.8, accuracy: 0.661
Epoch   109: reducing learning rate of group 0 to 5.0000e-04.
epoch: 109, loss: 664.2, accuracy: 0.6611
epoch: 110, loss: 660.35, accuracy: 0.6622
epoch: 111, loss: 660.98, accuracy: 0.6631
epoch: 112, loss: 660.64, accuracy: 0.6616
epoch: 113, loss: 660.71, accuracy: 0.663
epoch: 114, loss: 660.26, accuracy: 0.6617
epoch: 115, loss: 660.61, accuracy: 0.6607
epoch: 116, loss: 658.62, accuracy: 0.6653
epoch: 117, loss: 659.16, accuracy: 0.6607
epoch: 118, loss: 659.6, accuracy: 0.6618
epoch: 119, loss: 660.06, accuracy: 0.6599
epoch: 120, loss: 658.7, accuracy: 0.6634
Epoch   121: reducing learning rate of group 0 to 2.5000e-04.
epoch: 121, loss: 659.34, accuracy: 0.6606
epoch: 122, loss: 656.76, accuracy: 0.6614
epoch: 123, loss: 656.64, accuracy: 0.6637
epoch: 124, loss: 657.26, accuracy: 0.6633
epoch: 125, loss: 658.08, accuracy: 0.6622
epoch: 126, loss: 657.5, accuracy: 0.6635
epoch: 127, loss: 656.9, accuracy: 0.6618
epoch: 128, loss: 655.72, accuracy: 0.6625
epoch: 129, loss: 656.46, accuracy: 0.6621
epoch: 130, loss: 654.85, accuracy: 0.6639
epoch: 131, loss: 654.34, accuracy: 0.6637
epoch: 132, loss: 653.5, accuracy: 0.663
epoch: 133, loss: 654.39, accuracy: 0.6623
epoch: 134, loss: 653.6, accuracy: 0.6641
epoch: 135, loss: 654.21, accuracy: 0.6617
epoch: 136, loss: 653.71, accuracy: 0.6627
epoch: 137, loss: 655.41, accuracy: 0.6638
epoch: 138, loss: 655.12, accuracy: 0.6627
epoch: 139, loss: 655.23, accuracy: 0.6636
epoch: 140, loss: 653.41, accuracy: 0.664
epoch: 141, loss: 653.18, accuracy: 0.6624
epoch: 142, loss: 653.94, accuracy: 0.6639
epoch: 143, loss: 653.5, accuracy: 0.6639
epoch: 144, loss: 654.56, accuracy: 0.6629
epoch: 145, loss: 654.4, accuracy: 0.6627
epoch: 146, loss: 653.55, accuracy: 0.6619
epoch: 147, loss: 654.64, accuracy: 0.6631
epoch: 148, loss: 653.65, accuracy: 0.6632
epoch: 149, loss: 655.32, accuracy: 0.6619
time analysis:
    train 3466.7719 s
    all 3469.7669 s
Accuracy of     0 : 69 %
Accuracy of     1 : 76 %
Accuracy of     2 : 48 %
Accuracy of     3 : 43 %
Accuracy of     4 : 50 %
Accuracy of     5 : 54 %
Accuracy of     6 : 69 %
Accuracy of     7 : 71 %
Accuracy of     8 : 72 %
Accuracy of     9 : 70 %
```

## DAUConv2dj 2 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=2)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]             704
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,042
DAU params: 1,768
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1160.25, accuracy: 0.5155
epoch: 1, loss: 981.99, accuracy: 0.5564
epoch: 2, loss: 938.01, accuracy: 0.5614
epoch: 3, loss: 903.28, accuracy: 0.5746
epoch: 4, loss: 879.14, accuracy: 0.5964
epoch: 5, loss: 858.57, accuracy: 0.606
epoch: 6, loss: 842.87, accuracy: 0.5746
epoch: 7, loss: 830.46, accuracy: 0.5919
epoch: 8, loss: 813.91, accuracy: 0.615
epoch: 9, loss: 807.98, accuracy: 0.6117
epoch: 10, loss: 802.33, accuracy: 0.6076
epoch: 11, loss: 800.54, accuracy: 0.6259
epoch: 12, loss: 792.2, accuracy: 0.6161
epoch: 13, loss: 790.81, accuracy: 0.6437
epoch: 14, loss: 785.25, accuracy: 0.6225
epoch: 15, loss: 779.11, accuracy: 0.621
epoch: 16, loss: 776.26, accuracy: 0.6426
epoch: 17, loss: 771.3, accuracy: 0.6349
epoch: 18, loss: 767.15, accuracy: 0.6233
epoch: 19, loss: 765.57, accuracy: 0.6399
epoch: 20, loss: 761.69, accuracy: 0.6203
epoch: 21, loss: 758.26, accuracy: 0.6245
epoch: 22, loss: 758.42, accuracy: 0.6412
epoch: 23, loss: 756.79, accuracy: 0.6111
epoch: 24, loss: 753.7, accuracy: 0.642
epoch: 25, loss: 753.53, accuracy: 0.6051
epoch: 26, loss: 748.48, accuracy: 0.6399
epoch: 27, loss: 748.86, accuracy: 0.6499
epoch: 28, loss: 744.91, accuracy: 0.6319
epoch: 29, loss: 746.7, accuracy: 0.6495
epoch: 30, loss: 744.17, accuracy: 0.6412
epoch: 31, loss: 745.66, accuracy: 0.6382
epoch: 32, loss: 741.81, accuracy: 0.6505
epoch: 33, loss: 737.67, accuracy: 0.6519
epoch: 34, loss: 738.5, accuracy: 0.6335
epoch: 35, loss: 740.82, accuracy: 0.6516
epoch: 36, loss: 738.23, accuracy: 0.6622
epoch: 37, loss: 737.71, accuracy: 0.6617
Epoch    38: reducing learning rate of group 0 to 5.0000e-03.
epoch: 38, loss: 737.4, accuracy: 0.6408
epoch: 39, loss: 698.07, accuracy: 0.6502
epoch: 40, loss: 695.69, accuracy: 0.6673
epoch: 41, loss: 692.13, accuracy: 0.6514
epoch: 42, loss: 693.64, accuracy: 0.6639
epoch: 43, loss: 693.73, accuracy: 0.665
epoch: 44, loss: 692.39, accuracy: 0.6601
epoch: 45, loss: 694.51, accuracy: 0.6558
epoch: 46, loss: 691.72, accuracy: 0.6721
epoch: 47, loss: 690.6, accuracy: 0.6663
epoch: 48, loss: 691.38, accuracy: 0.665
epoch: 49, loss: 689.59, accuracy: 0.6622
epoch: 50, loss: 687.83, accuracy: 0.6638
epoch: 51, loss: 688.44, accuracy: 0.6679
epoch: 52, loss: 690.13, accuracy: 0.6658
epoch: 53, loss: 687.49, accuracy: 0.6739
epoch: 54, loss: 685.89, accuracy: 0.657
epoch: 55, loss: 687.76, accuracy: 0.6575
epoch: 56, loss: 685.41, accuracy: 0.6682
epoch: 57, loss: 687.02, accuracy: 0.6649
epoch: 58, loss: 685.48, accuracy: 0.6436
epoch: 59, loss: 681.68, accuracy: 0.664
epoch: 60, loss: 682.2, accuracy: 0.6601
epoch: 61, loss: 686.45, accuracy: 0.6642
epoch: 62, loss: 685.0, accuracy: 0.6687
epoch: 63, loss: 682.36, accuracy: 0.665
Epoch    64: reducing learning rate of group 0 to 2.5000e-03.
epoch: 64, loss: 681.63, accuracy: 0.6559
epoch: 65, loss: 659.31, accuracy: 0.6773
epoch: 66, loss: 661.98, accuracy: 0.6725
epoch: 67, loss: 660.66, accuracy: 0.6736
epoch: 68, loss: 662.16, accuracy: 0.6674
epoch: 69, loss: 659.5, accuracy: 0.673
epoch: 70, loss: 659.4, accuracy: 0.6708
epoch: 71, loss: 659.33, accuracy: 0.6679
epoch: 72, loss: 660.45, accuracy: 0.6707
epoch: 73, loss: 658.22, accuracy: 0.6699
epoch: 74, loss: 659.45, accuracy: 0.6672
epoch: 75, loss: 659.94, accuracy: 0.6706
epoch: 76, loss: 658.84, accuracy: 0.6724
epoch: 77, loss: 657.64, accuracy: 0.6728
Epoch    78: reducing learning rate of group 0 to 1.2500e-03.
epoch: 78, loss: 658.42, accuracy: 0.6648
epoch: 79, loss: 647.17, accuracy: 0.6746
epoch: 80, loss: 637.04, accuracy: 0.6768
epoch: 81, loss: 636.51, accuracy: 0.6776
epoch: 82, loss: 635.76, accuracy: 0.6778
epoch: 83, loss: 636.01, accuracy: 0.6762
epoch: 84, loss: 635.99, accuracy: 0.6771
epoch: 85, loss: 635.65, accuracy: 0.6765
epoch: 86, loss: 635.41, accuracy: 0.6768
epoch: 87, loss: 636.49, accuracy: 0.6771
Epoch    88: reducing learning rate of group 0 to 1.0000e-04.
epoch: 88, loss: 635.43, accuracy: 0.6779
epoch: 89, loss: 634.77, accuracy: 0.6743
epoch: 90, loss: 635.61, accuracy: 0.6776
epoch: 91, loss: 634.71, accuracy: 0.6748
epoch: 92, loss: 634.01, accuracy: 0.677
epoch: 93, loss: 634.97, accuracy: 0.6774
epoch: 94, loss: 634.31, accuracy: 0.6754
epoch: 95, loss: 634.12, accuracy: 0.676
epoch: 96, loss: 635.15, accuracy: 0.6791
epoch: 97, loss: 634.7, accuracy: 0.6778
epoch: 98, loss: 634.84, accuracy: 0.6755
epoch: 99, loss: 634.8, accuracy: 0.6765
epoch: 100, loss: 635.25, accuracy: 0.6758
epoch: 101, loss: 634.6, accuracy: 0.6769
epoch: 102, loss: 635.78, accuracy: 0.6743
epoch: 103, loss: 634.71, accuracy: 0.6756
epoch: 104, loss: 635.08, accuracy: 0.6745
epoch: 105, loss: 635.2, accuracy: 0.6753
epoch: 106, loss: 633.96, accuracy: 0.6757
epoch: 107, loss: 636.37, accuracy: 0.6744
epoch: 108, loss: 635.02, accuracy: 0.6746
epoch: 109, loss: 634.11, accuracy: 0.6757
epoch: 110, loss: 634.93, accuracy: 0.6766
epoch: 111, loss: 634.82, accuracy: 0.6757
epoch: 112, loss: 634.53, accuracy: 0.6744
epoch: 113, loss: 634.63, accuracy: 0.6761
epoch: 114, loss: 635.48, accuracy: 0.6762
epoch: 115, loss: 634.58, accuracy: 0.676
epoch: 116, loss: 634.67, accuracy: 0.6761
epoch: 117, loss: 634.93, accuracy: 0.6759
epoch: 118, loss: 634.21, accuracy: 0.6748
epoch: 119, loss: 634.62, accuracy: 0.6758
epoch: 120, loss: 634.66, accuracy: 0.6748
epoch: 121, loss: 634.04, accuracy: 0.6787
epoch: 122, loss: 634.13, accuracy: 0.676
epoch: 123, loss: 634.62, accuracy: 0.6778
epoch: 124, loss: 634.2, accuracy: 0.6749
epoch: 125, loss: 635.62, accuracy: 0.6779
epoch: 126, loss: 635.01, accuracy: 0.6759
epoch: 127, loss: 635.0, accuracy: 0.6774
epoch: 128, loss: 635.37, accuracy: 0.6754
epoch: 129, loss: 634.29, accuracy: 0.6778
epoch: 130, loss: 634.01, accuracy: 0.6763
epoch: 131, loss: 631.75, accuracy: 0.6757
epoch: 132, loss: 633.11, accuracy: 0.676
epoch: 133, loss: 633.31, accuracy: 0.6762
epoch: 134, loss: 634.49, accuracy: 0.6753
epoch: 135, loss: 634.15, accuracy: 0.6765
epoch: 136, loss: 634.59, accuracy: 0.676
epoch: 137, loss: 633.29, accuracy: 0.6757
epoch: 138, loss: 632.79, accuracy: 0.6764
epoch: 139, loss: 632.88, accuracy: 0.6752
epoch: 140, loss: 632.96, accuracy: 0.6776
epoch: 141, loss: 633.54, accuracy: 0.6785
epoch: 142, loss: 633.55, accuracy: 0.6768
epoch: 143, loss: 633.06, accuracy: 0.6761
epoch: 144, loss: 633.88, accuracy: 0.6766
epoch: 145, loss: 633.45, accuracy: 0.675
epoch: 146, loss: 633.76, accuracy: 0.6774
epoch: 147, loss: 633.12, accuracy: 0.678
epoch: 148, loss: 633.79, accuracy: 0.6769
epoch: 149, loss: 632.64, accuracy: 0.6763
time analysis:
    train 8296.8155 s
    all 8300.5564 s
Accuracy of     0 : 73 %
Accuracy of     1 : 84 %
Accuracy of     2 : 54 %
Accuracy of     3 : 45 %
Accuracy of     4 : 52 %
Accuracy of     5 : 57 %
Accuracy of     6 : 78 %
Accuracy of     7 : 60 %
Accuracy of     8 : 79 %
Accuracy of     9 : 74 %
```

## DAUConv2dj 3 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=3)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,024
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,592
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,890
DAU params: 2,616
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1190.95, accuracy: 0.5129
epoch: 1, loss: 985.96, accuracy: 0.5814
epoch: 2, loss: 925.58, accuracy: 0.5945
epoch: 3, loss: 895.99, accuracy: 0.5781
epoch: 4, loss: 875.9, accuracy: 0.6047
epoch: 5, loss: 858.6, accuracy: 0.6139
epoch: 6, loss: 844.36, accuracy: 0.6229
epoch: 7, loss: 834.29, accuracy: 0.568
epoch: 8, loss: 821.97, accuracy: 0.6278
epoch: 9, loss: 815.06, accuracy: 0.6199
epoch: 10, loss: 815.03, accuracy: 0.6273
epoch: 11, loss: 805.83, accuracy: 0.6337
epoch: 12, loss: 799.39, accuracy: 0.6219
epoch: 13, loss: 796.01, accuracy: 0.6209
epoch: 14, loss: 795.34, accuracy: 0.6073
epoch: 15, loss: 784.43, accuracy: 0.6391
epoch: 16, loss: 780.43, accuracy: 0.6253
epoch: 17, loss: 781.3, accuracy: 0.6246
epoch: 18, loss: 773.98, accuracy: 0.6415
epoch: 19, loss: 772.79, accuracy: 0.6354
epoch: 20, loss: 771.68, accuracy: 0.6319
epoch: 21, loss: 764.28, accuracy: 0.6468
epoch: 22, loss: 760.76, accuracy: 0.6499
epoch: 23, loss: 764.42, accuracy: 0.6313
epoch: 24, loss: 764.11, accuracy: 0.635
epoch: 25, loss: 757.32, accuracy: 0.6513
epoch: 26, loss: 752.56, accuracy: 0.6294
epoch: 27, loss: 750.37, accuracy: 0.6257
epoch: 28, loss: 753.36, accuracy: 0.6315
epoch: 29, loss: 749.66, accuracy: 0.6426
epoch: 30, loss: 747.56, accuracy: 0.6453
epoch: 31, loss: 744.51, accuracy: 0.6543
epoch: 32, loss: 747.4, accuracy: 0.6536
epoch: 33, loss: 743.51, accuracy: 0.6522
epoch: 34, loss: 742.11, accuracy: 0.6385
epoch: 35, loss: 742.51, accuracy: 0.6254
epoch: 36, loss: 738.68, accuracy: 0.6457
epoch: 37, loss: 739.32, accuracy: 0.6403
epoch: 38, loss: 738.36, accuracy: 0.621
epoch: 39, loss: 734.53, accuracy: 0.6511
epoch: 40, loss: 732.81, accuracy: 0.6388
epoch: 41, loss: 732.85, accuracy: 0.6507
epoch: 42, loss: 731.71, accuracy: 0.6568
epoch: 43, loss: 733.7, accuracy: 0.6515
epoch: 44, loss: 730.45, accuracy: 0.6382
epoch: 45, loss: 725.5, accuracy: 0.641
epoch: 46, loss: 729.07, accuracy: 0.6531
epoch: 47, loss: 729.25, accuracy: 0.6464
epoch: 48, loss: 726.52, accuracy: 0.653
epoch: 49, loss: 725.88, accuracy: 0.6572
Epoch    50: reducing learning rate of group 0 to 5.0000e-03.
epoch: 50, loss: 725.21, accuracy: 0.6551
epoch: 51, loss: 691.09, accuracy: 0.6632
epoch: 52, loss: 689.43, accuracy: 0.6655
epoch: 53, loss: 687.98, accuracy: 0.6619
epoch: 54, loss: 688.2, accuracy: 0.6699
epoch: 55, loss: 685.8, accuracy: 0.6684
epoch: 56, loss: 686.51, accuracy: 0.6566
epoch: 57, loss: 685.11, accuracy: 0.6626
epoch: 58, loss: 685.11, accuracy: 0.6652
epoch: 59, loss: 685.42, accuracy: 0.6585
epoch: 60, loss: 685.58, accuracy: 0.6676
epoch: 61, loss: 683.63, accuracy: 0.6662
epoch: 62, loss: 682.84, accuracy: 0.6617
epoch: 63, loss: 683.77, accuracy: 0.6651
epoch: 64, loss: 680.88, accuracy: 0.6595
epoch: 65, loss: 681.13, accuracy: 0.6649
epoch: 66, loss: 680.86, accuracy: 0.6662
epoch: 67, loss: 679.75, accuracy: 0.6621
epoch: 68, loss: 680.28, accuracy: 0.6619
epoch: 69, loss: 680.81, accuracy: 0.6688
epoch: 70, loss: 680.73, accuracy: 0.6638
epoch: 71, loss: 678.0, accuracy: 0.6661
epoch: 72, loss: 678.26, accuracy: 0.6702
epoch: 73, loss: 676.94, accuracy: 0.6644
epoch: 74, loss: 677.59, accuracy: 0.6638
epoch: 75, loss: 676.6, accuracy: 0.6682
epoch: 76, loss: 678.19, accuracy: 0.6679
epoch: 77, loss: 677.68, accuracy: 0.6576
epoch: 78, loss: 676.07, accuracy: 0.651
epoch: 79, loss: 677.72, accuracy: 0.6728
epoch: 80, loss: 643.86, accuracy: 0.6777
epoch: 81, loss: 639.93, accuracy: 0.6768
epoch: 82, loss: 641.19, accuracy: 0.6759
epoch: 83, loss: 639.07, accuracy: 0.677
epoch: 84, loss: 639.06, accuracy: 0.6772
epoch: 85, loss: 639.17, accuracy: 0.6783
epoch: 86, loss: 638.93, accuracy: 0.6764
epoch: 87, loss: 637.96, accuracy: 0.677
epoch: 88, loss: 638.91, accuracy: 0.6781
epoch: 89, loss: 637.5, accuracy: 0.6766
epoch: 90, loss: 638.37, accuracy: 0.6763
epoch: 91, loss: 638.53, accuracy: 0.6781
Epoch    92: reducing learning rate of group 0 to 2.5000e-04.
epoch: 92, loss: 638.2, accuracy: 0.6777
epoch: 93, loss: 634.87, accuracy: 0.6766
epoch: 94, loss: 634.85, accuracy: 0.6767
epoch: 95, loss: 634.69, accuracy: 0.6768
epoch: 96, loss: 634.47, accuracy: 0.6776
epoch: 97, loss: 635.45, accuracy: 0.6779
epoch: 98, loss: 634.12, accuracy: 0.6767
epoch: 99, loss: 635.41, accuracy: 0.6764
epoch: 100, loss: 634.2, accuracy: 0.6766
epoch: 101, loss: 635.48, accuracy: 0.6784
epoch: 102, loss: 634.59, accuracy: 0.6778
Epoch   103: reducing learning rate of group 0 to 1.2500e-04.
epoch: 103, loss: 634.31, accuracy: 0.6773
epoch: 104, loss: 633.34, accuracy: 0.6786
epoch: 105, loss: 634.13, accuracy: 0.6797
epoch: 106, loss: 633.05, accuracy: 0.6788
epoch: 107, loss: 633.38, accuracy: 0.6785
epoch: 108, loss: 633.49, accuracy: 0.6784
epoch: 109, loss: 632.56, accuracy: 0.6777
epoch: 110, loss: 633.52, accuracy: 0.6783
epoch: 111, loss: 633.13, accuracy: 0.6784
epoch: 112, loss: 632.75, accuracy: 0.6784
epoch: 113, loss: 633.61, accuracy: 0.6799
Epoch   114: reducing learning rate of group 0 to 1.0000e-04.
epoch: 114, loss: 633.29, accuracy: 0.6801
epoch: 115, loss: 633.98, accuracy: 0.6792
epoch: 116, loss: 632.52, accuracy: 0.6761
epoch: 117, loss: 632.72, accuracy: 0.678
epoch: 118, loss: 633.26, accuracy: 0.6771
epoch: 119, loss: 634.2, accuracy: 0.6766
epoch: 120, loss: 632.59, accuracy: 0.6779
epoch: 121, loss: 632.98, accuracy: 0.6771
epoch: 122, loss: 631.38, accuracy: 0.6779
epoch: 123, loss: 632.2, accuracy: 0.6784
epoch: 124, loss: 633.27, accuracy: 0.677
epoch: 125, loss: 633.31, accuracy: 0.6785
epoch: 126, loss: 633.65, accuracy: 0.6786
epoch: 127, loss: 632.17, accuracy: 0.6786
epoch: 128, loss: 632.5, accuracy: 0.6784
epoch: 129, loss: 631.67, accuracy: 0.68
epoch: 130, loss: 631.59, accuracy: 0.679
epoch: 131, loss: 632.49, accuracy: 0.6793
epoch: 132, loss: 631.56, accuracy: 0.6782
epoch: 133, loss: 631.56, accuracy: 0.6777
epoch: 134, loss: 631.89, accuracy: 0.6775
epoch: 135, loss: 631.94, accuracy: 0.6782
epoch: 136, loss: 630.94, accuracy: 0.6784
epoch: 137, loss: 631.56, accuracy: 0.6797
epoch: 138, loss: 633.37, accuracy: 0.6794
epoch: 139, loss: 631.95, accuracy: 0.6789
epoch: 140, loss: 630.97, accuracy: 0.6785
epoch: 141, loss: 631.78, accuracy: 0.6777
epoch: 142, loss: 632.27, accuracy: 0.6779
epoch: 143, loss: 631.52, accuracy: 0.6767
epoch: 144, loss: 631.47, accuracy: 0.6775
epoch: 145, loss: 632.05, accuracy: 0.6781
epoch: 146, loss: 631.66, accuracy: 0.6779
epoch: 147, loss: 631.95, accuracy: 0.6794
epoch: 148, loss: 630.82, accuracy: 0.6781
epoch: 149, loss: 631.43, accuracy: 0.6789
time analysis:
    train 12513.2794 s
    all 12516.8676 s
Accuracy of     0 : 73 %
Accuracy of     1 : 84 %
Accuracy of     2 : 59 %
Accuracy of     3 : 47 %
Accuracy of     4 : 60 %
Accuracy of     5 : 45 %
Accuracy of     6 : 73 %
Accuracy of     7 : 71 %
Accuracy of     8 : 75 %
Accuracy of     9 : 76 %
```

## DAUConv2dOneMu 2 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=2)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             452
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,036
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,762
DAU params: 1,488
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1195.3, accuracy: 0.5034
epoch: 1, loss: 1062.44, accuracy: 0.4842
epoch: 2, loss: 1027.22, accuracy: 0.533
epoch: 3, loss: 992.38, accuracy: 0.5443
epoch: 4, loss: 966.32, accuracy: 0.5401
epoch: 5, loss: 953.5, accuracy: 0.5682
epoch: 6, loss: 942.61, accuracy: 0.5655
epoch: 7, loss: 937.04, accuracy: 0.552
epoch: 8, loss: 927.35, accuracy: 0.5452
epoch: 9, loss: 922.92, accuracy: 0.5589
epoch: 10, loss: 917.99, accuracy: 0.5456
epoch: 11, loss: 913.29, accuracy: 0.57
epoch: 12, loss: 912.57, accuracy: 0.5502
epoch: 13, loss: 904.58, accuracy: 0.5633
epoch: 14, loss: 905.36, accuracy: 0.5517
epoch: 15, loss: 903.7, accuracy: 0.5648
epoch: 16, loss: 896.04, accuracy: 0.5741
epoch: 17, loss: 892.92, accuracy: 0.572
epoch: 18, loss: 890.12, accuracy: 0.5748
epoch: 19, loss: 893.84, accuracy: 0.5758
epoch: 20, loss: 891.35, accuracy: 0.5761
epoch: 21, loss: 889.21, accuracy: 0.5635
epoch: 22, loss: 888.35, accuracy: 0.5866
epoch: 23, loss: 888.25, accuracy: 0.5757
epoch: 24, loss: 882.15, accuracy: 0.574
epoch: 25, loss: 881.54, accuracy: 0.5626
epoch: 26, loss: 883.79, accuracy: 0.5767
epoch: 27, loss: 883.12, accuracy: 0.5693
epoch: 28, loss: 879.3, accuracy: 0.5674
epoch: 29, loss: 881.5, accuracy: 0.5848
epoch: 30, loss: 875.29, accuracy: 0.5715
epoch: 31, loss: 878.51, accuracy: 0.5639
epoch: 32, loss: 875.61, accuracy: 0.5629
epoch: 33, loss: 867.6, accuracy: 0.5768
epoch: 34, loss: 874.84, accuracy: 0.5776
epoch: 35, loss: 870.02, accuracy: 0.5794
epoch: 36, loss: 869.15, accuracy: 0.5721
epoch: 37, loss: 868.42, accuracy: 0.5804
Epoch    38: reducing learning rate of group 0 to 5.0000e-03.
epoch: 38, loss: 870.31, accuracy: 0.5715
epoch: 39, loss: 835.51, accuracy: 0.5999
epoch: 40, loss: 831.5, accuracy: 0.5978
epoch: 41, loss: 830.56, accuracy: 0.5894
epoch: 42, loss: 832.11, accuracy: 0.5866
epoch: 43, loss: 828.47, accuracy: 0.5962
epoch: 44, loss: 828.52, accuracy: 0.5969
epoch: 45, loss: 829.33, accuracy: 0.5943
epoch: 46, loss: 829.11, accuracy: 0.5958
epoch: 47, loss: 825.68, accuracy: 0.594
epoch: 48, loss: 826.75, accuracy: 0.5908
epoch: 49, loss: 828.03, accuracy: 0.5949
epoch: 50, loss: 827.47, accuracy: 0.5899
epoch: 51, loss: 823.55, accuracy: 0.5983
epoch: 52, loss: 826.21, accuracy: 0.6042
epoch: 53, loss: 823.18, accuracy: 0.6044
epoch: 54, loss: 826.33, accuracy: 0.597
epoch: 55, loss: 823.54, accuracy: 0.5988
Epoch    56: reducing learning rate of group 0 to 2.5000e-03.
epoch: 56, loss: 823.72, accuracy: 0.6041
epoch: 57, loss: 803.4, accuracy: 0.6043
epoch: 58, loss: 801.85, accuracy: 0.6059
epoch: 59, loss: 802.2, accuracy: 0.6051
epoch: 60, loss: 801.47, accuracy: 0.6004
epoch: 61, loss: 803.01, accuracy: 0.606
epoch: 62, loss: 800.94, accuracy: 0.6027
epoch: 63, loss: 800.39, accuracy: 0.6091
epoch: 64, loss: 799.96, accuracy: 0.5998
epoch: 65, loss: 799.76, accuracy: 0.6092
epoch: 66, loss: 799.4, accuracy: 0.6072
epoch: 67, loss: 798.4, accuracy: 0.6106
epoch: 68, loss: 797.48, accuracy: 0.6068
epoch: 69, loss: 800.2, accuracy: 0.6023
epoch: 70, loss: 798.85, accuracy: 0.6057
epoch: 71, loss: 798.34, accuracy: 0.6091
epoch: 72, loss: 796.61, accuracy: 0.6006
epoch: 73, loss: 798.13, accuracy: 0.6099
epoch: 74, loss: 799.12, accuracy: 0.603
epoch: 75, loss: 797.56, accuracy: 0.6016
epoch: 76, loss: 797.19, accuracy: 0.6072
epoch: 77, loss: 794.82, accuracy: 0.6113
epoch: 78, loss: 796.94, accuracy: 0.6119
epoch: 79, loss: 795.98, accuracy: 0.6072
epoch: 80, loss: 778.78, accuracy: 0.616
epoch: 81, loss: 778.01, accuracy: 0.6154
epoch: 82, loss: 775.76, accuracy: 0.6135
epoch: 83, loss: 775.91, accuracy: 0.6141
epoch: 84, loss: 776.35, accuracy: 0.6151
epoch: 85, loss: 775.3, accuracy: 0.6116
epoch: 86, loss: 775.11, accuracy: 0.6139
epoch: 87, loss: 774.61, accuracy: 0.6171
epoch: 88, loss: 776.12, accuracy: 0.6144
epoch: 89, loss: 775.17, accuracy: 0.6169
epoch: 90, loss: 776.72, accuracy: 0.6154
epoch: 91, loss: 775.54, accuracy: 0.6148
Epoch    92: reducing learning rate of group 0 to 1.2500e-04.
epoch: 92, loss: 774.53, accuracy: 0.6145
epoch: 93, loss: 774.34, accuracy: 0.615
epoch: 94, loss: 774.64, accuracy: 0.6153
epoch: 95, loss: 772.39, accuracy: 0.6161
epoch: 96, loss: 774.91, accuracy: 0.6158
epoch: 97, loss: 774.03, accuracy: 0.6171
epoch: 98, loss: 773.0, accuracy: 0.6154
epoch: 99, loss: 773.99, accuracy: 0.6127
epoch: 100, loss: 774.63, accuracy: 0.6159
epoch: 101, loss: 774.8, accuracy: 0.6167
Epoch   102: reducing learning rate of group 0 to 1.0000e-04.
epoch: 102, loss: 773.6, accuracy: 0.6135
epoch: 103, loss: 773.34, accuracy: 0.6159
epoch: 104, loss: 773.99, accuracy: 0.6154
epoch: 105, loss: 773.22, accuracy: 0.6145
epoch: 106, loss: 772.73, accuracy: 0.6138
epoch: 107, loss: 773.87, accuracy: 0.6155
epoch: 108, loss: 773.78, accuracy: 0.6145
epoch: 109, loss: 773.75, accuracy: 0.6154
epoch: 110, loss: 773.07, accuracy: 0.6166
epoch: 111, loss: 772.54, accuracy: 0.6161
epoch: 112, loss: 773.35, accuracy: 0.614
epoch: 113, loss: 773.93, accuracy: 0.6154
epoch: 114, loss: 773.39, accuracy: 0.6131
epoch: 115, loss: 773.39, accuracy: 0.6164
epoch: 116, loss: 773.97, accuracy: 0.6164
epoch: 117, loss: 774.14, accuracy: 0.6156
epoch: 118, loss: 772.56, accuracy: 0.6145
epoch: 119, loss: 773.26, accuracy: 0.6144
epoch: 120, loss: 772.04, accuracy: 0.6166
epoch: 121, loss: 773.96, accuracy: 0.617
epoch: 122, loss: 773.22, accuracy: 0.6153
epoch: 123, loss: 773.22, accuracy: 0.6188
epoch: 124, loss: 773.6, accuracy: 0.6162
epoch: 125, loss: 772.87, accuracy: 0.6156
epoch: 126, loss: 773.47, accuracy: 0.6147
epoch: 127, loss: 773.17, accuracy: 0.6164
epoch: 128, loss: 773.34, accuracy: 0.6155
epoch: 129, loss: 773.6, accuracy: 0.617
epoch: 130, loss: 773.05, accuracy: 0.6144
epoch: 131, loss: 771.92, accuracy: 0.6146
epoch: 132, loss: 772.12, accuracy: 0.6154
epoch: 133, loss: 772.49, accuracy: 0.6159
epoch: 134, loss: 771.69, accuracy: 0.6148
epoch: 135, loss: 771.36, accuracy: 0.6164
epoch: 136, loss: 772.06, accuracy: 0.6178
epoch: 137, loss: 772.34, accuracy: 0.6168
epoch: 138, loss: 772.21, accuracy: 0.6175
epoch: 139, loss: 770.96, accuracy: 0.6155
epoch: 140, loss: 770.76, accuracy: 0.6166
epoch: 141, loss: 771.62, accuracy: 0.6163
epoch: 142, loss: 771.65, accuracy: 0.6147
epoch: 143, loss: 772.73, accuracy: 0.6155
epoch: 144, loss: 772.45, accuracy: 0.6161
epoch: 145, loss: 771.2, accuracy: 0.615
epoch: 146, loss: 772.45, accuracy: 0.6165
epoch: 147, loss: 772.08, accuracy: 0.6164
epoch: 148, loss: 771.69, accuracy: 0.6158
epoch: 149, loss: 772.46, accuracy: 0.6128
time analysis:
    train 2636.6671 s
    all 2641.1822 s
Accuracy of     0 : 57 %
Accuracy of     1 : 80 %
Accuracy of     2 : 48 %
Accuracy of     3 : 39 %
Accuracy of     4 : 49 %
Accuracy of     5 : 55 %
Accuracy of     6 : 62 %
Accuracy of     7 : 64 %
Accuracy of     8 : 74 %
Accuracy of     9 : 67 %
```

## DAUConv2dOneMu 3 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=3)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             646
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,550
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,470
DAU params: 2,196
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1213.31, accuracy: 0.5059
epoch: 1, loss: 1070.07, accuracy: 0.5207
epoch: 2, loss: 1015.21, accuracy: 0.5468
epoch: 3, loss: 986.13, accuracy: 0.5585
epoch: 4, loss: 961.06, accuracy: 0.5554
epoch: 5, loss: 952.29, accuracy: 0.569
epoch: 6, loss: 939.23, accuracy: 0.5566
epoch: 7, loss: 932.76, accuracy: 0.5715
epoch: 8, loss: 923.19, accuracy: 0.5613
epoch: 9, loss: 922.62, accuracy: 0.5605
epoch: 10, loss: 911.14, accuracy: 0.5605
epoch: 11, loss: 908.55, accuracy: 0.556
epoch: 12, loss: 905.43, accuracy: 0.5678
epoch: 13, loss: 898.86, accuracy: 0.5666
epoch: 14, loss: 895.12, accuracy: 0.5667
epoch: 15, loss: 889.09, accuracy: 0.579
epoch: 16, loss: 888.45, accuracy: 0.579
epoch: 17, loss: 883.49, accuracy: 0.5839
epoch: 18, loss: 882.35, accuracy: 0.5685
epoch: 19, loss: 877.54, accuracy: 0.595
epoch: 20, loss: 874.75, accuracy: 0.5819
epoch: 21, loss: 873.24, accuracy: 0.5835
epoch: 22, loss: 865.96, accuracy: 0.5874
epoch: 23, loss: 867.22, accuracy: 0.5893
epoch: 24, loss: 863.88, accuracy: 0.5875
epoch: 25, loss: 861.7, accuracy: 0.5898
epoch: 26, loss: 860.61, accuracy: 0.5777
epoch: 27, loss: 862.53, accuracy: 0.5955
epoch: 28, loss: 852.05, accuracy: 0.5943
epoch: 29, loss: 853.7, accuracy: 0.5784
epoch: 30, loss: 855.04, accuracy: 0.5719
epoch: 31, loss: 852.54, accuracy: 0.5896
epoch: 32, loss: 848.87, accuracy: 0.594
epoch: 33, loss: 847.0, accuracy: 0.5757
epoch: 34, loss: 849.54, accuracy: 0.5981
epoch: 35, loss: 848.28, accuracy: 0.5867
epoch: 36, loss: 846.13, accuracy: 0.5858
epoch: 37, loss: 846.57, accuracy: 0.5895
epoch: 38, loss: 844.48, accuracy: 0.5936
epoch: 39, loss: 845.74, accuracy: 0.5858
epoch: 40, loss: 842.86, accuracy: 0.6046
epoch: 41, loss: 838.4, accuracy: 0.6076
epoch: 42, loss: 837.33, accuracy: 0.5924
epoch: 43, loss: 835.65, accuracy: 0.5926
epoch: 44, loss: 835.12, accuracy: 0.5974
epoch: 45, loss: 833.43, accuracy: 0.6037
epoch: 46, loss: 833.69, accuracy: 0.5898
epoch: 47, loss: 839.21, accuracy: 0.5964
epoch: 48, loss: 831.88, accuracy: 0.6105
epoch: 49, loss: 828.34, accuracy: 0.5929
epoch: 50, loss: 831.54, accuracy: 0.6033
epoch: 51, loss: 824.8, accuracy: 0.6064
epoch: 52, loss: 827.14, accuracy: 0.5821
epoch: 53, loss: 823.73, accuracy: 0.6064
epoch: 54, loss: 821.63, accuracy: 0.6001
epoch: 55, loss: 822.78, accuracy: 0.6082
epoch: 56, loss: 821.09, accuracy: 0.5982
epoch: 57, loss: 820.77, accuracy: 0.5978
epoch: 58, loss: 820.5, accuracy: 0.6054
epoch: 59, loss: 817.31, accuracy: 0.6136
epoch: 60, loss: 818.75, accuracy: 0.611
epoch: 61, loss: 817.62, accuracy: 0.6053
epoch: 62, loss: 815.7, accuracy: 0.6092
epoch: 63, loss: 815.41, accuracy: 0.605
epoch: 64, loss: 813.02, accuracy: 0.585
epoch: 65, loss: 814.83, accuracy: 0.5976
epoch: 66, loss: 812.16, accuracy: 0.6025
epoch: 67, loss: 809.21, accuracy: 0.6099
epoch: 68, loss: 809.8, accuracy: 0.6078
epoch: 69, loss: 809.78, accuracy: 0.6022
epoch: 70, loss: 805.55, accuracy: 0.6019
epoch: 71, loss: 807.21, accuracy: 0.6161
epoch: 72, loss: 807.21, accuracy: 0.6122
epoch: 73, loss: 805.05, accuracy: 0.6019
epoch: 74, loss: 805.03, accuracy: 0.6111
epoch: 75, loss: 802.73, accuracy: 0.6002
epoch: 76, loss: 804.74, accuracy: 0.6128
epoch: 77, loss: 807.12, accuracy: 0.6123
epoch: 78, loss: 802.26, accuracy: 0.6012
epoch: 79, loss: 806.17, accuracy: 0.5964
epoch: 80, loss: 746.23, accuracy: 0.6295
epoch: 81, loss: 742.53, accuracy: 0.6242
epoch: 82, loss: 740.9, accuracy: 0.6277
epoch: 83, loss: 741.92, accuracy: 0.6332
epoch: 84, loss: 738.59, accuracy: 0.6321
epoch: 85, loss: 738.2, accuracy: 0.6343
epoch: 86, loss: 739.66, accuracy: 0.632
epoch: 87, loss: 738.16, accuracy: 0.6313
epoch: 88, loss: 737.32, accuracy: 0.632
epoch: 89, loss: 736.83, accuracy: 0.633
epoch: 90, loss: 736.13, accuracy: 0.6286
epoch: 91, loss: 736.63, accuracy: 0.6284
epoch: 92, loss: 738.66, accuracy: 0.6312
epoch: 93, loss: 736.89, accuracy: 0.6335
epoch: 94, loss: 736.02, accuracy: 0.6345
Epoch    95: reducing learning rate of group 0 to 5.0000e-04.
epoch: 95, loss: 735.83, accuracy: 0.6329
epoch: 96, loss: 731.61, accuracy: 0.6314
epoch: 97, loss: 731.94, accuracy: 0.6326
epoch: 98, loss: 731.16, accuracy: 0.6318
epoch: 99, loss: 732.22, accuracy: 0.6306
epoch: 100, loss: 730.7, accuracy: 0.6338
epoch: 101, loss: 730.01, accuracy: 0.633
epoch: 102, loss: 729.57, accuracy: 0.6303
epoch: 103, loss: 730.37, accuracy: 0.634
epoch: 104, loss: 730.23, accuracy: 0.6374
epoch: 105, loss: 729.94, accuracy: 0.635
epoch: 106, loss: 729.12, accuracy: 0.6325
Epoch   107: reducing learning rate of group 0 to 2.5000e-04.
epoch: 107, loss: 729.53, accuracy: 0.6369
epoch: 108, loss: 728.55, accuracy: 0.6354
epoch: 109, loss: 727.77, accuracy: 0.6332
epoch: 110, loss: 727.54, accuracy: 0.6336
epoch: 111, loss: 727.22, accuracy: 0.637
epoch: 112, loss: 726.93, accuracy: 0.635
epoch: 113, loss: 727.32, accuracy: 0.6348
epoch: 114, loss: 727.55, accuracy: 0.6355
epoch: 115, loss: 727.38, accuracy: 0.6359
epoch: 116, loss: 726.55, accuracy: 0.6343
Epoch   117: reducing learning rate of group 0 to 1.2500e-04.
epoch: 117, loss: 727.16, accuracy: 0.6351
epoch: 118, loss: 725.52, accuracy: 0.6332
epoch: 119, loss: 724.95, accuracy: 0.6345
epoch: 120, loss: 725.53, accuracy: 0.6348
epoch: 121, loss: 726.04, accuracy: 0.6329
epoch: 122, loss: 725.49, accuracy: 0.6352
epoch: 123, loss: 725.45, accuracy: 0.633
epoch: 124, loss: 725.23, accuracy: 0.6358
epoch: 125, loss: 725.91, accuracy: 0.6356
epoch: 126, loss: 726.01, accuracy: 0.6356
Epoch   127: reducing learning rate of group 0 to 1.0000e-04.
epoch: 127, loss: 725.83, accuracy: 0.636
epoch: 128, loss: 725.41, accuracy: 0.6347
epoch: 129, loss: 724.85, accuracy: 0.6363
epoch: 130, loss: 724.85, accuracy: 0.6348
epoch: 131, loss: 724.72, accuracy: 0.6369
epoch: 132, loss: 724.15, accuracy: 0.636
epoch: 133, loss: 724.75, accuracy: 0.6347
epoch: 134, loss: 724.56, accuracy: 0.636
epoch: 135, loss: 724.37, accuracy: 0.6361
epoch: 136, loss: 724.84, accuracy: 0.6375
epoch: 137, loss: 723.89, accuracy: 0.6345
epoch: 138, loss: 723.96, accuracy: 0.6363
epoch: 139, loss: 723.76, accuracy: 0.6347
epoch: 140, loss: 725.21, accuracy: 0.6359
epoch: 141, loss: 724.42, accuracy: 0.6373
epoch: 142, loss: 724.15, accuracy: 0.6349
epoch: 143, loss: 724.73, accuracy: 0.6353
epoch: 144, loss: 725.47, accuracy: 0.636
epoch: 145, loss: 724.78, accuracy: 0.6355
epoch: 146, loss: 722.04, accuracy: 0.6359
epoch: 147, loss: 723.84, accuracy: 0.635
epoch: 148, loss: 724.03, accuracy: 0.6348
epoch: 149, loss: 724.23, accuracy: 0.6357
time analysis:
    train 3491.1316 s
    all 3494.1183 s
Accuracy of     0 : 75 %
Accuracy of     1 : 74 %
Accuracy of     2 : 45 %
Accuracy of     3 : 42 %
Accuracy of     4 : 47 %
Accuracy of     5 : 50 %
Accuracy of     6 : 73 %
Accuracy of     7 : 60 %
Accuracy of     8 : 70 %
Accuracy of     9 : 67 %
```

## DAUConv2dZeroMu 2 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=2)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             448
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]           1,032
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,754
DAU params: 1,480
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1241.65, accuracy: 0.4641
epoch: 1, loss: 1125.5, accuracy: 0.505
epoch: 2, loss: 1091.54, accuracy: 0.4924
epoch: 3, loss: 1078.79, accuracy: 0.5082
epoch: 4, loss: 1069.81, accuracy: 0.5081
epoch: 5, loss: 1059.22, accuracy: 0.5086
epoch: 6, loss: 1049.77, accuracy: 0.4847
epoch: 7, loss: 1045.9, accuracy: 0.5248
epoch: 8, loss: 1039.64, accuracy: 0.5142
epoch: 9, loss: 1035.41, accuracy: 0.5035
epoch: 10, loss: 1027.08, accuracy: 0.5144
epoch: 11, loss: 1024.6, accuracy: 0.5193
epoch: 12, loss: 1022.3, accuracy: 0.5242
epoch: 13, loss: 1021.13, accuracy: 0.5291
epoch: 14, loss: 1015.36, accuracy: 0.5306
epoch: 15, loss: 1016.62, accuracy: 0.5239
epoch: 16, loss: 1015.4, accuracy: 0.5194
epoch: 17, loss: 1009.75, accuracy: 0.5345
epoch: 18, loss: 1012.61, accuracy: 0.5216
epoch: 19, loss: 1007.31, accuracy: 0.5177
epoch: 20, loss: 1005.37, accuracy: 0.5238
epoch: 21, loss: 1002.87, accuracy: 0.5273
epoch: 22, loss: 1006.95, accuracy: 0.5153
epoch: 23, loss: 1004.33, accuracy: 0.5306
epoch: 24, loss: 1004.03, accuracy: 0.5254
epoch: 25, loss: 1002.09, accuracy: 0.5307
epoch: 26, loss: 999.38, accuracy: 0.5228
epoch: 27, loss: 1000.67, accuracy: 0.5219
epoch: 28, loss: 996.43, accuracy: 0.5335
epoch: 29, loss: 999.56, accuracy: 0.5096
epoch: 30, loss: 999.87, accuracy: 0.5304
epoch: 31, loss: 996.67, accuracy: 0.5048
epoch: 32, loss: 997.77, accuracy: 0.5331
Epoch    33: reducing learning rate of group 0 to 5.0000e-03.
epoch: 33, loss: 995.69, accuracy: 0.5363
epoch: 34, loss: 965.88, accuracy: 0.5399
epoch: 35, loss: 963.11, accuracy: 0.5256
epoch: 36, loss: 961.93, accuracy: 0.5443
epoch: 37, loss: 962.0, accuracy: 0.537
epoch: 38, loss: 957.75, accuracy: 0.541
epoch: 39, loss: 959.1, accuracy: 0.5332
epoch: 40, loss: 956.91, accuracy: 0.5333
epoch: 41, loss: 959.09, accuracy: 0.5396
epoch: 42, loss: 958.1, accuracy: 0.5366
epoch: 43, loss: 956.74, accuracy: 0.5379
epoch: 44, loss: 956.68, accuracy: 0.5421
epoch: 45, loss: 957.73, accuracy: 0.5426
epoch: 46, loss: 956.08, accuracy: 0.5454
epoch: 47, loss: 958.66, accuracy: 0.5343
Epoch    48: reducing learning rate of group 0 to 2.5000e-03.
epoch: 48, loss: 957.12, accuracy: 0.539
epoch: 49, loss: 937.37, accuracy: 0.5474
epoch: 50, loss: 939.04, accuracy: 0.5456
epoch: 51, loss: 936.9, accuracy: 0.5503
epoch: 52, loss: 936.95, accuracy: 0.5449
epoch: 53, loss: 936.23, accuracy: 0.5423
epoch: 54, loss: 936.91, accuracy: 0.546
epoch: 55, loss: 937.54, accuracy: 0.549
epoch: 56, loss: 936.19, accuracy: 0.5458
epoch: 57, loss: 935.44, accuracy: 0.5477
epoch: 58, loss: 935.2, accuracy: 0.542
epoch: 59, loss: 935.53, accuracy: 0.5528
epoch: 60, loss: 936.84, accuracy: 0.546
epoch: 61, loss: 934.73, accuracy: 0.5458
epoch: 62, loss: 935.65, accuracy: 0.5408
Epoch    63: reducing learning rate of group 0 to 1.2500e-03.
epoch: 63, loss: 935.68, accuracy: 0.545
epoch: 64, loss: 925.79, accuracy: 0.5523
epoch: 65, loss: 925.03, accuracy: 0.5504
epoch: 66, loss: 924.01, accuracy: 0.5502
epoch: 67, loss: 924.86, accuracy: 0.5506
epoch: 68, loss: 923.78, accuracy: 0.5553
epoch: 69, loss: 924.55, accuracy: 0.5499
epoch: 70, loss: 924.58, accuracy: 0.5505
epoch: 71, loss: 923.54, accuracy: 0.5557
epoch: 72, loss: 923.11, accuracy: 0.5466
Epoch    73: reducing learning rate of group 0 to 6.2500e-04.
epoch: 73, loss: 923.92, accuracy: 0.552
epoch: 74, loss: 917.6, accuracy: 0.554
epoch: 75, loss: 918.69, accuracy: 0.5542
epoch: 76, loss: 916.98, accuracy: 0.5552
epoch: 77, loss: 918.27, accuracy: 0.5516
epoch: 78, loss: 917.62, accuracy: 0.5527
epoch: 79, loss: 918.6, accuracy: 0.5538
epoch: 80, loss: 913.22, accuracy: 0.5532
epoch: 81, loss: 912.75, accuracy: 0.5517
epoch: 82, loss: 912.56, accuracy: 0.5526
epoch: 83, loss: 913.17, accuracy: 0.554
epoch: 84, loss: 913.29, accuracy: 0.5538
epoch: 85, loss: 913.09, accuracy: 0.5516
epoch: 86, loss: 912.92, accuracy: 0.552
epoch: 87, loss: 913.41, accuracy: 0.5518
epoch: 88, loss: 913.58, accuracy: 0.5541
epoch: 89, loss: 912.96, accuracy: 0.5508
epoch: 90, loss: 912.78, accuracy: 0.5527
epoch: 91, loss: 914.03, accuracy: 0.5514
epoch: 92, loss: 912.0, accuracy: 0.5537
epoch: 93, loss: 912.99, accuracy: 0.5524
epoch: 94, loss: 912.39, accuracy: 0.5533
epoch: 95, loss: 911.84, accuracy: 0.5533
epoch: 96, loss: 912.5, accuracy: 0.5523
epoch: 97, loss: 913.73, accuracy: 0.5548
epoch: 98, loss: 913.24, accuracy: 0.5526
epoch: 99, loss: 912.83, accuracy: 0.5511
epoch: 100, loss: 913.82, accuracy: 0.5527
epoch: 101, loss: 912.1, accuracy: 0.5543
epoch: 102, loss: 912.14, accuracy: 0.5525
epoch: 103, loss: 912.72, accuracy: 0.553
epoch: 104, loss: 913.43, accuracy: 0.5512
epoch: 105, loss: 913.25, accuracy: 0.5514
epoch: 106, loss: 911.58, accuracy: 0.5513
epoch: 107, loss: 913.75, accuracy: 0.5528
epoch: 108, loss: 912.11, accuracy: 0.5535
epoch: 109, loss: 912.6, accuracy: 0.5523
epoch: 110, loss: 912.72, accuracy: 0.5545
epoch: 111, loss: 913.4, accuracy: 0.5524
epoch: 112, loss: 913.77, accuracy: 0.5534
epoch: 113, loss: 912.15, accuracy: 0.552
epoch: 114, loss: 911.69, accuracy: 0.5508
epoch: 115, loss: 912.78, accuracy: 0.5525
epoch: 116, loss: 911.85, accuracy: 0.5539
epoch: 117, loss: 912.18, accuracy: 0.5547
epoch: 118, loss: 912.28, accuracy: 0.5521
epoch: 119, loss: 912.57, accuracy: 0.5512
epoch: 120, loss: 912.7, accuracy: 0.5517
epoch: 121, loss: 912.18, accuracy: 0.5529
epoch: 122, loss: 912.78, accuracy: 0.5541
epoch: 123, loss: 913.45, accuracy: 0.5542
epoch: 124, loss: 912.38, accuracy: 0.554
epoch: 125, loss: 912.82, accuracy: 0.5547
epoch: 126, loss: 911.95, accuracy: 0.5542
epoch: 127, loss: 912.55, accuracy: 0.5534
epoch: 128, loss: 912.35, accuracy: 0.5518
epoch: 129, loss: 911.73, accuracy: 0.5545
epoch: 130, loss: 912.91, accuracy: 0.5539
epoch: 131, loss: 912.59, accuracy: 0.5532
epoch: 132, loss: 911.82, accuracy: 0.5504
epoch: 133, loss: 911.56, accuracy: 0.5528
epoch: 134, loss: 912.4, accuracy: 0.5522
epoch: 135, loss: 911.37, accuracy: 0.5519
epoch: 136, loss: 910.69, accuracy: 0.5511
epoch: 137, loss: 910.98, accuracy: 0.5521
epoch: 138, loss: 912.13, accuracy: 0.5532
epoch: 139, loss: 912.66, accuracy: 0.5529
epoch: 140, loss: 911.42, accuracy: 0.5537
epoch: 141, loss: 912.37, accuracy: 0.5533
epoch: 142, loss: 911.51, accuracy: 0.5544
epoch: 143, loss: 912.53, accuracy: 0.5525
epoch: 144, loss: 911.72, accuracy: 0.5539
epoch: 145, loss: 913.5, accuracy: 0.5535
epoch: 146, loss: 911.19, accuracy: 0.5544
epoch: 147, loss: 912.64, accuracy: 0.5539
epoch: 148, loss: 912.44, accuracy: 0.5529
epoch: 149, loss: 911.44, accuracy: 0.5532
time analysis:
    train 2371.9434 s
    all 2375.6795 s
Accuracy of     0 : 67 %
Accuracy of     1 : 62 %
Accuracy of     2 : 40 %
Accuracy of     3 : 43 %
Accuracy of     4 : 43 %
Accuracy of     5 : 47 %
Accuracy of     6 : 60 %
Accuracy of     7 : 57 %
Accuracy of     8 : 70 %
Accuracy of     9 : 58 %
```

## DAUConv2dZeroMu 3 units
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'CIFAR10', 'data_folder': '/home/marko/Desktop/hdd/data', 'architecture': 'small_fc'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=3)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             640
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]           1,544
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,458
DAU params: 2,184
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1244.99, accuracy: 0.4876
epoch: 1, loss: 1127.07, accuracy: 0.5184
epoch: 2, loss: 1096.75, accuracy: 0.5029
epoch: 3, loss: 1082.66, accuracy: 0.5027
epoch: 4, loss: 1073.55, accuracy: 0.5219
epoch: 5, loss: 1065.06, accuracy: 0.5264
epoch: 6, loss: 1056.55, accuracy: 0.5211
epoch: 7, loss: 1049.51, accuracy: 0.52
epoch: 8, loss: 1041.67, accuracy: 0.5288
epoch: 9, loss: 1039.26, accuracy: 0.5116
epoch: 10, loss: 1034.97, accuracy: 0.509
epoch: 11, loss: 1029.54, accuracy: 0.525
epoch: 12, loss: 1027.82, accuracy: 0.5212
epoch: 13, loss: 1024.78, accuracy: 0.5221
epoch: 14, loss: 1023.64, accuracy: 0.5337
epoch: 15, loss: 1021.72, accuracy: 0.5128
epoch: 16, loss: 1020.99, accuracy: 0.5158
epoch: 17, loss: 1021.33, accuracy: 0.4968
epoch: 18, loss: 1019.41, accuracy: 0.518
epoch: 19, loss: 1011.98, accuracy: 0.5167
epoch: 20, loss: 1012.1, accuracy: 0.5313
epoch: 21, loss: 1008.6, accuracy: 0.5015
epoch: 22, loss: 1012.47, accuracy: 0.5331
epoch: 23, loss: 1011.1, accuracy: 0.5292
epoch: 24, loss: 1005.66, accuracy: 0.5176
epoch: 25, loss: 1008.68, accuracy: 0.5174
epoch: 26, loss: 1009.56, accuracy: 0.5295
epoch: 27, loss: 1007.59, accuracy: 0.5291
epoch: 28, loss: 1008.2, accuracy: 0.5223
epoch: 29, loss: 1002.39, accuracy: 0.5316
epoch: 30, loss: 1004.73, accuracy: 0.5285
epoch: 31, loss: 1001.67, accuracy: 0.5268
epoch: 32, loss: 1004.31, accuracy: 0.5111
epoch: 33, loss: 1002.35, accuracy: 0.5157
Epoch    34: reducing learning rate of group 0 to 5.0000e-03.
epoch: 34, loss: 1003.23, accuracy: 0.5327
epoch: 35, loss: 970.82, accuracy: 0.5323
epoch: 36, loss: 971.91, accuracy: 0.5431
epoch: 37, loss: 968.8, accuracy: 0.5375
epoch: 38, loss: 966.93, accuracy: 0.5367
epoch: 39, loss: 966.89, accuracy: 0.5413
epoch: 40, loss: 967.68, accuracy: 0.5437
epoch: 41, loss: 966.64, accuracy: 0.541
epoch: 42, loss: 966.5, accuracy: 0.5307
epoch: 43, loss: 965.12, accuracy: 0.5427
epoch: 44, loss: 966.63, accuracy: 0.5384
epoch: 45, loss: 966.42, accuracy: 0.543
epoch: 46, loss: 967.21, accuracy: 0.5363
epoch: 47, loss: 965.64, accuracy: 0.538
Epoch    48: reducing learning rate of group 0 to 2.5000e-03.
epoch: 48, loss: 964.77, accuracy: 0.5384
epoch: 49, loss: 948.42, accuracy: 0.545
epoch: 50, loss: 946.83, accuracy: 0.5422
epoch: 51, loss: 948.96, accuracy: 0.5476
epoch: 52, loss: 947.36, accuracy: 0.5444
epoch: 53, loss: 948.21, accuracy: 0.5415
epoch: 54, loss: 946.16, accuracy: 0.5388
epoch: 55, loss: 946.45, accuracy: 0.5499
epoch: 56, loss: 945.41, accuracy: 0.5462
epoch: 57, loss: 946.78, accuracy: 0.5411
epoch: 58, loss: 946.0, accuracy: 0.5424
epoch: 59, loss: 947.27, accuracy: 0.5431
epoch: 60, loss: 944.36, accuracy: 0.5469
epoch: 61, loss: 945.34, accuracy: 0.543
epoch: 62, loss: 944.71, accuracy: 0.5459
epoch: 63, loss: 943.26, accuracy: 0.5421
epoch: 64, loss: 945.74, accuracy: 0.5443
epoch: 65, loss: 945.06, accuracy: 0.5499
epoch: 66, loss: 944.27, accuracy: 0.5437
epoch: 67, loss: 945.97, accuracy: 0.5436
Epoch    68: reducing learning rate of group 0 to 1.2500e-03.
epoch: 68, loss: 946.38, accuracy: 0.5448
epoch: 69, loss: 934.27, accuracy: 0.5505
epoch: 70, loss: 936.02, accuracy: 0.5468
epoch: 71, loss: 935.01, accuracy: 0.548
epoch: 72, loss: 935.11, accuracy: 0.5495
epoch: 73, loss: 932.9, accuracy: 0.549
epoch: 74, loss: 935.23, accuracy: 0.5471
epoch: 75, loss: 934.17, accuracy: 0.5467
epoch: 76, loss: 933.79, accuracy: 0.5451
epoch: 77, loss: 934.24, accuracy: 0.5478
Epoch    78: reducing learning rate of group 0 to 6.2500e-04.
epoch: 78, loss: 934.76, accuracy: 0.5498
epoch: 79, loss: 929.08, accuracy: 0.5481
epoch: 80, loss: 924.65, accuracy: 0.551
epoch: 81, loss: 924.92, accuracy: 0.5502
epoch: 82, loss: 924.34, accuracy: 0.5516
epoch: 83, loss: 925.23, accuracy: 0.5486
epoch: 84, loss: 924.34, accuracy: 0.55
epoch: 85, loss: 924.64, accuracy: 0.5508
epoch: 86, loss: 922.73, accuracy: 0.5514
epoch: 87, loss: 924.25, accuracy: 0.5516
epoch: 88, loss: 923.46, accuracy: 0.5515
epoch: 89, loss: 924.06, accuracy: 0.5498
epoch: 90, loss: 924.82, accuracy: 0.5509
epoch: 91, loss: 924.11, accuracy: 0.5511
epoch: 92, loss: 924.82, accuracy: 0.5494
epoch: 93, loss: 925.65, accuracy: 0.5512
epoch: 94, loss: 923.65, accuracy: 0.5523
epoch: 95, loss: 922.5, accuracy: 0.549
epoch: 96, loss: 923.11, accuracy: 0.5518
epoch: 97, loss: 924.19, accuracy: 0.552
epoch: 98, loss: 923.2, accuracy: 0.55
epoch: 99, loss: 925.1, accuracy: 0.5483
epoch: 100, loss: 924.81, accuracy: 0.5493
epoch: 101, loss: 924.24, accuracy: 0.5525
epoch: 102, loss: 926.12, accuracy: 0.5504
epoch: 103, loss: 924.13, accuracy: 0.5506
epoch: 104, loss: 924.69, accuracy: 0.5512
epoch: 105, loss: 923.22, accuracy: 0.5516
epoch: 106, loss: 924.85, accuracy: 0.5517
epoch: 107, loss: 923.96, accuracy: 0.5522
epoch: 108, loss: 923.77, accuracy: 0.5488
epoch: 109, loss: 922.64, accuracy: 0.5501
epoch: 110, loss: 925.01, accuracy: 0.5515
epoch: 111, loss: 924.25, accuracy: 0.5491
epoch: 112, loss: 923.94, accuracy: 0.5505
epoch: 113, loss: 923.04, accuracy: 0.5526
epoch: 114, loss: 924.11, accuracy: 0.5507
epoch: 115, loss: 924.5, accuracy: 0.5517
epoch: 116, loss: 925.05, accuracy: 0.5511
epoch: 117, loss: 924.34, accuracy: 0.5507
epoch: 118, loss: 924.14, accuracy: 0.5518
epoch: 119, loss: 924.42, accuracy: 0.551
epoch: 120, loss: 923.08, accuracy: 0.5517
