start: 2019-07-03 12:57:56.954922
* CIFAR10, small_fc, Conv2d
* CIFAR10, small_fc, DAUConv2d, units: 2
* CIFAR10, small_fc, DAUConv2di, units: 2
* CIFAR10, small_fc, DAUConv2di, units: 3
* CIFAR10, small_fc, DAUConv2di, units: 4
* CIFAR10, small_fc, DAUConv2dj, units: 2
* CIFAR10, small_fc, DAUConv2dj, units: 3
* CIFAR10, small_fc, DAUConv2dj, units: 4
* CIFAR10, small_fc, DAUConv2dOneMu, units: 2
* CIFAR10, small_fc, DAUConv2dOneMu, units: 3
* CIFAR10, small_fc, DAUConv2dOneMu, units: 4
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.1
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.2
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.3
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.4
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5

## CIFAR10, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(64, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
            Conv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 7,842
DAU params: 0
other params: 7,842
----------------------------------------------------------------
epoch: 0, loss: 1120.31, accuracy: 0.5011
epoch: 1, loss: 928.76, accuracy: 0.575
epoch: 2, loss: 868.98, accuracy: 0.6066
epoch: 3, loss: 832.68, accuracy: 0.6358
epoch: 4, loss: 811.48, accuracy: 0.6222
epoch: 5, loss: 792.0, accuracy: 0.6376
epoch: 6, loss: 780.81, accuracy: 0.6345
epoch: 7, loss: 766.86, accuracy: 0.649
epoch: 8, loss: 756.92, accuracy: 0.6394
epoch: 9, loss: 749.74, accuracy: 0.651
epoch: 10, loss: 744.32, accuracy: 0.6527
epoch: 11, loss: 738.32, accuracy: 0.6578
epoch: 12, loss: 728.78, accuracy: 0.6623
epoch: 13, loss: 727.8, accuracy: 0.6545
epoch: 14, loss: 723.51, accuracy: 0.6595
epoch: 15, loss: 717.13, accuracy: 0.6504
epoch: 16, loss: 716.26, accuracy: 0.6543
epoch: 17, loss: 707.7, accuracy: 0.6567
epoch: 18, loss: 706.69, accuracy: 0.6501
epoch: 19, loss: 701.37, accuracy: 0.654
epoch: 20, loss: 696.48, accuracy: 0.6488
epoch: 21, loss: 697.09, accuracy: 0.6644
epoch: 22, loss: 692.3, accuracy: 0.6688
epoch: 23, loss: 689.91, accuracy: 0.6623
epoch: 24, loss: 689.07, accuracy: 0.6576
epoch: 25, loss: 684.54, accuracy: 0.6505
epoch: 26, loss: 683.72, accuracy: 0.6676
epoch: 27, loss: 678.32, accuracy: 0.6638
epoch: 28, loss: 681.3, accuracy: 0.6643
epoch: 29, loss: 675.02, accuracy: 0.6639
epoch: 30, loss: 675.05, accuracy: 0.6696
epoch: 31, loss: 670.52, accuracy: 0.6766
epoch: 32, loss: 670.48, accuracy: 0.6672
epoch: 33, loss: 670.21, accuracy: 0.6692
epoch: 34, loss: 668.7, accuracy: 0.6722
epoch: 35, loss: 665.9, accuracy: 0.6747
epoch: 36, loss: 666.8, accuracy: 0.6677
epoch: 37, loss: 663.04, accuracy: 0.6656
epoch: 38, loss: 662.8, accuracy: 0.6661
epoch: 39, loss: 657.83, accuracy: 0.6767
epoch: 40, loss: 659.71, accuracy: 0.6676
epoch: 41, loss: 657.77, accuracy: 0.6728
epoch: 42, loss: 654.49, accuracy: 0.6763
epoch: 43, loss: 654.82, accuracy: 0.6679
epoch: 44, loss: 654.05, accuracy: 0.6687
epoch: 45, loss: 651.48, accuracy: 0.6788
epoch: 46, loss: 653.61, accuracy: 0.6678
epoch: 47, loss: 653.4, accuracy: 0.6738
epoch: 48, loss: 650.87, accuracy: 0.6716
epoch: 49, loss: 646.76, accuracy: 0.6698
epoch: 50, loss: 647.7, accuracy: 0.6734
epoch: 51, loss: 650.06, accuracy: 0.6785
epoch: 52, loss: 644.51, accuracy: 0.663
epoch: 53, loss: 643.36, accuracy: 0.6568
epoch: 54, loss: 643.29, accuracy: 0.6699
epoch: 55, loss: 641.75, accuracy: 0.667
epoch: 56, loss: 641.97, accuracy: 0.6782
epoch: 57, loss: 638.8, accuracy: 0.6724
epoch: 58, loss: 640.94, accuracy: 0.6744
epoch: 59, loss: 640.02, accuracy: 0.6765
epoch: 60, loss: 639.27, accuracy: 0.6729
epoch: 61, loss: 637.43, accuracy: 0.6469
epoch: 62, loss: 637.52, accuracy: 0.6519
epoch: 63, loss: 637.15, accuracy: 0.6749
epoch: 64, loss: 635.21, accuracy: 0.6775
epoch: 65, loss: 636.84, accuracy: 0.6728
epoch: 66, loss: 633.88, accuracy: 0.6742
epoch: 67, loss: 633.36, accuracy: 0.676
epoch: 68, loss: 632.45, accuracy: 0.6759
epoch: 69, loss: 633.1, accuracy: 0.6643
epoch: 70, loss: 593.72, accuracy: 0.6848
epoch: 71, loss: 588.57, accuracy: 0.6858
epoch: 72, loss: 587.16, accuracy: 0.6838
epoch: 73, loss: 587.3, accuracy: 0.6863
epoch: 74, loss: 585.82, accuracy: 0.6853
epoch: 75, loss: 584.55, accuracy: 0.6876
epoch: 76, loss: 584.16, accuracy: 0.6868
epoch: 77, loss: 583.08, accuracy: 0.6861
epoch: 78, loss: 583.62, accuracy: 0.6851
epoch: 79, loss: 585.56, accuracy: 0.684
epoch: 80, loss: 584.21, accuracy: 0.6849
epoch: 81, loss: 582.01, accuracy: 0.6885
epoch: 82, loss: 582.52, accuracy: 0.6855
epoch: 83, loss: 582.09, accuracy: 0.6868
epoch: 84, loss: 582.1, accuracy: 0.6868
epoch: 85, loss: 580.9, accuracy: 0.6876
epoch: 86, loss: 581.96, accuracy: 0.682
epoch: 87, loss: 580.89, accuracy: 0.6879
epoch: 88, loss: 581.24, accuracy: 0.6873
epoch: 89, loss: 581.18, accuracy: 0.6872
epoch: 90, loss: 579.03, accuracy: 0.6849
epoch: 91, loss: 579.31, accuracy: 0.6866
epoch: 92, loss: 580.67, accuracy: 0.6856
epoch: 93, loss: 581.73, accuracy: 0.6808
epoch: 94, loss: 578.75, accuracy: 0.6877
epoch: 95, loss: 570.66, accuracy: 0.6898
epoch: 96, loss: 569.73, accuracy: 0.6882
epoch: 97, loss: 568.85, accuracy: 0.6879
epoch: 98, loss: 568.2, accuracy: 0.689
epoch: 99, loss: 568.51, accuracy: 0.6864
time analysis:
    train 535.6421 s
Accuracy of     0 : 62 %
Accuracy of     1 : 90 %
Accuracy of     2 : 58 %
Accuracy of     3 : 46 %
Accuracy of     4 : 63 %
Accuracy of     5 : 59 %
Accuracy of     6 : 76 %
Accuracy of     7 : 65 %
Accuracy of     8 : 72 %
Accuracy of     9 : 75 %
```

## CIFAR10, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,216
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           3,080
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 5,730
DAU params: 4,296
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1195.67, accuracy: 0.5176
epoch: 1, loss: 1006.66, accuracy: 0.5667
epoch: 2, loss: 947.2, accuracy: 0.5738
epoch: 3, loss: 907.59, accuracy: 0.5709
epoch: 4, loss: 878.37, accuracy: 0.62
epoch: 5, loss: 855.78, accuracy: 0.618
epoch: 6, loss: 837.52, accuracy: 0.6166
epoch: 7, loss: 826.12, accuracy: 0.633
epoch: 8, loss: 814.16, accuracy: 0.6196
epoch: 9, loss: 800.54, accuracy: 0.6379
epoch: 10, loss: 796.0, accuracy: 0.628
epoch: 11, loss: 784.77, accuracy: 0.6378
epoch: 12, loss: 779.32, accuracy: 0.6291
epoch: 13, loss: 774.4, accuracy: 0.6362
epoch: 14, loss: 765.39, accuracy: 0.6514
epoch: 15, loss: 763.27, accuracy: 0.6517
epoch: 16, loss: 757.65, accuracy: 0.6514
epoch: 17, loss: 752.94, accuracy: 0.6578
epoch: 18, loss: 749.8, accuracy: 0.6597
epoch: 19, loss: 746.34, accuracy: 0.6548
epoch: 20, loss: 740.3, accuracy: 0.6547
epoch: 21, loss: 739.28, accuracy: 0.6502
epoch: 22, loss: 733.75, accuracy: 0.6662
epoch: 23, loss: 735.06, accuracy: 0.646
epoch: 24, loss: 729.93, accuracy: 0.6399
epoch: 25, loss: 728.04, accuracy: 0.6677
epoch: 26, loss: 724.63, accuracy: 0.6563
epoch: 27, loss: 721.46, accuracy: 0.6727
epoch: 28, loss: 720.46, accuracy: 0.6665
epoch: 29, loss: 717.23, accuracy: 0.6697
epoch: 30, loss: 714.75, accuracy: 0.6652
epoch: 31, loss: 711.69, accuracy: 0.671
epoch: 32, loss: 711.98, accuracy: 0.6635
epoch: 33, loss: 709.86, accuracy: 0.6693
epoch: 34, loss: 710.39, accuracy: 0.668
epoch: 35, loss: 706.89, accuracy: 0.6621
epoch: 36, loss: 704.95, accuracy: 0.6779
epoch: 37, loss: 705.91, accuracy: 0.6649
epoch: 38, loss: 704.24, accuracy: 0.6713
epoch: 39, loss: 703.74, accuracy: 0.665
epoch: 40, loss: 701.85, accuracy: 0.672
epoch: 41, loss: 696.11, accuracy: 0.6734
epoch: 42, loss: 699.04, accuracy: 0.6739
epoch: 43, loss: 692.92, accuracy: 0.6659
epoch: 44, loss: 698.29, accuracy: 0.6694
epoch: 45, loss: 692.68, accuracy: 0.6701
epoch: 46, loss: 691.99, accuracy: 0.6647
epoch: 47, loss: 690.87, accuracy: 0.6614
epoch: 48, loss: 690.35, accuracy: 0.6732
epoch: 49, loss: 688.35, accuracy: 0.675
epoch: 50, loss: 690.85, accuracy: 0.6689
epoch: 51, loss: 688.62, accuracy: 0.6795
epoch: 52, loss: 684.82, accuracy: 0.6766
epoch: 53, loss: 684.14, accuracy: 0.6734
epoch: 54, loss: 683.57, accuracy: 0.678
epoch: 55, loss: 680.9, accuracy: 0.6716
epoch: 56, loss: 683.71, accuracy: 0.6711
epoch: 57, loss: 679.82, accuracy: 0.6789
epoch: 58, loss: 679.37, accuracy: 0.6727
epoch: 59, loss: 678.95, accuracy: 0.6744
epoch: 60, loss: 678.78, accuracy: 0.674
epoch: 61, loss: 676.41, accuracy: 0.6819
epoch: 62, loss: 678.41, accuracy: 0.659
epoch: 63, loss: 677.67, accuracy: 0.6733
epoch: 64, loss: 676.0, accuracy: 0.6763
epoch: 65, loss: 676.17, accuracy: 0.6763
epoch: 66, loss: 675.52, accuracy: 0.6758
epoch: 67, loss: 675.34, accuracy: 0.6561
epoch: 68, loss: 669.06, accuracy: 0.6654
epoch: 69, loss: 673.04, accuracy: 0.6696
epoch: 70, loss: 633.52, accuracy: 0.6857
epoch: 71, loss: 627.91, accuracy: 0.6888
epoch: 72, loss: 626.44, accuracy: 0.6875
epoch: 73, loss: 625.96, accuracy: 0.6915
epoch: 74, loss: 624.86, accuracy: 0.6887
epoch: 75, loss: 625.76, accuracy: 0.6878
epoch: 76, loss: 624.4, accuracy: 0.6853
epoch: 77, loss: 623.78, accuracy: 0.6859
epoch: 78, loss: 622.94, accuracy: 0.6913
epoch: 79, loss: 622.78, accuracy: 0.6873
epoch: 80, loss: 622.33, accuracy: 0.6883
epoch: 81, loss: 621.19, accuracy: 0.6891
epoch: 82, loss: 621.13, accuracy: 0.6893
epoch: 83, loss: 622.54, accuracy: 0.6875
epoch: 84, loss: 622.65, accuracy: 0.6888
epoch: 85, loss: 621.18, accuracy: 0.6893
epoch: 86, loss: 621.5, accuracy: 0.6858
Epoch    87: reducing learning rate of group 0 to 5.0000e-04.
epoch: 87, loss: 622.33, accuracy: 0.6932
epoch: 88, loss: 614.92, accuracy: 0.6862
epoch: 89, loss: 613.16, accuracy: 0.6874
epoch: 90, loss: 612.21, accuracy: 0.6911
epoch: 91, loss: 611.68, accuracy: 0.6908
epoch: 92, loss: 613.46, accuracy: 0.691
epoch: 93, loss: 613.93, accuracy: 0.691
epoch: 94, loss: 612.49, accuracy: 0.6915
epoch: 95, loss: 605.93, accuracy: 0.6926
epoch: 96, loss: 607.59, accuracy: 0.6918
epoch: 97, loss: 606.29, accuracy: 0.6918
epoch: 98, loss: 606.2, accuracy: 0.6911
epoch: 99, loss: 606.08, accuracy: 0.6907
time analysis:
    train 7626.4545 s
    all 7628.2277 s
Accuracy of     0 : 73 %
Accuracy of     1 : 88 %
Accuracy of     2 : 51 %
Accuracy of     3 : 46 %
Accuracy of     4 : 52 %
Accuracy of     5 : 59 %
Accuracy of     6 : 75 %
Accuracy of     7 : 67 %
Accuracy of     8 : 82 %
Accuracy of     9 : 70 %
```

## CIFAR10, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             460
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,288
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 3,182
DAU params: 1,748
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1228.4, accuracy: 0.492
epoch: 1, loss: 1062.36, accuracy: 0.5186
epoch: 2, loss: 1017.48, accuracy: 0.5214
epoch: 3, loss: 984.63, accuracy: 0.5538
epoch: 4, loss: 957.49, accuracy: 0.5742
epoch: 5, loss: 932.28, accuracy: 0.5797
epoch: 6, loss: 915.03, accuracy: 0.5895
epoch: 7, loss: 900.88, accuracy: 0.5614
epoch: 8, loss: 892.42, accuracy: 0.5926
epoch: 9, loss: 881.86, accuracy: 0.5957
epoch: 10, loss: 876.44, accuracy: 0.605
epoch: 11, loss: 869.06, accuracy: 0.6048
epoch: 12, loss: 863.0, accuracy: 0.6114
epoch: 13, loss: 857.75, accuracy: 0.6018
epoch: 14, loss: 853.65, accuracy: 0.6087
epoch: 15, loss: 845.38, accuracy: 0.6083
epoch: 16, loss: 845.35, accuracy: 0.6154
epoch: 17, loss: 838.33, accuracy: 0.6245
epoch: 18, loss: 835.56, accuracy: 0.6215
epoch: 19, loss: 831.38, accuracy: 0.6231
epoch: 20, loss: 826.48, accuracy: 0.6249
epoch: 21, loss: 826.91, accuracy: 0.6219
epoch: 22, loss: 820.35, accuracy: 0.623
epoch: 23, loss: 817.59, accuracy: 0.6195
epoch: 24, loss: 817.13, accuracy: 0.6223
epoch: 25, loss: 813.35, accuracy: 0.6161
epoch: 26, loss: 809.72, accuracy: 0.6276
epoch: 27, loss: 812.3, accuracy: 0.6286
epoch: 28, loss: 808.9, accuracy: 0.6276
epoch: 29, loss: 807.84, accuracy: 0.6316
epoch: 30, loss: 804.25, accuracy: 0.6179
epoch: 31, loss: 804.45, accuracy: 0.6352
epoch: 32, loss: 805.17, accuracy: 0.635
epoch: 33, loss: 802.23, accuracy: 0.6326
epoch: 34, loss: 799.91, accuracy: 0.6214
epoch: 35, loss: 798.48, accuracy: 0.6231
epoch: 36, loss: 797.36, accuracy: 0.6096
epoch: 37, loss: 795.92, accuracy: 0.6405
epoch: 38, loss: 794.27, accuracy: 0.637
epoch: 39, loss: 794.32, accuracy: 0.6335
epoch: 40, loss: 792.22, accuracy: 0.6328
epoch: 41, loss: 793.55, accuracy: 0.6301
epoch: 42, loss: 785.9, accuracy: 0.6349
epoch: 43, loss: 789.97, accuracy: 0.6276
epoch: 44, loss: 790.43, accuracy: 0.6352
epoch: 45, loss: 786.64, accuracy: 0.6226
epoch: 46, loss: 787.58, accuracy: 0.6346
epoch: 47, loss: 784.69, accuracy: 0.6427
epoch: 48, loss: 785.41, accuracy: 0.636
epoch: 49, loss: 782.78, accuracy: 0.6366
epoch: 50, loss: 783.89, accuracy: 0.6269
epoch: 51, loss: 781.17, accuracy: 0.6395
epoch: 52, loss: 780.95, accuracy: 0.6472
epoch: 53, loss: 780.16, accuracy: 0.6409
epoch: 54, loss: 780.54, accuracy: 0.635
epoch: 55, loss: 779.03, accuracy: 0.6089
epoch: 56, loss: 779.74, accuracy: 0.6447
epoch: 57, loss: 776.47, accuracy: 0.6387
epoch: 58, loss: 777.06, accuracy: 0.6457
epoch: 59, loss: 775.47, accuracy: 0.6274
epoch: 60, loss: 774.94, accuracy: 0.6248
epoch: 61, loss: 774.18, accuracy: 0.6435
epoch: 62, loss: 774.95, accuracy: 0.644
epoch: 63, loss: 773.1, accuracy: 0.6414
epoch: 64, loss: 771.9, accuracy: 0.6433
epoch: 65, loss: 770.85, accuracy: 0.6287
epoch: 66, loss: 770.31, accuracy: 0.6368
epoch: 67, loss: 770.64, accuracy: 0.6347
epoch: 68, loss: 771.77, accuracy: 0.6482
epoch: 69, loss: 768.31, accuracy: 0.6267
epoch: 70, loss: 734.91, accuracy: 0.654
epoch: 71, loss: 732.55, accuracy: 0.6588
epoch: 72, loss: 729.89, accuracy: 0.661
epoch: 73, loss: 729.0, accuracy: 0.6584
epoch: 74, loss: 730.61, accuracy: 0.6514
epoch: 75, loss: 728.61, accuracy: 0.657
epoch: 76, loss: 728.33, accuracy: 0.6563
epoch: 77, loss: 728.24, accuracy: 0.6569
epoch: 78, loss: 729.52, accuracy: 0.6586
epoch: 79, loss: 726.89, accuracy: 0.6584
epoch: 80, loss: 727.33, accuracy: 0.654
epoch: 81, loss: 727.72, accuracy: 0.6574
epoch: 82, loss: 725.34, accuracy: 0.6554
epoch: 83, loss: 724.91, accuracy: 0.6514
epoch: 84, loss: 726.86, accuracy: 0.6582
epoch: 85, loss: 727.39, accuracy: 0.6556
epoch: 86, loss: 725.82, accuracy: 0.6564
epoch: 87, loss: 724.91, accuracy: 0.6543
Epoch    88: reducing learning rate of group 0 to 5.0000e-04.
epoch: 88, loss: 725.47, accuracy: 0.6554
epoch: 89, loss: 719.66, accuracy: 0.6575
epoch: 90, loss: 720.24, accuracy: 0.6573
epoch: 91, loss: 719.09, accuracy: 0.6571
epoch: 92, loss: 719.1, accuracy: 0.6577
epoch: 93, loss: 719.54, accuracy: 0.6568
epoch: 94, loss: 718.59, accuracy: 0.657
epoch: 95, loss: 713.76, accuracy: 0.6589
epoch: 96, loss: 713.33, accuracy: 0.6558
epoch: 97, loss: 713.78, accuracy: 0.6582
epoch: 98, loss: 713.9, accuracy: 0.6584
epoch: 99, loss: 713.81, accuracy: 0.6595
time analysis:
    train 1827.8991 s
    all 1835.7099 s
Accuracy of     0 : 64 %
Accuracy of     1 : 84 %
Accuracy of     2 : 54 %
Accuracy of     3 : 45 %
Accuracy of     4 : 54 %
Accuracy of     5 : 55 %
Accuracy of     6 : 73 %
Accuracy of     7 : 65 %
Accuracy of     8 : 79 %
Accuracy of     9 : 66 %
```

## CIFAR10, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             658
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,928
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,020
DAU params: 2,586
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1243.3, accuracy: 0.4949
epoch: 1, loss: 1075.4, accuracy: 0.5285
epoch: 2, loss: 1008.88, accuracy: 0.5522
epoch: 3, loss: 972.32, accuracy: 0.5438
epoch: 4, loss: 946.99, accuracy: 0.5621
epoch: 5, loss: 922.0, accuracy: 0.5428
epoch: 6, loss: 910.05, accuracy: 0.5802
epoch: 7, loss: 892.79, accuracy: 0.5962
epoch: 8, loss: 881.31, accuracy: 0.5868
epoch: 9, loss: 873.1, accuracy: 0.6015
epoch: 10, loss: 865.85, accuracy: 0.6042
epoch: 11, loss: 853.4, accuracy: 0.6089
epoch: 12, loss: 846.37, accuracy: 0.6079
epoch: 13, loss: 840.0, accuracy: 0.5961
epoch: 14, loss: 833.5, accuracy: 0.6113
epoch: 15, loss: 825.84, accuracy: 0.6016
epoch: 16, loss: 824.61, accuracy: 0.6223
epoch: 17, loss: 817.99, accuracy: 0.6175
epoch: 18, loss: 811.68, accuracy: 0.6241
epoch: 19, loss: 806.59, accuracy: 0.6257
epoch: 20, loss: 805.41, accuracy: 0.6197
epoch: 21, loss: 799.91, accuracy: 0.6096
epoch: 22, loss: 798.09, accuracy: 0.6343
epoch: 23, loss: 795.26, accuracy: 0.6115
epoch: 24, loss: 790.18, accuracy: 0.625
epoch: 25, loss: 786.87, accuracy: 0.6267
epoch: 26, loss: 785.6, accuracy: 0.6409
epoch: 27, loss: 781.23, accuracy: 0.6304
epoch: 28, loss: 777.73, accuracy: 0.6316
epoch: 29, loss: 777.69, accuracy: 0.6269
epoch: 30, loss: 774.68, accuracy: 0.6325
epoch: 31, loss: 770.07, accuracy: 0.6355
epoch: 32, loss: 767.57, accuracy: 0.6246
epoch: 33, loss: 765.73, accuracy: 0.629
epoch: 34, loss: 765.73, accuracy: 0.6376
epoch: 35, loss: 763.52, accuracy: 0.6438
epoch: 36, loss: 762.34, accuracy: 0.635
epoch: 37, loss: 760.89, accuracy: 0.6433
epoch: 38, loss: 759.87, accuracy: 0.6217
epoch: 39, loss: 757.33, accuracy: 0.6402
epoch: 40, loss: 757.91, accuracy: 0.651
epoch: 41, loss: 753.45, accuracy: 0.6517
epoch: 42, loss: 756.18, accuracy: 0.6444
epoch: 43, loss: 752.24, accuracy: 0.6482
epoch: 44, loss: 748.24, accuracy: 0.6449
epoch: 45, loss: 749.58, accuracy: 0.6463
epoch: 46, loss: 749.67, accuracy: 0.6525
epoch: 47, loss: 748.37, accuracy: 0.6516
epoch: 48, loss: 746.43, accuracy: 0.6534
epoch: 49, loss: 741.54, accuracy: 0.6541
epoch: 50, loss: 742.99, accuracy: 0.6549
epoch: 51, loss: 741.9, accuracy: 0.6285
epoch: 52, loss: 741.51, accuracy: 0.6491
epoch: 53, loss: 739.0, accuracy: 0.6535
epoch: 54, loss: 738.97, accuracy: 0.6516
epoch: 55, loss: 734.84, accuracy: 0.6482
epoch: 56, loss: 733.02, accuracy: 0.6512
epoch: 57, loss: 734.98, accuracy: 0.6503
epoch: 58, loss: 732.43, accuracy: 0.6583
epoch: 59, loss: 733.98, accuracy: 0.6444
epoch: 60, loss: 733.62, accuracy: 0.6625
epoch: 61, loss: 728.87, accuracy: 0.6583
epoch: 62, loss: 733.2, accuracy: 0.6591
epoch: 63, loss: 730.11, accuracy: 0.6633
epoch: 64, loss: 731.2, accuracy: 0.6308
epoch: 65, loss: 729.63, accuracy: 0.6594
epoch: 66, loss: 726.59, accuracy: 0.6398
epoch: 67, loss: 728.19, accuracy: 0.6649
epoch: 68, loss: 725.46, accuracy: 0.6575
epoch: 69, loss: 725.02, accuracy: 0.639
epoch: 70, loss: 690.19, accuracy: 0.6691
epoch: 71, loss: 685.65, accuracy: 0.6738
epoch: 72, loss: 683.51, accuracy: 0.6684
epoch: 73, loss: 682.35, accuracy: 0.6773
epoch: 74, loss: 682.16, accuracy: 0.6726
epoch: 75, loss: 681.5, accuracy: 0.6741
epoch: 76, loss: 681.08, accuracy: 0.672
epoch: 77, loss: 682.43, accuracy: 0.6782
epoch: 78, loss: 678.85, accuracy: 0.6721
epoch: 79, loss: 681.0, accuracy: 0.674
epoch: 80, loss: 679.77, accuracy: 0.6747
epoch: 81, loss: 678.63, accuracy: 0.6732
epoch: 82, loss: 680.66, accuracy: 0.6755
epoch: 83, loss: 678.75, accuracy: 0.6721
Epoch    84: reducing learning rate of group 0 to 5.0000e-04.
epoch: 84, loss: 679.53, accuracy: 0.6765
epoch: 85, loss: 673.76, accuracy: 0.6784
epoch: 86, loss: 673.37, accuracy: 0.6762
epoch: 87, loss: 672.94, accuracy: 0.6762
epoch: 88, loss: 673.23, accuracy: 0.6784
epoch: 89, loss: 672.01, accuracy: 0.6772
epoch: 90, loss: 670.33, accuracy: 0.6767
epoch: 91, loss: 672.66, accuracy: 0.6782
epoch: 92, loss: 673.03, accuracy: 0.6781
epoch: 93, loss: 671.43, accuracy: 0.6797
epoch: 94, loss: 672.69, accuracy: 0.6763
epoch: 95, loss: 666.62, accuracy: 0.6785
epoch: 96, loss: 666.48, accuracy: 0.6799
epoch: 97, loss: 665.46, accuracy: 0.6812
epoch: 98, loss: 666.6, accuracy: 0.6763
epoch: 99, loss: 666.71, accuracy: 0.679
time analysis:
    train 2199.0308 s
    all 2201.6477 s
Accuracy of     0 : 67 %
Accuracy of     1 : 76 %
Accuracy of     2 : 50 %
Accuracy of     3 : 41 %
Accuracy of     4 : 58 %
Accuracy of     5 : 59 %
Accuracy of     6 : 75 %
Accuracy of     7 : 62 %
Accuracy of     8 : 75 %
Accuracy of     9 : 73 %
```

## CIFAR10, small_fc, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             856
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           2,568
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,858
DAU params: 3,424
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1242.5, accuracy: 0.4856
epoch: 1, loss: 1079.35, accuracy: 0.5123
epoch: 2, loss: 1019.27, accuracy: 0.5463
epoch: 3, loss: 976.51, accuracy: 0.5585
epoch: 4, loss: 944.92, accuracy: 0.5666
epoch: 5, loss: 927.2, accuracy: 0.5784
epoch: 6, loss: 904.59, accuracy: 0.5861
epoch: 7, loss: 891.25, accuracy: 0.6006
epoch: 8, loss: 877.32, accuracy: 0.5997
epoch: 9, loss: 866.69, accuracy: 0.6023
epoch: 10, loss: 859.17, accuracy: 0.583
epoch: 11, loss: 849.2, accuracy: 0.5898
epoch: 12, loss: 842.56, accuracy: 0.6162
epoch: 13, loss: 833.9, accuracy: 0.6234
epoch: 14, loss: 830.73, accuracy: 0.6184
epoch: 15, loss: 821.69, accuracy: 0.6333
epoch: 16, loss: 819.54, accuracy: 0.6146
epoch: 17, loss: 815.75, accuracy: 0.6336
epoch: 18, loss: 808.1, accuracy: 0.632
epoch: 19, loss: 806.43, accuracy: 0.6244
epoch: 20, loss: 800.77, accuracy: 0.6155
epoch: 21, loss: 799.27, accuracy: 0.6169
epoch: 22, loss: 796.58, accuracy: 0.6301
epoch: 23, loss: 792.6, accuracy: 0.6163
epoch: 24, loss: 790.18, accuracy: 0.6341
epoch: 25, loss: 788.56, accuracy: 0.6299
epoch: 26, loss: 782.71, accuracy: 0.6421
epoch: 27, loss: 782.72, accuracy: 0.6419
epoch: 28, loss: 780.99, accuracy: 0.6477
epoch: 29, loss: 773.52, accuracy: 0.6392
epoch: 30, loss: 774.41, accuracy: 0.6376
epoch: 31, loss: 769.58, accuracy: 0.6465
epoch: 32, loss: 770.42, accuracy: 0.6428
epoch: 33, loss: 766.1, accuracy: 0.6523
epoch: 34, loss: 763.23, accuracy: 0.6549
epoch: 35, loss: 762.75, accuracy: 0.6374
epoch: 36, loss: 760.96, accuracy: 0.6397
epoch: 37, loss: 758.31, accuracy: 0.6447
epoch: 38, loss: 755.47, accuracy: 0.6523
epoch: 39, loss: 752.25, accuracy: 0.6573
epoch: 40, loss: 754.26, accuracy: 0.6531
epoch: 41, loss: 747.78, accuracy: 0.6526
epoch: 42, loss: 748.2, accuracy: 0.6526
epoch: 43, loss: 744.62, accuracy: 0.6533
epoch: 44, loss: 747.07, accuracy: 0.6518
epoch: 45, loss: 741.6, accuracy: 0.6462
epoch: 46, loss: 741.53, accuracy: 0.6588
epoch: 47, loss: 739.56, accuracy: 0.6548
epoch: 48, loss: 739.25, accuracy: 0.6577
epoch: 49, loss: 736.28, accuracy: 0.6579
epoch: 50, loss: 734.92, accuracy: 0.6596
epoch: 51, loss: 732.45, accuracy: 0.6459
epoch: 52, loss: 733.37, accuracy: 0.6511
epoch: 53, loss: 730.92, accuracy: 0.6566
epoch: 54, loss: 728.3, accuracy: 0.6441
epoch: 55, loss: 729.19, accuracy: 0.6349
epoch: 56, loss: 726.7, accuracy: 0.6465
epoch: 57, loss: 727.56, accuracy: 0.6492
epoch: 58, loss: 723.65, accuracy: 0.6559
epoch: 59, loss: 722.02, accuracy: 0.6597
epoch: 60, loss: 720.32, accuracy: 0.6614
epoch: 61, loss: 721.44, accuracy: 0.6487
epoch: 62, loss: 720.75, accuracy: 0.6562
epoch: 63, loss: 715.66, accuracy: 0.667
epoch: 64, loss: 716.72, accuracy: 0.6564
epoch: 65, loss: 717.5, accuracy: 0.6669
epoch: 66, loss: 715.96, accuracy: 0.6651
epoch: 67, loss: 715.39, accuracy: 0.6582
epoch: 68, loss: 714.19, accuracy: 0.6602
epoch: 69, loss: 711.59, accuracy: 0.6586
epoch: 70, loss: 676.38, accuracy: 0.6759
epoch: 71, loss: 671.15, accuracy: 0.6753
epoch: 72, loss: 671.61, accuracy: 0.6795
epoch: 73, loss: 671.53, accuracy: 0.6775
epoch: 74, loss: 669.39, accuracy: 0.6794
epoch: 75, loss: 669.0, accuracy: 0.6779
epoch: 76, loss: 670.13, accuracy: 0.6766
epoch: 77, loss: 668.04, accuracy: 0.6822
epoch: 78, loss: 669.01, accuracy: 0.6797
epoch: 79, loss: 668.15, accuracy: 0.6747
epoch: 80, loss: 666.2, accuracy: 0.6807
epoch: 81, loss: 666.25, accuracy: 0.6827
epoch: 82, loss: 665.98, accuracy: 0.6809
epoch: 83, loss: 665.95, accuracy: 0.6757
epoch: 84, loss: 664.96, accuracy: 0.6803
epoch: 85, loss: 665.02, accuracy: 0.6799
epoch: 86, loss: 666.25, accuracy: 0.6811
epoch: 87, loss: 664.06, accuracy: 0.6806
epoch: 88, loss: 663.59, accuracy: 0.6796
epoch: 89, loss: 664.11, accuracy: 0.6771
epoch: 90, loss: 663.62, accuracy: 0.6796
epoch: 91, loss: 664.63, accuracy: 0.6808
epoch: 92, loss: 665.69, accuracy: 0.6803
Epoch    93: reducing learning rate of group 0 to 5.0000e-04.
epoch: 93, loss: 663.91, accuracy: 0.6758
epoch: 94, loss: 657.01, accuracy: 0.6814
epoch: 95, loss: 652.08, accuracy: 0.6833
epoch: 96, loss: 652.09, accuracy: 0.6841
epoch: 97, loss: 651.14, accuracy: 0.6851
epoch: 98, loss: 650.8, accuracy: 0.684
epoch: 99, loss: 652.32, accuracy: 0.6845
time analysis:
    train 2588.7841 s
    all 2591.6763 s
Accuracy of     0 : 69 %
Accuracy of     1 : 84 %
Accuracy of     2 : 50 %
Accuracy of     3 : 31 %
Accuracy of     4 : 67 %
Accuracy of     5 : 57 %
Accuracy of     6 : 71 %
Accuracy of     7 : 64 %
Accuracy of     8 : 81 %
Accuracy of     9 : 74 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]             704
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 3,202
DAU params: 1,768
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1222.68, accuracy: 0.4978
epoch: 1, loss: 1063.79, accuracy: 0.5402
epoch: 2, loss: 999.46, accuracy: 0.5463
epoch: 3, loss: 952.96, accuracy: 0.5688
epoch: 4, loss: 923.21, accuracy: 0.5914
epoch: 5, loss: 895.99, accuracy: 0.6005
epoch: 6, loss: 884.59, accuracy: 0.606
epoch: 7, loss: 870.91, accuracy: 0.6009
epoch: 8, loss: 858.09, accuracy: 0.6143
epoch: 9, loss: 850.59, accuracy: 0.6081
epoch: 10, loss: 845.56, accuracy: 0.6071
epoch: 11, loss: 842.86, accuracy: 0.6251
epoch: 12, loss: 829.7, accuracy: 0.5846
epoch: 13, loss: 825.93, accuracy: 0.6265
epoch: 14, loss: 818.84, accuracy: 0.6308
epoch: 15, loss: 815.56, accuracy: 0.633
epoch: 16, loss: 808.21, accuracy: 0.6385
epoch: 17, loss: 803.25, accuracy: 0.6239
epoch: 18, loss: 802.18, accuracy: 0.6311
epoch: 19, loss: 798.98, accuracy: 0.6351
epoch: 20, loss: 792.51, accuracy: 0.6225
epoch: 21, loss: 792.61, accuracy: 0.6254
epoch: 22, loss: 788.67, accuracy: 0.6254
epoch: 23, loss: 785.6, accuracy: 0.6407
epoch: 24, loss: 782.67, accuracy: 0.6354
epoch: 25, loss: 782.47, accuracy: 0.6344
epoch: 26, loss: 780.12, accuracy: 0.6391
epoch: 27, loss: 776.17, accuracy: 0.643
epoch: 28, loss: 775.33, accuracy: 0.6365
epoch: 29, loss: 774.12, accuracy: 0.6446
epoch: 30, loss: 773.16, accuracy: 0.6439
epoch: 31, loss: 771.61, accuracy: 0.6423
epoch: 32, loss: 769.52, accuracy: 0.6337
epoch: 33, loss: 767.14, accuracy: 0.6402
epoch: 34, loss: 764.84, accuracy: 0.6466
epoch: 35, loss: 764.99, accuracy: 0.6452
epoch: 36, loss: 763.13, accuracy: 0.6464
epoch: 37, loss: 762.09, accuracy: 0.6374
epoch: 38, loss: 758.77, accuracy: 0.6422
epoch: 39, loss: 759.29, accuracy: 0.6471
epoch: 40, loss: 757.79, accuracy: 0.6434
epoch: 41, loss: 755.62, accuracy: 0.6467
epoch: 42, loss: 757.93, accuracy: 0.6346
epoch: 43, loss: 752.72, accuracy: 0.6363
epoch: 44, loss: 754.4, accuracy: 0.6544
epoch: 45, loss: 753.01, accuracy: 0.65
epoch: 46, loss: 751.06, accuracy: 0.656
epoch: 47, loss: 750.54, accuracy: 0.6571
epoch: 48, loss: 748.54, accuracy: 0.6553
epoch: 49, loss: 749.52, accuracy: 0.6464
epoch: 50, loss: 746.64, accuracy: 0.6546
epoch: 51, loss: 747.47, accuracy: 0.6318
epoch: 52, loss: 745.61, accuracy: 0.6524
epoch: 53, loss: 746.36, accuracy: 0.6621
epoch: 54, loss: 743.49, accuracy: 0.6448
epoch: 55, loss: 742.88, accuracy: 0.6531
epoch: 56, loss: 742.84, accuracy: 0.6504
epoch: 57, loss: 742.2, accuracy: 0.6418
epoch: 58, loss: 737.87, accuracy: 0.6401
epoch: 59, loss: 738.98, accuracy: 0.6578
epoch: 60, loss: 740.86, accuracy: 0.6546
epoch: 61, loss: 739.32, accuracy: 0.6466
epoch: 62, loss: 737.6, accuracy: 0.6511
epoch: 63, loss: 737.93, accuracy: 0.6406
epoch: 64, loss: 736.57, accuracy: 0.6555
epoch: 65, loss: 736.04, accuracy: 0.6509
epoch: 66, loss: 735.63, accuracy: 0.6641
epoch: 67, loss: 734.42, accuracy: 0.6465
epoch: 68, loss: 735.19, accuracy: 0.6463
epoch: 69, loss: 733.21, accuracy: 0.6369
epoch: 70, loss: 700.32, accuracy: 0.6724
epoch: 71, loss: 697.56, accuracy: 0.6711
epoch: 72, loss: 695.28, accuracy: 0.6667
epoch: 73, loss: 695.95, accuracy: 0.6754
epoch: 74, loss: 694.6, accuracy: 0.6721
epoch: 75, loss: 694.28, accuracy: 0.6726
epoch: 76, loss: 695.0, accuracy: 0.6749
epoch: 77, loss: 693.82, accuracy: 0.67
epoch: 78, loss: 694.83, accuracy: 0.6711
epoch: 79, loss: 693.37, accuracy: 0.671
epoch: 80, loss: 692.55, accuracy: 0.675
epoch: 81, loss: 691.72, accuracy: 0.6717
epoch: 82, loss: 691.94, accuracy: 0.6737
epoch: 83, loss: 691.85, accuracy: 0.6731
epoch: 84, loss: 690.25, accuracy: 0.6727
epoch: 85, loss: 691.37, accuracy: 0.6734
epoch: 86, loss: 692.12, accuracy: 0.6721
epoch: 87, loss: 691.52, accuracy: 0.6727
epoch: 88, loss: 690.83, accuracy: 0.6755
epoch: 89, loss: 689.55, accuracy: 0.6742
epoch: 90, loss: 689.86, accuracy: 0.6745
epoch: 91, loss: 692.62, accuracy: 0.671
epoch: 92, loss: 689.4, accuracy: 0.6721
epoch: 93, loss: 688.89, accuracy: 0.672
epoch: 94, loss: 688.98, accuracy: 0.6788
epoch: 95, loss: 682.08, accuracy: 0.6794
epoch: 96, loss: 680.68, accuracy: 0.6789
epoch: 97, loss: 680.78, accuracy: 0.6772
epoch: 98, loss: 681.32, accuracy: 0.6769
epoch: 99, loss: 681.65, accuracy: 0.6761
time analysis:
    train 7647.556 s
    all 7650.7459 s
Accuracy of     0 : 73 %
Accuracy of     1 : 78 %
Accuracy of     2 : 56 %
Accuracy of     3 : 41 %
Accuracy of     4 : 61 %
Accuracy of     5 : 57 %
Accuracy of     6 : 78 %
Accuracy of     7 : 68 %
Accuracy of     8 : 79 %
Accuracy of     9 : 71 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,024
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,592
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,050
DAU params: 2,616
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1230.44, accuracy: 0.5115
epoch: 1, loss: 1057.63, accuracy: 0.5367
epoch: 2, loss: 985.27, accuracy: 0.5655
epoch: 3, loss: 951.38, accuracy: 0.5752
epoch: 4, loss: 930.37, accuracy: 0.5838
epoch: 5, loss: 912.21, accuracy: 0.5892
epoch: 6, loss: 900.32, accuracy: 0.5665
epoch: 7, loss: 889.04, accuracy: 0.6101
epoch: 8, loss: 877.23, accuracy: 0.5947
epoch: 9, loss: 871.0, accuracy: 0.5936
epoch: 10, loss: 862.85, accuracy: 0.6136
epoch: 11, loss: 857.84, accuracy: 0.6097
epoch: 12, loss: 846.81, accuracy: 0.6132
epoch: 13, loss: 844.6, accuracy: 0.6126
epoch: 14, loss: 835.42, accuracy: 0.6181
epoch: 15, loss: 830.1, accuracy: 0.6137
epoch: 16, loss: 825.83, accuracy: 0.6162
epoch: 17, loss: 825.79, accuracy: 0.6237
epoch: 18, loss: 819.49, accuracy: 0.6335
epoch: 19, loss: 814.16, accuracy: 0.6341
epoch: 20, loss: 811.01, accuracy: 0.6119
epoch: 21, loss: 809.13, accuracy: 0.6239
epoch: 22, loss: 805.79, accuracy: 0.6379
epoch: 23, loss: 802.54, accuracy: 0.622
epoch: 24, loss: 798.36, accuracy: 0.6378
epoch: 25, loss: 796.55, accuracy: 0.6327
epoch: 26, loss: 793.57, accuracy: 0.6341
epoch: 27, loss: 789.7, accuracy: 0.6441
epoch: 28, loss: 786.37, accuracy: 0.6284
epoch: 29, loss: 786.04, accuracy: 0.6369
epoch: 30, loss: 782.6, accuracy: 0.6204
epoch: 31, loss: 779.32, accuracy: 0.6469
epoch: 32, loss: 776.8, accuracy: 0.6399
epoch: 33, loss: 777.75, accuracy: 0.636
epoch: 34, loss: 776.0, accuracy: 0.6507
epoch: 35, loss: 772.68, accuracy: 0.6362
epoch: 36, loss: 769.85, accuracy: 0.6453
epoch: 37, loss: 766.94, accuracy: 0.635
epoch: 38, loss: 767.96, accuracy: 0.6417
epoch: 39, loss: 765.94, accuracy: 0.6326
epoch: 40, loss: 765.61, accuracy: 0.654
epoch: 41, loss: 763.81, accuracy: 0.6456
epoch: 42, loss: 765.54, accuracy: 0.6574
epoch: 43, loss: 760.72, accuracy: 0.6179
epoch: 44, loss: 761.64, accuracy: 0.6375
epoch: 45, loss: 758.08, accuracy: 0.6517
epoch: 46, loss: 759.53, accuracy: 0.6585
epoch: 47, loss: 755.56, accuracy: 0.6586
epoch: 48, loss: 755.48, accuracy: 0.6507
epoch: 49, loss: 756.41, accuracy: 0.6331
epoch: 50, loss: 756.27, accuracy: 0.6456
epoch: 51, loss: 752.24, accuracy: 0.6513
epoch: 52, loss: 751.13, accuracy: 0.6522
epoch: 53, loss: 751.28, accuracy: 0.6555
epoch: 54, loss: 750.73, accuracy: 0.6492
epoch: 55, loss: 750.59, accuracy: 0.6497
epoch: 56, loss: 749.02, accuracy: 0.6521
epoch: 57, loss: 748.84, accuracy: 0.6521
epoch: 58, loss: 747.72, accuracy: 0.6496
epoch: 59, loss: 744.9, accuracy: 0.6425
epoch: 60, loss: 744.13, accuracy: 0.6414
epoch: 61, loss: 744.06, accuracy: 0.6572
epoch: 62, loss: 744.84, accuracy: 0.6533
epoch: 63, loss: 744.43, accuracy: 0.6507
epoch: 64, loss: 743.56, accuracy: 0.6394
epoch: 65, loss: 741.41, accuracy: 0.6576
epoch: 66, loss: 740.16, accuracy: 0.6532
epoch: 67, loss: 743.49, accuracy: 0.6598
epoch: 68, loss: 741.46, accuracy: 0.659
epoch: 69, loss: 740.61, accuracy: 0.6592
epoch: 70, loss: 705.82, accuracy: 0.6696
epoch: 71, loss: 701.91, accuracy: 0.6673
epoch: 72, loss: 703.11, accuracy: 0.6715
epoch: 73, loss: 702.23, accuracy: 0.667
epoch: 74, loss: 700.35, accuracy: 0.6725
epoch: 75, loss: 700.72, accuracy: 0.668
epoch: 76, loss: 700.93, accuracy: 0.6718
epoch: 77, loss: 699.48, accuracy: 0.672
epoch: 78, loss: 698.65, accuracy: 0.6706
epoch: 79, loss: 697.95, accuracy: 0.6671
epoch: 80, loss: 699.98, accuracy: 0.6711
epoch: 81, loss: 698.88, accuracy: 0.6738
epoch: 82, loss: 698.95, accuracy: 0.6683
epoch: 83, loss: 697.39, accuracy: 0.6714
epoch: 84, loss: 697.86, accuracy: 0.6707
Epoch    85: reducing learning rate of group 0 to 5.0000e-04.
epoch: 85, loss: 697.97, accuracy: 0.6676
epoch: 86, loss: 692.46, accuracy: 0.6687
epoch: 87, loss: 693.92, accuracy: 0.6713
epoch: 88, loss: 691.25, accuracy: 0.6723
epoch: 89, loss: 692.03, accuracy: 0.6727
epoch: 90, loss: 691.72, accuracy: 0.6733
epoch: 91, loss: 691.36, accuracy: 0.6709
epoch: 92, loss: 691.83, accuracy: 0.6693
epoch: 93, loss: 692.09, accuracy: 0.6724
epoch: 94, loss: 690.54, accuracy: 0.6691
epoch: 95, loss: 687.22, accuracy: 0.6737
epoch: 96, loss: 686.73, accuracy: 0.6724
epoch: 97, loss: 687.89, accuracy: 0.6731
epoch: 98, loss: 686.94, accuracy: 0.6735
epoch: 99, loss: 686.71, accuracy: 0.6718
time analysis:
    train 13232.6121 s
    all 13240.2761 s
Accuracy of     0 : 75 %
Accuracy of     1 : 82 %
Accuracy of     2 : 55 %
Accuracy of     3 : 42 %
Accuracy of     4 : 56 %
Accuracy of     5 : 52 %
Accuracy of     6 : 78 %
Accuracy of     7 : 68 %
Accuracy of     8 : 75 %
Accuracy of     9 : 71 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,344
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           2,120
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,898
DAU params: 3,464
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1213.82, accuracy: 0.494
epoch: 1, loss: 1048.49, accuracy: 0.5319
epoch: 2, loss: 968.35, accuracy: 0.5576
epoch: 3, loss: 923.88, accuracy: 0.5902
epoch: 4, loss: 892.52, accuracy: 0.6011
epoch: 5, loss: 868.48, accuracy: 0.6084
epoch: 6, loss: 849.58, accuracy: 0.6203
epoch: 7, loss: 834.22, accuracy: 0.6274
epoch: 8, loss: 822.75, accuracy: 0.6243
epoch: 9, loss: 811.41, accuracy: 0.6249
epoch: 10, loss: 808.37, accuracy: 0.6308
epoch: 11, loss: 799.84, accuracy: 0.6387
epoch: 12, loss: 795.14, accuracy: 0.639
epoch: 13, loss: 790.35, accuracy: 0.6437
epoch: 14, loss: 788.66, accuracy: 0.6464
epoch: 15, loss: 779.11, accuracy: 0.6484
epoch: 16, loss: 779.12, accuracy: 0.6408
epoch: 17, loss: 770.63, accuracy: 0.6404
epoch: 18, loss: 770.27, accuracy: 0.6487
epoch: 19, loss: 764.39, accuracy: 0.6459
epoch: 20, loss: 762.11, accuracy: 0.6476
epoch: 21, loss: 758.79, accuracy: 0.6551
epoch: 22, loss: 756.18, accuracy: 0.6456
epoch: 23, loss: 754.51, accuracy: 0.6624
epoch: 24, loss: 749.65, accuracy: 0.648
epoch: 25, loss: 747.9, accuracy: 0.66
epoch: 26, loss: 745.99, accuracy: 0.6573
epoch: 27, loss: 744.54, accuracy: 0.6688
epoch: 28, loss: 741.75, accuracy: 0.661
epoch: 29, loss: 738.65, accuracy: 0.6531
epoch: 30, loss: 737.66, accuracy: 0.6543
epoch: 31, loss: 736.12, accuracy: 0.6452
epoch: 32, loss: 734.49, accuracy: 0.6684
epoch: 33, loss: 732.56, accuracy: 0.6546
epoch: 34, loss: 730.99, accuracy: 0.6684
epoch: 35, loss: 731.65, accuracy: 0.6674
epoch: 36, loss: 728.87, accuracy: 0.6633
epoch: 37, loss: 727.96, accuracy: 0.6716
epoch: 38, loss: 726.41, accuracy: 0.6676
epoch: 39, loss: 726.35, accuracy: 0.6658
epoch: 40, loss: 721.49, accuracy: 0.6651
epoch: 41, loss: 722.71, accuracy: 0.6686
epoch: 42, loss: 719.84, accuracy: 0.669
epoch: 43, loss: 718.64, accuracy: 0.6654
epoch: 44, loss: 719.16, accuracy: 0.6622
epoch: 45, loss: 715.99, accuracy: 0.6641
epoch: 46, loss: 716.3, accuracy: 0.6653
epoch: 47, loss: 715.09, accuracy: 0.6612
epoch: 48, loss: 716.89, accuracy: 0.6747
epoch: 49, loss: 712.84, accuracy: 0.6757
epoch: 50, loss: 713.19, accuracy: 0.6674
epoch: 51, loss: 711.2, accuracy: 0.6718
epoch: 52, loss: 711.35, accuracy: 0.6724
epoch: 53, loss: 709.41, accuracy: 0.6724
epoch: 54, loss: 709.54, accuracy: 0.6666
epoch: 55, loss: 708.12, accuracy: 0.6745
epoch: 56, loss: 709.05, accuracy: 0.6706
epoch: 57, loss: 706.08, accuracy: 0.6613
epoch: 58, loss: 705.29, accuracy: 0.6751
epoch: 59, loss: 702.04, accuracy: 0.6753
epoch: 60, loss: 705.59, accuracy: 0.6685
epoch: 61, loss: 701.65, accuracy: 0.6729
epoch: 62, loss: 699.68, accuracy: 0.666
epoch: 63, loss: 699.81, accuracy: 0.674
epoch: 64, loss: 700.69, accuracy: 0.6761
epoch: 65, loss: 699.51, accuracy: 0.676
epoch: 66, loss: 698.09, accuracy: 0.6731
epoch: 67, loss: 696.23, accuracy: 0.6742
epoch: 68, loss: 698.44, accuracy: 0.6755
epoch: 69, loss: 695.31, accuracy: 0.6785
epoch: 70, loss: 660.55, accuracy: 0.6902
epoch: 71, loss: 659.4, accuracy: 0.6889
epoch: 72, loss: 658.08, accuracy: 0.6903
epoch: 73, loss: 658.37, accuracy: 0.6875
epoch: 74, loss: 655.81, accuracy: 0.6902
epoch: 75, loss: 656.05, accuracy: 0.6903
epoch: 76, loss: 655.02, accuracy: 0.6902
epoch: 77, loss: 653.9, accuracy: 0.6848
epoch: 78, loss: 654.65, accuracy: 0.6904
epoch: 79, loss: 653.0, accuracy: 0.6914
epoch: 80, loss: 653.29, accuracy: 0.6903
epoch: 81, loss: 653.03, accuracy: 0.6887
epoch: 82, loss: 652.26, accuracy: 0.6869
epoch: 83, loss: 651.65, accuracy: 0.6863
epoch: 84, loss: 652.97, accuracy: 0.6867
epoch: 85, loss: 651.51, accuracy: 0.6888
epoch: 86, loss: 652.22, accuracy: 0.6884
epoch: 87, loss: 649.75, accuracy: 0.6895
epoch: 88, loss: 651.46, accuracy: 0.6842
epoch: 89, loss: 651.0, accuracy: 0.6913
epoch: 90, loss: 650.95, accuracy: 0.6887
epoch: 91, loss: 651.47, accuracy: 0.6859
epoch: 92, loss: 650.06, accuracy: 0.6878
Epoch    93: reducing learning rate of group 0 to 5.0000e-04.
epoch: 93, loss: 651.05, accuracy: 0.6877
epoch: 94, loss: 643.35, accuracy: 0.6925
epoch: 95, loss: 639.74, accuracy: 0.6897
epoch: 96, loss: 638.29, accuracy: 0.6916
epoch: 97, loss: 638.81, accuracy: 0.6908
epoch: 98, loss: 638.9, accuracy: 0.6914
epoch: 99, loss: 638.35, accuracy: 0.6919
time analysis:
    train 18777.9895 s
    all 18790.6654 s
Accuracy of     0 : 76 %
Accuracy of     1 : 84 %
Accuracy of     2 : 56 %
Accuracy of     3 : 38 %
Accuracy of     4 : 60 %
Accuracy of     5 : 59 %
Accuracy of     6 : 80 %
Accuracy of     7 : 71 %
Accuracy of     8 : 75 %
Accuracy of     9 : 74 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             452
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,036
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,922
DAU params: 1,488
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1287.5, accuracy: 0.4433
epoch: 1, loss: 1165.26, accuracy: 0.4782
epoch: 2, loss: 1127.83, accuracy: 0.4894
epoch: 3, loss: 1088.87, accuracy: 0.5034
epoch: 4, loss: 1050.33, accuracy: 0.5208
epoch: 5, loss: 1025.04, accuracy: 0.5345
epoch: 6, loss: 1003.17, accuracy: 0.5315
epoch: 7, loss: 993.31, accuracy: 0.5543
epoch: 8, loss: 975.09, accuracy: 0.557
epoch: 9, loss: 968.25, accuracy: 0.5605
epoch: 10, loss: 960.79, accuracy: 0.5614
epoch: 11, loss: 954.67, accuracy: 0.561
epoch: 12, loss: 951.16, accuracy: 0.5547
epoch: 13, loss: 946.0, accuracy: 0.57
epoch: 14, loss: 941.1, accuracy: 0.572
epoch: 15, loss: 937.94, accuracy: 0.5616
epoch: 16, loss: 935.73, accuracy: 0.5851
epoch: 17, loss: 930.77, accuracy: 0.5738
epoch: 18, loss: 930.47, accuracy: 0.5756
epoch: 19, loss: 927.95, accuracy: 0.5756
epoch: 20, loss: 924.98, accuracy: 0.579
epoch: 21, loss: 923.75, accuracy: 0.55
epoch: 22, loss: 922.96, accuracy: 0.5723
epoch: 23, loss: 920.97, accuracy: 0.5767
epoch: 24, loss: 920.74, accuracy: 0.5732
epoch: 25, loss: 918.88, accuracy: 0.5748
epoch: 26, loss: 918.38, accuracy: 0.5777
epoch: 27, loss: 915.2, accuracy: 0.5829
epoch: 28, loss: 915.63, accuracy: 0.5766
epoch: 29, loss: 915.34, accuracy: 0.5756
epoch: 30, loss: 912.71, accuracy: 0.5509
epoch: 31, loss: 912.02, accuracy: 0.5743
epoch: 32, loss: 909.06, accuracy: 0.5836
epoch: 33, loss: 907.9, accuracy: 0.5842
epoch: 34, loss: 907.84, accuracy: 0.5763
epoch: 35, loss: 909.11, accuracy: 0.576
epoch: 36, loss: 905.94, accuracy: 0.5722
epoch: 37, loss: 906.93, accuracy: 0.5823
epoch: 38, loss: 903.12, accuracy: 0.5911
epoch: 39, loss: 905.94, accuracy: 0.5849
epoch: 40, loss: 901.37, accuracy: 0.5628
epoch: 41, loss: 903.79, accuracy: 0.5889
epoch: 42, loss: 901.22, accuracy: 0.5859
epoch: 43, loss: 901.59, accuracy: 0.5923
epoch: 44, loss: 899.84, accuracy: 0.5816
epoch: 45, loss: 899.31, accuracy: 0.5796
epoch: 46, loss: 897.49, accuracy: 0.5826
epoch: 47, loss: 898.79, accuracy: 0.5821
epoch: 48, loss: 897.9, accuracy: 0.5857
epoch: 49, loss: 894.88, accuracy: 0.5783
epoch: 50, loss: 894.48, accuracy: 0.5803
epoch: 51, loss: 896.36, accuracy: 0.5822
epoch: 52, loss: 895.24, accuracy: 0.59
epoch: 53, loss: 894.1, accuracy: 0.5877
epoch: 54, loss: 891.65, accuracy: 0.584
epoch: 55, loss: 891.13, accuracy: 0.5885
epoch: 56, loss: 890.15, accuracy: 0.5768
epoch: 57, loss: 891.12, accuracy: 0.5905
epoch: 58, loss: 893.65, accuracy: 0.5795
epoch: 59, loss: 890.86, accuracy: 0.5852
epoch: 60, loss: 889.97, accuracy: 0.5879
epoch: 61, loss: 888.53, accuracy: 0.584
epoch: 62, loss: 888.42, accuracy: 0.5855
epoch: 63, loss: 888.66, accuracy: 0.594
epoch: 64, loss: 888.46, accuracy: 0.5903
epoch: 65, loss: 886.66, accuracy: 0.5912
epoch: 66, loss: 886.4, accuracy: 0.5713
epoch: 67, loss: 887.78, accuracy: 0.5897
epoch: 68, loss: 884.41, accuracy: 0.592
epoch: 69, loss: 885.17, accuracy: 0.5948
epoch: 70, loss: 855.72, accuracy: 0.6065
epoch: 71, loss: 853.23, accuracy: 0.6073
epoch: 72, loss: 851.92, accuracy: 0.6062
epoch: 73, loss: 851.44, accuracy: 0.602
epoch: 74, loss: 851.28, accuracy: 0.6042
epoch: 75, loss: 850.43, accuracy: 0.607
epoch: 76, loss: 849.87, accuracy: 0.6032
epoch: 77, loss: 849.32, accuracy: 0.6045
epoch: 78, loss: 850.76, accuracy: 0.6064
epoch: 79, loss: 850.83, accuracy: 0.6068
epoch: 80, loss: 849.26, accuracy: 0.6033
epoch: 81, loss: 847.8, accuracy: 0.6042
epoch: 82, loss: 848.15, accuracy: 0.6031
epoch: 83, loss: 847.78, accuracy: 0.6042
epoch: 84, loss: 847.99, accuracy: 0.6057
epoch: 85, loss: 848.18, accuracy: 0.6047
epoch: 86, loss: 847.46, accuracy: 0.6028
epoch: 87, loss: 846.75, accuracy: 0.6067
epoch: 88, loss: 847.62, accuracy: 0.6061
epoch: 89, loss: 845.94, accuracy: 0.6012
epoch: 90, loss: 849.08, accuracy: 0.6075
epoch: 91, loss: 847.25, accuracy: 0.602
epoch: 92, loss: 846.89, accuracy: 0.6051
epoch: 93, loss: 844.08, accuracy: 0.6061
epoch: 94, loss: 847.89, accuracy: 0.6028
epoch: 95, loss: 838.86, accuracy: 0.606
epoch: 96, loss: 838.51, accuracy: 0.6099
epoch: 97, loss: 837.91, accuracy: 0.6086
epoch: 98, loss: 838.01, accuracy: 0.6072
epoch: 99, loss: 840.26, accuracy: 0.6071
time analysis:
    train 1835.2177 s
    all 1852.8565 s
Accuracy of     0 : 67 %
Accuracy of     1 : 76 %
Accuracy of     2 : 46 %
Accuracy of     3 : 41 %
Accuracy of     4 : 47 %
Accuracy of     5 : 52 %
Accuracy of     6 : 76 %
Accuracy of     7 : 62 %
Accuracy of     8 : 68 %
Accuracy of     9 : 56 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             646
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,550
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 3,630
DAU params: 2,196
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1279.56, accuracy: 0.4648
epoch: 1, loss: 1146.38, accuracy: 0.4887
epoch: 2, loss: 1071.79, accuracy: 0.5254
epoch: 3, loss: 1036.78, accuracy: 0.5358
epoch: 4, loss: 1007.98, accuracy: 0.5522
epoch: 5, loss: 991.13, accuracy: 0.5224
epoch: 6, loss: 976.48, accuracy: 0.5495
epoch: 7, loss: 969.18, accuracy: 0.5472
epoch: 8, loss: 957.89, accuracy: 0.5593
epoch: 9, loss: 947.76, accuracy: 0.5634
epoch: 10, loss: 941.86, accuracy: 0.5687
epoch: 11, loss: 936.09, accuracy: 0.5702
epoch: 12, loss: 925.11, accuracy: 0.5723
epoch: 13, loss: 920.88, accuracy: 0.5659
epoch: 14, loss: 917.64, accuracy: 0.5603
epoch: 15, loss: 914.19, accuracy: 0.5794
epoch: 16, loss: 907.26, accuracy: 0.5793
epoch: 17, loss: 902.17, accuracy: 0.5799
epoch: 18, loss: 898.31, accuracy: 0.5729
epoch: 19, loss: 895.97, accuracy: 0.5752
epoch: 20, loss: 892.03, accuracy: 0.5614
epoch: 21, loss: 888.72, accuracy: 0.5856
epoch: 22, loss: 885.99, accuracy: 0.5773
epoch: 23, loss: 881.55, accuracy: 0.5705
epoch: 24, loss: 881.13, accuracy: 0.5844
epoch: 25, loss: 876.52, accuracy: 0.5825
epoch: 26, loss: 876.29, accuracy: 0.5861
epoch: 27, loss: 874.75, accuracy: 0.5876
epoch: 28, loss: 873.66, accuracy: 0.5913
epoch: 29, loss: 870.25, accuracy: 0.592
epoch: 30, loss: 868.96, accuracy: 0.5806
epoch: 31, loss: 868.53, accuracy: 0.5967
epoch: 32, loss: 863.94, accuracy: 0.5688
epoch: 33, loss: 864.08, accuracy: 0.5957
epoch: 34, loss: 864.16, accuracy: 0.5957
epoch: 35, loss: 862.74, accuracy: 0.5849
epoch: 36, loss: 859.18, accuracy: 0.5971
epoch: 37, loss: 858.34, accuracy: 0.5839
epoch: 38, loss: 858.21, accuracy: 0.5934
epoch: 39, loss: 857.71, accuracy: 0.5903
epoch: 40, loss: 854.61, accuracy: 0.602
epoch: 41, loss: 855.02, accuracy: 0.5869
epoch: 42, loss: 852.95, accuracy: 0.6068
epoch: 43, loss: 846.25, accuracy: 0.6006
epoch: 44, loss: 841.21, accuracy: 0.59
epoch: 45, loss: 837.41, accuracy: 0.6036
epoch: 46, loss: 832.46, accuracy: 0.5978
epoch: 47, loss: 831.16, accuracy: 0.5926
epoch: 48, loss: 828.35, accuracy: 0.6127
epoch: 49, loss: 828.12, accuracy: 0.6099
epoch: 50, loss: 825.34, accuracy: 0.6174
epoch: 51, loss: 822.16, accuracy: 0.6099
epoch: 52, loss: 820.97, accuracy: 0.6104
epoch: 53, loss: 820.04, accuracy: 0.6132
epoch: 54, loss: 820.19, accuracy: 0.6093
epoch: 55, loss: 817.48, accuracy: 0.6149
epoch: 56, loss: 817.89, accuracy: 0.6172
epoch: 57, loss: 815.17, accuracy: 0.6136
epoch: 58, loss: 814.27, accuracy: 0.6185
epoch: 59, loss: 813.8, accuracy: 0.6271
epoch: 60, loss: 808.82, accuracy: 0.6111
epoch: 61, loss: 809.35, accuracy: 0.6134
epoch: 62, loss: 809.44, accuracy: 0.618
epoch: 63, loss: 806.05, accuracy: 0.6255
epoch: 64, loss: 806.68, accuracy: 0.6234
epoch: 65, loss: 805.43, accuracy: 0.6185
epoch: 66, loss: 803.32, accuracy: 0.6278
epoch: 67, loss: 802.24, accuracy: 0.6175
epoch: 68, loss: 799.37, accuracy: 0.6194
epoch: 69, loss: 798.64, accuracy: 0.6295
epoch: 70, loss: 769.15, accuracy: 0.6349
epoch: 71, loss: 766.54, accuracy: 0.6388
epoch: 72, loss: 765.58, accuracy: 0.6365
epoch: 73, loss: 764.35, accuracy: 0.6387
epoch: 74, loss: 763.34, accuracy: 0.6391
epoch: 75, loss: 761.51, accuracy: 0.6404
epoch: 76, loss: 763.24, accuracy: 0.6399
epoch: 77, loss: 762.09, accuracy: 0.6399
epoch: 78, loss: 760.8, accuracy: 0.6367
epoch: 79, loss: 760.84, accuracy: 0.6354
epoch: 80, loss: 759.96, accuracy: 0.6386
epoch: 81, loss: 759.06, accuracy: 0.6383
epoch: 82, loss: 757.87, accuracy: 0.6371
epoch: 83, loss: 758.55, accuracy: 0.6404
epoch: 84, loss: 759.22, accuracy: 0.6425
epoch: 85, loss: 758.0, accuracy: 0.639
epoch: 86, loss: 756.23, accuracy: 0.6403
epoch: 87, loss: 756.1, accuracy: 0.6367
epoch: 88, loss: 757.02, accuracy: 0.6426
epoch: 89, loss: 756.8, accuracy: 0.6413
epoch: 90, loss: 754.36, accuracy: 0.6439
epoch: 91, loss: 755.64, accuracy: 0.6452
epoch: 92, loss: 754.82, accuracy: 0.6371
epoch: 93, loss: 754.31, accuracy: 0.6401
epoch: 94, loss: 754.39, accuracy: 0.6442
epoch: 95, loss: 746.48, accuracy: 0.6467
epoch: 96, loss: 746.96, accuracy: 0.6454
epoch: 97, loss: 745.45, accuracy: 0.6449
epoch: 98, loss: 745.97, accuracy: 0.646
epoch: 99, loss: 745.1, accuracy: 0.6452
time analysis:
    train 2191.9481 s
    all 2194.5496 s
Accuracy of     0 : 71 %
Accuracy of     1 : 78 %
Accuracy of     2 : 48 %
Accuracy of     3 : 35 %
Accuracy of     4 : 61 %
Accuracy of     5 : 52 %
Accuracy of     6 : 80 %
Accuracy of     7 : 71 %
Accuracy of     8 : 72 %
Accuracy of     9 : 69 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             840
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           2,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,338
DAU params: 2,904
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1246.86, accuracy: 0.4741
epoch: 1, loss: 1079.59, accuracy: 0.5318
epoch: 2, loss: 1010.47, accuracy: 0.5376
epoch: 3, loss: 950.11, accuracy: 0.56
epoch: 4, loss: 916.58, accuracy: 0.5845
epoch: 5, loss: 896.67, accuracy: 0.5934
epoch: 6, loss: 881.44, accuracy: 0.6044
epoch: 7, loss: 868.41, accuracy: 0.595
epoch: 8, loss: 857.9, accuracy: 0.6222
epoch: 9, loss: 850.52, accuracy: 0.6197
epoch: 10, loss: 843.47, accuracy: 0.6231
epoch: 11, loss: 833.84, accuracy: 0.6214
epoch: 12, loss: 828.83, accuracy: 0.6309
epoch: 13, loss: 825.11, accuracy: 0.6133
epoch: 14, loss: 817.68, accuracy: 0.6057
epoch: 15, loss: 816.3, accuracy: 0.6255
epoch: 16, loss: 812.31, accuracy: 0.622
epoch: 17, loss: 808.58, accuracy: 0.6286
epoch: 18, loss: 805.52, accuracy: 0.628
epoch: 19, loss: 798.79, accuracy: 0.6164
epoch: 20, loss: 800.94, accuracy: 0.6442
epoch: 21, loss: 795.12, accuracy: 0.6297
epoch: 22, loss: 795.56, accuracy: 0.6431
epoch: 23, loss: 791.1, accuracy: 0.6349
epoch: 24, loss: 786.6, accuracy: 0.6371
epoch: 25, loss: 784.73, accuracy: 0.6321
epoch: 26, loss: 784.93, accuracy: 0.6293
epoch: 27, loss: 781.38, accuracy: 0.628
epoch: 28, loss: 780.73, accuracy: 0.6442
epoch: 29, loss: 780.61, accuracy: 0.6431
epoch: 30, loss: 776.19, accuracy: 0.631
epoch: 31, loss: 774.73, accuracy: 0.6423
epoch: 32, loss: 773.43, accuracy: 0.6376
epoch: 33, loss: 769.25, accuracy: 0.6302
epoch: 34, loss: 768.2, accuracy: 0.6519
epoch: 35, loss: 765.02, accuracy: 0.6311
epoch: 36, loss: 764.98, accuracy: 0.6381
epoch: 37, loss: 762.29, accuracy: 0.6227
epoch: 38, loss: 764.02, accuracy: 0.6465
epoch: 39, loss: 760.75, accuracy: 0.6355
epoch: 40, loss: 760.22, accuracy: 0.643
epoch: 41, loss: 757.09, accuracy: 0.636
epoch: 42, loss: 758.11, accuracy: 0.6315
epoch: 43, loss: 758.32, accuracy: 0.6585
epoch: 44, loss: 756.79, accuracy: 0.6342
epoch: 45, loss: 756.93, accuracy: 0.643
epoch: 46, loss: 752.86, accuracy: 0.6479
epoch: 47, loss: 752.03, accuracy: 0.6459
epoch: 48, loss: 750.66, accuracy: 0.6409
epoch: 49, loss: 748.69, accuracy: 0.6508
epoch: 50, loss: 747.77, accuracy: 0.6456
epoch: 51, loss: 746.95, accuracy: 0.6499
epoch: 52, loss: 747.54, accuracy: 0.6452
epoch: 53, loss: 747.75, accuracy: 0.6397
epoch: 54, loss: 743.96, accuracy: 0.6479
epoch: 55, loss: 748.21, accuracy: 0.6408
epoch: 56, loss: 743.7, accuracy: 0.6469
epoch: 57, loss: 746.38, accuracy: 0.6547
epoch: 58, loss: 743.19, accuracy: 0.6467
epoch: 59, loss: 743.69, accuracy: 0.6448
epoch: 60, loss: 743.33, accuracy: 0.6542
epoch: 61, loss: 739.47, accuracy: 0.6497
epoch: 62, loss: 738.79, accuracy: 0.6624
epoch: 63, loss: 740.58, accuracy: 0.6423
epoch: 64, loss: 739.75, accuracy: 0.649
epoch: 65, loss: 737.3, accuracy: 0.6399
epoch: 66, loss: 739.0, accuracy: 0.6454
epoch: 67, loss: 737.17, accuracy: 0.6555
epoch: 68, loss: 734.15, accuracy: 0.653
epoch: 69, loss: 735.51, accuracy: 0.6585
epoch: 70, loss: 700.52, accuracy: 0.6693
epoch: 71, loss: 697.42, accuracy: 0.6644
epoch: 72, loss: 697.23, accuracy: 0.6655
epoch: 73, loss: 695.55, accuracy: 0.6692
epoch: 74, loss: 694.56, accuracy: 0.6697
epoch: 75, loss: 694.41, accuracy: 0.6705
epoch: 76, loss: 693.8, accuracy: 0.6684
epoch: 77, loss: 693.06, accuracy: 0.6698
epoch: 78, loss: 692.65, accuracy: 0.6681
epoch: 79, loss: 692.06, accuracy: 0.6712
epoch: 80, loss: 690.04, accuracy: 0.6682
epoch: 81, loss: 692.38, accuracy: 0.673
epoch: 82, loss: 692.08, accuracy: 0.6703
epoch: 83, loss: 691.92, accuracy: 0.6724
epoch: 84, loss: 690.41, accuracy: 0.6699
epoch: 85, loss: 689.69, accuracy: 0.671
epoch: 86, loss: 688.61, accuracy: 0.6683
epoch: 87, loss: 689.55, accuracy: 0.6684
epoch: 88, loss: 690.27, accuracy: 0.6684
epoch: 89, loss: 689.37, accuracy: 0.6695
epoch: 90, loss: 688.66, accuracy: 0.6724
epoch: 91, loss: 689.13, accuracy: 0.6704
Epoch    92: reducing learning rate of group 0 to 5.0000e-04.
epoch: 92, loss: 688.71, accuracy: 0.6669
epoch: 93, loss: 683.43, accuracy: 0.6717
epoch: 94, loss: 682.03, accuracy: 0.6721
epoch: 95, loss: 678.62, accuracy: 0.6727
epoch: 96, loss: 677.28, accuracy: 0.6719
epoch: 97, loss: 678.17, accuracy: 0.671
epoch: 98, loss: 678.04, accuracy: 0.6733
epoch: 99, loss: 679.19, accuracy: 0.6706
time analysis:
    train 2569.7432 s
    all 2572.5931 s
Accuracy of     0 : 80 %
Accuracy of     1 : 86 %
Accuracy of     2 : 49 %
Accuracy of     3 : 41 %
Accuracy of     4 : 61 %
Accuracy of     5 : 52 %
Accuracy of     6 : 69 %
Accuracy of     7 : 59 %
Accuracy of     8 : 81 %
Accuracy of     9 : 75 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.1)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.1)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1262.33, accuracy: 0.4485
epoch: 1, loss: 1144.57, accuracy: 0.4845
epoch: 2, loss: 1116.58, accuracy: 0.5109
epoch: 3, loss: 1102.55, accuracy: 0.5071
epoch: 4, loss: 1091.29, accuracy: 0.4935
epoch: 5, loss: 1087.01, accuracy: 0.5141
epoch: 6, loss: 1081.61, accuracy: 0.518
epoch: 7, loss: 1076.24, accuracy: 0.4923
epoch: 8, loss: 1072.98, accuracy: 0.514
epoch: 9, loss: 1069.95, accuracy: 0.5173
epoch: 10, loss: 1068.76, accuracy: 0.5236
epoch: 11, loss: 1066.08, accuracy: 0.5311
epoch: 12, loss: 1063.41, accuracy: 0.5229
epoch: 13, loss: 1059.07, accuracy: 0.5231
epoch: 14, loss: 1059.53, accuracy: 0.5235
epoch: 15, loss: 1058.78, accuracy: 0.5171
epoch: 16, loss: 1053.3, accuracy: 0.5147
epoch: 17, loss: 1055.54, accuracy: 0.5263
epoch: 18, loss: 1051.68, accuracy: 0.5278
epoch: 19, loss: 1048.19, accuracy: 0.5307
epoch: 20, loss: 1048.65, accuracy: 0.5183
epoch: 21, loss: 1047.32, accuracy: 0.5224
epoch: 22, loss: 1048.02, accuracy: 0.5295
epoch: 23, loss: 1045.43, accuracy: 0.5198
epoch: 24, loss: 1043.88, accuracy: 0.5323
epoch: 25, loss: 1044.25, accuracy: 0.53
epoch: 26, loss: 1040.58, accuracy: 0.534
epoch: 27, loss: 1040.41, accuracy: 0.5347
epoch: 28, loss: 1038.94, accuracy: 0.527
epoch: 29, loss: 1039.82, accuracy: 0.5296
epoch: 30, loss: 1037.36, accuracy: 0.5404
epoch: 31, loss: 1038.39, accuracy: 0.5288
epoch: 32, loss: 1040.09, accuracy: 0.5317
epoch: 33, loss: 1035.4, accuracy: 0.5401
epoch: 34, loss: 1036.7, accuracy: 0.5328
epoch: 35, loss: 1031.99, accuracy: 0.5333
epoch: 36, loss: 1033.25, accuracy: 0.5333
epoch: 37, loss: 1034.71, accuracy: 0.522
epoch: 38, loss: 1033.12, accuracy: 0.5411
epoch: 39, loss: 1032.34, accuracy: 0.522
epoch: 40, loss: 1032.77, accuracy: 0.5349
epoch: 41, loss: 1029.86, accuracy: 0.5339
epoch: 42, loss: 1031.48, accuracy: 0.5296
epoch: 43, loss: 1029.83, accuracy: 0.5417
epoch: 44, loss: 1027.63, accuracy: 0.5303
epoch: 45, loss: 1030.22, accuracy: 0.5369
epoch: 46, loss: 1029.17, accuracy: 0.5359
epoch: 47, loss: 1028.96, accuracy: 0.5418
epoch: 48, loss: 1028.13, accuracy: 0.5318
epoch: 49, loss: 1026.51, accuracy: 0.5435
epoch: 50, loss: 1027.45, accuracy: 0.5352
epoch: 51, loss: 1028.05, accuracy: 0.5366
epoch: 52, loss: 1026.72, accuracy: 0.5378
epoch: 53, loss: 1026.81, accuracy: 0.5353
epoch: 54, loss: 1025.67, accuracy: 0.5253
Epoch    55: reducing learning rate of group 0 to 2.5000e-03.
epoch: 55, loss: 1025.51, accuracy: 0.5263
epoch: 56, loss: 1011.89, accuracy: 0.5393
epoch: 57, loss: 1013.24, accuracy: 0.5444
epoch: 58, loss: 1011.21, accuracy: 0.5439
epoch: 59, loss: 1010.81, accuracy: 0.5431
epoch: 60, loss: 1009.58, accuracy: 0.5395
epoch: 61, loss: 1011.9, accuracy: 0.5358
epoch: 62, loss: 1008.22, accuracy: 0.5345
epoch: 63, loss: 1009.75, accuracy: 0.5452
epoch: 64, loss: 1010.54, accuracy: 0.5396
epoch: 65, loss: 1010.88, accuracy: 0.5396
epoch: 66, loss: 1011.07, accuracy: 0.5455
epoch: 67, loss: 1008.43, accuracy: 0.5461
Epoch    68: reducing learning rate of group 0 to 1.2500e-03.
epoch: 68, loss: 1010.39, accuracy: 0.5383
epoch: 69, loss: 1001.75, accuracy: 0.5437
epoch: 70, loss: 996.37, accuracy: 0.5478
epoch: 71, loss: 994.1, accuracy: 0.5493
epoch: 72, loss: 993.33, accuracy: 0.5508
epoch: 73, loss: 993.73, accuracy: 0.5505
epoch: 74, loss: 993.69, accuracy: 0.5494
epoch: 75, loss: 993.04, accuracy: 0.5505
epoch: 76, loss: 993.09, accuracy: 0.5508
epoch: 77, loss: 993.25, accuracy: 0.5508
epoch: 78, loss: 994.59, accuracy: 0.5524
epoch: 79, loss: 993.54, accuracy: 0.5503
epoch: 80, loss: 993.75, accuracy: 0.5498
Epoch    81: reducing learning rate of group 0 to 1.2500e-04.
epoch: 81, loss: 992.42, accuracy: 0.5511
epoch: 82, loss: 991.13, accuracy: 0.5516
epoch: 83, loss: 992.03, accuracy: 0.55
epoch: 84, loss: 993.88, accuracy: 0.5503
epoch: 85, loss: 993.48, accuracy: 0.5506
epoch: 86, loss: 992.78, accuracy: 0.5491
epoch: 87, loss: 991.08, accuracy: 0.5507
epoch: 88, loss: 992.33, accuracy: 0.5504
epoch: 89, loss: 994.46, accuracy: 0.5513
epoch: 90, loss: 993.61, accuracy: 0.551
epoch: 91, loss: 991.79, accuracy: 0.5514
Epoch    92: reducing learning rate of group 0 to 1.0000e-04.
epoch: 92, loss: 991.48, accuracy: 0.5509
epoch: 93, loss: 992.12, accuracy: 0.5502
epoch: 94, loss: 993.59, accuracy: 0.5501
epoch: 95, loss: 991.43, accuracy: 0.55
epoch: 96, loss: 993.06, accuracy: 0.5504
epoch: 97, loss: 992.06, accuracy: 0.5494
epoch: 98, loss: 990.87, accuracy: 0.5509
epoch: 99, loss: 990.9, accuracy: 0.5495
time analysis:
    train 1092.6778 s
    all 1095.7797 s
Accuracy of     0 : 64 %
Accuracy of     1 : 62 %
Accuracy of     2 : 31 %
Accuracy of     3 : 35 %
Accuracy of     4 : 45 %
Accuracy of     5 : 45 %
Accuracy of     6 : 64 %
Accuracy of     7 : 50 %
Accuracy of     8 : 70 %
Accuracy of     9 : 52 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.2)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1253.85, accuracy: 0.4892
epoch: 1, loss: 1154.01, accuracy: 0.4865
epoch: 2, loss: 1134.22, accuracy: 0.4857
epoch: 3, loss: 1124.24, accuracy: 0.5045
epoch: 4, loss: 1119.1, accuracy: 0.4968
epoch: 5, loss: 1113.04, accuracy: 0.5001
epoch: 6, loss: 1109.82, accuracy: 0.4958
epoch: 7, loss: 1104.8, accuracy: 0.5008
epoch: 8, loss: 1100.8, accuracy: 0.5027
epoch: 9, loss: 1098.13, accuracy: 0.5009
epoch: 10, loss: 1097.86, accuracy: 0.5004
epoch: 11, loss: 1096.04, accuracy: 0.5081
epoch: 12, loss: 1094.31, accuracy: 0.5061
epoch: 13, loss: 1088.24, accuracy: 0.4983
epoch: 14, loss: 1090.35, accuracy: 0.5074
epoch: 15, loss: 1087.76, accuracy: 0.506
epoch: 16, loss: 1086.77, accuracy: 0.5046
epoch: 17, loss: 1085.29, accuracy: 0.4992
epoch: 18, loss: 1085.63, accuracy: 0.5059
epoch: 19, loss: 1084.68, accuracy: 0.5052
epoch: 20, loss: 1083.94, accuracy: 0.5164
epoch: 21, loss: 1082.8, accuracy: 0.5055
epoch: 22, loss: 1081.67, accuracy: 0.5082
epoch: 23, loss: 1083.04, accuracy: 0.5136
epoch: 24, loss: 1080.48, accuracy: 0.5072
epoch: 25, loss: 1080.46, accuracy: 0.5092
epoch: 26, loss: 1080.09, accuracy: 0.5125
epoch: 27, loss: 1079.39, accuracy: 0.4958
epoch: 28, loss: 1078.43, accuracy: 0.5103
epoch: 29, loss: 1079.2, accuracy: 0.5102
epoch: 30, loss: 1078.07, accuracy: 0.5082
epoch: 31, loss: 1077.94, accuracy: 0.5161
epoch: 32, loss: 1077.27, accuracy: 0.5118
epoch: 33, loss: 1075.69, accuracy: 0.517
epoch: 34, loss: 1073.71, accuracy: 0.5084
epoch: 35, loss: 1074.47, accuracy: 0.512
epoch: 36, loss: 1074.19, accuracy: 0.5134
epoch: 37, loss: 1075.94, accuracy: 0.5147
epoch: 38, loss: 1072.75, accuracy: 0.5158
epoch: 39, loss: 1071.5, accuracy: 0.5097
epoch: 40, loss: 1074.31, accuracy: 0.5102
epoch: 41, loss: 1073.3, accuracy: 0.5137
epoch: 42, loss: 1072.4, accuracy: 0.51
epoch: 43, loss: 1073.59, accuracy: 0.5191
epoch: 44, loss: 1072.76, accuracy: 0.518
Epoch    45: reducing learning rate of group 0 to 2.5000e-03.
epoch: 45, loss: 1070.46, accuracy: 0.5154
epoch: 46, loss: 1057.85, accuracy: 0.5228
epoch: 47, loss: 1057.51, accuracy: 0.5156
epoch: 48, loss: 1057.89, accuracy: 0.5225
epoch: 49, loss: 1056.04, accuracy: 0.5185
epoch: 50, loss: 1055.81, accuracy: 0.5148
epoch: 51, loss: 1057.37, accuracy: 0.5248
epoch: 52, loss: 1057.08, accuracy: 0.5178
epoch: 53, loss: 1057.21, accuracy: 0.5251
epoch: 54, loss: 1056.93, accuracy: 0.5199
epoch: 55, loss: 1056.08, accuracy: 0.525
epoch: 56, loss: 1053.59, accuracy: 0.5223
epoch: 57, loss: 1054.28, accuracy: 0.5233
epoch: 58, loss: 1053.9, accuracy: 0.527
epoch: 59, loss: 1054.91, accuracy: 0.5217
epoch: 60, loss: 1054.53, accuracy: 0.5253
epoch: 61, loss: 1053.33, accuracy: 0.522
Epoch    62: reducing learning rate of group 0 to 1.2500e-03.
epoch: 62, loss: 1054.78, accuracy: 0.5245
epoch: 63, loss: 1047.41, accuracy: 0.529
epoch: 64, loss: 1045.15, accuracy: 0.5263
epoch: 65, loss: 1046.96, accuracy: 0.5263
epoch: 66, loss: 1045.6, accuracy: 0.5292
epoch: 67, loss: 1046.99, accuracy: 0.528
epoch: 68, loss: 1045.57, accuracy: 0.5248
epoch: 69, loss: 1045.61, accuracy: 0.5274
epoch: 70, loss: 1039.27, accuracy: 0.5294
epoch: 71, loss: 1039.15, accuracy: 0.5312
epoch: 72, loss: 1038.76, accuracy: 0.5295
epoch: 73, loss: 1038.46, accuracy: 0.5293
epoch: 74, loss: 1037.95, accuracy: 0.5278
epoch: 75, loss: 1038.63, accuracy: 0.5307
epoch: 76, loss: 1036.61, accuracy: 0.5299
epoch: 77, loss: 1038.25, accuracy: 0.5285
epoch: 78, loss: 1038.14, accuracy: 0.5295
epoch: 79, loss: 1037.51, accuracy: 0.5304
epoch: 80, loss: 1037.26, accuracy: 0.5308
epoch: 81, loss: 1038.26, accuracy: 0.5313
Epoch    82: reducing learning rate of group 0 to 1.2500e-04.
epoch: 82, loss: 1038.78, accuracy: 0.5281
epoch: 83, loss: 1037.68, accuracy: 0.5289
epoch: 84, loss: 1036.88, accuracy: 0.5276
epoch: 85, loss: 1038.18, accuracy: 0.5281
epoch: 86, loss: 1035.0, accuracy: 0.5286
epoch: 87, loss: 1036.73, accuracy: 0.5308
epoch: 88, loss: 1037.23, accuracy: 0.5296
epoch: 89, loss: 1037.75, accuracy: 0.5314
epoch: 90, loss: 1035.57, accuracy: 0.529
epoch: 91, loss: 1037.72, accuracy: 0.5287
epoch: 92, loss: 1036.22, accuracy: 0.5294
Epoch    93: reducing learning rate of group 0 to 1.0000e-04.
epoch: 93, loss: 1037.54, accuracy: 0.5306
epoch: 94, loss: 1036.45, accuracy: 0.5298
epoch: 95, loss: 1037.13, accuracy: 0.5303
epoch: 96, loss: 1036.11, accuracy: 0.5295
epoch: 97, loss: 1036.29, accuracy: 0.5302
epoch: 98, loss: 1036.78, accuracy: 0.5312
epoch: 99, loss: 1036.57, accuracy: 0.5305
time analysis:
    train 1103.4054 s
    all 1105.6832 s
Accuracy of     0 : 58 %
Accuracy of     1 : 64 %
Accuracy of     2 : 30 %
Accuracy of     3 : 42 %
Accuracy of     4 : 34 %
Accuracy of     5 : 37 %
Accuracy of     6 : 60 %
Accuracy of     7 : 56 %
Accuracy of     8 : 77 %
Accuracy of     9 : 56 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.3)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.3)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1240.92, accuracy: 0.4761
epoch: 1, loss: 1135.36, accuracy: 0.4966
epoch: 2, loss: 1116.83, accuracy: 0.5066
epoch: 3, loss: 1105.72, accuracy: 0.4938
epoch: 4, loss: 1098.62, accuracy: 0.501
epoch: 5, loss: 1093.74, accuracy: 0.5052
epoch: 6, loss: 1088.87, accuracy: 0.5058
epoch: 7, loss: 1086.11, accuracy: 0.5082
epoch: 8, loss: 1086.1, accuracy: 0.5095
epoch: 9, loss: 1080.98, accuracy: 0.5175
epoch: 10, loss: 1079.48, accuracy: 0.5064
epoch: 11, loss: 1077.22, accuracy: 0.5181
epoch: 12, loss: 1074.23, accuracy: 0.5195
epoch: 13, loss: 1072.33, accuracy: 0.5144
epoch: 14, loss: 1073.0, accuracy: 0.5182
epoch: 15, loss: 1070.11, accuracy: 0.521
epoch: 16, loss: 1067.14, accuracy: 0.5158
epoch: 17, loss: 1070.58, accuracy: 0.5162
epoch: 18, loss: 1066.61, accuracy: 0.529
epoch: 19, loss: 1068.33, accuracy: 0.5223
epoch: 20, loss: 1066.46, accuracy: 0.5184
epoch: 21, loss: 1062.89, accuracy: 0.5201
epoch: 22, loss: 1064.21, accuracy: 0.5173
epoch: 23, loss: 1063.64, accuracy: 0.5144
epoch: 24, loss: 1062.51, accuracy: 0.5199
epoch: 25, loss: 1061.1, accuracy: 0.5166
epoch: 26, loss: 1061.2, accuracy: 0.5102
epoch: 27, loss: 1058.33, accuracy: 0.5205
epoch: 28, loss: 1055.13, accuracy: 0.5286
epoch: 29, loss: 1056.29, accuracy: 0.5135
epoch: 30, loss: 1052.98, accuracy: 0.526
epoch: 31, loss: 1053.14, accuracy: 0.5037
epoch: 32, loss: 1053.41, accuracy: 0.4996
epoch: 33, loss: 1051.07, accuracy: 0.5214
epoch: 34, loss: 1050.28, accuracy: 0.5234
epoch: 35, loss: 1047.56, accuracy: 0.4817
epoch: 36, loss: 1049.32, accuracy: 0.5309
epoch: 37, loss: 1048.89, accuracy: 0.5244
epoch: 38, loss: 1044.54, accuracy: 0.5213
epoch: 39, loss: 1045.54, accuracy: 0.5251
epoch: 40, loss: 1043.47, accuracy: 0.5261
epoch: 41, loss: 1045.81, accuracy: 0.5179
epoch: 42, loss: 1043.44, accuracy: 0.5023
epoch: 43, loss: 1043.97, accuracy: 0.5172
epoch: 44, loss: 1042.71, accuracy: 0.5093
epoch: 45, loss: 1042.68, accuracy: 0.5253
epoch: 46, loss: 1038.74, accuracy: 0.5295
epoch: 47, loss: 1042.31, accuracy: 0.5026
epoch: 48, loss: 1040.6, accuracy: 0.5089
epoch: 49, loss: 1039.0, accuracy: 0.5194
epoch: 50, loss: 1037.13, accuracy: 0.5236
epoch: 51, loss: 1038.88, accuracy: 0.5271
epoch: 52, loss: 1037.37, accuracy: 0.5195
epoch: 53, loss: 1038.35, accuracy: 0.5285
epoch: 54, loss: 1035.97, accuracy: 0.5183
epoch: 55, loss: 1035.61, accuracy: 0.526
epoch: 56, loss: 1033.57, accuracy: 0.5196
epoch: 57, loss: 1034.18, accuracy: 0.5248
epoch: 58, loss: 1033.98, accuracy: 0.5196
epoch: 59, loss: 1032.91, accuracy: 0.5174
epoch: 60, loss: 1030.82, accuracy: 0.524
epoch: 61, loss: 1030.46, accuracy: 0.5279
epoch: 62, loss: 1031.78, accuracy: 0.5309
epoch: 63, loss: 1030.77, accuracy: 0.5204
epoch: 64, loss: 1031.28, accuracy: 0.524
epoch: 65, loss: 1029.75, accuracy: 0.5083
epoch: 66, loss: 1028.49, accuracy: 0.5231
epoch: 67, loss: 1026.59, accuracy: 0.51
epoch: 68, loss: 1026.8, accuracy: 0.5174
epoch: 69, loss: 1027.4, accuracy: 0.5027
epoch: 70, loss: 1004.11, accuracy: 0.5415
epoch: 71, loss: 1000.25, accuracy: 0.5426
epoch: 72, loss: 1001.04, accuracy: 0.5439
epoch: 73, loss: 1000.87, accuracy: 0.5399
epoch: 74, loss: 999.43, accuracy: 0.5429
epoch: 75, loss: 998.64, accuracy: 0.5399
epoch: 76, loss: 999.56, accuracy: 0.5421
epoch: 77, loss: 999.56, accuracy: 0.5435
epoch: 78, loss: 998.66, accuracy: 0.5399
epoch: 79, loss: 999.58, accuracy: 0.5435
epoch: 80, loss: 997.74, accuracy: 0.5419
epoch: 81, loss: 997.55, accuracy: 0.5418
epoch: 82, loss: 997.81, accuracy: 0.5429
epoch: 83, loss: 997.22, accuracy: 0.5409
epoch: 84, loss: 996.95, accuracy: 0.5437
epoch: 85, loss: 998.36, accuracy: 0.5414
epoch: 86, loss: 996.33, accuracy: 0.5426
epoch: 87, loss: 997.64, accuracy: 0.543
epoch: 88, loss: 993.64, accuracy: 0.5427
epoch: 89, loss: 996.86, accuracy: 0.5381
epoch: 90, loss: 996.6, accuracy: 0.5441
epoch: 91, loss: 996.32, accuracy: 0.5464
epoch: 92, loss: 994.7, accuracy: 0.5448
epoch: 93, loss: 995.81, accuracy: 0.5438
Epoch    94: reducing learning rate of group 0 to 5.0000e-04.
epoch: 94, loss: 996.09, accuracy: 0.5462
epoch: 95, loss: 990.07, accuracy: 0.5472
epoch: 96, loss: 988.73, accuracy: 0.5476
epoch: 97, loss: 987.81, accuracy: 0.5435
epoch: 98, loss: 988.2, accuracy: 0.5468
epoch: 99, loss: 988.17, accuracy: 0.5462
time analysis:
    train 1110.8019 s
    all 1113.0843 s
Accuracy of     0 : 66 %
Accuracy of     1 : 66 %
Accuracy of     2 : 36 %
Accuracy of     3 : 42 %
Accuracy of     4 : 40 %
Accuracy of     5 : 42 %
Accuracy of     6 : 69 %
Accuracy of     7 : 48 %
Accuracy of     8 : 72 %
Accuracy of     9 : 58 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.4)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.4)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1262.21, accuracy: 0.4772
epoch: 1, loss: 1147.64, accuracy: 0.4911
epoch: 2, loss: 1124.11, accuracy: 0.5008
epoch: 3, loss: 1111.54, accuracy: 0.5024
epoch: 4, loss: 1103.4, accuracy: 0.5076
epoch: 5, loss: 1098.44, accuracy: 0.5135
epoch: 6, loss: 1090.03, accuracy: 0.5121
epoch: 7, loss: 1085.34, accuracy: 0.5072
epoch: 8, loss: 1080.49, accuracy: 0.5025
epoch: 9, loss: 1077.1, accuracy: 0.5092
epoch: 10, loss: 1073.81, accuracy: 0.5221
epoch: 11, loss: 1069.29, accuracy: 0.5217
epoch: 12, loss: 1067.08, accuracy: 0.5076
epoch: 13, loss: 1062.26, accuracy: 0.505
epoch: 14, loss: 1062.21, accuracy: 0.5113
epoch: 15, loss: 1059.6, accuracy: 0.5269
epoch: 16, loss: 1055.11, accuracy: 0.5233
epoch: 17, loss: 1054.02, accuracy: 0.5094
epoch: 18, loss: 1050.06, accuracy: 0.5209
epoch: 19, loss: 1050.23, accuracy: 0.5171
epoch: 20, loss: 1048.87, accuracy: 0.5256
epoch: 21, loss: 1048.37, accuracy: 0.5226
epoch: 22, loss: 1045.73, accuracy: 0.5189
epoch: 23, loss: 1042.08, accuracy: 0.5176
epoch: 24, loss: 1041.63, accuracy: 0.5217
epoch: 25, loss: 1042.12, accuracy: 0.5163
epoch: 26, loss: 1040.23, accuracy: 0.5252
epoch: 27, loss: 1039.49, accuracy: 0.5223
epoch: 28, loss: 1038.05, accuracy: 0.5383
epoch: 29, loss: 1038.1, accuracy: 0.5174
epoch: 30, loss: 1035.53, accuracy: 0.5361
epoch: 31, loss: 1035.63, accuracy: 0.5363
epoch: 32, loss: 1034.18, accuracy: 0.5029
epoch: 33, loss: 1032.7, accuracy: 0.5271
epoch: 34, loss: 1030.81, accuracy: 0.5282
epoch: 35, loss: 1030.91, accuracy: 0.5295
epoch: 36, loss: 1029.98, accuracy: 0.5359
epoch: 37, loss: 1031.23, accuracy: 0.5394
epoch: 38, loss: 1027.68, accuracy: 0.5417
epoch: 39, loss: 1029.39, accuracy: 0.5403
epoch: 40, loss: 1027.92, accuracy: 0.5304
epoch: 41, loss: 1027.97, accuracy: 0.537
epoch: 42, loss: 1027.19, accuracy: 0.5366
epoch: 43, loss: 1023.53, accuracy: 0.5337
epoch: 44, loss: 1024.34, accuracy: 0.5238
epoch: 45, loss: 1022.62, accuracy: 0.5361
epoch: 46, loss: 1023.59, accuracy: 0.5294
epoch: 47, loss: 1022.65, accuracy: 0.5332
epoch: 48, loss: 1022.84, accuracy: 0.5321
epoch: 49, loss: 1018.64, accuracy: 0.5324
epoch: 50, loss: 1019.3, accuracy: 0.533
epoch: 51, loss: 1020.08, accuracy: 0.5407
epoch: 52, loss: 1019.71, accuracy: 0.5363
epoch: 53, loss: 1017.08, accuracy: 0.5376
epoch: 54, loss: 1016.43, accuracy: 0.5358
epoch: 55, loss: 1017.86, accuracy: 0.5237
epoch: 56, loss: 1017.82, accuracy: 0.5398
epoch: 57, loss: 1014.21, accuracy: 0.5334
epoch: 58, loss: 1013.86, accuracy: 0.5388
epoch: 59, loss: 1015.8, accuracy: 0.5367
epoch: 60, loss: 1012.0, accuracy: 0.5371
epoch: 61, loss: 1015.12, accuracy: 0.5355
epoch: 62, loss: 1010.95, accuracy: 0.5288
epoch: 63, loss: 1009.47, accuracy: 0.5279
epoch: 64, loss: 1010.07, accuracy: 0.5321
epoch: 65, loss: 1010.72, accuracy: 0.5387
epoch: 66, loss: 1010.26, accuracy: 0.5377
epoch: 67, loss: 1010.4, accuracy: 0.5309
epoch: 68, loss: 1011.13, accuracy: 0.5317
epoch: 69, loss: 1008.43, accuracy: 0.5366
epoch: 70, loss: 987.39, accuracy: 0.5514
epoch: 71, loss: 986.06, accuracy: 0.5504
epoch: 72, loss: 986.15, accuracy: 0.5512
epoch: 73, loss: 984.81, accuracy: 0.5491
epoch: 74, loss: 986.11, accuracy: 0.5484
epoch: 75, loss: 985.16, accuracy: 0.5484
epoch: 76, loss: 982.71, accuracy: 0.5478
epoch: 77, loss: 985.19, accuracy: 0.5488
epoch: 78, loss: 984.89, accuracy: 0.548
epoch: 79, loss: 984.23, accuracy: 0.5504
epoch: 80, loss: 982.79, accuracy: 0.5484
epoch: 81, loss: 984.67, accuracy: 0.5527
Epoch    82: reducing learning rate of group 0 to 5.0000e-04.
epoch: 82, loss: 983.7, accuracy: 0.5491
epoch: 83, loss: 979.91, accuracy: 0.5541
epoch: 84, loss: 980.68, accuracy: 0.5526
epoch: 85, loss: 979.38, accuracy: 0.5526
epoch: 86, loss: 980.62, accuracy: 0.5519
epoch: 87, loss: 981.3, accuracy: 0.5516
epoch: 88, loss: 979.01, accuracy: 0.5483
epoch: 89, loss: 979.25, accuracy: 0.5503
epoch: 90, loss: 978.85, accuracy: 0.5513
epoch: 91, loss: 978.25, accuracy: 0.5535
epoch: 92, loss: 979.27, accuracy: 0.5504
epoch: 93, loss: 980.71, accuracy: 0.5546
epoch: 94, loss: 978.98, accuracy: 0.5511
epoch: 95, loss: 976.2, accuracy: 0.5524
epoch: 96, loss: 976.51, accuracy: 0.5537
epoch: 97, loss: 977.47, accuracy: 0.5535
epoch: 98, loss: 975.42, accuracy: 0.5543
epoch: 99, loss: 976.18, accuracy: 0.5533
time analysis:
    train 1109.9843 s
    all 1112.3219 s
Accuracy of     0 : 64 %
Accuracy of     1 : 64 %
Accuracy of     2 : 36 %
Accuracy of     3 : 36 %
Accuracy of     4 : 38 %
Accuracy of     5 : 50 %
Accuracy of     6 : 73 %
Accuracy of     7 : 53 %
Accuracy of     8 : 72 %
Accuracy of     9 : 56 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1252.04, accuracy: 0.4919
epoch: 1, loss: 1139.03, accuracy: 0.4776
epoch: 2, loss: 1118.32, accuracy: 0.4909
epoch: 3, loss: 1106.23, accuracy: 0.5019
epoch: 4, loss: 1099.27, accuracy: 0.5057
epoch: 5, loss: 1090.46, accuracy: 0.5105
epoch: 6, loss: 1087.65, accuracy: 0.5094
epoch: 7, loss: 1081.27, accuracy: 0.5201
epoch: 8, loss: 1080.99, accuracy: 0.5198
epoch: 9, loss: 1077.32, accuracy: 0.5194
epoch: 10, loss: 1069.24, accuracy: 0.5205
epoch: 11, loss: 1070.17, accuracy: 0.5129
epoch: 12, loss: 1066.12, accuracy: 0.5164
epoch: 13, loss: 1064.46, accuracy: 0.5118
epoch: 14, loss: 1060.92, accuracy: 0.5222
epoch: 15, loss: 1060.94, accuracy: 0.5166
epoch: 16, loss: 1056.71, accuracy: 0.5181
epoch: 17, loss: 1052.97, accuracy: 0.5125
epoch: 18, loss: 1050.55, accuracy: 0.5057
epoch: 19, loss: 1049.67, accuracy: 0.5239
epoch: 20, loss: 1048.24, accuracy: 0.5135
epoch: 21, loss: 1046.29, accuracy: 0.5299
epoch: 22, loss: 1045.58, accuracy: 0.5189
epoch: 23, loss: 1044.73, accuracy: 0.528
epoch: 24, loss: 1043.21, accuracy: 0.5242
epoch: 25, loss: 1041.55, accuracy: 0.5245
epoch: 26, loss: 1039.85, accuracy: 0.5149
epoch: 27, loss: 1041.52, accuracy: 0.5218
epoch: 28, loss: 1039.49, accuracy: 0.5236
epoch: 29, loss: 1038.14, accuracy: 0.5204
epoch: 30, loss: 1033.53, accuracy: 0.5185
epoch: 31, loss: 1034.37, accuracy: 0.5197
epoch: 32, loss: 1033.13, accuracy: 0.5312
epoch: 33, loss: 1034.45, accuracy: 0.5286
epoch: 34, loss: 1031.65, accuracy: 0.5199
epoch: 35, loss: 1033.22, accuracy: 0.5276
epoch: 36, loss: 1032.06, accuracy: 0.5252
epoch: 37, loss: 1031.44, accuracy: 0.5261
epoch: 38, loss: 1029.25, accuracy: 0.5273
epoch: 39, loss: 1029.43, accuracy: 0.5172
epoch: 40, loss: 1030.78, accuracy: 0.5316
epoch: 41, loss: 1030.53, accuracy: 0.5294
epoch: 42, loss: 1026.78, accuracy: 0.5068
epoch: 43, loss: 1028.86, accuracy: 0.533
epoch: 44, loss: 1028.83, accuracy: 0.5287
epoch: 45, loss: 1026.34, accuracy: 0.5259
epoch: 46, loss: 1024.97, accuracy: 0.5258
epoch: 47, loss: 1028.47, accuracy: 0.5275
epoch: 48, loss: 1025.52, accuracy: 0.5223
epoch: 49, loss: 1026.28, accuracy: 0.5339
epoch: 50, loss: 1026.76, accuracy: 0.5169
epoch: 51, loss: 1024.53, accuracy: 0.5247
Epoch    52: reducing learning rate of group 0 to 2.5000e-03.
epoch: 52, loss: 1024.15, accuracy: 0.5267
epoch: 53, loss: 1009.22, accuracy: 0.5341
epoch: 54, loss: 1007.68, accuracy: 0.5349
epoch: 55, loss: 1005.81, accuracy: 0.5336
epoch: 56, loss: 1006.22, accuracy: 0.5362
epoch: 57, loss: 1006.59, accuracy: 0.5377
epoch: 58, loss: 1004.06, accuracy: 0.5369
epoch: 59, loss: 1004.22, accuracy: 0.5328
epoch: 60, loss: 1005.01, accuracy: 0.5339
epoch: 61, loss: 1004.41, accuracy: 0.5378
epoch: 62, loss: 1002.95, accuracy: 0.5348
epoch: 63, loss: 1003.75, accuracy: 0.534
epoch: 64, loss: 1002.11, accuracy: 0.5329
epoch: 65, loss: 1002.38, accuracy: 0.5349
epoch: 66, loss: 1001.61, accuracy: 0.5294
epoch: 67, loss: 1002.04, accuracy: 0.5403
epoch: 68, loss: 1001.1, accuracy: 0.5413
epoch: 69, loss: 1001.43, accuracy: 0.5381
epoch: 70, loss: 987.78, accuracy: 0.5477
epoch: 71, loss: 987.45, accuracy: 0.5433
epoch: 72, loss: 985.09, accuracy: 0.5432
epoch: 73, loss: 986.95, accuracy: 0.544
epoch: 74, loss: 985.13, accuracy: 0.543
epoch: 75, loss: 986.77, accuracy: 0.5452
epoch: 76, loss: 984.98, accuracy: 0.5435
epoch: 77, loss: 986.87, accuracy: 0.5443
Epoch    78: reducing learning rate of group 0 to 2.5000e-04.
epoch: 78, loss: 985.7, accuracy: 0.5438
epoch: 79, loss: 984.18, accuracy: 0.545
epoch: 80, loss: 984.4, accuracy: 0.5466
epoch: 81, loss: 982.95, accuracy: 0.5462
epoch: 82, loss: 984.41, accuracy: 0.5461
epoch: 83, loss: 983.32, accuracy: 0.544
epoch: 84, loss: 984.18, accuracy: 0.5436
epoch: 85, loss: 983.31, accuracy: 0.547
epoch: 86, loss: 983.09, accuracy: 0.5453
epoch: 87, loss: 983.01, accuracy: 0.5431
epoch: 88, loss: 984.14, accuracy: 0.5444
Epoch    89: reducing learning rate of group 0 to 1.2500e-04.
epoch: 89, loss: 984.09, accuracy: 0.5444
epoch: 90, loss: 982.81, accuracy: 0.5463
epoch: 91, loss: 981.82, accuracy: 0.5442
epoch: 92, loss: 982.27, accuracy: 0.5449
epoch: 93, loss: 982.27, accuracy: 0.5467
epoch: 94, loss: 982.22, accuracy: 0.5457
epoch: 95, loss: 979.76, accuracy: 0.5458
epoch: 96, loss: 980.38, accuracy: 0.5467
epoch: 97, loss: 980.58, accuracy: 0.5478
epoch: 98, loss: 980.06, accuracy: 0.5467
epoch: 99, loss: 981.9, accuracy: 0.5453
time analysis:
    train 1147.0741 s
    all 1149.3828 s
Accuracy of     0 : 62 %
Accuracy of     1 : 64 %
Accuracy of     2 : 32 %
Accuracy of     3 : 27 %
Accuracy of     4 : 40 %
Accuracy of     5 : 44 %
Accuracy of     6 : 66 %
Accuracy of     7 : 51 %
Accuracy of     8 : 63 %
Accuracy of     9 : 58 %
```
