start: 2019-07-02 13:24:33.745473
* CIFAR10, 62k_params, Conv2d
* CIFAR10, 62k_params, DAUConv2di, units: 2
* CIFAR10, 62k_params, DAUConv2d, units: 2
* CIFAR10, 62k_params, DAUConv2dj, units: 2

## CIFAR10, 62k_params, Conv2d
```text
Config: {'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(32, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv3): Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 32, 32, 32]             896
       BatchNorm2d-2           [-1, 32, 32, 32]              64
         MaxPool2d-3           [-1, 32, 16, 16]               0
            Conv2d-4           [-1, 64, 16, 16]          18,496
       BatchNorm2d-5           [-1, 64, 16, 16]             128
         MaxPool2d-6             [-1, 64, 8, 8]               0
            Conv2d-7            [-1, 128, 8, 8]          73,856
       BatchNorm2d-8            [-1, 128, 8, 8]             256
         MaxPool2d-9            [-1, 128, 4, 4]               0
           Linear-10                   [-1, 10]          20,490
================================================================
total params: 114,186
DAU params: 0
other params: 114,186
----------------------------------------------------------------
epoch: 0, loss: 1081.64, accuracy: 0.6021
epoch: 1, loss: 739.38, accuracy: 0.6937
epoch: 2, loss: 629.98, accuracy: 0.705
epoch: 3, loss: 570.67, accuracy: 0.7054
epoch: 4, loss: 520.58, accuracy: 0.7314
epoch: 5, loss: 482.44, accuracy: 0.7106
epoch: 6, loss: 453.26, accuracy: 0.725
epoch: 7, loss: 423.98, accuracy: 0.7428
epoch: 8, loss: 397.84, accuracy: 0.7426
epoch: 9, loss: 369.68, accuracy: 0.7457
epoch: 10, loss: 355.05, accuracy: 0.7351
epoch: 11, loss: 336.3, accuracy: 0.733
epoch: 12, loss: 318.92, accuracy: 0.7381
epoch: 13, loss: 302.76, accuracy: 0.7265
epoch: 14, loss: 284.72, accuracy: 0.7254
epoch: 15, loss: 273.28, accuracy: 0.7253
epoch: 16, loss: 263.51, accuracy: 0.7405
epoch: 17, loss: 247.05, accuracy: 0.734
epoch: 18, loss: 239.65, accuracy: 0.7312
epoch: 19, loss: 229.96, accuracy: 0.735
epoch: 20, loss: 221.17, accuracy: 0.7299
epoch: 21, loss: 216.02, accuracy: 0.7364
epoch: 22, loss: 208.53, accuracy: 0.7363
epoch: 23, loss: 199.22, accuracy: 0.7315
epoch: 24, loss: 201.33, accuracy: 0.7348
epoch: 25, loss: 186.77, accuracy: 0.7295
epoch: 26, loss: 186.86, accuracy: 0.7333
epoch: 27, loss: 174.91, accuracy: 0.7319
epoch: 28, loss: 170.6, accuracy: 0.7177
epoch: 29, loss: 174.06, accuracy: 0.7338
epoch: 30, loss: 165.19, accuracy: 0.7262
epoch: 31, loss: 168.46, accuracy: 0.7338
epoch: 32, loss: 157.18, accuracy: 0.7311
epoch: 33, loss: 154.51, accuracy: 0.7312
epoch: 34, loss: 149.11, accuracy: 0.7351
epoch: 35, loss: 154.01, accuracy: 0.7324
epoch: 36, loss: 150.97, accuracy: 0.7347
epoch: 37, loss: 144.13, accuracy: 0.7309
epoch: 38, loss: 143.31, accuracy: 0.7302
epoch: 39, loss: 138.25, accuracy: 0.7299
epoch: 40, loss: 130.51, accuracy: 0.7265
epoch: 41, loss: 142.86, accuracy: 0.7275
epoch: 42, loss: 140.18, accuracy: 0.7277
epoch: 43, loss: 130.47, accuracy: 0.73
epoch: 44, loss: 129.53, accuracy: 0.3827
epoch: 45, loss: 135.18, accuracy: 0.7291
epoch: 46, loss: 125.44, accuracy: 0.7318
epoch: 47, loss: 125.23, accuracy: 0.7294
epoch: 48, loss: 127.76, accuracy: 0.7211
epoch: 49, loss: 121.43, accuracy: 0.7166
epoch: 50, loss: 128.61, accuracy: 0.7342
epoch: 51, loss: 113.98, accuracy: 0.7258
epoch: 52, loss: 122.53, accuracy: 0.7235
epoch: 53, loss: 121.15, accuracy: 0.7318
epoch: 54, loss: 122.13, accuracy: 0.7204
epoch: 55, loss: 116.34, accuracy: 0.7244
epoch: 56, loss: 120.59, accuracy: 0.7354
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 113.9, accuracy: 0.7236
epoch: 58, loss: 44.55, accuracy: 0.7422
epoch: 59, loss: 25.75, accuracy: 0.7449
epoch: 60, loss: 31.0, accuracy: 0.7387
epoch: 61, loss: 30.8, accuracy: 0.7454
epoch: 62, loss: 33.99, accuracy: 0.738
epoch: 63, loss: 40.62, accuracy: 0.7447
epoch: 64, loss: 28.49, accuracy: 0.7456
epoch: 65, loss: 34.46, accuracy: 0.7412
epoch: 66, loss: 38.91, accuracy: 0.741
epoch: 67, loss: 28.95, accuracy: 0.739
Epoch    68: reducing learning rate of group 0 to 2.5000e-03.
epoch: 68, loss: 33.41, accuracy: 0.7366
epoch: 69, loss: 14.06, accuracy: 0.7479
epoch: 70, loss: 8.94, accuracy: 0.7471
epoch: 71, loss: 8.28, accuracy: 0.7445
epoch: 72, loss: 8.39, accuracy: 0.7462
epoch: 73, loss: 7.99, accuracy: 0.7498
epoch: 74, loss: 9.98, accuracy: 0.7452
epoch: 75, loss: 5.59, accuracy: 0.7459
epoch: 76, loss: 3.74, accuracy: 0.75
epoch: 77, loss: 3.17, accuracy: 0.7519
epoch: 78, loss: 2.92, accuracy: 0.7498
epoch: 79, loss: 2.86, accuracy: 0.7528
epoch: 80, loss: 2.45, accuracy: 0.7478
epoch: 81, loss: 2.14, accuracy: 0.7485
epoch: 82, loss: 2.13, accuracy: 0.7505
epoch: 83, loss: 2.34, accuracy: 0.7523
epoch: 84, loss: 2.02, accuracy: 0.7505
epoch: 85, loss: 2.03, accuracy: 0.7525
epoch: 86, loss: 1.83, accuracy: 0.7524
epoch: 87, loss: 1.7, accuracy: 0.7519
epoch: 88, loss: 1.85, accuracy: 0.7512
epoch: 89, loss: 1.52, accuracy: 0.753
epoch: 90, loss: 1.61, accuracy: 0.7514
epoch: 91, loss: 1.55, accuracy: 0.7526
epoch: 92, loss: 1.45, accuracy: 0.7533
epoch: 93, loss: 1.42, accuracy: 0.7527
epoch: 94, loss: 1.58, accuracy: 0.7516
epoch: 95, loss: 1.62, accuracy: 0.7532
epoch: 96, loss: 1.37, accuracy: 0.7518
epoch: 97, loss: 1.37, accuracy: 0.7527
epoch: 98, loss: 1.33, accuracy: 0.7521
epoch: 99, loss: 1.48, accuracy: 0.7534
time analysis:
    train 600.0134 s
Accuracy of     0 : 85 %
Accuracy of     1 : 90 %
Accuracy of     2 : 72 %
Accuracy of     3 : 47 %
Accuracy of     4 : 67 %
Accuracy of     5 : 66 %
Accuracy of     6 : 73 %
Accuracy of     7 : 76 %
Accuracy of     8 : 84 %
Accuracy of     9 : 74 %
```

## CIFAR10, 62k_params, DAUConv2di, units: 2
```text
Config: {'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=64, no_units=2, sigma=0.5)
  (conv3): DAUConv2di(in_channels=64, out_channels=128, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 32, 32]             236
       BatchNorm2d-2           [-1, 32, 32, 32]              64
         MaxPool2d-3           [-1, 32, 16, 16]               0
        DAUConv2di-4           [-1, 64, 16, 16]           4,288
       BatchNorm2d-5           [-1, 64, 16, 16]             128
         MaxPool2d-6             [-1, 64, 8, 8]               0
        DAUConv2di-7            [-1, 128, 8, 8]          16,768
       BatchNorm2d-8            [-1, 128, 8, 8]             256
         MaxPool2d-9            [-1, 128, 4, 4]               0
           Linear-10                   [-1, 10]          20,490
================================================================
total params: 42,230
DAU params: 21,292
other params: 20,938
----------------------------------------------------------------
epoch: 0, loss: 1156.28, accuracy: 0.5414
epoch: 1, loss: 898.87, accuracy: 0.5829
epoch: 2, loss: 809.24, accuracy: 0.6348
epoch: 3, loss: 756.61, accuracy: 0.6417
epoch: 4, loss: 715.88, accuracy: 0.6355
epoch: 5, loss: 685.13, accuracy: 0.6522
epoch: 6, loss: 660.79, accuracy: 0.664
epoch: 7, loss: 630.41, accuracy: 0.6816
epoch: 8, loss: 622.84, accuracy: 0.692
epoch: 9, loss: 605.99, accuracy: 0.6909
epoch: 10, loss: 593.4, accuracy: 0.6942
epoch: 11, loss: 582.48, accuracy: 0.6753
epoch: 12, loss: 567.87, accuracy: 0.6963
epoch: 13, loss: 565.77, accuracy: 0.6922
epoch: 14, loss: 549.27, accuracy: 0.6923
epoch: 15, loss: 543.75, accuracy: 0.6884
epoch: 16, loss: 534.19, accuracy: 0.6928
epoch: 17, loss: 525.99, accuracy: 0.6981
epoch: 18, loss: 522.11, accuracy: 0.6889
epoch: 19, loss: 515.96, accuracy: 0.6993
epoch: 20, loss: 510.74, accuracy: 0.6953
epoch: 21, loss: 500.23, accuracy: 0.6983
epoch: 22, loss: 497.01, accuracy: 0.6943
epoch: 23, loss: 491.44, accuracy: 0.6915
epoch: 24, loss: 490.83, accuracy: 0.6996
epoch: 25, loss: 484.63, accuracy: 0.6956
epoch: 26, loss: 484.05, accuracy: 0.6916
epoch: 27, loss: 477.1, accuracy: 0.6954
epoch: 28, loss: 475.14, accuracy: 0.7096
epoch: 29, loss: 471.71, accuracy: 0.7023
epoch: 30, loss: 462.34, accuracy: 0.7009
epoch: 31, loss: 462.04, accuracy: 0.7101
epoch: 32, loss: 461.28, accuracy: 0.7105
epoch: 33, loss: 454.35, accuracy: 0.7104
epoch: 34, loss: 455.88, accuracy: 0.718
epoch: 35, loss: 451.83, accuracy: 0.7031
epoch: 36, loss: 450.73, accuracy: 0.688
epoch: 37, loss: 447.27, accuracy: 0.7018
epoch: 38, loss: 441.75, accuracy: 0.7022
epoch: 39, loss: 439.88, accuracy: 0.714
epoch: 40, loss: 437.1, accuracy: 0.7088
epoch: 41, loss: 437.15, accuracy: 0.7141
epoch: 42, loss: 430.98, accuracy: 0.6999
epoch: 43, loss: 434.94, accuracy: 0.7023
epoch: 44, loss: 433.55, accuracy: 0.7053
epoch: 45, loss: 423.91, accuracy: 0.697
epoch: 46, loss: 423.21, accuracy: 0.7035
epoch: 47, loss: 425.58, accuracy: 0.7089
epoch: 48, loss: 420.25, accuracy: 0.7136
epoch: 49, loss: 422.36, accuracy: 0.705
epoch: 50, loss: 418.08, accuracy: 0.7046
epoch: 51, loss: 415.33, accuracy: 0.7025
epoch: 52, loss: 415.04, accuracy: 0.7113
epoch: 53, loss: 412.04, accuracy: 0.6991
epoch: 54, loss: 411.96, accuracy: 0.7042
epoch: 55, loss: 405.39, accuracy: 0.7082
epoch: 56, loss: 406.28, accuracy: 0.6973
epoch: 57, loss: 408.05, accuracy: 0.7091
epoch: 58, loss: 400.86, accuracy: 0.7089
epoch: 59, loss: 399.37, accuracy: 0.7019
epoch: 60, loss: 408.49, accuracy: 0.7062
epoch: 61, loss: 398.48, accuracy: 0.7118
epoch: 62, loss: 396.88, accuracy: 0.7127
epoch: 63, loss: 396.32, accuracy: 0.6883
epoch: 64, loss: 394.95, accuracy: 0.7082
epoch: 65, loss: 390.86, accuracy: 0.7108
epoch: 66, loss: 394.1, accuracy: 0.7018
epoch: 67, loss: 397.34, accuracy: 0.704
epoch: 68, loss: 390.0, accuracy: 0.6978
epoch: 69, loss: 385.98, accuracy: 0.7103
epoch: 70, loss: 386.09, accuracy: 0.7152
epoch: 71, loss: 383.37, accuracy: 0.7136
epoch: 72, loss: 388.5, accuracy: 0.6996
epoch: 73, loss: 384.42, accuracy: 0.7105
epoch: 74, loss: 386.61, accuracy: 0.7146
epoch: 75, loss: 271.47, accuracy: 0.7326
epoch: 76, loss: 246.9, accuracy: 0.7313
epoch: 77, loss: 239.04, accuracy: 0.7314
epoch: 78, loss: 234.05, accuracy: 0.7316
epoch: 79, loss: 230.26, accuracy: 0.731
epoch: 80, loss: 227.64, accuracy: 0.7326
epoch: 81, loss: 226.48, accuracy: 0.7324
epoch: 82, loss: 222.29, accuracy: 0.7306
epoch: 83, loss: 219.34, accuracy: 0.731
epoch: 84, loss: 217.95, accuracy: 0.729
epoch: 85, loss: 217.6, accuracy: 0.731
epoch: 86, loss: 215.25, accuracy: 0.7301
epoch: 87, loss: 212.65, accuracy: 0.7302
epoch: 88, loss: 212.96, accuracy: 0.7303
epoch: 89, loss: 210.72, accuracy: 0.7296
epoch: 90, loss: 209.0, accuracy: 0.7306
epoch: 91, loss: 207.35, accuracy: 0.7316
epoch: 92, loss: 207.54, accuracy: 0.7312
epoch: 93, loss: 205.12, accuracy: 0.7288
epoch: 94, loss: 205.08, accuracy: 0.7319
epoch: 95, loss: 204.08, accuracy: 0.7286
epoch: 96, loss: 204.56, accuracy: 0.7278
epoch: 97, loss: 201.61, accuracy: 0.7275
epoch: 98, loss: 199.9, accuracy: 0.7261
epoch: 99, loss: 199.57, accuracy: 0.7282
time analysis:
    train 2037.8838 s
    all 2039.6024 s
Accuracy of     0 : 82 %
Accuracy of     1 : 86 %
Accuracy of     2 : 69 %
Accuracy of     3 : 57 %
Accuracy of     4 : 61 %
Accuracy of     5 : 57 %
Accuracy of     6 : 69 %
Accuracy of     7 : 70 %
Accuracy of     8 : 89 %
Accuracy of     9 : 82 %
```

## CIFAR10, 62k_params, DAUConv2d, units: 2
```text
Config: {'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=64, no_units=2, sigma=0.5)
  (conv3): DAUConv2d(in_channels=64, out_channels=128, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 32, 32]             608
       BatchNorm2d-2           [-1, 32, 32, 32]              64
         MaxPool2d-3           [-1, 32, 16, 16]               0
         DAUConv2d-4           [-1, 64, 16, 16]          12,352
       BatchNorm2d-5           [-1, 64, 16, 16]             128
         MaxPool2d-6             [-1, 64, 8, 8]               0
         DAUConv2d-7            [-1, 128, 8, 8]          49,280
       BatchNorm2d-8            [-1, 128, 8, 8]             256
         MaxPool2d-9            [-1, 128, 4, 4]               0
           Linear-10                   [-1, 10]          20,490
================================================================
total params: 83,178
DAU params: 62,240
other params: 20,938
----------------------------------------------------------------
epoch: 0, loss: 1069.19, accuracy: 0.6158
epoch: 1, loss: 768.37, accuracy: 0.6456
epoch: 2, loss: 679.81, accuracy: 0.6866
epoch: 3, loss: 620.81, accuracy: 0.6958
epoch: 4, loss: 586.43, accuracy: 0.7208
epoch: 5, loss: 555.37, accuracy: 0.721
epoch: 6, loss: 534.17, accuracy: 0.6986
epoch: 7, loss: 520.5, accuracy: 0.7319
epoch: 8, loss: 498.24, accuracy: 0.7112
epoch: 9, loss: 486.75, accuracy: 0.7201
epoch: 10, loss: 474.08, accuracy: 0.7233
epoch: 11, loss: 459.67, accuracy: 0.7356
epoch: 12, loss: 448.0, accuracy: 0.7426
epoch: 13, loss: 442.0, accuracy: 0.7407
epoch: 14, loss: 430.46, accuracy: 0.716
epoch: 15, loss: 421.28, accuracy: 0.734
epoch: 16, loss: 418.98, accuracy: 0.7347
epoch: 17, loss: 402.79, accuracy: 0.7517
epoch: 18, loss: 395.69, accuracy: 0.7383
epoch: 19, loss: 388.99, accuracy: 0.7394
epoch: 20, loss: 387.09, accuracy: 0.7429
epoch: 21, loss: 377.61, accuracy: 0.7437
epoch: 22, loss: 368.28, accuracy: 0.7456
epoch: 23, loss: 371.21, accuracy: 0.722
epoch: 24, loss: 360.19, accuracy: 0.7395
epoch: 25, loss: 362.84, accuracy: 0.7415
epoch: 26, loss: 349.73, accuracy: 0.7309
epoch: 27, loss: 352.7, accuracy: 0.738
epoch: 28, loss: 346.36, accuracy: 0.7335
epoch: 29, loss: 347.85, accuracy: 0.7359
epoch: 30, loss: 339.37, accuracy: 0.7486
epoch: 31, loss: 332.43, accuracy: 0.7351
epoch: 32, loss: 333.05, accuracy: 0.7421
epoch: 33, loss: 327.67, accuracy: 0.7449
epoch: 34, loss: 325.22, accuracy: 0.7365
epoch: 35, loss: 320.58, accuracy: 0.7463
epoch: 36, loss: 320.27, accuracy: 0.7394
epoch: 37, loss: 317.49, accuracy: 0.7339
epoch: 38, loss: 313.65, accuracy: 0.7395
epoch: 39, loss: 311.99, accuracy: 0.7414
epoch: 40, loss: 310.45, accuracy: 0.7454
epoch: 41, loss: 312.36, accuracy: 0.7305
epoch: 42, loss: 302.13, accuracy: 0.7463
epoch: 43, loss: 306.54, accuracy: 0.7368
epoch: 44, loss: 302.82, accuracy: 0.7413
epoch: 45, loss: 295.97, accuracy: 0.7338
epoch: 46, loss: 297.79, accuracy: 0.7337
epoch: 47, loss: 292.9, accuracy: 0.7392
epoch: 48, loss: 286.82, accuracy: 0.7382
epoch: 49, loss: 294.35, accuracy: 0.7368
epoch: 50, loss: 293.62, accuracy: 0.7327
epoch: 51, loss: 286.8, accuracy: 0.7281
epoch: 52, loss: 285.72, accuracy: 0.7324
epoch: 53, loss: 284.42, accuracy: 0.7446
epoch: 54, loss: 282.25, accuracy: 0.7371
epoch: 55, loss: 283.6, accuracy: 0.7367
epoch: 56, loss: 277.0, accuracy: 0.7416
epoch: 57, loss: 280.02, accuracy: 0.7463
epoch: 58, loss: 273.04, accuracy: 0.7407
epoch: 59, loss: 277.12, accuracy: 0.7411
epoch: 60, loss: 273.99, accuracy: 0.7348
epoch: 61, loss: 276.73, accuracy: 0.7416
epoch: 62, loss: 272.83, accuracy: 0.7124
epoch: 63, loss: 264.99, accuracy: 0.7407
epoch: 64, loss: 268.82, accuracy: 0.7379
epoch: 65, loss: 262.9, accuracy: 0.7501
epoch: 66, loss: 265.42, accuracy: 0.7432
epoch: 67, loss: 265.41, accuracy: 0.7474
epoch: 68, loss: 263.02, accuracy: 0.7352
epoch: 69, loss: 255.34, accuracy: 0.7378
epoch: 70, loss: 264.4, accuracy: 0.7432
epoch: 71, loss: 259.4, accuracy: 0.7509
epoch: 72, loss: 261.87, accuracy: 0.7405
epoch: 73, loss: 260.79, accuracy: 0.7332
epoch: 74, loss: 255.72, accuracy: 0.7475
epoch: 75, loss: 142.47, accuracy: 0.7654
epoch: 76, loss: 104.97, accuracy: 0.7677
epoch: 77, loss: 90.09, accuracy: 0.7672
epoch: 78, loss: 81.05, accuracy: 0.7657
epoch: 79, loss: 72.22, accuracy: 0.7691
epoch: 80, loss: 67.57, accuracy: 0.7668
epoch: 81, loss: 63.31, accuracy: 0.7684
epoch: 82, loss: 57.93, accuracy: 0.7658
epoch: 83, loss: 55.18, accuracy: 0.7637
epoch: 84, loss: 53.09, accuracy: 0.7663
epoch: 85, loss: 49.36, accuracy: 0.7668
epoch: 86, loss: 48.1, accuracy: 0.7662
epoch: 87, loss: 44.38, accuracy: 0.7651
epoch: 88, loss: 43.02, accuracy: 0.7678
epoch: 89, loss: 41.32, accuracy: 0.7661
epoch: 90, loss: 40.47, accuracy: 0.7632
epoch: 91, loss: 38.93, accuracy: 0.7657
epoch: 92, loss: 36.77, accuracy: 0.7656
epoch: 93, loss: 35.13, accuracy: 0.7659
epoch: 94, loss: 33.56, accuracy: 0.7663
epoch: 95, loss: 32.86, accuracy: 0.7664
epoch: 96, loss: 31.61, accuracy: 0.7643
epoch: 97, loss: 31.05, accuracy: 0.7645
epoch: 98, loss: 30.2, accuracy: 0.7648
epoch: 99, loss: 29.29, accuracy: 0.7648
time analysis:
    train 44736.8126 s
    all 44739.5869 s
Accuracy of     0 : 83 %
Accuracy of     1 : 86 %
Accuracy of     2 : 69 %
Accuracy of     3 : 56 %
Accuracy of     4 : 81 %
Accuracy of     5 : 62 %
Accuracy of     6 : 83 %
Accuracy of     7 : 85 %
Accuracy of     8 : 81 %
Accuracy of     9 : 80 %
```

## CIFAR10, 62k_params, DAUConv2dj, units: 2
```text
Config: {'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=64, no_units=2, sigma=0.5)
  (conv3): DAUConv2dj(in_channels=64, out_channels=128, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 32, 32]             352
       BatchNorm2d-2           [-1, 32, 32, 32]              64
         MaxPool2d-3           [-1, 32, 16, 16]               0
        DAUConv2dj-4           [-1, 64, 16, 16]           4,416
       BatchNorm2d-5           [-1, 64, 16, 16]             128
         MaxPool2d-6             [-1, 64, 8, 8]               0
        DAUConv2dj-7            [-1, 128, 8, 8]          17,024
       BatchNorm2d-8            [-1, 128, 8, 8]             256
         MaxPool2d-9            [-1, 128, 4, 4]               0
           Linear-10                   [-1, 10]          20,490
================================================================
total params: 42,730
DAU params: 21,792
other params: 20,938
----------------------------------------------------------------
epoch: 0, loss: 1114.75, accuracy: 0.5258
epoch: 1, loss: 852.21, accuracy: 0.6374
epoch: 2, loss: 767.95, accuracy: 0.6175
epoch: 3, loss: 719.33, accuracy: 0.6383
epoch: 4, loss: 689.76, accuracy: 0.6827
epoch: 5, loss: 662.34, accuracy: 0.686
epoch: 6, loss: 645.52, accuracy: 0.6599
epoch: 7, loss: 627.17, accuracy: 0.6785
epoch: 8, loss: 611.39, accuracy: 0.6872
epoch: 9, loss: 601.24, accuracy: 0.6735
epoch: 10, loss: 591.11, accuracy: 0.6765
epoch: 11, loss: 579.94, accuracy: 0.7064
epoch: 12, loss: 572.71, accuracy: 0.6873
epoch: 13, loss: 562.49, accuracy: 0.6946
epoch: 14, loss: 556.2, accuracy: 0.7011
epoch: 15, loss: 548.61, accuracy: 0.7087
epoch: 16, loss: 542.77, accuracy: 0.7177
epoch: 17, loss: 531.47, accuracy: 0.7086
epoch: 18, loss: 533.18, accuracy: 0.7177
epoch: 19, loss: 523.39, accuracy: 0.7208
epoch: 20, loss: 517.26, accuracy: 0.7283
epoch: 21, loss: 513.0, accuracy: 0.7172
epoch: 22, loss: 507.29, accuracy: 0.714
epoch: 23, loss: 503.79, accuracy: 0.7255
epoch: 24, loss: 499.51, accuracy: 0.7111
epoch: 25, loss: 496.69, accuracy: 0.7099
epoch: 26, loss: 492.45, accuracy: 0.7174
epoch: 27, loss: 492.37, accuracy: 0.7267
epoch: 28, loss: 486.31, accuracy: 0.7234
epoch: 29, loss: 483.04, accuracy: 0.724
epoch: 30, loss: 481.3, accuracy: 0.7239
epoch: 31, loss: 477.7, accuracy: 0.7096
epoch: 32, loss: 474.56, accuracy: 0.72
epoch: 33, loss: 467.4, accuracy: 0.7247
epoch: 34, loss: 471.2, accuracy: 0.6505
epoch: 35, loss: 465.86, accuracy: 0.7173
epoch: 36, loss: 461.49, accuracy: 0.7194
epoch: 37, loss: 460.39, accuracy: 0.7159
epoch: 38, loss: 457.92, accuracy: 0.7187
epoch: 39, loss: 457.99, accuracy: 0.7245
epoch: 40, loss: 455.52, accuracy: 0.7082
epoch: 41, loss: 456.14, accuracy: 0.713
epoch: 42, loss: 452.09, accuracy: 0.731
epoch: 43, loss: 451.57, accuracy: 0.7301
epoch: 44, loss: 448.22, accuracy: 0.7275
epoch: 45, loss: 446.05, accuracy: 0.7318
epoch: 46, loss: 439.94, accuracy: 0.7172
epoch: 47, loss: 441.51, accuracy: 0.7391
epoch: 48, loss: 438.9, accuracy: 0.7362
epoch: 49, loss: 431.31, accuracy: 0.7281
epoch: 50, loss: 435.77, accuracy: 0.7163
