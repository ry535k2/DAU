start: 2019-07-03 13:02:55.174824
* CIFAR10, paper, Conv2d
* CIFAR10, paper, DAUConv2di, units: 2
* CIFAR10, paper, DAUConv2d, units: 2
* CIFAR10, paper, DAUConv2dj, units: 2
* CIFAR10, paper, DAUConv2dOneMu, units: 2

## CIFAR10, paper, Conv2d
```text
Config: {'num_workers': 10, 'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 96, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(96, 96, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv3): Conv2d(96, 192, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(192, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=3072, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 96, 32, 32]           2,688
       BatchNorm2d-2           [-1, 96, 32, 32]             192
         MaxPool2d-3           [-1, 96, 16, 16]               0
            Conv2d-4           [-1, 96, 16, 16]          83,040
       BatchNorm2d-5           [-1, 96, 16, 16]             192
         MaxPool2d-6             [-1, 96, 8, 8]               0
            Conv2d-7            [-1, 192, 8, 8]         166,080
       BatchNorm2d-8            [-1, 192, 8, 8]             384
         MaxPool2d-9            [-1, 192, 4, 4]               0
           Linear-10                   [-1, 10]          30,730
================================================================
total params: 283,306
DAU params: 0
other params: 283,306
----------------------------------------------------------------
epoch: 0, loss: 1005.75, accuracy: 0.6633
epoch: 1, loss: 634.07, accuracy: 0.7077
epoch: 2, loss: 516.01, accuracy: 0.7401
epoch: 3, loss: 439.48, accuracy: 0.7599
epoch: 4, loss: 369.34, accuracy: 0.755
epoch: 5, loss: 314.83, accuracy: 0.7636
epoch: 6, loss: 262.97, accuracy: 0.7771
epoch: 7, loss: 218.06, accuracy: 0.7562
epoch: 8, loss: 183.2, accuracy: 0.7777
epoch: 9, loss: 151.08, accuracy: 0.7672
epoch: 10, loss: 136.0, accuracy: 0.7789
epoch: 11, loss: 113.63, accuracy: 0.7645
epoch: 12, loss: 110.23, accuracy: 0.7606
epoch: 13, loss: 99.45, accuracy: 0.7703
epoch: 14, loss: 92.98, accuracy: 0.7667
epoch: 15, loss: 77.72, accuracy: 0.7638
epoch: 16, loss: 83.53, accuracy: 0.7702
epoch: 17, loss: 74.75, accuracy: 0.7663
epoch: 18, loss: 71.87, accuracy: 0.7719
epoch: 19, loss: 67.13, accuracy: 0.7558
epoch: 20, loss: 68.99, accuracy: 0.7744
epoch: 21, loss: 63.6, accuracy: 0.7738
epoch: 22, loss: 63.36, accuracy: 0.7599
epoch: 23, loss: 51.39, accuracy: 0.7691
epoch: 24, loss: 58.43, accuracy: 0.7629
epoch: 25, loss: 59.54, accuracy: 0.7652
epoch: 26, loss: 56.09, accuracy: 0.7681
epoch: 27, loss: 45.13, accuracy: 0.7716
epoch: 28, loss: 57.59, accuracy: 0.7566
epoch: 29, loss: 56.34, accuracy: 0.7667
epoch: 30, loss: 52.95, accuracy: 0.7712
epoch: 31, loss: 40.98, accuracy: 0.7667
epoch: 32, loss: 52.36, accuracy: 0.7709
epoch: 33, loss: 43.61, accuracy: 0.7727
epoch: 34, loss: 51.15, accuracy: 0.7736
epoch: 35, loss: 47.8, accuracy: 0.7678
epoch: 36, loss: 46.54, accuracy: 0.7786
Epoch    37: reducing learning rate of group 0 to 2.5000e-03.
epoch: 37, loss: 42.9, accuracy: 0.768
epoch: 38, loss: 13.96, accuracy: 0.7879
epoch: 39, loss: 6.36, accuracy: 0.7856
epoch: 40, loss: 5.44, accuracy: 0.7847
epoch: 41, loss: 9.91, accuracy: 0.7811
epoch: 42, loss: 12.58, accuracy: 0.7848
epoch: 43, loss: 7.63, accuracy: 0.7793
epoch: 44, loss: 9.5, accuracy: 0.781
epoch: 45, loss: 10.69, accuracy: 0.7853
epoch: 46, loss: 9.51, accuracy: 0.7863
epoch: 47, loss: 8.88, accuracy: 0.7775
Epoch    48: reducing learning rate of group 0 to 1.2500e-03.
epoch: 48, loss: 7.21, accuracy: 0.7871
epoch: 49, loss: 2.9, accuracy: 0.7896
epoch: 50, loss: 1.0, accuracy: 0.7865
epoch: 51, loss: 0.89, accuracy: 0.7917
epoch: 52, loss: 1.03, accuracy: 0.7865
epoch: 53, loss: 3.26, accuracy: 0.7863
epoch: 54, loss: 1.37, accuracy: 0.7898
epoch: 55, loss: 1.98, accuracy: 0.7874
epoch: 56, loss: 2.26, accuracy: 0.7874
epoch: 57, loss: 1.79, accuracy: 0.789
epoch: 58, loss: 1.05, accuracy: 0.7885
Epoch    59: reducing learning rate of group 0 to 6.2500e-04.
epoch: 59, loss: 2.15, accuracy: 0.7883
epoch: 60, loss: 0.62, accuracy: 0.7905
epoch: 61, loss: 0.38, accuracy: 0.7906
epoch: 62, loss: 0.37, accuracy: 0.7912
epoch: 63, loss: 0.37, accuracy: 0.7921
epoch: 64, loss: 0.38, accuracy: 0.7903
epoch: 65, loss: 0.48, accuracy: 0.79
epoch: 66, loss: 0.17, accuracy: 0.7921
epoch: 67, loss: 0.13, accuracy: 0.7919
epoch: 68, loss: 0.3, accuracy: 0.7882
epoch: 69, loss: 0.35, accuracy: 0.7926
epoch: 70, loss: 0.15, accuracy: 0.7939
epoch: 71, loss: 0.1, accuracy: 0.7935
epoch: 72, loss: 0.11, accuracy: 0.7928
epoch: 73, loss: 0.07, accuracy: 0.7938
epoch: 74, loss: 0.06, accuracy: 0.7944
epoch: 75, loss: 0.05, accuracy: 0.7949
epoch: 76, loss: 0.04, accuracy: 0.7937
epoch: 77, loss: 0.06, accuracy: 0.7943
epoch: 78, loss: 0.04, accuracy: 0.7934
epoch: 79, loss: 0.03, accuracy: 0.7957
epoch: 80, loss: 0.03, accuracy: 0.7937
epoch: 81, loss: 0.03, accuracy: 0.7942
epoch: 82, loss: 0.03, accuracy: 0.7949
epoch: 83, loss: 0.03, accuracy: 0.7942
epoch: 84, loss: 0.04, accuracy: 0.7959
epoch: 85, loss: 0.02, accuracy: 0.7951
epoch: 86, loss: 0.02, accuracy: 0.7956
epoch: 87, loss: 0.03, accuracy: 0.794
epoch: 88, loss: 0.05, accuracy: 0.7941
epoch: 89, loss: 0.02, accuracy: 0.7944
epoch: 90, loss: 0.02, accuracy: 0.7954
epoch: 91, loss: 0.02, accuracy: 0.7939
epoch: 92, loss: 0.02, accuracy: 0.7953
epoch: 93, loss: 0.03, accuracy: 0.7955
epoch: 94, loss: 0.02, accuracy: 0.7957
epoch: 95, loss: 0.02, accuracy: 0.7942
epoch: 96, loss: 0.02, accuracy: 0.7985
epoch: 97, loss: 0.02, accuracy: 0.796
epoch: 98, loss: 0.01, accuracy: 0.7954
epoch: 99, loss: 0.02, accuracy: 0.7959
time analysis:
    train 541.1763 s
Accuracy of     0 : 80 %
Accuracy of     1 : 88 %
Accuracy of     2 : 72 %
Accuracy of     3 : 63 %
Accuracy of     4 : 67 %
Accuracy of     5 : 71 %
Accuracy of     6 : 82 %
Accuracy of     7 : 78 %
Accuracy of     8 : 91 %
Accuracy of     9 : 84 %
```

## CIFAR10, paper, DAUConv2di, units: 2
```text
Config: {'num_workers': 10, 'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=96, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=96, out_channels=96, no_units=2, sigma=0.5)
  (conv3): DAUConv2di(in_channels=96, out_channels=192, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(192, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=3072, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 96, 32, 32]             684
       BatchNorm2d-2           [-1, 96, 32, 32]             192
         MaxPool2d-3           [-1, 96, 16, 16]               0
        DAUConv2di-4           [-1, 96, 16, 16]          18,912
       BatchNorm2d-5           [-1, 96, 16, 16]             192
         MaxPool2d-6             [-1, 96, 8, 8]               0
        DAUConv2di-7            [-1, 192, 8, 8]          37,440
       BatchNorm2d-8            [-1, 192, 8, 8]             384
         MaxPool2d-9            [-1, 192, 4, 4]               0
           Linear-10                   [-1, 10]          30,730
================================================================
total params: 88,534
DAU params: 57,036
other params: 31,498
----------------------------------------------------------------
epoch: 0, loss: 1178.64, accuracy: 0.5368
epoch: 1, loss: 903.47, accuracy: 0.577
epoch: 2, loss: 786.8, accuracy: 0.6361
epoch: 3, loss: 715.38, accuracy: 0.6679
epoch: 4, loss: 659.87, accuracy: 0.6832
epoch: 5, loss: 614.17, accuracy: 0.6746
epoch: 6, loss: 576.05, accuracy: 0.6973
epoch: 7, loss: 547.53, accuracy: 0.703
epoch: 8, loss: 517.44, accuracy: 0.7154
epoch: 9, loss: 496.5, accuracy: 0.6934
epoch: 10, loss: 474.23, accuracy: 0.7171
epoch: 11, loss: 450.52, accuracy: 0.7097
epoch: 12, loss: 435.38, accuracy: 0.7139
epoch: 13, loss: 414.63, accuracy: 0.7085
epoch: 14, loss: 397.82, accuracy: 0.7179
epoch: 15, loss: 388.0, accuracy: 0.7198
epoch: 16, loss: 370.57, accuracy: 0.7242
epoch: 17, loss: 359.65, accuracy: 0.7103
epoch: 18, loss: 344.55, accuracy: 0.7207
epoch: 19, loss: 338.42, accuracy: 0.7097
epoch: 20, loss: 323.97, accuracy: 0.7128
epoch: 21, loss: 313.27, accuracy: 0.7182
epoch: 22, loss: 304.85, accuracy: 0.718
epoch: 23, loss: 296.86, accuracy: 0.7206
epoch: 24, loss: 287.96, accuracy: 0.72
epoch: 25, loss: 280.91, accuracy: 0.7332
epoch: 26, loss: 271.12, accuracy: 0.7118
epoch: 27, loss: 267.83, accuracy: 0.7169
epoch: 28, loss: 259.5, accuracy: 0.7291
epoch: 29, loss: 250.4, accuracy: 0.7096
epoch: 30, loss: 247.25, accuracy: 0.7144
epoch: 31, loss: 245.82, accuracy: 0.7126
epoch: 32, loss: 235.88, accuracy: 0.7244
epoch: 33, loss: 231.39, accuracy: 0.7207
epoch: 34, loss: 230.03, accuracy: 0.7163
epoch: 35, loss: 225.23, accuracy: 0.7136
epoch: 36, loss: 216.03, accuracy: 0.704
epoch: 37, loss: 207.91, accuracy: 0.723
epoch: 38, loss: 204.82, accuracy: 0.7098
epoch: 39, loss: 204.3, accuracy: 0.7105
epoch: 40, loss: 199.04, accuracy: 0.717
epoch: 41, loss: 195.72, accuracy: 0.7146
epoch: 42, loss: 194.57, accuracy: 0.718
epoch: 43, loss: 190.79, accuracy: 0.7142
epoch: 44, loss: 188.84, accuracy: 0.7087
epoch: 45, loss: 182.69, accuracy: 0.7127
epoch: 46, loss: 182.79, accuracy: 0.707
epoch: 47, loss: 179.0, accuracy: 0.7036
epoch: 48, loss: 181.1, accuracy: 0.7106
epoch: 49, loss: 173.13, accuracy: 0.7204
epoch: 50, loss: 168.02, accuracy: 0.7154
epoch: 51, loss: 163.43, accuracy: 0.7151
epoch: 52, loss: 166.4, accuracy: 0.7069
epoch: 53, loss: 162.08, accuracy: 0.7139
epoch: 54, loss: 167.21, accuracy: 0.7161
epoch: 55, loss: 160.87, accuracy: 0.7169
epoch: 56, loss: 156.7, accuracy: 0.7162
epoch: 57, loss: 156.3, accuracy: 0.7118
epoch: 58, loss: 153.42, accuracy: 0.7195
epoch: 59, loss: 154.93, accuracy: 0.7205
epoch: 60, loss: 150.5, accuracy: 0.7111
epoch: 61, loss: 148.36, accuracy: 0.7133
epoch: 62, loss: 147.21, accuracy: 0.7125
epoch: 63, loss: 147.57, accuracy: 0.7106
epoch: 64, loss: 143.55, accuracy: 0.7175
epoch: 65, loss: 142.73, accuracy: 0.7108
epoch: 66, loss: 142.18, accuracy: 0.714
epoch: 67, loss: 139.01, accuracy: 0.717
epoch: 68, loss: 135.74, accuracy: 0.7174
epoch: 69, loss: 138.71, accuracy: 0.7189
epoch: 70, loss: 59.86, accuracy: 0.7337
epoch: 71, loss: 39.56, accuracy: 0.7302
epoch: 72, loss: 34.21, accuracy: 0.7291
epoch: 73, loss: 31.13, accuracy: 0.7288
epoch: 74, loss: 31.01, accuracy: 0.7287
epoch: 75, loss: 28.86, accuracy: 0.7289
epoch: 76, loss: 27.61, accuracy: 0.7283
epoch: 77, loss: 25.77, accuracy: 0.7288
epoch: 78, loss: 24.97, accuracy: 0.7315
epoch: 79, loss: 24.06, accuracy: 0.7289
epoch: 80, loss: 23.58, accuracy: 0.7318
epoch: 81, loss: 23.65, accuracy: 0.7286
epoch: 82, loss: 22.05, accuracy: 0.7339
epoch: 83, loss: 21.74, accuracy: 0.7293
epoch: 84, loss: 19.58, accuracy: 0.7308
epoch: 85, loss: 20.78, accuracy: 0.7287
epoch: 86, loss: 20.97, accuracy: 0.726
epoch: 87, loss: 19.53, accuracy: 0.7336
epoch: 88, loss: 18.91, accuracy: 0.7287
epoch: 89, loss: 19.54, accuracy: 0.7313
epoch: 90, loss: 18.04, accuracy: 0.7324
epoch: 91, loss: 19.03, accuracy: 0.7269
epoch: 92, loss: 17.53, accuracy: 0.7259
epoch: 93, loss: 16.84, accuracy: 0.7264
epoch: 94, loss: 17.68, accuracy: 0.7282
epoch: 95, loss: 12.51, accuracy: 0.7304
epoch: 96, loss: 10.67, accuracy: 0.7313
epoch: 97, loss: 9.66, accuracy: 0.73
epoch: 98, loss: 9.59, accuracy: 0.7293
epoch: 99, loss: 9.41, accuracy: 0.7328
time analysis:
    train 2184.3677 s
    all 2194.9393 s
Accuracy of     0 : 87 %
Accuracy of     1 : 84 %
Accuracy of     2 : 65 %
Accuracy of     3 : 53 %
Accuracy of     4 : 70 %
Accuracy of     5 : 67 %
Accuracy of     6 : 75 %
Accuracy of     7 : 68 %
Accuracy of     8 : 79 %
Accuracy of     9 : 73 %
```

## CIFAR10, paper, DAUConv2d, units: 2
```text
Config: {'num_workers': 10, 'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=96, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=96, out_channels=96, no_units=2, sigma=0.5)
  (conv3): DAUConv2d(in_channels=96, out_channels=192, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(192, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=3072, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 96, 32, 32]           1,824
       BatchNorm2d-2           [-1, 96, 32, 32]             192
         MaxPool2d-3           [-1, 96, 16, 16]               0
         DAUConv2d-4           [-1, 96, 16, 16]          55,392
       BatchNorm2d-5           [-1, 96, 16, 16]             192
         MaxPool2d-6             [-1, 96, 8, 8]               0
         DAUConv2d-7            [-1, 192, 8, 8]         110,784
       BatchNorm2d-8            [-1, 192, 8, 8]             384
         MaxPool2d-9            [-1, 192, 4, 4]               0
           Linear-10                   [-1, 10]          30,730
================================================================
total params: 199,498
DAU params: 168,000
other params: 31,498
----------------------------------------------------------------
epoch: 0, loss: 1037.22, accuracy: 0.6203
epoch: 1, loss: 726.46, accuracy: 0.6881
epoch: 2, loss: 613.96, accuracy: 0.6962
epoch: 3, loss: 540.09, accuracy: 0.7292
epoch: 4, loss: 488.36, accuracy: 0.7248
epoch: 5, loss: 445.34, accuracy: 0.7413
epoch: 6, loss: 407.73, accuracy: 0.7382
epoch: 7, loss: 370.62, accuracy: 0.7545
epoch: 8, loss: 346.5, accuracy: 0.7528
epoch: 9, loss: 321.69, accuracy: 0.7588
epoch: 10, loss: 295.89, accuracy: 0.7188
epoch: 11, loss: 275.06, accuracy: 0.7561
epoch: 12, loss: 253.28, accuracy: 0.7671
epoch: 13, loss: 241.46, accuracy: 0.765
epoch: 14, loss: 226.89, accuracy: 0.7579
epoch: 15, loss: 210.58, accuracy: 0.7629
epoch: 16, loss: 196.97, accuracy: 0.7583
epoch: 17, loss: 185.76, accuracy: 0.7593
epoch: 18, loss: 178.65, accuracy: 0.7524
epoch: 19, loss: 170.34, accuracy: 0.7476
epoch: 20, loss: 165.24, accuracy: 0.7617
epoch: 21, loss: 151.28, accuracy: 0.7525
epoch: 22, loss: 151.43, accuracy: 0.7512
epoch: 23, loss: 145.98, accuracy: 0.7495
epoch: 24, loss: 139.55, accuracy: 0.7496
epoch: 25, loss: 138.06, accuracy: 0.7693
epoch: 26, loss: 130.13, accuracy: 0.7697
epoch: 27, loss: 131.82, accuracy: 0.7567
epoch: 28, loss: 117.28, accuracy: 0.7542
epoch: 29, loss: 122.89, accuracy: 0.7607
epoch: 30, loss: 117.63, accuracy: 0.7658
epoch: 31, loss: 112.84, accuracy: 0.7663
epoch: 32, loss: 114.23, accuracy: 0.7664
epoch: 33, loss: 111.7, accuracy: 0.758
epoch: 34, loss: 109.15, accuracy: 0.7576
epoch: 35, loss: 98.84, accuracy: 0.7633
epoch: 36, loss: 108.66, accuracy: 0.7628
epoch: 37, loss: 113.67, accuracy: 0.7633
epoch: 38, loss: 98.96, accuracy: 0.7677
epoch: 39, loss: 101.06, accuracy: 0.7524
epoch: 40, loss: 93.78, accuracy: 0.7528
epoch: 41, loss: 97.36, accuracy: 0.7599
epoch: 42, loss: 103.27, accuracy: 0.7663
epoch: 43, loss: 99.79, accuracy: 0.7658
epoch: 44, loss: 88.95, accuracy: 0.7618
epoch: 45, loss: 94.63, accuracy: 0.7499
epoch: 46, loss: 92.88, accuracy: 0.7553
epoch: 47, loss: 88.43, accuracy: 0.7583
epoch: 48, loss: 92.45, accuracy: 0.7659
epoch: 49, loss: 90.37, accuracy: 0.754
epoch: 50, loss: 85.12, accuracy: 0.768
epoch: 51, loss: 89.44, accuracy: 0.7613
epoch: 52, loss: 82.37, accuracy: 0.7516
epoch: 53, loss: 82.98, accuracy: 0.7614
epoch: 54, loss: 92.38, accuracy: 0.7615
epoch: 55, loss: 90.6, accuracy: 0.7594
epoch: 56, loss: 83.22, accuracy: 0.7635
epoch: 57, loss: 78.53, accuracy: 0.7616
epoch: 58, loss: 78.76, accuracy: 0.7602
epoch: 59, loss: 84.61, accuracy: 0.7615
epoch: 60, loss: 88.84, accuracy: 0.764
epoch: 61, loss: 72.77, accuracy: 0.7643
epoch: 62, loss: 76.18, accuracy: 0.7589
epoch: 63, loss: 89.76, accuracy: 0.7504
epoch: 64, loss: 76.25, accuracy: 0.7573
epoch: 65, loss: 85.15, accuracy: 0.7601
epoch: 66, loss: 79.01, accuracy: 0.7654
Epoch    67: reducing learning rate of group 0 to 2.5000e-03.
epoch: 67, loss: 83.77, accuracy: 0.7688
epoch: 68, loss: 27.06, accuracy: 0.7785
epoch: 69, loss: 13.33, accuracy: 0.7789
epoch: 70, loss: 6.58, accuracy: 0.7854
epoch: 71, loss: 3.46, accuracy: 0.7872
epoch: 72, loss: 2.89, accuracy: 0.786
epoch: 73, loss: 2.54, accuracy: 0.7861
epoch: 74, loss: 2.27, accuracy: 0.7865
epoch: 75, loss: 2.08, accuracy: 0.7893
epoch: 76, loss: 2.58, accuracy: 0.7872
epoch: 77, loss: 2.53, accuracy: 0.7889
epoch: 78, loss: 1.59, accuracy: 0.7911
epoch: 79, loss: 1.55, accuracy: 0.7906
epoch: 80, loss: 1.57, accuracy: 0.7869
epoch: 81, loss: 1.52, accuracy: 0.7859
epoch: 82, loss: 1.79, accuracy: 0.7864
epoch: 83, loss: 1.12, accuracy: 0.7883
epoch: 84, loss: 1.27, accuracy: 0.7872
epoch: 85, loss: 1.23, accuracy: 0.7883
epoch: 86, loss: 1.24, accuracy: 0.7887
epoch: 87, loss: 1.16, accuracy: 0.7882
epoch: 88, loss: 1.03, accuracy: 0.7902
epoch: 89, loss: 1.08, accuracy: 0.7883
epoch: 90, loss: 0.96, accuracy: 0.7868
epoch: 91, loss: 1.14, accuracy: 0.7886
epoch: 92, loss: 1.05, accuracy: 0.7885
epoch: 93, loss: 0.92, accuracy: 0.7864
epoch: 94, loss: 1.06, accuracy: 0.7874
epoch: 95, loss: 0.71, accuracy: 0.7899
epoch: 96, loss: 0.48, accuracy: 0.788
epoch: 97, loss: 0.42, accuracy: 0.7886
epoch: 98, loss: 0.49, accuracy: 0.7896
epoch: 99, loss: 0.42, accuracy: 0.7908
time analysis:
    train 52562.5824 s
    all 52572.8152 s
Accuracy of     0 : 85 %
Accuracy of     1 : 94 %
Accuracy of     2 : 70 %
Accuracy of     3 : 63 %
Accuracy of     4 : 74 %
Accuracy of     5 : 69 %
Accuracy of     6 : 78 %
Accuracy of     7 : 81 %
Accuracy of     8 : 84 %
Accuracy of     9 : 88 %
```

## CIFAR10, paper, DAUConv2dj, units: 2
```text
Config: {'num_workers': 10, 'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=96, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=96, out_channels=96, no_units=2, sigma=0.5)
  (conv3): DAUConv2dj(in_channels=96, out_channels=192, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(192, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=3072, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 96, 32, 32]           1,056
       BatchNorm2d-2           [-1, 96, 32, 32]             192
         MaxPool2d-3           [-1, 96, 16, 16]               0
        DAUConv2dj-4           [-1, 96, 16, 16]          18,912
       BatchNorm2d-5           [-1, 96, 16, 16]             192
         MaxPool2d-6             [-1, 96, 8, 8]               0
        DAUConv2dj-7            [-1, 192, 8, 8]          37,824
       BatchNorm2d-8            [-1, 192, 8, 8]             384
         MaxPool2d-9            [-1, 192, 4, 4]               0
           Linear-10                   [-1, 10]          30,730
================================================================
total params: 89,290
DAU params: 57,792
other params: 31,498
----------------------------------------------------------------
epoch: 0, loss: 1093.13, accuracy: 0.5783
epoch: 1, loss: 810.89, accuracy: 0.6147
epoch: 2, loss: 705.94, accuracy: 0.6445
epoch: 3, loss: 646.24, accuracy: 0.6937
epoch: 4, loss: 603.42, accuracy: 0.6987
epoch: 5, loss: 561.07, accuracy: 0.7063
epoch: 6, loss: 534.52, accuracy: 0.7157
epoch: 7, loss: 506.99, accuracy: 0.7281
epoch: 8, loss: 486.31, accuracy: 0.7285
epoch: 9, loss: 464.89, accuracy: 0.73
epoch: 10, loss: 446.88, accuracy: 0.727
epoch: 11, loss: 423.7, accuracy: 0.7253
epoch: 12, loss: 408.97, accuracy: 0.7338
epoch: 13, loss: 397.31, accuracy: 0.7267
epoch: 14, loss: 385.69, accuracy: 0.7299
epoch: 15, loss: 365.65, accuracy: 0.7397
epoch: 16, loss: 356.53, accuracy: 0.7329
epoch: 17, loss: 347.58, accuracy: 0.7313
epoch: 18, loss: 335.15, accuracy: 0.7442
epoch: 19, loss: 324.11, accuracy: 0.7387
epoch: 20, loss: 319.63, accuracy: 0.718
epoch: 21, loss: 312.12, accuracy: 0.7296
epoch: 22, loss: 301.71, accuracy: 0.7324
epoch: 23, loss: 293.83, accuracy: 0.7446
epoch: 24, loss: 288.08, accuracy: 0.7365
epoch: 25, loss: 281.28, accuracy: 0.7403
epoch: 26, loss: 271.07, accuracy: 0.7211
epoch: 27, loss: 266.31, accuracy: 0.7387
epoch: 28, loss: 261.71, accuracy: 0.7365
epoch: 29, loss: 255.69, accuracy: 0.7317
epoch: 30, loss: 248.22, accuracy: 0.7321
epoch: 31, loss: 241.7, accuracy: 0.7377
epoch: 32, loss: 240.36, accuracy: 0.7413
epoch: 33, loss: 236.57, accuracy: 0.7395
epoch: 34, loss: 229.05, accuracy: 0.741
epoch: 35, loss: 228.0, accuracy: 0.7307
epoch: 36, loss: 224.45, accuracy: 0.7379
epoch: 37, loss: 215.93, accuracy: 0.7343
epoch: 38, loss: 214.19, accuracy: 0.7347
epoch: 39, loss: 213.55, accuracy: 0.7418
epoch: 40, loss: 206.57, accuracy: 0.738
epoch: 41, loss: 205.96, accuracy: 0.7386
epoch: 42, loss: 203.54, accuracy: 0.7302
epoch: 43, loss: 198.19, accuracy: 0.7395
epoch: 44, loss: 198.3, accuracy: 0.7359
epoch: 45, loss: 195.66, accuracy: 0.7342
epoch: 46, loss: 187.01, accuracy: 0.7379
epoch: 47, loss: 189.78, accuracy: 0.7297
epoch: 48, loss: 185.84, accuracy: 0.7424
epoch: 49, loss: 182.98, accuracy: 0.7371
epoch: 50, loss: 181.17, accuracy: 0.7356
epoch: 51, loss: 175.03, accuracy: 0.7359
epoch: 52, loss: 181.8, accuracy: 0.7218
epoch: 53, loss: 171.54, accuracy: 0.7368
epoch: 54, loss: 170.21, accuracy: 0.73
epoch: 55, loss: 167.05, accuracy: 0.7314
epoch: 56, loss: 170.82, accuracy: 0.7377
epoch: 57, loss: 164.71, accuracy: 0.7371
epoch: 58, loss: 165.64, accuracy: 0.736
epoch: 59, loss: 158.15, accuracy: 0.7352
epoch: 60, loss: 170.54, accuracy: 0.7308
epoch: 61, loss: 155.84, accuracy: 0.738
epoch: 62, loss: 159.39, accuracy: 0.733
epoch: 63, loss: 161.19, accuracy: 0.733
epoch: 64, loss: 151.16, accuracy: 0.7386
epoch: 65, loss: 150.77, accuracy: 0.7388
epoch: 66, loss: 144.31, accuracy: 0.7344
epoch: 67, loss: 156.23, accuracy: 0.7425
epoch: 68, loss: 144.14, accuracy: 0.7364
epoch: 69, loss: 146.39, accuracy: 0.7306
epoch: 70, loss: 69.43, accuracy: 0.7564
epoch: 71, loss: 47.41, accuracy: 0.7541
epoch: 72, loss: 42.62, accuracy: 0.7534
epoch: 73, loss: 40.28, accuracy: 0.7538
epoch: 74, loss: 37.44, accuracy: 0.7534
epoch: 75, loss: 36.42, accuracy: 0.7569
epoch: 76, loss: 34.88, accuracy: 0.7543
epoch: 77, loss: 33.05, accuracy: 0.7526
epoch: 78, loss: 32.24, accuracy: 0.7502
epoch: 79, loss: 32.61, accuracy: 0.7536
epoch: 80, loss: 30.66, accuracy: 0.7558
epoch: 81, loss: 29.8, accuracy: 0.7536
epoch: 82, loss: 29.9, accuracy: 0.7517
epoch: 83, loss: 29.46, accuracy: 0.7523
epoch: 84, loss: 29.3, accuracy: 0.7506
epoch: 85, loss: 27.89, accuracy: 0.7501
epoch: 86, loss: 26.5, accuracy: 0.7521
epoch: 87, loss: 24.98, accuracy: 0.7499
epoch: 88, loss: 26.74, accuracy: 0.7547
epoch: 89, loss: 24.14, accuracy: 0.7523
epoch: 90, loss: 25.28, accuracy: 0.7517
epoch: 91, loss: 24.73, accuracy: 0.7557
epoch: 92, loss: 22.67, accuracy: 0.7548
epoch: 93, loss: 23.91, accuracy: 0.7506
epoch: 94, loss: 24.26, accuracy: 0.753
epoch: 95, loss: 16.59, accuracy: 0.7528
epoch: 96, loss: 14.53, accuracy: 0.7563
epoch: 97, loss: 14.16, accuracy: 0.7548
epoch: 98, loss: 14.35, accuracy: 0.7551
epoch: 99, loss: 13.7, accuracy: 0.7562
time analysis:
    train 48431.1578 s
    all 48486.4764 s
Accuracy of     0 : 83 %
Accuracy of     1 : 90 %
Accuracy of     2 : 74 %
Accuracy of     3 : 57 %
Accuracy of     4 : 65 %
Accuracy of     5 : 69 %
Accuracy of     6 : 82 %
Accuracy of     7 : 73 %
Accuracy of     8 : 81 %
Accuracy of     9 : 75 %
```

## CIFAR10, paper, DAUConv2dOneMu, units: 2
```text
Config: {'num_workers': 10, 'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=96, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=96, out_channels=96, no_units=2, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=96, out_channels=192, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(192, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=3072, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 96, 32, 32]             676
       BatchNorm2d-2           [-1, 96, 32, 32]             192
         MaxPool2d-3           [-1, 96, 16, 16]               0
    DAUConv2dOneMu-4           [-1, 96, 16, 16]          18,532
       BatchNorm2d-5           [-1, 96, 16, 16]             192
         MaxPool2d-6             [-1, 96, 8, 8]               0
    DAUConv2dOneMu-7            [-1, 192, 8, 8]          37,060
       BatchNorm2d-8            [-1, 192, 8, 8]             384
         MaxPool2d-9            [-1, 192, 4, 4]               0
           Linear-10                   [-1, 10]          30,730
================================================================
total params: 87,766
DAU params: 56,268
other params: 31,498
----------------------------------------------------------------
epoch: 0, loss: 1223.7, accuracy: 0.5004
epoch: 1, loss: 967.05, accuracy: 0.587
epoch: 2, loss: 867.23, accuracy: 0.6165
epoch: 3, loss: 819.71, accuracy: 0.6176
epoch: 4, loss: 791.58, accuracy: 0.6225
epoch: 5, loss: 762.86, accuracy: 0.6311
epoch: 6, loss: 743.68, accuracy: 0.6284
epoch: 7, loss: 726.64, accuracy: 0.6407
epoch: 8, loss: 708.47, accuracy: 0.6539
epoch: 9, loss: 695.96, accuracy: 0.6479
epoch: 10, loss: 681.77, accuracy: 0.6282
epoch: 11, loss: 673.7, accuracy: 0.6281
epoch: 12, loss: 659.52, accuracy: 0.642
epoch: 13, loss: 654.77, accuracy: 0.6579
epoch: 14, loss: 644.43, accuracy: 0.656
epoch: 15, loss: 635.4, accuracy: 0.6571
epoch: 16, loss: 626.08, accuracy: 0.6368
epoch: 17, loss: 620.84, accuracy: 0.6559
epoch: 18, loss: 612.62, accuracy: 0.6651
epoch: 19, loss: 608.12, accuracy: 0.6574
epoch: 20, loss: 598.2, accuracy: 0.6457
epoch: 21, loss: 594.04, accuracy: 0.6709
epoch: 22, loss: 588.06, accuracy: 0.6601
epoch: 23, loss: 581.64, accuracy: 0.6612
epoch: 24, loss: 576.87, accuracy: 0.6629
epoch: 25, loss: 577.72, accuracy: 0.663
epoch: 26, loss: 565.14, accuracy: 0.6696
epoch: 27, loss: 567.15, accuracy: 0.6492
epoch: 28, loss: 558.88, accuracy: 0.6586
epoch: 29, loss: 558.2, accuracy: 0.6664
epoch: 30, loss: 552.36, accuracy: 0.6689
epoch: 31, loss: 549.32, accuracy: 0.6502
epoch: 32, loss: 545.95, accuracy: 0.6428
epoch: 33, loss: 543.08, accuracy: 0.6558
epoch: 34, loss: 537.74, accuracy: 0.6644
epoch: 35, loss: 535.85, accuracy: 0.6319
epoch: 36, loss: 531.81, accuracy: 0.6464
epoch: 37, loss: 526.7, accuracy: 0.6643
epoch: 38, loss: 525.92, accuracy: 0.6644
epoch: 39, loss: 517.07, accuracy: 0.672
epoch: 40, loss: 520.45, accuracy: 0.6521
epoch: 41, loss: 516.45, accuracy: 0.6632
epoch: 42, loss: 512.23, accuracy: 0.6566
epoch: 43, loss: 510.35, accuracy: 0.6533
epoch: 44, loss: 504.61, accuracy: 0.6645
epoch: 45, loss: 504.42, accuracy: 0.663
epoch: 46, loss: 501.54, accuracy: 0.6697
epoch: 47, loss: 498.37, accuracy: 0.6634
epoch: 48, loss: 494.0, accuracy: 0.6569
epoch: 49, loss: 493.48, accuracy: 0.6632
epoch: 50, loss: 489.85, accuracy: 0.6605
epoch: 51, loss: 488.25, accuracy: 0.6665
epoch: 52, loss: 481.99, accuracy: 0.6594
epoch: 53, loss: 478.93, accuracy: 0.6603
epoch: 54, loss: 478.08, accuracy: 0.6676
epoch: 55, loss: 471.51, accuracy: 0.6669
epoch: 56, loss: 471.61, accuracy: 0.6644
epoch: 57, loss: 470.11, accuracy: 0.6696
epoch: 58, loss: 465.33, accuracy: 0.6529
epoch: 59, loss: 464.52, accuracy: 0.6601
epoch: 60, loss: 461.49, accuracy: 0.649
epoch: 61, loss: 461.79, accuracy: 0.6585
epoch: 62, loss: 456.08, accuracy: 0.661
epoch: 63, loss: 456.08, accuracy: 0.6678
epoch: 64, loss: 451.98, accuracy: 0.6665
epoch: 65, loss: 449.74, accuracy: 0.6613
epoch: 66, loss: 447.03, accuracy: 0.6631
epoch: 67, loss: 445.51, accuracy: 0.6504
epoch: 68, loss: 444.45, accuracy: 0.6625
epoch: 69, loss: 441.82, accuracy: 0.6646
epoch: 70, loss: 349.29, accuracy: 0.6863
epoch: 71, loss: 336.0, accuracy: 0.686
epoch: 72, loss: 331.96, accuracy: 0.686
epoch: 73, loss: 327.86, accuracy: 0.6876
epoch: 74, loss: 324.93, accuracy: 0.686
epoch: 75, loss: 322.22, accuracy: 0.6787
epoch: 76, loss: 322.73, accuracy: 0.682
epoch: 77, loss: 320.31, accuracy: 0.6818
epoch: 78, loss: 317.03, accuracy: 0.6795
epoch: 79, loss: 317.24, accuracy: 0.684
epoch: 80, loss: 313.53, accuracy: 0.6804
epoch: 81, loss: 315.33, accuracy: 0.6836
epoch: 82, loss: 312.21, accuracy: 0.6834
epoch: 83, loss: 310.45, accuracy: 0.684
epoch: 84, loss: 309.29, accuracy: 0.682
epoch: 85, loss: 308.12, accuracy: 0.6832
epoch: 86, loss: 304.22, accuracy: 0.6822
epoch: 87, loss: 305.18, accuracy: 0.6808
epoch: 88, loss: 305.34, accuracy: 0.6816
epoch: 89, loss: 302.55, accuracy: 0.6765
epoch: 90, loss: 302.94, accuracy: 0.682
epoch: 91, loss: 301.87, accuracy: 0.6745
epoch: 92, loss: 301.56, accuracy: 0.6725
epoch: 93, loss: 299.11, accuracy: 0.6767
epoch: 94, loss: 299.24, accuracy: 0.676
epoch: 95, loss: 278.1, accuracy: 0.6808
epoch: 96, loss: 276.4, accuracy: 0.6834
epoch: 97, loss: 275.11, accuracy: 0.6828
epoch: 98, loss: 276.07, accuracy: 0.6838
epoch: 99, loss: 273.04, accuracy: 0.684
time analysis:
    train 2246.3982 s
    all 2300.19 s
Accuracy of     0 : 80 %
Accuracy of     1 : 78 %
Accuracy of     2 : 56 %
Accuracy of     3 : 53 %
Accuracy of     4 : 60 %
Accuracy of     5 : 66 %
Accuracy of     6 : 69 %
Accuracy of     7 : 70 %
Accuracy of     8 : 81 %
Accuracy of     9 : 80 %
```

finish: 2019-07-04 18:31:36.792557
