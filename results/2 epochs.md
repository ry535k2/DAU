start: 2019-06-27 14:14:44.971292
* MNIST, small_fc, Conv2d
* MNIST, small_fc, DAUConv2d, units: 2
* MNIST, small_fc, DAUConv2d, units: 3
* MNIST, small_fc, DAUConv2di, units: 2
* MNIST, small_fc, DAUConv2di, units: 3
* MNIST, small_fc, DAUConv2dj, units: 2
* MNIST, small_fc, DAUConv2dj, units: 3
* MNIST, small_fc, DAUConv2dOneMu, units: 2
* MNIST, small_fc, DAUConv2dOneMu, units: 3
* MNIST, small_fc, DAUConv2dOneMu, units: 4
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9
* FashionMNIST, small_fc, Conv2d
* FashionMNIST, small_fc, DAUConv2d, units: 2
* FashionMNIST, small_fc, DAUConv2d, units: 3
* FashionMNIST, small_fc, DAUConv2di, units: 2
* FashionMNIST, small_fc, DAUConv2di, units: 3
* FashionMNIST, small_fc, DAUConv2dj, units: 2
* FashionMNIST, small_fc, DAUConv2dj, units: 3
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 2
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 3
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 4
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9
* CIFAR10, small_fc, Conv2d
* CIFAR10, small_fc, DAUConv2d, units: 2
* CIFAR10, small_fc, DAUConv2d, units: 3
* CIFAR10, small_fc, DAUConv2di, units: 2
* CIFAR10, small_fc, DAUConv2di, units: 3
* CIFAR10, small_fc, DAUConv2dj, units: 2
* CIFAR10, small_fc, DAUConv2dj, units: 3
* CIFAR10, small_fc, DAUConv2dOneMu, units: 2
* CIFAR10, small_fc, DAUConv2dOneMu, units: 3
* CIFAR10, small_fc, DAUConv2dOneMu, units: 4
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9

## MNIST, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(1, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(32, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
            Conv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 0
other params: 3,442
----------------------------------------------------------------
epoch: 0, loss: 185.07, accuracy: 0.9722
epoch: 1, loss: 74.6, accuracy: 0.9747
time analysis:
    train 11.4562 s
Accuracy of     0 : 100 %
Accuracy of     1 : 95 %
Accuracy of     2 : 88 %
Accuracy of     3 : 96 %
Accuracy of     4 : 94 %
Accuracy of     5 : 95 %
Accuracy of     6 : 98 %
Accuracy of     7 : 96 %
Accuracy of     8 : 100 %
Accuracy of     9 : 97 %
```

## MNIST, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           1,544
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,578
DAU params: 1,768
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 249.38, accuracy: 0.9734
epoch: 1, loss: 87.88, accuracy: 0.9787
time analysis:
    all 36.9437 s
    train 36.0881 s
Accuracy of     0 : 98 %
Accuracy of     1 : 98 %
Accuracy of     2 : 97 %
Accuracy of     3 : 95 %
Accuracy of     4 : 94 %
Accuracy of     5 : 97 %
Accuracy of     6 : 96 %
Accuracy of     7 : 96 %
Accuracy of     8 : 100 %
Accuracy of     9 : 100 %
```

## MNIST, small_fc, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 2,632
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 226.47, accuracy: 0.9698
epoch: 1, loss: 86.41, accuracy: 0.9811
time analysis:
    all 50.6074 s
    train 49.3636 s
Accuracy of     0 : 98 %
Accuracy of     1 : 97 %
Accuracy of     2 : 98 %
Accuracy of     3 : 100 %
Accuracy of     4 : 95 %
Accuracy of     5 : 95 %
Accuracy of     6 : 98 %
Accuracy of     7 : 93 %
Accuracy of     8 : 100 %
Accuracy of     9 : 97 %
```

## MNIST, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             648
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,558
DAU params: 748
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 297.09, accuracy: 0.9405
epoch: 1, loss: 133.54, accuracy: 0.967
time analysis:
    all 32.2898 s
    train 30.7276 s
Accuracy of     0 : 100 %
Accuracy of     1 : 98 %
Accuracy of     2 : 94 %
Accuracy of     3 : 95 %
Accuracy of     4 : 95 %
Accuracy of     5 : 95 %
Accuracy of     6 : 92 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 97 %
```

## MNIST, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             968
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,912
DAU params: 1,102
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 311.8, accuracy: 0.9669
epoch: 1, loss: 100.24, accuracy: 0.977
time analysis:
    all 34.864 s
    train 32.8886 s
Accuracy of     0 : 98 %
Accuracy of     1 : 97 %
Accuracy of     2 : 95 %
Accuracy of     3 : 100 %
Accuracy of     4 : 92 %
Accuracy of     5 : 90 %
Accuracy of     6 : 96 %
Accuracy of     7 : 98 %
Accuracy of     8 : 100 %
Accuracy of     9 : 96 %
```

## MNIST, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             552
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,586
DAU params: 776
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 268.24, accuracy: 0.9662
epoch: 1, loss: 117.24, accuracy: 0.9676
time analysis:
    all 44.3994 s
    train 42.2608 s
Accuracy of     0 : 100 %
Accuracy of     1 : 98 %
Accuracy of     2 : 94 %
Accuracy of     3 : 95 %
Accuracy of     4 : 97 %
Accuracy of     5 : 90 %
Accuracy of     6 : 98 %
Accuracy of     7 : 93 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## MNIST, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             824
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,954
DAU params: 1,144
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 281.35, accuracy: 0.9658
epoch: 1, loss: 95.07, accuracy: 0.9763
time analysis:
    all 52.3377 s
    train 50.4405 s
Accuracy of     0 : 100 %
Accuracy of     1 : 97 %
Accuracy of     2 : 98 %
Accuracy of     3 : 93 %
Accuracy of     4 : 97 %
Accuracy of     5 : 97 %
Accuracy of     6 : 96 %
Accuracy of     7 : 94 %
Accuracy of     8 : 100 %
Accuracy of     9 : 97 %
```

## MNIST, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             524
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,434
DAU params: 624
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 345.18, accuracy: 0.9396
epoch: 1, loss: 151.75, accuracy: 0.954
time analysis:
    all 31.0943 s
    train 29.653 s
Accuracy of     0 : 96 %
Accuracy of     1 : 98 %
Accuracy of     2 : 95 %
Accuracy of     3 : 92 %
Accuracy of     4 : 92 %
Accuracy of     5 : 97 %
Accuracy of     6 : 96 %
Accuracy of     7 : 96 %
Accuracy of     8 : 100 %
Accuracy of     9 : 96 %
```

## MNIST, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             782
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,726
DAU params: 916
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 266.22, accuracy: 0.9684
epoch: 1, loss: 93.87, accuracy: 0.9796
time analysis:
    all 36.7387 s
    train 34.7238 s
Accuracy of     0 : 98 %
Accuracy of     1 : 98 %
Accuracy of     2 : 97 %
Accuracy of     3 : 95 %
Accuracy of     4 : 95 %
Accuracy of     5 : 92 %
Accuracy of     6 : 98 %
Accuracy of     7 : 94 %
Accuracy of     8 : 98 %
Accuracy of     9 : 97 %
```

## MNIST, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             168
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,040
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,018
DAU params: 1,208
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 291.91, accuracy: 0.9572
epoch: 1, loss: 105.6, accuracy: 0.9645
time analysis:
    all 37.097 s
    train 34.9235 s
Accuracy of     0 : 100 %
Accuracy of     1 : 97 %
Accuracy of     2 : 91 %
Accuracy of     3 : 95 %
Accuracy of     4 : 94 %
Accuracy of     5 : 97 %
Accuracy of     6 : 98 %
Accuracy of     7 : 98 %
Accuracy of     8 : 100 %
Accuracy of     9 : 94 %
```

## MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 984.76, accuracy: 0.6737
epoch: 1, loss: 726.96, accuracy: 0.5139
time analysis:
    all 16.067 s
    train 14.301 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 76 %
Accuracy of     3 : 65 %
Accuracy of     4 : 60 %
Accuracy of     5 : 70 %
Accuracy of     6 : 71 %
Accuracy of     7 : 67 %
Accuracy of     8 : 85 %
Accuracy of     9 : 75 %
```

## MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.7)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.7)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 854.57, accuracy: 0.6826
epoch: 1, loss: 598.14, accuracy: 0.7973
time analysis:
    all 15.4712 s
    train 14.4821 s
Accuracy of     0 : 78 %
Accuracy of     1 : 97 %
Accuracy of     2 : 77 %
Accuracy of     3 : 87 %
Accuracy of     4 : 83 %
Accuracy of     5 : 70 %
Accuracy of     6 : 53 %
Accuracy of     7 : 72 %
Accuracy of     8 : 90 %
Accuracy of     9 : 61 %
```

## MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.9)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.9)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 672.55, accuracy: 0.6583
epoch: 1, loss: 449.76, accuracy: 0.7935
time analysis:
    all 15.4338 s
    train 14.4587 s
Accuracy of     0 : 90 %
Accuracy of     1 : 95 %
Accuracy of     2 : 76 %
Accuracy of     3 : 90 %
Accuracy of     4 : 83 %
Accuracy of     5 : 85 %
Accuracy of     6 : 67 %
Accuracy of     7 : 77 %
Accuracy of     8 : 91 %
Accuracy of     9 : 87 %
```

## FashionMNIST, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(1, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(32, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
            Conv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 0
other params: 3,442
----------------------------------------------------------------
epoch: 0, loss: 455.03, accuracy: 0.8465
epoch: 1, loss: 351.5, accuracy: 0.855
time analysis:
    all 12.5934 s
    train 11.5619 s
Accuracy of     0 : 94 %
Accuracy of     1 : 96 %
Accuracy of     2 : 84 %
Accuracy of     3 : 80 %
Accuracy of     4 : 56 %
Accuracy of     5 : 95 %
Accuracy of     6 : 56 %
Accuracy of     7 : 92 %
Accuracy of     8 : 100 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           1,544
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,578
DAU params: 1,768
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 495.03, accuracy: 0.837
epoch: 1, loss: 375.45, accuracy: 0.8465
time analysis:
    all 37.2763 s
    train 36.4129 s
Accuracy of     0 : 71 %
Accuracy of     1 : 92 %
Accuracy of     2 : 70 %
Accuracy of     3 : 80 %
Accuracy of     4 : 70 %
Accuracy of     5 : 96 %
Accuracy of     6 : 63 %
Accuracy of     7 : 87 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 2,632
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 501.04, accuracy: 0.8437
epoch: 1, loss: 372.73, accuracy: 0.8561
time analysis:
    all 54.7026 s
    train 53.4738 s
Accuracy of     0 : 75 %
Accuracy of     1 : 95 %
Accuracy of     2 : 88 %
Accuracy of     3 : 81 %
Accuracy of     4 : 55 %
Accuracy of     5 : 95 %
Accuracy of     6 : 57 %
Accuracy of     7 : 94 %
Accuracy of     8 : 95 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             648
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,558
DAU params: 748
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 550.98, accuracy: 0.8264
epoch: 1, loss: 416.72, accuracy: 0.8321
time analysis:
    all 30.7575 s
    train 28.7846 s
Accuracy of     0 : 65 %
Accuracy of     1 : 95 %
Accuracy of     2 : 61 %
Accuracy of     3 : 77 %
Accuracy of     4 : 70 %
Accuracy of     5 : 100 %
Accuracy of     6 : 69 %
Accuracy of     7 : 94 %
Accuracy of     8 : 98 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             968
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,912
DAU params: 1,102
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 521.02, accuracy: 0.826
epoch: 1, loss: 387.78, accuracy: 0.8506
time analysis:
    all 28.1726 s
    train 26.7751 s
Accuracy of     0 : 84 %
Accuracy of     1 : 93 %
Accuracy of     2 : 62 %
Accuracy of     3 : 83 %
Accuracy of     4 : 88 %
Accuracy of     5 : 95 %
Accuracy of     6 : 45 %
Accuracy of     7 : 94 %
Accuracy of     8 : 98 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             552
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,586
DAU params: 776
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 525.3, accuracy: 0.8329
epoch: 1, loss: 396.96, accuracy: 0.8487
time analysis:
    all 41.7044 s
    train 40.1234 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 74 %
Accuracy of     3 : 87 %
Accuracy of     4 : 78 %
Accuracy of     5 : 96 %
Accuracy of     6 : 30 %
Accuracy of     7 : 92 %
Accuracy of     8 : 100 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             824
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,954
DAU params: 1,144
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 518.66, accuracy: 0.8349
epoch: 1, loss: 387.6, accuracy: 0.8565
time analysis:
    all 107.0169 s
    train 104.934 s
Accuracy of     0 : 82 %
Accuracy of     1 : 92 %
Accuracy of     2 : 85 %
Accuracy of     3 : 83 %
Accuracy of     4 : 70 %
Accuracy of     5 : 95 %
Accuracy of     6 : 56 %
Accuracy of     7 : 94 %
Accuracy of     8 : 95 %
Accuracy of     9 : 96 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             524
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,434
DAU params: 624
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 567.61, accuracy: 0.8081
epoch: 1, loss: 430.16, accuracy: 0.8293
time analysis:
    all 33.7495 s
    train 31.6035 s
Accuracy of     0 : 75 %
Accuracy of     1 : 96 %
Accuracy of     2 : 72 %
Accuracy of     3 : 86 %
Accuracy of     4 : 50 %
Accuracy of     5 : 96 %
Accuracy of     6 : 57 %
Accuracy of     7 : 87 %
Accuracy of     8 : 98 %
Accuracy of     9 : 96 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             782
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,726
DAU params: 916
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 550.2, accuracy: 0.8281
epoch: 1, loss: 403.57, accuracy: 0.8418
time analysis:
    all 100.0062 s
    train 98.1176 s
Accuracy of     0 : 76 %
Accuracy of     1 : 93 %
Accuracy of     2 : 68 %
Accuracy of     3 : 84 %
Accuracy of     4 : 70 %
Accuracy of     5 : 91 %
Accuracy of     6 : 59 %
Accuracy of     7 : 94 %
Accuracy of     8 : 96 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             168
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,040
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,018
DAU params: 1,208
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 525.72, accuracy: 0.8052
epoch: 1, loss: 411.02, accuracy: 0.8405
time analysis:
    all 125.9848 s
    train 124.3777 s
Accuracy of     0 : 88 %
Accuracy of     1 : 89 %
Accuracy of     2 : 87 %
Accuracy of     3 : 88 %
Accuracy of     4 : 65 %
Accuracy of     5 : 95 %
Accuracy of     6 : 28 %
Accuracy of     7 : 94 %
Accuracy of     8 : 95 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 667.34, accuracy: 0.7552
epoch: 1, loss: 531.03, accuracy: 0.7735
time analysis:
    all 57.3941 s
    train 50.2846 s
Accuracy of     0 : 76 %
Accuracy of     1 : 87 %
Accuracy of     2 : 80 %
Accuracy of     3 : 81 %
Accuracy of     4 : 48 %
Accuracy of     5 : 91 %
Accuracy of     6 : 40 %
Accuracy of     7 : 83 %
Accuracy of     8 : 93 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.7)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.7)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 631.55, accuracy: 0.6544
epoch: 1, loss: 501.52, accuracy: 0.7668
time analysis:
    all 29.8604 s
    train 27.3633 s
Accuracy of     0 : 65 %
Accuracy of     1 : 92 %
Accuracy of     2 : 62 %
Accuracy of     3 : 84 %
Accuracy of     4 : 75 %
Accuracy of     5 : 93 %
Accuracy of     6 : 42 %
Accuracy of     7 : 90 %
Accuracy of     8 : 95 %
Accuracy of     9 : 88 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.9)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.9)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 637.18, accuracy: 0.7625
epoch: 1, loss: 529.08, accuracy: 0.796
time analysis:
    all 15.6927 s
    train 14.6572 s
Accuracy of     0 : 69 %
Accuracy of     1 : 87 %
Accuracy of     2 : 74 %
Accuracy of     3 : 84 %
Accuracy of     4 : 68 %
Accuracy of     5 : 91 %
Accuracy of     6 : 25 %
Accuracy of     7 : 87 %
Accuracy of     8 : 95 %
Accuracy of     9 : 98 %
```

## CIFAR10, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(64, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
            Conv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 0
other params: 11,682
----------------------------------------------------------------
epoch: 0, loss: 1091.55, accuracy: 0.5226
epoch: 1, loss: 912.23, accuracy: 0.6049
time analysis:
    all 13.3291 s
    train 11.4825 s
Accuracy of     0 : 62 %
Accuracy of     1 : 76 %
Accuracy of     2 : 41 %
Accuracy of     3 : 45 %
Accuracy of     4 : 47 %
Accuracy of     5 : 37 %
Accuracy of     6 : 80 %
Accuracy of     7 : 59 %
Accuracy of     8 : 74 %
Accuracy of     9 : 65 %
```

## CIFAR10, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,216
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           3,080
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 9,570
DAU params: 4,296
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1159.92, accuracy: 0.5366
epoch: 1, loss: 967.31, accuracy: 0.5701
time analysis:
    all 112.3649 s
    train 110.6224 s
Accuracy of     0 : 80 %
Accuracy of     1 : 74 %
Accuracy of     2 : 36 %
Accuracy of     3 : 41 %
Accuracy of     4 : 38 %
Accuracy of     5 : 57 %
Accuracy of     6 : 53 %
Accuracy of     7 : 68 %
Accuracy of     8 : 68 %
Accuracy of     9 : 57 %
```

## CIFAR10, small_fc, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 6,408
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1141.46, accuracy: 0.5543
epoch: 1, loss: 940.29, accuracy: 0.5881
time analysis:
    all 171.0636 s
    train 167.4615 s
Accuracy of     0 : 73 %
Accuracy of     1 : 70 %
Accuracy of     2 : 43 %
Accuracy of     3 : 24 %
Accuracy of     4 : 54 %
Accuracy of     5 : 38 %
Accuracy of     6 : 73 %
Accuracy of     7 : 65 %
Accuracy of     8 : 62 %
Accuracy of     9 : 66 %
```

## CIFAR10, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             460
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,288
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,022
DAU params: 1,748
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1223.23, accuracy: 0.4965
epoch: 1, loss: 1050.1, accuracy: 0.5199
time analysis:
    all 38.7753 s
    train 34.2958 s
Accuracy of     0 : 50 %
Accuracy of     1 : 74 %
Accuracy of     2 : 49 %
Accuracy of     3 : 43 %
Accuracy of     4 : 32 %
Accuracy of     5 : 44 %
Accuracy of     6 : 62 %
Accuracy of     7 : 37 %
Accuracy of     8 : 79 %
Accuracy of     9 : 43 %
```

## CIFAR10, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             658
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,928
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,860
DAU params: 2,586
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1215.02, accuracy: 0.4879
epoch: 1, loss: 1057.54, accuracy: 0.5137
time analysis:
    all 48.7888 s
    train 45.8556 s
Accuracy of     0 : 76 %
Accuracy of     1 : 76 %
Accuracy of     2 : 16 %
Accuracy of     3 : 41 %
Accuracy of     4 : 41 %
Accuracy of     5 : 45 %
Accuracy of     6 : 66 %
Accuracy of     7 : 46 %
Accuracy of     8 : 72 %
Accuracy of     9 : 50 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]             704
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,042
DAU params: 1,768
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1159.98, accuracy: 0.541
epoch: 1, loss: 986.9, accuracy: 0.5357
time analysis:
    all 114.3998 s
    train 110.7378 s
Accuracy of     0 : 67 %
Accuracy of     1 : 64 %
Accuracy of     2 : 25 %
Accuracy of     3 : 16 %
Accuracy of     4 : 34 %
Accuracy of     5 : 64 %
Accuracy of     6 : 75 %
Accuracy of     7 : 56 %
Accuracy of     8 : 70 %
Accuracy of     9 : 61 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,024
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,592
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,890
DAU params: 2,616
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1169.78, accuracy: 0.4915
epoch: 1, loss: 1012.44, accuracy: 0.5119
time analysis:
    all 171.3646 s
    train 167.7644 s
Accuracy of     0 : 50 %
Accuracy of     1 : 66 %
Accuracy of     2 : 22 %
Accuracy of     3 : 19 %
Accuracy of     4 : 40 %
Accuracy of     5 : 61 %
Accuracy of     6 : 64 %
Accuracy of     7 : 75 %
Accuracy of     8 : 75 %
Accuracy of     9 : 64 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             452
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,036
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,762
DAU params: 1,488
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1215.39, accuracy: 0.4864
epoch: 1, loss: 1077.42, accuracy: 0.5257
time analysis:
    all 39.4069 s
    train 34.9196 s
Accuracy of     0 : 51 %
Accuracy of     1 : 76 %
Accuracy of     2 : 35 %
Accuracy of     3 : 27 %
Accuracy of     4 : 38 %
Accuracy of     5 : 42 %
Accuracy of     6 : 69 %
Accuracy of     7 : 59 %
Accuracy of     8 : 74 %
Accuracy of     9 : 55 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             646
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,550
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,470
DAU params: 2,196
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1251.05, accuracy: 0.4928
epoch: 1, loss: 1128.81, accuracy: 0.5009
time analysis:
    all 49.4881 s
    train 46.5449 s
Accuracy of     0 : 58 %
Accuracy of     1 : 62 %
Accuracy of     2 : 34 %
Accuracy of     3 : 24 %
Accuracy of     4 : 45 %
Accuracy of     5 : 49 %
Accuracy of     6 : 60 %
Accuracy of     7 : 40 %
Accuracy of     8 : 74 %
Accuracy of     9 : 46 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             840
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           2,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 8,178
DAU params: 2,904
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1223.98, accuracy: 0.5008
epoch: 1, loss: 1041.67, accuracy: 0.5587
time analysis:
    all 63.2928 s
    train 59.5556 s
Accuracy of     0 : 67 %
Accuracy of     1 : 70 %
Accuracy of     2 : 35 %
Accuracy of     3 : 39 %
Accuracy of     4 : 23 %
Accuracy of     5 : 49 %
Accuracy of     6 : 66 %
Accuracy of     7 : 53 %
Accuracy of     8 : 72 %
Accuracy of     9 : 53 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1239.63, accuracy: 0.4776
epoch: 1, loss: 1121.57, accuracy: 0.4961
time analysis:
    all 33.9863 s
    train 29.0053 s
Accuracy of     0 : 66 %
Accuracy of     1 : 74 %
Accuracy of     2 : 10 %
Accuracy of     3 : 35 %
Accuracy of     4 : 41 %
Accuracy of     5 : 42 %
Accuracy of     6 : 46 %
Accuracy of     7 : 65 %
Accuracy of     8 : 62 %
Accuracy of     9 : 53 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.7
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.7)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.7)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1243.2, accuracy: 0.4603
epoch: 1, loss: 1135.83, accuracy: 0.4977
time analysis:
    all 25.3897 s
    train 23.0024 s
Accuracy of     0 : 58 %
Accuracy of     1 : 66 %
Accuracy of     2 : 16 %
Accuracy of     3 : 42 %
Accuracy of     4 : 38 %
Accuracy of     5 : 28 %
Accuracy of     6 : 67 %
Accuracy of     7 : 62 %
Accuracy of     8 : 63 %
Accuracy of     9 : 64 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.9
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.9)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.9)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1299.52, accuracy: 0.4382
epoch: 1, loss: 1194.65, accuracy: 0.486
time analysis:
    all 25.5364 s
    train 23.1861 s
Accuracy of     0 : 60 %
Accuracy of     1 : 56 %
Accuracy of     2 : 39 %
Accuracy of     3 : 45 %
Accuracy of     4 : 30 %
Accuracy of     5 : 33 %
Accuracy of     6 : 60 %
Accuracy of     7 : 37 %
Accuracy of     8 : 56 %
Accuracy of     9 : 62 %
```

finish: 2019-06-27 14:48:05.886347
