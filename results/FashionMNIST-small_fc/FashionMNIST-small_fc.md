start: 2019-06-27 12:43:03.875159

## Conv2d
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(1, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(32, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
            Conv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 0
other params: 3,442
----------------------------------------------------------------
epoch: 0, loss: 456.4, accuracy: 0.8338
epoch: 1, loss: 346.66, accuracy: 0.8619
epoch: 2, loss: 325.75, accuracy: 0.8608
epoch: 3, loss: 314.91, accuracy: 0.869
epoch: 4, loss: 303.9, accuracy: 0.8702
epoch: 5, loss: 298.14, accuracy: 0.8759
epoch: 6, loss: 291.33, accuracy: 0.8781
epoch: 7, loss: 287.42, accuracy: 0.8761
epoch: 8, loss: 284.08, accuracy: 0.8748
epoch: 9, loss: 281.32, accuracy: 0.8745
time analysis:
    train 56.8091 s
Accuracy of     0 : 94 %
Accuracy of     1 : 95 %
Accuracy of     2 : 85 %
Accuracy of     3 : 80 %
Accuracy of     4 : 70 %
Accuracy of     5 : 98 %
Accuracy of     6 : 53 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 100 %
```

## DAUConv2d, units: 2
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           1,544
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,578
DAU params: 1,768
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 510.77, accuracy: 0.8182
epoch: 1, loss: 378.62, accuracy: 0.829
epoch: 2, loss: 351.19, accuracy: 0.8651
epoch: 3, loss: 337.21, accuracy: 0.8693
epoch: 4, loss: 326.76, accuracy: 0.8525
epoch: 5, loss: 317.09, accuracy: 0.8698
epoch: 6, loss: 311.61, accuracy: 0.8789
epoch: 7, loss: 306.3, accuracy: 0.8756
epoch: 8, loss: 300.36, accuracy: 0.8807
epoch: 9, loss: 298.64, accuracy: 0.8779
time analysis:
    train 181.7434 s
    all 182.6134 s
Accuracy of     0 : 90 %
Accuracy of     1 : 95 %
Accuracy of     2 : 78 %
Accuracy of     3 : 76 %
Accuracy of     4 : 76 %
Accuracy of     5 : 96 %
Accuracy of     6 : 59 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## DAUConv2d, units: 3
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 2,632
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 534.26, accuracy: 0.8394
epoch: 1, loss: 379.2, accuracy: 0.8599
epoch: 2, loss: 345.93, accuracy: 0.8622
epoch: 3, loss: 329.46, accuracy: 0.8686
epoch: 4, loss: 318.5, accuracy: 0.8709
epoch: 5, loss: 309.6, accuracy: 0.857
epoch: 6, loss: 303.61, accuracy: 0.8818
epoch: 7, loss: 299.36, accuracy: 0.8729
epoch: 8, loss: 294.67, accuracy: 0.8831
epoch: 9, loss: 290.05, accuracy: 0.8733
time analysis:
    train 247.4878 s
    all 248.7085 s
Accuracy of     0 : 84 %
Accuracy of     1 : 93 %
Accuracy of     2 : 70 %
Accuracy of     3 : 79 %
Accuracy of     4 : 78 %
Accuracy of     5 : 98 %
Accuracy of     6 : 70 %
Accuracy of     7 : 98 %
Accuracy of     8 : 100 %
Accuracy of     9 : 98 %
```

## DAUConv2di, units: 2
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             648
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,558
DAU params: 748
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 520.98, accuracy: 0.8022
epoch: 1, loss: 392.1, accuracy: 0.8482
epoch: 2, loss: 365.17, accuracy: 0.8593
epoch: 3, loss: 352.68, accuracy: 0.8579
epoch: 4, loss: 342.8, accuracy: 0.8637
epoch: 5, loss: 338.81, accuracy: 0.8466
epoch: 6, loss: 331.68, accuracy: 0.8732
epoch: 7, loss: 328.2, accuracy: 0.864
epoch: 8, loss: 324.46, accuracy: 0.8703
epoch: 9, loss: 323.97, accuracy: 0.8685
time analysis:
    train 111.611 s
    all 113.0324 s
Accuracy of     0 : 82 %
Accuracy of     1 : 92 %
Accuracy of     2 : 78 %
Accuracy of     3 : 84 %
Accuracy of     4 : 73 %
Accuracy of     5 : 95 %
Accuracy of     6 : 67 %
Accuracy of     7 : 94 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## DAUConv2di, units: 3
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             968
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,912
DAU params: 1,102
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 564.43, accuracy: 0.8182
epoch: 1, loss: 402.94, accuracy: 0.8419
epoch: 2, loss: 365.91, accuracy: 0.8426
epoch: 3, loss: 347.47, accuracy: 0.8676
epoch: 4, loss: 338.57, accuracy: 0.8569
epoch: 5, loss: 326.93, accuracy: 0.858
epoch: 6, loss: 318.07, accuracy: 0.8738
epoch: 7, loss: 314.88, accuracy: 0.8781
epoch: 8, loss: 310.2, accuracy: 0.8665
epoch: 9, loss: 303.47, accuracy: 0.8708
time analysis:
    train 134.7048 s
    all 135.9989 s
Accuracy of     0 : 71 %
Accuracy of     1 : 95 %
Accuracy of     2 : 80 %
Accuracy of     3 : 86 %
Accuracy of     4 : 75 %
Accuracy of     5 : 96 %
Accuracy of     6 : 63 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## DAUConv2dj, units: 2
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             552
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,586
DAU params: 776
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 518.78, accuracy: 0.8126
epoch: 1, loss: 403.0, accuracy: 0.8315
epoch: 2, loss: 373.61, accuracy: 0.851
epoch: 3, loss: 362.87, accuracy: 0.847
epoch: 4, loss: 354.1, accuracy: 0.8494
epoch: 5, loss: 348.57, accuracy: 0.8565
epoch: 6, loss: 344.98, accuracy: 0.8554
epoch: 7, loss: 339.17, accuracy: 0.8667
epoch: 8, loss: 334.81, accuracy: 0.8651
epoch: 9, loss: 330.15, accuracy: 0.8655
time analysis:
    train 183.8545 s
    all 185.3453 s
Accuracy of     0 : 76 %
Accuracy of     1 : 95 %
Accuracy of     2 : 80 %
Accuracy of     3 : 79 %
Accuracy of     4 : 81 %
Accuracy of     5 : 96 %
Accuracy of     6 : 50 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 94 %
```

## DAUConv2dj, units: 3
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             824
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,954
DAU params: 1,144
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 542.43, accuracy: 0.8383
epoch: 1, loss: 397.27, accuracy: 0.8439
epoch: 2, loss: 364.26, accuracy: 0.8614
epoch: 3, loss: 347.93, accuracy: 0.8649
epoch: 4, loss: 334.63, accuracy: 0.8556
epoch: 5, loss: 328.79, accuracy: 0.8675
epoch: 6, loss: 321.31, accuracy: 0.8743
epoch: 7, loss: 318.7, accuracy: 0.8713
epoch: 8, loss: 311.68, accuracy: 0.8612
epoch: 9, loss: 311.95, accuracy: 0.8559
time analysis:
    train 248.5846 s
    all 249.8338 s
Accuracy of     0 : 63 %
Accuracy of     1 : 90 %
Accuracy of     2 : 77 %
Accuracy of     3 : 72 %
Accuracy of     4 : 75 %
Accuracy of     5 : 98 %
Accuracy of     6 : 70 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 94 %
```

## DAUConv2dOneMu, units: 2
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             524
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,434
DAU params: 624
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 578.74, accuracy: 0.8139
epoch: 1, loss: 443.6, accuracy: 0.8356
epoch: 2, loss: 422.15, accuracy: 0.8392
epoch: 3, loss: 404.69, accuracy: 0.8473
epoch: 4, loss: 390.86, accuracy: 0.8397
epoch: 5, loss: 385.56, accuracy: 0.8524
epoch: 6, loss: 378.35, accuracy: 0.8536
epoch: 7, loss: 376.14, accuracy: 0.8515
epoch: 8, loss: 364.02, accuracy: 0.8484
epoch: 9, loss: 364.41, accuracy: 0.8475
time analysis:
    train 115.1725 s
    all 116.6292 s
Accuracy of     0 : 88 %
Accuracy of     1 : 98 %
Accuracy of     2 : 81 %
Accuracy of     3 : 76 %
Accuracy of     4 : 58 %
Accuracy of     5 : 98 %
Accuracy of     6 : 53 %
Accuracy of     7 : 85 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## DAUConv2dOneMu, units: 3
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             782
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,726
DAU params: 916
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 567.62, accuracy: 0.8078
epoch: 1, loss: 422.91, accuracy: 0.8333
epoch: 2, loss: 386.51, accuracy: 0.8451
epoch: 3, loss: 366.37, accuracy: 0.862
epoch: 4, loss: 353.15, accuracy: 0.8469
epoch: 5, loss: 343.59, accuracy: 0.8707
epoch: 6, loss: 337.05, accuracy: 0.862
epoch: 7, loss: 332.28, accuracy: 0.8683
epoch: 8, loss: 328.53, accuracy: 0.8721
epoch: 9, loss: 320.5, accuracy: 0.8657
time analysis:
    train 136.4412 s
    all 137.7473 s
Accuracy of     0 : 80 %
Accuracy of     1 : 96 %
Accuracy of     2 : 88 %
Accuracy of     3 : 88 %
Accuracy of     4 : 70 %
Accuracy of     5 : 98 %
Accuracy of     6 : 59 %
Accuracy of     7 : 92 %
Accuracy of     8 : 95 %
Accuracy of     9 : 96 %
```

## DAUConv2dOneMu, units: 4
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             168
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,040
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,018
DAU params: 1,208
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 550.4, accuracy: 0.8205
epoch: 1, loss: 406.85, accuracy: 0.8301
epoch: 2, loss: 375.42, accuracy: 0.8432
epoch: 3, loss: 357.48, accuracy: 0.8642
epoch: 4, loss: 346.35, accuracy: 0.8674
epoch: 5, loss: 336.82, accuracy: 0.852
epoch: 6, loss: 329.72, accuracy: 0.8743
epoch: 7, loss: 325.96, accuracy: 0.8647
epoch: 8, loss: 320.47, accuracy: 0.8653
epoch: 9, loss: 318.96, accuracy: 0.8732
time analysis:
    train 158.1305 s
    all 159.6622 s
Accuracy of     0 : 88 %
Accuracy of     1 : 92 %
Accuracy of     2 : 84 %
Accuracy of     3 : 87 %
Accuracy of     4 : 66 %
Accuracy of     5 : 98 %
Accuracy of     6 : 57 %
Accuracy of     7 : 100 %
Accuracy of     8 : 95 %
Accuracy of     9 : 91 %
```

## DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 648.84, accuracy: 0.7475
epoch: 1, loss: 522.31, accuracy: 0.7899
epoch: 2, loss: 500.13, accuracy: 0.7971
epoch: 3, loss: 487.79, accuracy: 0.8016
epoch: 4, loss: 476.15, accuracy: 0.7542
epoch: 5, loss: 467.88, accuracy: 0.7998
epoch: 6, loss: 463.54, accuracy: 0.8122
epoch: 7, loss: 456.34, accuracy: 0.7973
epoch: 8, loss: 455.85, accuracy: 0.8111
epoch: 9, loss: 452.52, accuracy: 0.8027
time analysis:
    train 71.3157 s
    all 73.0524 s
Accuracy of     0 : 84 %
Accuracy of     1 : 90 %
Accuracy of     2 : 78 %
Accuracy of     3 : 83 %
Accuracy of     4 : 58 %
Accuracy of     5 : 96 %
Accuracy of     6 : 33 %
Accuracy of     7 : 85 %
Accuracy of     8 : 96 %
Accuracy of     9 : 91 %
```

## DAUConv2dZeroMu, units: 1, sigma: 0.7
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.7)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.7)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 640.83, accuracy: 0.7755
epoch: 1, loss: 517.16, accuracy: 0.7122
epoch: 2, loss: 498.68, accuracy: 0.4476
epoch: 3, loss: 484.74, accuracy: 0.8041
epoch: 4, loss: 473.04, accuracy: 0.7869
epoch: 5, loss: 467.1, accuracy: 0.8025
epoch: 6, loss: 458.81, accuracy: 0.7728
epoch: 7, loss: 454.87, accuracy: 0.7537
epoch: 8, loss: 447.02, accuracy: 0.7958
epoch: 9, loss: 445.64, accuracy: 0.7825
time analysis:
    train 71.2737 s
    all 72.2543 s
Accuracy of     0 : 71 %
Accuracy of     1 : 89 %
Accuracy of     2 : 67 %
Accuracy of     3 : 84 %
Accuracy of     4 : 71 %
Accuracy of     5 : 95 %
Accuracy of     6 : 38 %
Accuracy of     7 : 90 %
Accuracy of     8 : 98 %
Accuracy of     9 : 94 %
```

## DAUConv2dZeroMu, units: 1, sigma: 0.9
```text
Config: {'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10, 'dataset': 'FashionMNIST', 'architecture': 'small_fc', 'data_folder': '/home/marko/Desktop/hdd/data'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.9)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.9)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 648.77, accuracy: 0.7481
epoch: 1, loss: 524.99, accuracy: 0.7395
epoch: 2, loss: 499.65, accuracy: 0.7421
epoch: 3, loss: 486.36, accuracy: 0.8035
epoch: 4, loss: 474.73, accuracy: 0.8128
epoch: 5, loss: 469.52, accuracy: 0.796
epoch: 6, loss: 463.89, accuracy: 0.7922
epoch: 7, loss: 459.07, accuracy: 0.793
epoch: 8, loss: 454.2, accuracy: 0.8139
epoch: 9, loss: 453.58, accuracy: 0.7997
time analysis:
    train 72.4316 s
    all 73.3858 s
Accuracy of     0 : 67 %
Accuracy of     1 : 90 %
Accuracy of     2 : 70 %
Accuracy of     3 : 81 %
Accuracy of     4 : 70 %
Accuracy of     5 : 86 %
Accuracy of     6 : 40 %
Accuracy of     7 : 89 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

finish: 2019-06-27 13:13:12.353657
