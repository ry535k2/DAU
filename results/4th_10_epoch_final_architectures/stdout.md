start: 2019-07-03 09:22:37.636256
* CIFAR10, small_fc, Conv2d
* CIFAR10, small_fc, DAUConv2di, units: 2
* CIFAR10, small_fc, DAUConv2di, units: 3
* CIFAR10, small_fc, DAUConv2di, units: 4
* CIFAR10, small_fc, DAUConv2dOneMu, units: 3
* CIFAR10, small_fc, DAUConv2dOneMu, units: 4
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1
* CIFAR10, 2_layers, Conv2d
* CIFAR10, 2_layers, DAUConv2di, units: 2
* CIFAR10, 2_layers, DAUConv2di, units: 3
* CIFAR10, 2_layers, DAUConv2di, units: 4
* CIFAR10, 2_layers, DAUConv2dOneMu, units: 3
* CIFAR10, 2_layers, DAUConv2dOneMu, units: 4
* CIFAR10, 2_layers, DAUConv2dZeroMu, units: 1
* CIFAR10, 3_layers, Conv2d
* CIFAR10, 3_layers, DAUConv2di, units: 2
* CIFAR10, 3_layers, DAUConv2di, units: 3
* CIFAR10, 3_layers, DAUConv2di, units: 4
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 3
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 4
* CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1

## CIFAR10, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(64, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
            Conv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 7,842
DAU params: 0
other params: 7,842
----------------------------------------------------------------
epoch: 0, loss: 1115.77, accuracy: 0.524
epoch: 1, loss: 934.17, accuracy: 0.5883
epoch: 2, loss: 874.95, accuracy: 0.6041
epoch: 3, loss: 842.51, accuracy: 0.5918
epoch: 4, loss: 823.44, accuracy: 0.6202
epoch: 5, loss: 806.3, accuracy: 0.6
epoch: 6, loss: 747.7, accuracy: 0.6529
epoch: 7, loss: 735.2, accuracy: 0.6534
epoch: 8, loss: 728.8, accuracy: 0.6554
epoch: 9, loss: 723.92, accuracy: 0.6555
time analysis:
    train 55.1908 s
Accuracy of     0 : 62 %
Accuracy of     1 : 82 %
Accuracy of     2 : 55 %
Accuracy of     3 : 49 %
Accuracy of     4 : 54 %
Accuracy of     5 : 42 %
Accuracy of     6 : 73 %
Accuracy of     7 : 60 %
Accuracy of     8 : 72 %
Accuracy of     9 : 70 %
```

## CIFAR10, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             460
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,288
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 3,182
DAU params: 1,748
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1229.65, accuracy: 0.5026
epoch: 1, loss: 1060.93, accuracy: 0.5211
epoch: 2, loss: 1010.33, accuracy: 0.5531
epoch: 3, loss: 972.83, accuracy: 0.5551
epoch: 4, loss: 943.98, accuracy: 0.5596
epoch: 5, loss: 925.44, accuracy: 0.5772
epoch: 6, loss: 868.04, accuracy: 0.5972
epoch: 7, loss: 861.24, accuracy: 0.602
epoch: 8, loss: 855.48, accuracy: 0.6037
epoch: 9, loss: 849.25, accuracy: 0.6063
time analysis:
    all 181.3406 s
    train 179.6002 s
Accuracy of     0 : 71 %
Accuracy of     1 : 80 %
Accuracy of     2 : 39 %
Accuracy of     3 : 35 %
Accuracy of     4 : 50 %
Accuracy of     5 : 44 %
Accuracy of     6 : 66 %
Accuracy of     7 : 53 %
Accuracy of     8 : 74 %
Accuracy of     9 : 61 %
```

## CIFAR10, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             658
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,928
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,020
DAU params: 2,586
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1214.89, accuracy: 0.4959
epoch: 1, loss: 1057.88, accuracy: 0.5175
epoch: 2, loss: 1003.25, accuracy: 0.5594
epoch: 3, loss: 957.42, accuracy: 0.573
epoch: 4, loss: 926.76, accuracy: 0.57
epoch: 5, loss: 902.11, accuracy: 0.5781
epoch: 6, loss: 843.18, accuracy: 0.6209
epoch: 7, loss: 831.19, accuracy: 0.6177
epoch: 8, loss: 826.02, accuracy: 0.6247
epoch: 9, loss: 818.14, accuracy: 0.622
time analysis:
    all 219.1445 s
    train 216.5939 s
Accuracy of     0 : 60 %
Accuracy of     1 : 76 %
Accuracy of     2 : 55 %
Accuracy of     3 : 39 %
Accuracy of     4 : 47 %
Accuracy of     5 : 38 %
Accuracy of     6 : 80 %
Accuracy of     7 : 62 %
Accuracy of     8 : 75 %
Accuracy of     9 : 57 %
```

## CIFAR10, small_fc, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             856
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           2,568
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,858
DAU params: 3,424
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1224.26, accuracy: 0.4752
epoch: 1, loss: 1077.95, accuracy: 0.519
epoch: 2, loss: 1012.16, accuracy: 0.5375
epoch: 3, loss: 960.56, accuracy: 0.5689
epoch: 4, loss: 924.66, accuracy: 0.5743
epoch: 5, loss: 901.1, accuracy: 0.5903
epoch: 6, loss: 841.04, accuracy: 0.6173
epoch: 7, loss: 829.86, accuracy: 0.6241
epoch: 8, loss: 823.97, accuracy: 0.6138
epoch: 9, loss: 818.21, accuracy: 0.6265
time analysis:
    all 259.6962 s
    train 256.874 s
Accuracy of     0 : 73 %
Accuracy of     1 : 68 %
Accuracy of     2 : 37 %
Accuracy of     3 : 35 %
Accuracy of     4 : 41 %
Accuracy of     5 : 55 %
Accuracy of     6 : 66 %
Accuracy of     7 : 64 %
Accuracy of     8 : 77 %
Accuracy of     9 : 65 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             646
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,550
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 3,630
DAU params: 2,196
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1272.29, accuracy: 0.4671
epoch: 1, loss: 1120.85, accuracy: 0.4812
epoch: 2, loss: 1075.62, accuracy: 0.522
epoch: 3, loss: 1022.32, accuracy: 0.5232
epoch: 4, loss: 979.7, accuracy: 0.5492
epoch: 5, loss: 950.0, accuracy: 0.5717
epoch: 6, loss: 888.21, accuracy: 0.5956
epoch: 7, loss: 878.39, accuracy: 0.6005
epoch: 8, loss: 873.44, accuracy: 0.5979
epoch: 9, loss: 866.66, accuracy: 0.6003
time analysis:
    all 220.0841 s
    train 217.0305 s
Accuracy of     0 : 69 %
Accuracy of     1 : 70 %
Accuracy of     2 : 46 %
Accuracy of     3 : 45 %
Accuracy of     4 : 40 %
Accuracy of     5 : 35 %
Accuracy of     6 : 76 %
Accuracy of     7 : 65 %
Accuracy of     8 : 75 %
Accuracy of     9 : 71 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             840
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           2,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 4,338
DAU params: 2,904
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1268.48, accuracy: 0.4834
epoch: 1, loss: 1116.18, accuracy: 0.4767
epoch: 2, loss: 1059.61, accuracy: 0.5304
epoch: 3, loss: 1029.85, accuracy: 0.5445
epoch: 4, loss: 1005.11, accuracy: 0.5505
epoch: 5, loss: 984.77, accuracy: 0.5597
epoch: 6, loss: 929.51, accuracy: 0.5741
epoch: 7, loss: 919.0, accuracy: 0.5818
epoch: 8, loss: 911.72, accuracy: 0.5854
epoch: 9, loss: 904.7, accuracy: 0.5788
time analysis:
    all 257.4958 s
    train 254.7145 s
Accuracy of     0 : 75 %
Accuracy of     1 : 70 %
Accuracy of     2 : 48 %
Accuracy of     3 : 17 %
Accuracy of     4 : 34 %
Accuracy of     5 : 62 %
Accuracy of     6 : 66 %
Accuracy of     7 : 50 %
Accuracy of     8 : 72 %
Accuracy of     9 : 69 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=128, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 4, 4]               0
            Linear-7                   [-1, 10]           1,290
================================================================
total params: 2,210
DAU params: 776
other params: 1,434
----------------------------------------------------------------
epoch: 0, loss: 1262.7, accuracy: 0.4734
epoch: 1, loss: 1151.42, accuracy: 0.4905
epoch: 2, loss: 1125.15, accuracy: 0.4839
epoch: 3, loss: 1110.09, accuracy: 0.4934
epoch: 4, loss: 1102.94, accuracy: 0.4946
epoch: 5, loss: 1094.2, accuracy: 0.505
epoch: 6, loss: 1062.75, accuracy: 0.523
epoch: 7, loss: 1058.15, accuracy: 0.5219
epoch: 8, loss: 1054.32, accuracy: 0.5224
epoch: 9, loss: 1054.19, accuracy: 0.5259
time analysis:
    all 117.614 s
    train 114.5588 s
Accuracy of     0 : 58 %
Accuracy of     1 : 66 %
Accuracy of     2 : 29 %
Accuracy of     3 : 34 %
Accuracy of     4 : 40 %
Accuracy of     5 : 45 %
Accuracy of     6 : 51 %
Accuracy of     7 : 51 %
Accuracy of     8 : 74 %
Accuracy of     9 : 56 %
```

## CIFAR10, 2_layers, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 16, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(16, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 16, 32, 32]             448
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
            Conv2d-4           [-1, 32, 16, 16]           4,640
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 25,674
DAU params: 0
other params: 25,674
----------------------------------------------------------------
epoch: 0, loss: 1017.22, accuracy: 0.6328
epoch: 1, loss: 771.02, accuracy: 0.6601
epoch: 2, loss: 705.75, accuracy: 0.6707
epoch: 3, loss: 669.12, accuracy: 0.675
epoch: 4, loss: 639.86, accuracy: 0.677
epoch: 5, loss: 618.39, accuracy: 0.6912
epoch: 6, loss: 522.3, accuracy: 0.713
epoch: 7, loss: 506.45, accuracy: 0.7125
epoch: 8, loss: 497.4, accuracy: 0.7102
epoch: 9, loss: 491.28, accuracy: 0.7121
time analysis:
    all 56.0584 s
    train 53.7599 s
Accuracy of     0 : 75 %
Accuracy of     1 : 88 %
Accuracy of     2 : 53 %
Accuracy of     3 : 39 %
Accuracy of     4 : 58 %
Accuracy of     5 : 64 %
Accuracy of     6 : 80 %
Accuracy of     7 : 78 %
Accuracy of     8 : 81 %
Accuracy of     9 : 79 %
```

## CIFAR10, 2_layers, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=16, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=32, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 32, 32]             124
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
        DAUConv2di-4           [-1, 32, 16, 16]           1,120
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 21,830
DAU params: 1,244
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1166.73, accuracy: 0.5099
epoch: 1, loss: 933.89, accuracy: 0.59
epoch: 2, loss: 854.34, accuracy: 0.6081
epoch: 3, loss: 802.23, accuracy: 0.634
epoch: 4, loss: 768.66, accuracy: 0.6337
epoch: 5, loss: 749.06, accuracy: 0.6429
epoch: 6, loss: 656.7, accuracy: 0.6732
epoch: 7, loss: 642.35, accuracy: 0.6712
epoch: 8, loss: 638.33, accuracy: 0.6695
epoch: 9, loss: 630.77, accuracy: 0.6741
time analysis:
    all 127.1575 s
    train 125.3962 s
Accuracy of     0 : 71 %
Accuracy of     1 : 80 %
Accuracy of     2 : 50 %
Accuracy of     3 : 45 %
Accuracy of     4 : 67 %
Accuracy of     5 : 55 %
Accuracy of     6 : 73 %
Accuracy of     7 : 79 %
Accuracy of     8 : 75 %
Accuracy of     9 : 76 %
```

## CIFAR10, 2_layers, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=32, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 32, 32]             178
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
        DAUConv2di-4           [-1, 32, 16, 16]           1,664
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 22,428
DAU params: 1,842
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1130.36, accuracy: 0.5688
epoch: 1, loss: 889.5, accuracy: 0.6075
epoch: 2, loss: 812.28, accuracy: 0.6216
epoch: 3, loss: 772.04, accuracy: 0.633
epoch: 4, loss: 741.43, accuracy: 0.6376
epoch: 5, loss: 718.38, accuracy: 0.6489
epoch: 6, loss: 633.19, accuracy: 0.6726
epoch: 7, loss: 618.32, accuracy: 0.6654
epoch: 8, loss: 614.8, accuracy: 0.6747
epoch: 9, loss: 607.62, accuracy: 0.6762
time analysis:
    all 140.8865 s
    train 138.7612 s
Accuracy of     0 : 69 %
Accuracy of     1 : 82 %
Accuracy of     2 : 41 %
Accuracy of     3 : 54 %
Accuracy of     4 : 58 %
Accuracy of     5 : 52 %
Accuracy of     6 : 80 %
Accuracy of     7 : 57 %
Accuracy of     8 : 77 %
Accuracy of     9 : 71 %
```

## CIFAR10, 2_layers, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=32, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 32, 32]             232
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
        DAUConv2di-4           [-1, 32, 16, 16]           2,208
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 23,026
DAU params: 2,440
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1132.33, accuracy: 0.5645
epoch: 1, loss: 893.66, accuracy: 0.6119
epoch: 2, loss: 807.34, accuracy: 0.6217
epoch: 3, loss: 761.46, accuracy: 0.6447
epoch: 4, loss: 729.66, accuracy: 0.6326
epoch: 5, loss: 707.04, accuracy: 0.6204
epoch: 6, loss: 613.75, accuracy: 0.6738
epoch: 7, loss: 599.51, accuracy: 0.6799
epoch: 8, loss: 591.68, accuracy: 0.6792
epoch: 9, loss: 586.01, accuracy: 0.6774
time analysis:
    all 157.3591 s
    train 155.123 s
Accuracy of     0 : 73 %
Accuracy of     1 : 80 %
Accuracy of     2 : 68 %
Accuracy of     3 : 43 %
Accuracy of     4 : 61 %
Accuracy of     5 : 61 %
Accuracy of     6 : 67 %
Accuracy of     7 : 70 %
Accuracy of     8 : 75 %
Accuracy of     9 : 71 %
```

## CIFAR10, 2_layers, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=32, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 32, 32]             166
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
    DAUConv2dOneMu-4           [-1, 32, 16, 16]           1,574
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 22,326
DAU params: 1,740
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1201.62, accuracy: 0.5256
epoch: 1, loss: 988.52, accuracy: 0.5674
epoch: 2, loss: 913.04, accuracy: 0.5908
epoch: 3, loss: 868.25, accuracy: 0.5883
epoch: 4, loss: 843.18, accuracy: 0.6018
epoch: 5, loss: 821.32, accuracy: 0.5974
epoch: 6, loss: 740.09, accuracy: 0.6291
epoch: 7, loss: 726.74, accuracy: 0.6346
epoch: 8, loss: 718.47, accuracy: 0.6366
epoch: 9, loss: 713.19, accuracy: 0.6303
time analysis:
    all 141.4786 s
    train 139.1088 s
Accuracy of     0 : 69 %
Accuracy of     1 : 68 %
Accuracy of     2 : 43 %
Accuracy of     3 : 53 %
Accuracy of     4 : 52 %
Accuracy of     5 : 47 %
Accuracy of     6 : 67 %
Accuracy of     7 : 64 %
Accuracy of     8 : 75 %
Accuracy of     9 : 71 %
```

## CIFAR10, 2_layers, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=32, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 32, 32]             216
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
    DAUConv2dOneMu-4           [-1, 32, 16, 16]           2,088
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 22,890
DAU params: 2,304
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1179.64, accuracy: 0.5203
epoch: 1, loss: 994.46, accuracy: 0.5321
epoch: 2, loss: 919.25, accuracy: 0.5838
epoch: 3, loss: 870.43, accuracy: 0.5822
epoch: 4, loss: 829.58, accuracy: 0.6022
epoch: 5, loss: 792.21, accuracy: 0.6318
epoch: 6, loss: 693.83, accuracy: 0.6503
epoch: 7, loss: 679.27, accuracy: 0.6499
epoch: 8, loss: 668.89, accuracy: 0.656
epoch: 9, loss: 660.92, accuracy: 0.6578
time analysis:
    all 158.1257 s
    train 155.8918 s
Accuracy of     0 : 71 %
Accuracy of     1 : 78 %
Accuracy of     2 : 43 %
Accuracy of     3 : 39 %
Accuracy of     4 : 56 %
Accuracy of     5 : 57 %
Accuracy of     6 : 75 %
Accuracy of     7 : 67 %
Accuracy of     8 : 72 %
Accuracy of     9 : 71 %
```

## CIFAR10, 2_layers, DAUConv2dZeroMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=16, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=16, out_channels=32, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=2048, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 16, 32, 32]              64
       BatchNorm2d-2           [-1, 16, 32, 32]              32
         MaxPool2d-3           [-1, 16, 16, 16]               0
   DAUConv2dZeroMu-4           [-1, 32, 16, 16]             544
       BatchNorm2d-5           [-1, 32, 16, 16]              64
         MaxPool2d-6             [-1, 32, 8, 8]               0
            Linear-7                   [-1, 10]          20,490
================================================================
total params: 21,194
DAU params: 608
other params: 20,586
----------------------------------------------------------------
epoch: 0, loss: 1201.65, accuracy: 0.4986
epoch: 1, loss: 1060.3, accuracy: 0.5224
epoch: 2, loss: 1023.3, accuracy: 0.5092
epoch: 3, loss: 999.42, accuracy: 0.5404
epoch: 4, loss: 985.9, accuracy: 0.5432
epoch: 5, loss: 970.84, accuracy: 0.5345
epoch: 6, loss: 903.88, accuracy: 0.561
epoch: 7, loss: 894.53, accuracy: 0.5595
epoch: 8, loss: 890.06, accuracy: 0.5661
epoch: 9, loss: 883.55, accuracy: 0.5604
time analysis:
    all 60.8823 s
    train 58.4437 s
Accuracy of     0 : 71 %
Accuracy of     1 : 68 %
Accuracy of     2 : 36 %
Accuracy of     3 : 38 %
Accuracy of     4 : 45 %
Accuracy of     5 : 45 %
Accuracy of     6 : 64 %
Accuracy of     7 : 65 %
Accuracy of     8 : 62 %
Accuracy of     9 : 66 %
```

## CIFAR10, 3_layers, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(8, 16, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv3): Conv2d(16, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1            [-1, 8, 32, 32]             224
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
            Conv2d-4           [-1, 16, 16, 16]           1,168
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
            Conv2d-7             [-1, 32, 8, 8]           4,640
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 11,274
DAU params: 0
other params: 11,274
----------------------------------------------------------------
epoch: 0, loss: 1048.61, accuracy: 0.5319
epoch: 1, loss: 815.36, accuracy: 0.6459
epoch: 2, loss: 734.13, accuracy: 0.6655
epoch: 3, loss: 690.99, accuracy: 0.6669
epoch: 4, loss: 664.45, accuracy: 0.6615
epoch: 5, loss: 646.97, accuracy: 0.6712
epoch: 6, loss: 565.16, accuracy: 0.7169
epoch: 7, loss: 548.4, accuracy: 0.7144
epoch: 8, loss: 542.87, accuracy: 0.7127
epoch: 9, loss: 538.05, accuracy: 0.7134
time analysis:
    all 60.3289 s
    train 58.5887 s
Accuracy of     0 : 76 %
Accuracy of     1 : 90 %
Accuracy of     2 : 63 %
Accuracy of     3 : 46 %
Accuracy of     4 : 67 %
Accuracy of     5 : 62 %
Accuracy of     6 : 73 %
Accuracy of     7 : 71 %
Accuracy of     8 : 82 %
Accuracy of     9 : 76 %
```

## CIFAR10, 3_layers, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=8, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=8, out_channels=16, no_units=2, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=32, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1            [-1, 8, 32, 32]              68
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
        DAUConv2di-4           [-1, 16, 16, 16]             304
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
        DAUConv2di-7             [-1, 32, 8, 8]           1,120
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 6,734
DAU params: 1,492
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1209.49, accuracy: 0.5099
epoch: 1, loss: 1013.18, accuracy: 0.5409
epoch: 2, loss: 934.28, accuracy: 0.573
epoch: 3, loss: 891.89, accuracy: 0.5847
epoch: 4, loss: 862.57, accuracy: 0.5986
epoch: 5, loss: 842.75, accuracy: 0.5978
epoch: 6, loss: 776.56, accuracy: 0.6302
epoch: 7, loss: 765.95, accuracy: 0.6366
epoch: 8, loss: 759.68, accuracy: 0.636
epoch: 9, loss: 754.67, accuracy: 0.6342
time analysis:
    all 147.5296 s
    train 145.8315 s
Accuracy of     0 : 71 %
Accuracy of     1 : 80 %
Accuracy of     2 : 45 %
Accuracy of     3 : 34 %
Accuracy of     4 : 47 %
Accuracy of     5 : 69 %
Accuracy of     6 : 64 %
Accuracy of     7 : 70 %
Accuracy of     8 : 77 %
Accuracy of     9 : 71 %
```

## CIFAR10, 3_layers, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=8, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=8, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=32, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1            [-1, 8, 32, 32]              98
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
        DAUConv2di-4           [-1, 16, 16, 16]             448
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
        DAUConv2di-7             [-1, 32, 8, 8]           1,664
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 7,452
DAU params: 2,210
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1168.81, accuracy: 0.4967
epoch: 1, loss: 993.67, accuracy: 0.5588
epoch: 2, loss: 925.46, accuracy: 0.5964
epoch: 3, loss: 873.11, accuracy: 0.6103
epoch: 4, loss: 835.6, accuracy: 0.6142
epoch: 5, loss: 808.17, accuracy: 0.6119
epoch: 6, loss: 733.87, accuracy: 0.6567
epoch: 7, loss: 720.8, accuracy: 0.66
epoch: 8, loss: 714.23, accuracy: 0.6621
epoch: 9, loss: 707.67, accuracy: 0.6628
time analysis:
    all 161.2881 s
    train 158.9831 s
Accuracy of     0 : 76 %
Accuracy of     1 : 78 %
Accuracy of     2 : 50 %
Accuracy of     3 : 41 %
Accuracy of     4 : 52 %
Accuracy of     5 : 62 %
Accuracy of     6 : 64 %
Accuracy of     7 : 68 %
Accuracy of     8 : 77 %
Accuracy of     9 : 71 %
```

## CIFAR10, 3_layers, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=8, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=8, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=32, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1            [-1, 8, 32, 32]             128
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
        DAUConv2di-4           [-1, 16, 16, 16]             592
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
        DAUConv2di-7             [-1, 32, 8, 8]           2,208
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 8,170
DAU params: 2,928
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1166.58, accuracy: 0.5037
epoch: 1, loss: 979.19, accuracy: 0.564
epoch: 2, loss: 907.11, accuracy: 0.5896
epoch: 3, loss: 855.61, accuracy: 0.6059
epoch: 4, loss: 822.72, accuracy: 0.6146
epoch: 5, loss: 792.35, accuracy: 0.6248
epoch: 6, loss: 721.34, accuracy: 0.6621
epoch: 7, loss: 708.57, accuracy: 0.6617
epoch: 8, loss: 701.47, accuracy: 0.6617
epoch: 9, loss: 697.2, accuracy: 0.6659
time analysis:
    all 177.4197 s
    train 175.0887 s
Accuracy of     0 : 71 %
Accuracy of     1 : 82 %
Accuracy of     2 : 55 %
Accuracy of     3 : 35 %
Accuracy of     4 : 60 %
Accuracy of     5 : 55 %
Accuracy of     6 : 67 %
Accuracy of     7 : 68 %
Accuracy of     8 : 77 %
Accuracy of     9 : 66 %
```

## CIFAR10, 3_layers, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=8, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=8, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=32, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1            [-1, 8, 32, 32]              86
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
    DAUConv2dOneMu-4           [-1, 16, 16, 16]             406
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
    DAUConv2dOneMu-7             [-1, 32, 8, 8]           1,574
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 7,308
DAU params: 2,066
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1174.54, accuracy: 0.5255
epoch: 1, loss: 965.92, accuracy: 0.5761
epoch: 2, loss: 906.75, accuracy: 0.5518
epoch: 3, loss: 873.11, accuracy: 0.5985
epoch: 4, loss: 849.29, accuracy: 0.5968
epoch: 5, loss: 832.83, accuracy: 0.6014
epoch: 6, loss: 765.9, accuracy: 0.6406
epoch: 7, loss: 756.6, accuracy: 0.6405
epoch: 8, loss: 753.45, accuracy: 0.6341
epoch: 9, loss: 748.32, accuracy: 0.6477
time analysis:
    all 164.6687 s
    train 162.2411 s
Accuracy of     0 : 78 %
Accuracy of     1 : 76 %
Accuracy of     2 : 50 %
Accuracy of     3 : 39 %
Accuracy of     4 : 50 %
Accuracy of     5 : 52 %
Accuracy of     6 : 73 %
Accuracy of     7 : 65 %
Accuracy of     8 : 77 %
Accuracy of     9 : 66 %
```

## CIFAR10, 3_layers, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=8, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=8, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=32, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1            [-1, 8, 32, 32]             112
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
    DAUConv2dOneMu-4           [-1, 16, 16, 16]             536
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
    DAUConv2dOneMu-7             [-1, 32, 8, 8]           2,088
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 7,978
DAU params: 2,736
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1190.1, accuracy: 0.5102
epoch: 1, loss: 999.39, accuracy: 0.5582
epoch: 2, loss: 915.56, accuracy: 0.584
epoch: 3, loss: 862.33, accuracy: 0.6124
epoch: 4, loss: 825.81, accuracy: 0.6202
epoch: 5, loss: 798.33, accuracy: 0.6332
epoch: 6, loss: 728.72, accuracy: 0.6588
epoch: 7, loss: 715.39, accuracy: 0.6655
epoch: 8, loss: 709.83, accuracy: 0.6674
epoch: 9, loss: 703.64, accuracy: 0.6695
time analysis:
    all 179.7863 s
    train 177.433 s
Accuracy of     0 : 78 %
Accuracy of     1 : 68 %
Accuracy of     2 : 48 %
Accuracy of     3 : 39 %
Accuracy of     4 : 60 %
Accuracy of     5 : 50 %
Accuracy of     6 : 67 %
Accuracy of     7 : 65 %
Accuracy of     8 : 68 %
Accuracy of     9 : 70 %
```

## CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'num_workers': 10, 'out_folder': '/home/marko/Desktop/hdd/out/'}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=8, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=8, out_channels=16, no_units=1, sigma=0.5)
  (conv3): DAUConv2dZeroMu(in_channels=16, out_channels=32, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1            [-1, 8, 32, 32]              32
       BatchNorm2d-2            [-1, 8, 32, 32]              16
         MaxPool2d-3            [-1, 8, 16, 16]               0
   DAUConv2dZeroMu-4           [-1, 16, 16, 16]             144
       BatchNorm2d-5           [-1, 16, 16, 16]              32
         MaxPool2d-6             [-1, 16, 8, 8]               0
   DAUConv2dZeroMu-7             [-1, 32, 8, 8]             544
       BatchNorm2d-8             [-1, 32, 8, 8]              64
         MaxPool2d-9             [-1, 32, 4, 4]               0
           Linear-10                   [-1, 10]           5,130
================================================================
total params: 5,962
DAU params: 720
other params: 5,242
----------------------------------------------------------------
epoch: 0, loss: 1236.51, accuracy: 0.4844
epoch: 1, loss: 1103.5, accuracy: 0.5058
epoch: 2, loss: 1064.13, accuracy: 0.5216
epoch: 3, loss: 1040.91, accuracy: 0.5054
epoch: 4, loss: 1028.38, accuracy: 0.5269
epoch: 5, loss: 1015.44, accuracy: 0.5268
epoch: 6, loss: 966.54, accuracy: 0.5511
epoch: 7, loss: 959.85, accuracy: 0.5509
epoch: 8, loss: 953.92, accuracy: 0.5529
epoch: 9, loss: 952.99, accuracy: 0.5545
time analysis:
    all 74.1837 s
    train 71.7264 s
Accuracy of     0 : 71 %
Accuracy of     1 : 62 %
Accuracy of     2 : 54 %
Accuracy of     3 : 46 %
Accuracy of     4 : 38 %
Accuracy of     5 : 42 %
Accuracy of     6 : 58 %
Accuracy of     7 : 53 %
Accuracy of     8 : 70 %
Accuracy of     9 : 56 %
```

finish: 2019-07-03 10:14:39.554026
