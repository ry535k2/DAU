start: 2019-06-27 16:47:54.425699
* FashionMNIST, small_fc, Conv2d
* FashionMNIST, small_fc, DAUConv2d, units: 2
* FashionMNIST, small_fc, DAUConv2d, units: 3
* FashionMNIST, small_fc, DAUConv2d, units: 4
* FashionMNIST, small_fc, DAUConv2d, units: 5
* FashionMNIST, small_fc, DAUConv2d, units: 6
* FashionMNIST, small_fc, DAUConv2di, units: 2
* FashionMNIST, small_fc, DAUConv2di, units: 3
* FashionMNIST, small_fc, DAUConv2di, units: 4
* FashionMNIST, small_fc, DAUConv2dj, units: 2
* FashionMNIST, small_fc, DAUConv2dj, units: 3
* FashionMNIST, small_fc, DAUConv2dj, units: 4
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 1
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 2
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 3
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 4
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 5
* FashionMNIST, small_fc, DAUConv2dOneMu, units: 6
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1.5
* FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 2
* FashionMNIST, 3_layers, Conv2d
* FashionMNIST, 3_layers, DAUConv2d, units: 2
* FashionMNIST, 3_layers, DAUConv2d, units: 3
* FashionMNIST, 3_layers, DAUConv2d, units: 4
* FashionMNIST, 3_layers, DAUConv2d, units: 5
* FashionMNIST, 3_layers, DAUConv2d, units: 6
* FashionMNIST, 3_layers, DAUConv2di, units: 2
* FashionMNIST, 3_layers, DAUConv2di, units: 3
* FashionMNIST, 3_layers, DAUConv2di, units: 4
* FashionMNIST, 3_layers, DAUConv2dj, units: 2
* FashionMNIST, 3_layers, DAUConv2dj, units: 3
* FashionMNIST, 3_layers, DAUConv2dj, units: 4
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 1
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 2
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 3
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 4
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 5
* FashionMNIST, 3_layers, DAUConv2dOneMu, units: 6
* FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 0.5
* FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1
* FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1.5
* FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 2
* CIFAR10, small_fc, Conv2d
* CIFAR10, small_fc, DAUConv2d, units: 2
* CIFAR10, small_fc, DAUConv2d, units: 3
* CIFAR10, small_fc, DAUConv2d, units: 4
* CIFAR10, small_fc, DAUConv2d, units: 5
* CIFAR10, small_fc, DAUConv2d, units: 6
* CIFAR10, small_fc, DAUConv2di, units: 2
* CIFAR10, small_fc, DAUConv2di, units: 3
* CIFAR10, small_fc, DAUConv2di, units: 4
* CIFAR10, small_fc, DAUConv2dj, units: 2
* CIFAR10, small_fc, DAUConv2dj, units: 3
* CIFAR10, small_fc, DAUConv2dj, units: 4
* CIFAR10, small_fc, DAUConv2dOneMu, units: 1
* CIFAR10, small_fc, DAUConv2dOneMu, units: 2
* CIFAR10, small_fc, DAUConv2dOneMu, units: 3
* CIFAR10, small_fc, DAUConv2dOneMu, units: 4
* CIFAR10, small_fc, DAUConv2dOneMu, units: 5
* CIFAR10, small_fc, DAUConv2dOneMu, units: 6
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1.5
* CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 2
* CIFAR10, 3_layers, Conv2d
* CIFAR10, 3_layers, DAUConv2d, units: 2
* CIFAR10, 3_layers, DAUConv2d, units: 3
* CIFAR10, 3_layers, DAUConv2d, units: 4
* CIFAR10, 3_layers, DAUConv2d, units: 5
* CIFAR10, 3_layers, DAUConv2d, units: 6
* CIFAR10, 3_layers, DAUConv2di, units: 2
* CIFAR10, 3_layers, DAUConv2di, units: 3
* CIFAR10, 3_layers, DAUConv2di, units: 4
* CIFAR10, 3_layers, DAUConv2dj, units: 2
* CIFAR10, 3_layers, DAUConv2dj, units: 3
* CIFAR10, 3_layers, DAUConv2dj, units: 4
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 1
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 2
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 3
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 4
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 5
* CIFAR10, 3_layers, DAUConv2dOneMu, units: 6
* CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 0.5
* CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1
* CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1.5
* CIFAR10, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 2
* MNIST, small_fc, Conv2d
* MNIST, small_fc, DAUConv2d, units: 2
* MNIST, small_fc, DAUConv2d, units: 3
* MNIST, small_fc, DAUConv2d, units: 4
* MNIST, small_fc, DAUConv2d, units: 5
* MNIST, small_fc, DAUConv2d, units: 6
* MNIST, small_fc, DAUConv2di, units: 2
* MNIST, small_fc, DAUConv2di, units: 3
* MNIST, small_fc, DAUConv2di, units: 4
* MNIST, small_fc, DAUConv2dj, units: 2
* MNIST, small_fc, DAUConv2dj, units: 3
* MNIST, small_fc, DAUConv2dj, units: 4
* MNIST, small_fc, DAUConv2dOneMu, units: 1
* MNIST, small_fc, DAUConv2dOneMu, units: 2
* MNIST, small_fc, DAUConv2dOneMu, units: 3
* MNIST, small_fc, DAUConv2dOneMu, units: 4
* MNIST, small_fc, DAUConv2dOneMu, units: 5
* MNIST, small_fc, DAUConv2dOneMu, units: 6
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1.5
* MNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 2
* MNIST, 3_layers, Conv2d
* MNIST, 3_layers, DAUConv2d, units: 2
* MNIST, 3_layers, DAUConv2d, units: 3
* MNIST, 3_layers, DAUConv2d, units: 4
* MNIST, 3_layers, DAUConv2d, units: 5
* MNIST, 3_layers, DAUConv2d, units: 6
* MNIST, 3_layers, DAUConv2di, units: 2
* MNIST, 3_layers, DAUConv2di, units: 3
* MNIST, 3_layers, DAUConv2di, units: 4
* MNIST, 3_layers, DAUConv2dj, units: 2
* MNIST, 3_layers, DAUConv2dj, units: 3
* MNIST, 3_layers, DAUConv2dj, units: 4
* MNIST, 3_layers, DAUConv2dOneMu, units: 1
* MNIST, 3_layers, DAUConv2dOneMu, units: 2
* MNIST, 3_layers, DAUConv2dOneMu, units: 3
* MNIST, 3_layers, DAUConv2dOneMu, units: 4
* MNIST, 3_layers, DAUConv2dOneMu, units: 5
* MNIST, 3_layers, DAUConv2dOneMu, units: 6
* MNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 0.5
* MNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1
* MNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1.5
* MNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 2

## FashionMNIST, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(1, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(32, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
            Conv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 0
other params: 3,442
----------------------------------------------------------------
epoch: 0, loss: 457.97, accuracy: 0.8367
epoch: 1, loss: 349.35, accuracy: 0.8669
epoch: 2, loss: 327.29, accuracy: 0.8554
epoch: 3, loss: 315.4, accuracy: 0.8686
epoch: 4, loss: 305.11, accuracy: 0.8542
epoch: 5, loss: 299.65, accuracy: 0.8772
epoch: 6, loss: 291.45, accuracy: 0.8769
epoch: 7, loss: 288.27, accuracy: 0.88
epoch: 8, loss: 284.38, accuracy: 0.8724
epoch: 9, loss: 281.03, accuracy: 0.8757
epoch: 10, loss: 278.17, accuracy: 0.8775
epoch: 11, loss: 274.26, accuracy: 0.881
epoch: 12, loss: 273.53, accuracy: 0.8743
epoch: 13, loss: 270.37, accuracy: 0.8863
epoch: 14, loss: 269.73, accuracy: 0.8833
epoch: 15, loss: 267.51, accuracy: 0.8806
epoch: 16, loss: 264.8, accuracy: 0.8778
epoch: 17, loss: 264.56, accuracy: 0.8802
epoch: 18, loss: 260.34, accuracy: 0.8819
epoch: 19, loss: 259.94, accuracy: 0.8797
epoch: 20, loss: 259.58, accuracy: 0.8813
epoch: 21, loss: 257.77, accuracy: 0.8845
epoch: 22, loss: 256.35, accuracy: 0.8812
epoch: 23, loss: 254.57, accuracy: 0.8782
epoch: 24, loss: 253.61, accuracy: 0.8769
epoch: 25, loss: 254.6, accuracy: 0.8828
epoch: 26, loss: 251.13, accuracy: 0.8836
epoch: 27, loss: 252.63, accuracy: 0.8775
epoch: 28, loss: 250.04, accuracy: 0.8896
epoch: 29, loss: 250.55, accuracy: 0.8831
epoch: 30, loss: 248.99, accuracy: 0.8859
epoch: 31, loss: 248.6, accuracy: 0.8817
epoch: 32, loss: 246.05, accuracy: 0.8894
epoch: 33, loss: 244.42, accuracy: 0.8823
epoch: 34, loss: 244.87, accuracy: 0.8852
epoch: 35, loss: 244.89, accuracy: 0.8833
epoch: 36, loss: 243.33, accuracy: 0.8836
epoch: 37, loss: 243.5, accuracy: 0.8817
epoch: 38, loss: 244.71, accuracy: 0.8847
epoch: 39, loss: 241.85, accuracy: 0.8821
epoch: 40, loss: 242.23, accuracy: 0.8763
epoch: 41, loss: 242.97, accuracy: 0.8841
epoch: 42, loss: 239.97, accuracy: 0.8767
epoch: 43, loss: 238.65, accuracy: 0.884
epoch: 44, loss: 239.23, accuracy: 0.8854
epoch: 45, loss: 236.7, accuracy: 0.8825
epoch: 46, loss: 239.21, accuracy: 0.8817
epoch: 47, loss: 237.68, accuracy: 0.8823
epoch: 48, loss: 237.49, accuracy: 0.8708
epoch: 49, loss: 238.17, accuracy: 0.8859
epoch: 50, loss: 236.21, accuracy: 0.8812
epoch: 51, loss: 235.9, accuracy: 0.8849
epoch: 52, loss: 237.25, accuracy: 0.8872
epoch: 53, loss: 236.19, accuracy: 0.8871
epoch: 54, loss: 236.38, accuracy: 0.8837
epoch: 55, loss: 233.23, accuracy: 0.8844
epoch: 56, loss: 235.0, accuracy: 0.8851
epoch: 57, loss: 234.42, accuracy: 0.8719
epoch: 58, loss: 233.98, accuracy: 0.8788
epoch: 59, loss: 233.0, accuracy: 0.8866
epoch: 60, loss: 233.78, accuracy: 0.8854
epoch: 61, loss: 231.83, accuracy: 0.8836
epoch: 62, loss: 233.39, accuracy: 0.8866
epoch: 63, loss: 234.02, accuracy: 0.886
epoch: 64, loss: 233.11, accuracy: 0.8875
epoch: 65, loss: 232.49, accuracy: 0.8827
epoch: 66, loss: 230.86, accuracy: 0.8854
epoch: 67, loss: 232.32, accuracy: 0.8882
epoch: 68, loss: 230.08, accuracy: 0.8854
epoch: 69, loss: 229.49, accuracy: 0.8845
epoch: 70, loss: 231.33, accuracy: 0.8827
epoch: 71, loss: 231.02, accuracy: 0.8809
epoch: 72, loss: 231.22, accuracy: 0.8842
epoch: 73, loss: 232.16, accuracy: 0.8863
epoch: 74, loss: 229.72, accuracy: 0.8852
epoch: 75, loss: 208.29, accuracy: 0.8898
epoch: 76, loss: 205.93, accuracy: 0.8913
epoch: 77, loss: 204.69, accuracy: 0.8911
epoch: 78, loss: 204.23, accuracy: 0.8916
epoch: 79, loss: 203.44, accuracy: 0.8904
epoch: 80, loss: 203.04, accuracy: 0.8896
epoch: 81, loss: 202.43, accuracy: 0.8903
epoch: 82, loss: 202.62, accuracy: 0.8889
epoch: 83, loss: 203.03, accuracy: 0.8903
epoch: 84, loss: 202.22, accuracy: 0.892
epoch: 85, loss: 202.28, accuracy: 0.8904
epoch: 86, loss: 201.89, accuracy: 0.8896
epoch: 87, loss: 201.52, accuracy: 0.8901
epoch: 88, loss: 201.39, accuracy: 0.8909
epoch: 89, loss: 202.0, accuracy: 0.8906
epoch: 90, loss: 201.59, accuracy: 0.8906
epoch: 91, loss: 201.28, accuracy: 0.8901
epoch: 92, loss: 201.44, accuracy: 0.8908
epoch: 93, loss: 199.87, accuracy: 0.8899
epoch: 94, loss: 201.09, accuracy: 0.89
epoch: 95, loss: 200.86, accuracy: 0.8886
epoch: 96, loss: 200.85, accuracy: 0.8894
epoch: 97, loss: 201.22, accuracy: 0.8904
epoch: 98, loss: 200.56, accuracy: 0.8888
Epoch    99: reducing learning rate of group 0 to 5.0000e-04.
epoch: 99, loss: 200.61, accuracy: 0.8908
time analysis:
    train 572.3246 s
Accuracy of     0 : 84 %
Accuracy of     1 : 96 %
Accuracy of     2 : 82 %
Accuracy of     3 : 88 %
Accuracy of     4 : 75 %
Accuracy of     5 : 100 %
Accuracy of     6 : 74 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           1,544
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,578
DAU params: 1,768
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 527.72, accuracy: 0.8049
epoch: 1, loss: 390.39, accuracy: 0.8389
epoch: 2, loss: 359.89, accuracy: 0.8439
epoch: 3, loss: 342.86, accuracy: 0.8629
epoch: 4, loss: 329.16, accuracy: 0.8667
epoch: 5, loss: 320.4, accuracy: 0.8625
epoch: 6, loss: 313.66, accuracy: 0.8628
epoch: 7, loss: 306.64, accuracy: 0.8741
epoch: 8, loss: 302.72, accuracy: 0.8745
epoch: 9, loss: 297.91, accuracy: 0.8801
epoch: 10, loss: 293.1, accuracy: 0.8736
epoch: 11, loss: 290.27, accuracy: 0.8865
epoch: 12, loss: 287.07, accuracy: 0.8629
epoch: 13, loss: 282.42, accuracy: 0.8804
epoch: 14, loss: 282.14, accuracy: 0.8853
epoch: 15, loss: 281.25, accuracy: 0.8832
epoch: 16, loss: 278.06, accuracy: 0.8852
epoch: 17, loss: 277.47, accuracy: 0.8833
epoch: 18, loss: 276.07, accuracy: 0.8835
epoch: 19, loss: 274.75, accuracy: 0.8765
epoch: 20, loss: 273.38, accuracy: 0.8797
epoch: 21, loss: 272.46, accuracy: 0.8884
epoch: 22, loss: 271.59, accuracy: 0.8846
epoch: 23, loss: 269.93, accuracy: 0.8878
epoch: 24, loss: 269.11, accuracy: 0.8853
epoch: 25, loss: 268.66, accuracy: 0.889
epoch: 26, loss: 267.07, accuracy: 0.8936
epoch: 27, loss: 269.45, accuracy: 0.8894
epoch: 28, loss: 267.49, accuracy: 0.8881
epoch: 29, loss: 265.69, accuracy: 0.8887
epoch: 30, loss: 265.6, accuracy: 0.8862
epoch: 31, loss: 264.45, accuracy: 0.8949
epoch: 32, loss: 263.34, accuracy: 0.8925
epoch: 33, loss: 262.52, accuracy: 0.8908
epoch: 34, loss: 262.43, accuracy: 0.8933
epoch: 35, loss: 261.15, accuracy: 0.8919
epoch: 36, loss: 262.83, accuracy: 0.8875
epoch: 37, loss: 262.4, accuracy: 0.8922
epoch: 38, loss: 259.69, accuracy: 0.8862
epoch: 39, loss: 261.48, accuracy: 0.8919
epoch: 40, loss: 260.32, accuracy: 0.8884
epoch: 41, loss: 260.61, accuracy: 0.8884
epoch: 42, loss: 259.84, accuracy: 0.8919
epoch: 43, loss: 257.25, accuracy: 0.8839
epoch: 44, loss: 258.56, accuracy: 0.8923
epoch: 45, loss: 256.71, accuracy: 0.8842
epoch: 46, loss: 257.53, accuracy: 0.896
epoch: 47, loss: 256.1, accuracy: 0.8955
epoch: 48, loss: 256.33, accuracy: 0.8962
epoch: 49, loss: 255.87, accuracy: 0.8958
epoch: 50, loss: 254.49, accuracy: 0.8908
epoch: 51, loss: 257.05, accuracy: 0.8893
epoch: 52, loss: 254.26, accuracy: 0.8935
epoch: 53, loss: 257.66, accuracy: 0.8884
epoch: 54, loss: 255.88, accuracy: 0.8987
epoch: 55, loss: 253.61, accuracy: 0.8926
epoch: 56, loss: 256.24, accuracy: 0.8857
epoch: 57, loss: 253.96, accuracy: 0.8977
epoch: 58, loss: 253.24, accuracy: 0.8939
epoch: 59, loss: 253.31, accuracy: 0.8925
epoch: 60, loss: 252.78, accuracy: 0.8953
epoch: 61, loss: 253.16, accuracy: 0.8965
epoch: 62, loss: 252.98, accuracy: 0.8926
epoch: 63, loss: 252.77, accuracy: 0.8938
epoch: 64, loss: 252.02, accuracy: 0.8902
epoch: 65, loss: 251.25, accuracy: 0.8959
epoch: 66, loss: 251.0, accuracy: 0.8941
epoch: 67, loss: 250.07, accuracy: 0.898
epoch: 68, loss: 251.49, accuracy: 0.891
epoch: 69, loss: 251.48, accuracy: 0.892
epoch: 70, loss: 252.64, accuracy: 0.8915
epoch: 71, loss: 249.97, accuracy: 0.8981
epoch: 72, loss: 249.84, accuracy: 0.895
Epoch    73: reducing learning rate of group 0 to 5.0000e-03.
epoch: 73, loss: 251.72, accuracy: 0.8989
epoch: 74, loss: 236.48, accuracy: 0.9004
epoch: 75, loss: 222.14, accuracy: 0.9042
epoch: 76, loss: 220.62, accuracy: 0.9045
epoch: 77, loss: 219.62, accuracy: 0.9038
epoch: 78, loss: 219.4, accuracy: 0.9049
epoch: 79, loss: 218.95, accuracy: 0.9037
epoch: 80, loss: 219.2, accuracy: 0.9037
epoch: 81, loss: 218.16, accuracy: 0.9031
epoch: 82, loss: 218.24, accuracy: 0.9044
epoch: 83, loss: 218.19, accuracy: 0.9046
epoch: 84, loss: 217.36, accuracy: 0.9044
epoch: 85, loss: 217.38, accuracy: 0.9048
epoch: 86, loss: 217.77, accuracy: 0.9028
epoch: 87, loss: 217.09, accuracy: 0.9048
epoch: 88, loss: 217.61, accuracy: 0.9044
epoch: 89, loss: 217.07, accuracy: 0.9049
epoch: 90, loss: 217.38, accuracy: 0.9045
epoch: 91, loss: 216.75, accuracy: 0.9044
epoch: 92, loss: 216.09, accuracy: 0.9044
epoch: 93, loss: 216.61, accuracy: 0.9041
epoch: 94, loss: 216.26, accuracy: 0.9038
epoch: 95, loss: 216.25, accuracy: 0.9048
epoch: 96, loss: 216.11, accuracy: 0.9044
epoch: 97, loss: 217.61, accuracy: 0.9041
Epoch    98: reducing learning rate of group 0 to 2.5000e-04.
epoch: 98, loss: 215.91, accuracy: 0.9038
epoch: 99, loss: 215.39, accuracy: 0.9036
time analysis:
    all 1819.6853 s
    train 1818.8129 s
Accuracy of     0 : 90 %
Accuracy of     1 : 93 %
Accuracy of     2 : 90 %
Accuracy of     3 : 87 %
Accuracy of     4 : 76 %
Accuracy of     5 : 96 %
Accuracy of     6 : 69 %
Accuracy of     7 : 100 %
Accuracy of     8 : 95 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           2,312
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 3,442
DAU params: 2,632
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 511.45, accuracy: 0.8277
epoch: 1, loss: 374.72, accuracy: 0.8481
epoch: 2, loss: 343.47, accuracy: 0.8591
epoch: 3, loss: 328.5, accuracy: 0.865
epoch: 4, loss: 317.65, accuracy: 0.8774
epoch: 5, loss: 308.59, accuracy: 0.8732
epoch: 6, loss: 301.19, accuracy: 0.8766
epoch: 7, loss: 297.25, accuracy: 0.8828
epoch: 8, loss: 292.89, accuracy: 0.8757
epoch: 9, loss: 290.13, accuracy: 0.8782
epoch: 10, loss: 289.11, accuracy: 0.8712
epoch: 11, loss: 284.87, accuracy: 0.876
epoch: 12, loss: 281.55, accuracy: 0.8823
epoch: 13, loss: 279.0, accuracy: 0.8855
epoch: 14, loss: 277.68, accuracy: 0.8817
epoch: 15, loss: 274.61, accuracy: 0.8818
epoch: 16, loss: 272.11, accuracy: 0.8862
epoch: 17, loss: 270.96, accuracy: 0.8873
epoch: 18, loss: 270.86, accuracy: 0.8812
epoch: 19, loss: 269.38, accuracy: 0.8827
epoch: 20, loss: 269.3, accuracy: 0.8855
epoch: 21, loss: 266.82, accuracy: 0.8869
epoch: 22, loss: 266.23, accuracy: 0.8826
epoch: 23, loss: 265.3, accuracy: 0.8738
epoch: 24, loss: 263.38, accuracy: 0.8838
epoch: 25, loss: 263.85, accuracy: 0.8919
epoch: 26, loss: 261.26, accuracy: 0.8846
epoch: 27, loss: 260.39, accuracy: 0.8886
epoch: 28, loss: 261.65, accuracy: 0.8836
epoch: 29, loss: 260.96, accuracy: 0.8877
epoch: 30, loss: 260.87, accuracy: 0.8864
epoch: 31, loss: 257.85, accuracy: 0.8877
epoch: 32, loss: 257.65, accuracy: 0.8913
epoch: 33, loss: 257.21, accuracy: 0.8915
epoch: 34, loss: 257.39, accuracy: 0.8914
epoch: 35, loss: 254.77, accuracy: 0.8861
epoch: 36, loss: 256.95, accuracy: 0.8917
epoch: 37, loss: 254.15, accuracy: 0.8913
epoch: 38, loss: 254.48, accuracy: 0.8843
epoch: 39, loss: 255.28, accuracy: 0.8919
epoch: 40, loss: 251.98, accuracy: 0.8867
epoch: 41, loss: 253.09, accuracy: 0.8911
epoch: 42, loss: 252.1, accuracy: 0.8844
epoch: 43, loss: 253.07, accuracy: 0.885
epoch: 44, loss: 250.61, accuracy: 0.886
epoch: 45, loss: 252.8, accuracy: 0.889
epoch: 46, loss: 251.64, accuracy: 0.8875
epoch: 47, loss: 249.63, accuracy: 0.8882
epoch: 48, loss: 249.64, accuracy: 0.8953
epoch: 49, loss: 249.4, accuracy: 0.8818
epoch: 50, loss: 247.34, accuracy: 0.8944
epoch: 51, loss: 247.03, accuracy: 0.8929
epoch: 52, loss: 248.5, accuracy: 0.8864
epoch: 53, loss: 249.39, accuracy: 0.8895
epoch: 54, loss: 245.95, accuracy: 0.8887
epoch: 55, loss: 246.35, accuracy: 0.8875
epoch: 56, loss: 246.56, accuracy: 0.8933
epoch: 57, loss: 246.16, accuracy: 0.8946
epoch: 58, loss: 243.99, accuracy: 0.8918
epoch: 59, loss: 246.21, accuracy: 0.896
epoch: 60, loss: 244.99, accuracy: 0.8948
epoch: 61, loss: 246.72, accuracy: 0.8924
epoch: 62, loss: 244.87, accuracy: 0.8968
epoch: 63, loss: 243.48, accuracy: 0.8875
epoch: 64, loss: 245.5, accuracy: 0.8869
epoch: 65, loss: 243.27, accuracy: 0.8878
epoch: 66, loss: 244.11, accuracy: 0.8925
epoch: 67, loss: 244.27, accuracy: 0.896
epoch: 68, loss: 243.79, accuracy: 0.8931
epoch: 69, loss: 243.0, accuracy: 0.8935
epoch: 70, loss: 242.07, accuracy: 0.8956
epoch: 71, loss: 241.4, accuracy: 0.891
epoch: 72, loss: 241.43, accuracy: 0.8935
epoch: 73, loss: 241.38, accuracy: 0.8949
epoch: 74, loss: 242.42, accuracy: 0.8891
epoch: 75, loss: 217.27, accuracy: 0.9013
epoch: 76, loss: 212.37, accuracy: 0.9028
epoch: 77, loss: 210.43, accuracy: 0.9012
epoch: 78, loss: 210.22, accuracy: 0.9014
epoch: 79, loss: 209.05, accuracy: 0.9015
epoch: 80, loss: 209.12, accuracy: 0.9008
epoch: 81, loss: 208.98, accuracy: 0.9012
epoch: 82, loss: 208.18, accuracy: 0.9019
epoch: 83, loss: 207.72, accuracy: 0.9012
epoch: 84, loss: 208.01, accuracy: 0.9014
epoch: 85, loss: 206.74, accuracy: 0.9015
epoch: 86, loss: 207.37, accuracy: 0.9019
epoch: 87, loss: 206.5, accuracy: 0.902
epoch: 88, loss: 205.79, accuracy: 0.9016
epoch: 89, loss: 205.63, accuracy: 0.9014
epoch: 90, loss: 205.82, accuracy: 0.9014
epoch: 91, loss: 206.24, accuracy: 0.9013
epoch: 92, loss: 204.91, accuracy: 0.9005
epoch: 93, loss: 205.32, accuracy: 0.9022
epoch: 94, loss: 206.01, accuracy: 0.9018
epoch: 95, loss: 204.83, accuracy: 0.9021
epoch: 96, loss: 204.59, accuracy: 0.9019
epoch: 97, loss: 205.62, accuracy: 0.9003
epoch: 98, loss: 204.69, accuracy: 0.9019
epoch: 99, loss: 204.63, accuracy: 0.9001
time analysis:
    all 2472.7267 s
    train 2471.4193 s
Accuracy of     0 : 80 %
Accuracy of     1 : 95 %
Accuracy of     2 : 81 %
Accuracy of     3 : 90 %
Accuracy of     4 : 83 %
Accuracy of     5 : 98 %
Accuracy of     6 : 73 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             416
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           3,080
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 4,306
DAU params: 3,496
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 532.12, accuracy: 0.8383
epoch: 1, loss: 383.32, accuracy: 0.8573
epoch: 2, loss: 355.49, accuracy: 0.8618
epoch: 3, loss: 340.48, accuracy: 0.859
epoch: 4, loss: 328.34, accuracy: 0.8717
epoch: 5, loss: 318.75, accuracy: 0.8622
epoch: 6, loss: 313.28, accuracy: 0.8797
epoch: 7, loss: 307.29, accuracy: 0.8721
epoch: 8, loss: 303.98, accuracy: 0.8676
epoch: 9, loss: 299.77, accuracy: 0.8662
epoch: 10, loss: 296.88, accuracy: 0.8814
epoch: 11, loss: 292.82, accuracy: 0.8837
epoch: 12, loss: 291.54, accuracy: 0.8778
epoch: 13, loss: 288.7, accuracy: 0.8817
epoch: 14, loss: 287.33, accuracy: 0.8784
epoch: 15, loss: 285.79, accuracy: 0.8743
epoch: 16, loss: 284.41, accuracy: 0.8804
epoch: 17, loss: 282.06, accuracy: 0.8742
epoch: 18, loss: 282.52, accuracy: 0.8777
epoch: 19, loss: 279.14, accuracy: 0.8849
epoch: 20, loss: 280.83, accuracy: 0.8759
epoch: 21, loss: 277.14, accuracy: 0.8728
epoch: 22, loss: 275.34, accuracy: 0.8844
epoch: 23, loss: 274.22, accuracy: 0.8815
epoch: 24, loss: 273.2, accuracy: 0.8845
epoch: 25, loss: 274.27, accuracy: 0.8867
epoch: 26, loss: 271.45, accuracy: 0.8825
epoch: 27, loss: 273.06, accuracy: 0.8876
epoch: 28, loss: 271.67, accuracy: 0.8797
epoch: 29, loss: 269.77, accuracy: 0.877
epoch: 30, loss: 268.82, accuracy: 0.8832
epoch: 31, loss: 267.02, accuracy: 0.8855
epoch: 32, loss: 269.48, accuracy: 0.8821
epoch: 33, loss: 267.7, accuracy: 0.8792
epoch: 34, loss: 265.74, accuracy: 0.887
epoch: 35, loss: 267.29, accuracy: 0.8877
epoch: 36, loss: 267.43, accuracy: 0.8826
epoch: 37, loss: 265.15, accuracy: 0.8897
epoch: 38, loss: 263.67, accuracy: 0.8842
epoch: 39, loss: 264.19, accuracy: 0.8826
epoch: 40, loss: 263.52, accuracy: 0.8881
epoch: 41, loss: 262.74, accuracy: 0.8778
epoch: 42, loss: 261.62, accuracy: 0.8886
epoch: 43, loss: 262.76, accuracy: 0.8888
epoch: 44, loss: 260.72, accuracy: 0.8863
epoch: 45, loss: 262.2, accuracy: 0.8937
epoch: 46, loss: 260.22, accuracy: 0.8868
epoch: 47, loss: 260.34, accuracy: 0.8839
epoch: 48, loss: 258.36, accuracy: 0.884
epoch: 49, loss: 260.55, accuracy: 0.8816
epoch: 50, loss: 258.7, accuracy: 0.8942
epoch: 51, loss: 258.26, accuracy: 0.8946
epoch: 52, loss: 260.41, accuracy: 0.893
epoch: 53, loss: 258.3, accuracy: 0.8823
Epoch    54: reducing learning rate of group 0 to 5.0000e-03.
epoch: 54, loss: 258.31, accuracy: 0.8806
epoch: 55, loss: 241.79, accuracy: 0.8933
epoch: 56, loss: 239.46, accuracy: 0.8928
epoch: 57, loss: 240.36, accuracy: 0.8968
epoch: 58, loss: 240.02, accuracy: 0.8958
epoch: 59, loss: 238.57, accuracy: 0.8956
epoch: 60, loss: 238.51, accuracy: 0.8954
epoch: 61, loss: 237.85, accuracy: 0.8909
epoch: 62, loss: 238.94, accuracy: 0.8932
epoch: 63, loss: 238.1, accuracy: 0.8941
epoch: 64, loss: 235.84, accuracy: 0.893
epoch: 65, loss: 236.61, accuracy: 0.893
epoch: 66, loss: 236.4, accuracy: 0.8932
epoch: 67, loss: 236.64, accuracy: 0.8922
epoch: 68, loss: 235.7, accuracy: 0.8931
epoch: 69, loss: 235.92, accuracy: 0.8925
epoch: 70, loss: 235.38, accuracy: 0.8951
epoch: 71, loss: 236.77, accuracy: 0.8947
epoch: 72, loss: 234.84, accuracy: 0.8917
epoch: 73, loss: 234.22, accuracy: 0.893
epoch: 74, loss: 233.89, accuracy: 0.8956
epoch: 75, loss: 220.26, accuracy: 0.8997
epoch: 76, loss: 218.48, accuracy: 0.8996
epoch: 77, loss: 217.83, accuracy: 0.8987
epoch: 78, loss: 217.38, accuracy: 0.8997
epoch: 79, loss: 216.51, accuracy: 0.8987
epoch: 80, loss: 216.99, accuracy: 0.9001
epoch: 81, loss: 216.86, accuracy: 0.8989
epoch: 82, loss: 216.95, accuracy: 0.8995
epoch: 83, loss: 215.96, accuracy: 0.899
epoch: 84, loss: 216.45, accuracy: 0.8995
epoch: 85, loss: 215.95, accuracy: 0.8996
epoch: 86, loss: 216.74, accuracy: 0.8992
epoch: 87, loss: 216.32, accuracy: 0.8996
epoch: 88, loss: 216.24, accuracy: 0.8981
Epoch    89: reducing learning rate of group 0 to 2.5000e-04.
epoch: 89, loss: 216.19, accuracy: 0.9007
epoch: 90, loss: 214.42, accuracy: 0.8999
epoch: 91, loss: 214.78, accuracy: 0.9
epoch: 92, loss: 215.0, accuracy: 0.9005
epoch: 93, loss: 214.87, accuracy: 0.8999
epoch: 94, loss: 215.43, accuracy: 0.9005
epoch: 95, loss: 215.18, accuracy: 0.8995
epoch: 96, loss: 214.32, accuracy: 0.8999
epoch: 97, loss: 214.56, accuracy: 0.9002
epoch: 98, loss: 214.73, accuracy: 0.9004
epoch: 99, loss: 214.52, accuracy: 0.8991
time analysis:
    all 3094.9658 s
    train 3093.4661 s
Accuracy of     0 : 80 %
Accuracy of     1 : 93 %
Accuracy of     2 : 84 %
Accuracy of     3 : 83 %
Accuracy of     4 : 80 %
Accuracy of     5 : 98 %
Accuracy of     6 : 73 %
Accuracy of     7 : 98 %
Accuracy of     8 : 95 %
Accuracy of     9 : 100 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=5, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             512
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           3,848
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 5,170
DAU params: 4,360
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 496.99, accuracy: 0.826
epoch: 1, loss: 378.55, accuracy: 0.8404
epoch: 2, loss: 353.58, accuracy: 0.8475
epoch: 3, loss: 336.89, accuracy: 0.8726
epoch: 4, loss: 322.81, accuracy: 0.8762
epoch: 5, loss: 313.5, accuracy: 0.8673
epoch: 6, loss: 306.89, accuracy: 0.8707
epoch: 7, loss: 299.94, accuracy: 0.8805
epoch: 8, loss: 295.71, accuracy: 0.8774
epoch: 9, loss: 289.49, accuracy: 0.8692
epoch: 10, loss: 283.97, accuracy: 0.8753
epoch: 11, loss: 284.81, accuracy: 0.8858
epoch: 12, loss: 280.31, accuracy: 0.888
epoch: 13, loss: 276.55, accuracy: 0.8842
epoch: 14, loss: 272.24, accuracy: 0.8868
epoch: 15, loss: 271.84, accuracy: 0.881
epoch: 16, loss: 268.03, accuracy: 0.8932
epoch: 17, loss: 268.97, accuracy: 0.882
epoch: 18, loss: 267.18, accuracy: 0.8906
epoch: 19, loss: 263.59, accuracy: 0.8851
epoch: 20, loss: 263.68, accuracy: 0.8943
epoch: 21, loss: 261.28, accuracy: 0.8827
epoch: 22, loss: 258.87, accuracy: 0.885
epoch: 23, loss: 258.49, accuracy: 0.8861
epoch: 24, loss: 257.15, accuracy: 0.8913
epoch: 25, loss: 257.19, accuracy: 0.8898
epoch: 26, loss: 257.11, accuracy: 0.8904
epoch: 27, loss: 255.07, accuracy: 0.8883
epoch: 28, loss: 254.25, accuracy: 0.8857
epoch: 29, loss: 256.13, accuracy: 0.8884
epoch: 30, loss: 253.58, accuracy: 0.892
epoch: 31, loss: 253.88, accuracy: 0.8915
epoch: 32, loss: 252.5, accuracy: 0.8905
epoch: 33, loss: 250.06, accuracy: 0.8893
epoch: 34, loss: 249.75, accuracy: 0.8921
epoch: 35, loss: 251.47, accuracy: 0.8921
epoch: 36, loss: 249.36, accuracy: 0.8952
epoch: 37, loss: 249.15, accuracy: 0.8765
epoch: 38, loss: 248.2, accuracy: 0.8916
epoch: 39, loss: 249.72, accuracy: 0.891
epoch: 40, loss: 249.23, accuracy: 0.8964
epoch: 41, loss: 248.37, accuracy: 0.8858
epoch: 42, loss: 246.65, accuracy: 0.8974
epoch: 43, loss: 245.45, accuracy: 0.8961
epoch: 44, loss: 245.49, accuracy: 0.8959
epoch: 45, loss: 246.22, accuracy: 0.8967
epoch: 46, loss: 245.54, accuracy: 0.8898
epoch: 47, loss: 243.07, accuracy: 0.8956
epoch: 48, loss: 243.83, accuracy: 0.898
epoch: 49, loss: 244.54, accuracy: 0.8857
epoch: 50, loss: 243.86, accuracy: 0.8911
epoch: 51, loss: 243.62, accuracy: 0.9002
epoch: 52, loss: 242.5, accuracy: 0.8939
epoch: 53, loss: 242.81, accuracy: 0.8902
epoch: 54, loss: 241.29, accuracy: 0.8959
epoch: 55, loss: 242.33, accuracy: 0.8848
epoch: 56, loss: 242.85, accuracy: 0.8925
epoch: 57, loss: 243.08, accuracy: 0.897
epoch: 58, loss: 239.72, accuracy: 0.8926
epoch: 59, loss: 239.57, accuracy: 0.8888
epoch: 60, loss: 239.61, accuracy: 0.8845
epoch: 61, loss: 240.69, accuracy: 0.8978
epoch: 62, loss: 242.13, accuracy: 0.8945
epoch: 63, loss: 239.8, accuracy: 0.8994
Epoch    64: reducing learning rate of group 0 to 5.0000e-03.
epoch: 64, loss: 239.62, accuracy: 0.8991
epoch: 65, loss: 222.77, accuracy: 0.8996
epoch: 66, loss: 220.04, accuracy: 0.8982
epoch: 67, loss: 219.32, accuracy: 0.9001
epoch: 68, loss: 219.23, accuracy: 0.9023
epoch: 69, loss: 217.69, accuracy: 0.9015
epoch: 70, loss: 216.47, accuracy: 0.8995
epoch: 71, loss: 217.59, accuracy: 0.9023
epoch: 72, loss: 216.79, accuracy: 0.8992
epoch: 73, loss: 217.07, accuracy: 0.9019
epoch: 74, loss: 216.71, accuracy: 0.9049
epoch: 75, loss: 201.13, accuracy: 0.9057
epoch: 76, loss: 198.1, accuracy: 0.9049
epoch: 77, loss: 198.18, accuracy: 0.9059
epoch: 78, loss: 197.21, accuracy: 0.9053
epoch: 79, loss: 196.08, accuracy: 0.9057
epoch: 80, loss: 197.14, accuracy: 0.9054
epoch: 81, loss: 196.23, accuracy: 0.9069
epoch: 82, loss: 195.98, accuracy: 0.9059
epoch: 83, loss: 195.87, accuracy: 0.9044
epoch: 84, loss: 195.9, accuracy: 0.9054
epoch: 85, loss: 196.58, accuracy: 0.9043
epoch: 86, loss: 195.81, accuracy: 0.9058
epoch: 87, loss: 195.79, accuracy: 0.9037
epoch: 88, loss: 195.3, accuracy: 0.9046
epoch: 89, loss: 195.49, accuracy: 0.9062
epoch: 90, loss: 195.27, accuracy: 0.9061
epoch: 91, loss: 195.75, accuracy: 0.9064
epoch: 92, loss: 194.86, accuracy: 0.9046
epoch: 93, loss: 195.1, accuracy: 0.9053
epoch: 94, loss: 194.66, accuracy: 0.9051
epoch: 95, loss: 194.29, accuracy: 0.9053
epoch: 96, loss: 193.67, accuracy: 0.9044
epoch: 97, loss: 194.67, accuracy: 0.9057
epoch: 98, loss: 194.05, accuracy: 0.9056
epoch: 99, loss: 194.5, accuracy: 0.905
time analysis:
    all 3804.4974 s
    train 3802.8232 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 85 %
Accuracy of     3 : 80 %
Accuracy of     4 : 83 %
Accuracy of     5 : 96 %
Accuracy of     6 : 70 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 100 %
```

## FashionMNIST, small_fc, DAUConv2d, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=32, no_units=6, sigma=0.5)
  (conv2): DAUConv2d(in_channels=32, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 32, 28, 28]             608
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
         DAUConv2d-4            [-1, 8, 14, 14]           4,616
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 6,034
DAU params: 5,224
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 508.89, accuracy: 0.8362
epoch: 1, loss: 374.02, accuracy: 0.8484
epoch: 2, loss: 345.96, accuracy: 0.866
epoch: 3, loss: 328.61, accuracy: 0.867
epoch: 4, loss: 317.89, accuracy: 0.8638
epoch: 5, loss: 309.04, accuracy: 0.8672
epoch: 6, loss: 299.73, accuracy: 0.8778
epoch: 7, loss: 294.32, accuracy: 0.8725
epoch: 8, loss: 291.58, accuracy: 0.8793
epoch: 9, loss: 286.03, accuracy: 0.8857
epoch: 10, loss: 285.0, accuracy: 0.8815
epoch: 11, loss: 281.57, accuracy: 0.8863
epoch: 12, loss: 278.0, accuracy: 0.8844
epoch: 13, loss: 276.07, accuracy: 0.8875
epoch: 14, loss: 274.3, accuracy: 0.8831
epoch: 15, loss: 272.18, accuracy: 0.8791
epoch: 16, loss: 271.28, accuracy: 0.8837
epoch: 17, loss: 270.18, accuracy: 0.8876
epoch: 18, loss: 267.46, accuracy: 0.8857
epoch: 19, loss: 265.39, accuracy: 0.8886
epoch: 20, loss: 266.26, accuracy: 0.879
epoch: 21, loss: 264.42, accuracy: 0.8844
epoch: 22, loss: 263.55, accuracy: 0.8867
epoch: 23, loss: 264.78, accuracy: 0.8911
epoch: 24, loss: 261.16, accuracy: 0.8858
epoch: 25, loss: 261.27, accuracy: 0.8922
epoch: 26, loss: 259.13, accuracy: 0.8881
epoch: 27, loss: 262.09, accuracy: 0.8915
epoch: 28, loss: 259.55, accuracy: 0.8833
epoch: 29, loss: 259.47, accuracy: 0.8884
epoch: 30, loss: 257.42, accuracy: 0.8855
epoch: 31, loss: 257.86, accuracy: 0.8882
epoch: 32, loss: 256.29, accuracy: 0.8893
epoch: 33, loss: 256.52, accuracy: 0.8853
epoch: 34, loss: 253.52, accuracy: 0.8904
epoch: 35, loss: 250.95, accuracy: 0.8923
epoch: 36, loss: 252.24, accuracy: 0.891
epoch: 37, loss: 252.84, accuracy: 0.8926
epoch: 38, loss: 251.31, accuracy: 0.8946
epoch: 39, loss: 252.22, accuracy: 0.8821
epoch: 40, loss: 250.54, accuracy: 0.8915
epoch: 41, loss: 251.19, accuracy: 0.8916
epoch: 42, loss: 252.59, accuracy: 0.8962
epoch: 43, loss: 248.6, accuracy: 0.8942
epoch: 44, loss: 250.08, accuracy: 0.8871
epoch: 45, loss: 249.12, accuracy: 0.8971
epoch: 46, loss: 248.73, accuracy: 0.8941
epoch: 47, loss: 246.83, accuracy: 0.8884
epoch: 48, loss: 246.7, accuracy: 0.8953
epoch: 49, loss: 245.97, accuracy: 0.8916
epoch: 50, loss: 244.94, accuracy: 0.8917
epoch: 51, loss: 246.41, accuracy: 0.8859
epoch: 52, loss: 246.94, accuracy: 0.8917
epoch: 53, loss: 245.05, accuracy: 0.8921
epoch: 54, loss: 244.3, accuracy: 0.8924
epoch: 55, loss: 243.61, accuracy: 0.8958
epoch: 56, loss: 244.54, accuracy: 0.8945
epoch: 57, loss: 243.2, accuracy: 0.8923
epoch: 58, loss: 244.1, accuracy: 0.8887
epoch: 59, loss: 241.36, accuracy: 0.8963
epoch: 60, loss: 243.25, accuracy: 0.8969
epoch: 61, loss: 242.32, accuracy: 0.8932
epoch: 62, loss: 241.54, accuracy: 0.8935
epoch: 63, loss: 242.34, accuracy: 0.8941
epoch: 64, loss: 241.64, accuracy: 0.8933
Epoch    65: reducing learning rate of group 0 to 5.0000e-03.
epoch: 65, loss: 241.97, accuracy: 0.8947
epoch: 66, loss: 225.71, accuracy: 0.8983
epoch: 67, loss: 222.11, accuracy: 0.8998
epoch: 68, loss: 223.15, accuracy: 0.8984
epoch: 69, loss: 222.6, accuracy: 0.8994
epoch: 70, loss: 222.71, accuracy: 0.8973
epoch: 71, loss: 222.02, accuracy: 0.9011
epoch: 72, loss: 221.47, accuracy: 0.8986
epoch: 73, loss: 222.05, accuracy: 0.8994
epoch: 74, loss: 221.23, accuracy: 0.899
epoch: 75, loss: 205.85, accuracy: 0.9056
epoch: 76, loss: 203.6, accuracy: 0.9045
epoch: 77, loss: 202.59, accuracy: 0.904
epoch: 78, loss: 202.04, accuracy: 0.904
epoch: 79, loss: 201.9, accuracy: 0.9032
epoch: 80, loss: 202.4, accuracy: 0.9031
epoch: 81, loss: 201.38, accuracy: 0.9039
epoch: 82, loss: 200.94, accuracy: 0.9031
epoch: 83, loss: 200.7, accuracy: 0.9022
epoch: 84, loss: 200.79, accuracy: 0.902
epoch: 85, loss: 200.97, accuracy: 0.9027
epoch: 86, loss: 200.36, accuracy: 0.9042
epoch: 87, loss: 200.67, accuracy: 0.9029
epoch: 88, loss: 199.99, accuracy: 0.9026
epoch: 89, loss: 199.98, accuracy: 0.903
epoch: 90, loss: 199.95, accuracy: 0.9024
epoch: 91, loss: 199.97, accuracy: 0.9046
epoch: 92, loss: 199.72, accuracy: 0.9037
epoch: 93, loss: 199.77, accuracy: 0.9045
epoch: 94, loss: 199.5, accuracy: 0.9035
epoch: 95, loss: 200.07, accuracy: 0.9042
epoch: 96, loss: 199.42, accuracy: 0.903
epoch: 97, loss: 199.73, accuracy: 0.9033
epoch: 98, loss: 199.08, accuracy: 0.9026
epoch: 99, loss: 198.81, accuracy: 0.9034
time analysis:
    all 4464.11 s
    train 4462.2163 s
Accuracy of     0 : 80 %
Accuracy of     1 : 96 %
Accuracy of     2 : 88 %
Accuracy of     3 : 86 %
Accuracy of     4 : 85 %
Accuracy of     5 : 100 %
Accuracy of     6 : 67 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             648
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,558
DAU params: 748
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 565.36, accuracy: 0.8192
epoch: 1, loss: 416.0, accuracy: 0.828
epoch: 2, loss: 380.94, accuracy: 0.8442
epoch: 3, loss: 363.12, accuracy: 0.8591
epoch: 4, loss: 349.84, accuracy: 0.8562
epoch: 5, loss: 341.1, accuracy: 0.8643
epoch: 6, loss: 337.36, accuracy: 0.8509
epoch: 7, loss: 332.75, accuracy: 0.8574
epoch: 8, loss: 328.09, accuracy: 0.8599
epoch: 9, loss: 323.91, accuracy: 0.8664
epoch: 10, loss: 323.52, accuracy: 0.8732
epoch: 11, loss: 319.82, accuracy: 0.8716
epoch: 12, loss: 318.79, accuracy: 0.8715
epoch: 13, loss: 317.43, accuracy: 0.8701
epoch: 14, loss: 315.93, accuracy: 0.8688
epoch: 15, loss: 312.79, accuracy: 0.8628
epoch: 16, loss: 311.45, accuracy: 0.8578
epoch: 17, loss: 310.49, accuracy: 0.8655
epoch: 18, loss: 310.54, accuracy: 0.8693
epoch: 19, loss: 307.07, accuracy: 0.8695
epoch: 20, loss: 307.89, accuracy: 0.8741
epoch: 21, loss: 306.56, accuracy: 0.8729
epoch: 22, loss: 304.51, accuracy: 0.8736
epoch: 23, loss: 302.74, accuracy: 0.8735
epoch: 24, loss: 303.09, accuracy: 0.8536
epoch: 25, loss: 302.68, accuracy: 0.8777
epoch: 26, loss: 302.07, accuracy: 0.8678
epoch: 27, loss: 302.25, accuracy: 0.8726
epoch: 28, loss: 299.59, accuracy: 0.8728
epoch: 29, loss: 299.5, accuracy: 0.8679
epoch: 30, loss: 299.02, accuracy: 0.8759
epoch: 31, loss: 299.17, accuracy: 0.8754
epoch: 32, loss: 296.02, accuracy: 0.8735
epoch: 33, loss: 295.46, accuracy: 0.8764
epoch: 34, loss: 296.28, accuracy: 0.8795
epoch: 35, loss: 296.87, accuracy: 0.8739
epoch: 36, loss: 296.43, accuracy: 0.8795
epoch: 37, loss: 294.94, accuracy: 0.8761
epoch: 38, loss: 296.09, accuracy: 0.8803
epoch: 39, loss: 296.87, accuracy: 0.8751
epoch: 40, loss: 296.19, accuracy: 0.8743
epoch: 41, loss: 293.83, accuracy: 0.8808
epoch: 42, loss: 292.7, accuracy: 0.8783
epoch: 43, loss: 293.12, accuracy: 0.879
epoch: 44, loss: 293.39, accuracy: 0.8786
epoch: 45, loss: 293.35, accuracy: 0.8765
epoch: 46, loss: 292.86, accuracy: 0.8791
epoch: 47, loss: 289.76, accuracy: 0.8818
epoch: 48, loss: 291.66, accuracy: 0.8748
epoch: 49, loss: 289.42, accuracy: 0.8761
epoch: 50, loss: 290.19, accuracy: 0.8812
epoch: 51, loss: 291.05, accuracy: 0.8825
epoch: 52, loss: 289.32, accuracy: 0.8753
epoch: 53, loss: 288.34, accuracy: 0.8807
epoch: 54, loss: 287.97, accuracy: 0.8763
epoch: 55, loss: 287.87, accuracy: 0.8817
epoch: 56, loss: 287.78, accuracy: 0.8797
epoch: 57, loss: 287.34, accuracy: 0.877
epoch: 58, loss: 287.23, accuracy: 0.8807
epoch: 59, loss: 288.12, accuracy: 0.8781
epoch: 60, loss: 285.05, accuracy: 0.8817
epoch: 61, loss: 285.85, accuracy: 0.8767
epoch: 62, loss: 285.3, accuracy: 0.8777
epoch: 63, loss: 285.56, accuracy: 0.8713
epoch: 64, loss: 283.63, accuracy: 0.8739
epoch: 65, loss: 285.02, accuracy: 0.8834
epoch: 66, loss: 285.07, accuracy: 0.8791
epoch: 67, loss: 284.75, accuracy: 0.874
epoch: 68, loss: 284.23, accuracy: 0.8786
epoch: 69, loss: 282.65, accuracy: 0.8811
epoch: 70, loss: 285.2, accuracy: 0.8808
epoch: 71, loss: 284.66, accuracy: 0.8745
epoch: 72, loss: 283.43, accuracy: 0.8768
epoch: 73, loss: 285.31, accuracy: 0.8792
epoch: 74, loss: 282.65, accuracy: 0.8767
epoch: 75, loss: 263.29, accuracy: 0.8859
epoch: 76, loss: 260.15, accuracy: 0.8848
epoch: 77, loss: 259.29, accuracy: 0.8865
epoch: 78, loss: 259.54, accuracy: 0.8853
epoch: 79, loss: 259.15, accuracy: 0.8871
epoch: 80, loss: 259.68, accuracy: 0.8855
epoch: 81, loss: 258.95, accuracy: 0.8864
epoch: 82, loss: 258.24, accuracy: 0.8854
epoch: 83, loss: 258.6, accuracy: 0.8837
epoch: 84, loss: 258.24, accuracy: 0.8854
epoch: 85, loss: 257.7, accuracy: 0.8849
epoch: 86, loss: 257.88, accuracy: 0.887
epoch: 87, loss: 257.15, accuracy: 0.8867
epoch: 88, loss: 257.58, accuracy: 0.8866
epoch: 89, loss: 258.42, accuracy: 0.8857
epoch: 90, loss: 257.15, accuracy: 0.8855
epoch: 91, loss: 257.04, accuracy: 0.8872
epoch: 92, loss: 256.85, accuracy: 0.8839
epoch: 93, loss: 256.85, accuracy: 0.8867
epoch: 94, loss: 256.89, accuracy: 0.8866
epoch: 95, loss: 256.93, accuracy: 0.8873
epoch: 96, loss: 256.77, accuracy: 0.8861
epoch: 97, loss: 256.87, accuracy: 0.8868
Epoch    98: reducing learning rate of group 0 to 5.0000e-04.
epoch: 98, loss: 256.7, accuracy: 0.8869
epoch: 99, loss: 255.34, accuracy: 0.8863
time analysis:
    all 1114.4894 s
    train 1112.4087 s
Accuracy of     0 : 86 %
Accuracy of     1 : 93 %
Accuracy of     2 : 85 %
Accuracy of     3 : 84 %
Accuracy of     4 : 78 %
Accuracy of     5 : 96 %
Accuracy of     6 : 64 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]             968
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,912
DAU params: 1,102
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 560.83, accuracy: 0.8293
epoch: 1, loss: 423.05, accuracy: 0.8361
epoch: 2, loss: 391.14, accuracy: 0.8504
epoch: 3, loss: 370.9, accuracy: 0.8427
epoch: 4, loss: 357.8, accuracy: 0.8519
epoch: 5, loss: 347.23, accuracy: 0.8641
epoch: 6, loss: 339.76, accuracy: 0.8602
epoch: 7, loss: 334.66, accuracy: 0.8695
epoch: 8, loss: 329.14, accuracy: 0.8583
epoch: 9, loss: 323.65, accuracy: 0.8666
epoch: 10, loss: 322.35, accuracy: 0.8731
epoch: 11, loss: 317.26, accuracy: 0.8678
epoch: 12, loss: 315.37, accuracy: 0.8733
epoch: 13, loss: 312.72, accuracy: 0.8771
epoch: 14, loss: 312.24, accuracy: 0.8725
epoch: 15, loss: 306.21, accuracy: 0.8768
epoch: 16, loss: 307.38, accuracy: 0.8711
epoch: 17, loss: 305.03, accuracy: 0.8672
epoch: 18, loss: 303.21, accuracy: 0.8804
epoch: 19, loss: 300.88, accuracy: 0.8824
epoch: 20, loss: 300.37, accuracy: 0.8679
epoch: 21, loss: 300.56, accuracy: 0.8689
epoch: 22, loss: 297.39, accuracy: 0.8815
epoch: 23, loss: 296.93, accuracy: 0.8776
epoch: 24, loss: 293.9, accuracy: 0.8797
epoch: 25, loss: 293.73, accuracy: 0.8804
epoch: 26, loss: 293.22, accuracy: 0.878
epoch: 27, loss: 290.19, accuracy: 0.8739
epoch: 28, loss: 289.55, accuracy: 0.8874
epoch: 29, loss: 290.09, accuracy: 0.8793
epoch: 30, loss: 289.32, accuracy: 0.8825
epoch: 31, loss: 289.35, accuracy: 0.8844
epoch: 32, loss: 287.27, accuracy: 0.8759
epoch: 33, loss: 286.43, accuracy: 0.8798
epoch: 34, loss: 286.6, accuracy: 0.8786
epoch: 35, loss: 284.32, accuracy: 0.8838
epoch: 36, loss: 285.03, accuracy: 0.8806
epoch: 37, loss: 283.74, accuracy: 0.8742
epoch: 38, loss: 282.25, accuracy: 0.8745
epoch: 39, loss: 284.76, accuracy: 0.8846
epoch: 40, loss: 281.85, accuracy: 0.8837
epoch: 41, loss: 282.39, accuracy: 0.8835
epoch: 42, loss: 280.0, accuracy: 0.8777
epoch: 43, loss: 282.25, accuracy: 0.8859
epoch: 44, loss: 278.98, accuracy: 0.8826
epoch: 45, loss: 278.15, accuracy: 0.8793
epoch: 46, loss: 279.28, accuracy: 0.8755
epoch: 47, loss: 277.9, accuracy: 0.8827
epoch: 48, loss: 278.16, accuracy: 0.8777
epoch: 49, loss: 276.48, accuracy: 0.8856
epoch: 50, loss: 274.91, accuracy: 0.8789
epoch: 51, loss: 276.91, accuracy: 0.8873
epoch: 52, loss: 274.19, accuracy: 0.8865
epoch: 53, loss: 274.99, accuracy: 0.8805
epoch: 54, loss: 275.56, accuracy: 0.8816
epoch: 55, loss: 274.79, accuracy: 0.8872
epoch: 56, loss: 272.52, accuracy: 0.8876
epoch: 57, loss: 273.18, accuracy: 0.8838
epoch: 58, loss: 274.22, accuracy: 0.8856
epoch: 59, loss: 273.68, accuracy: 0.8892
epoch: 60, loss: 272.72, accuracy: 0.8825
epoch: 61, loss: 273.61, accuracy: 0.8846
Epoch    62: reducing learning rate of group 0 to 5.0000e-03.
epoch: 62, loss: 273.73, accuracy: 0.8865
epoch: 63, loss: 258.23, accuracy: 0.8876
epoch: 64, loss: 258.0, accuracy: 0.8868
epoch: 65, loss: 257.06, accuracy: 0.8904
epoch: 66, loss: 255.7, accuracy: 0.8883
epoch: 67, loss: 256.03, accuracy: 0.8893
epoch: 68, loss: 255.94, accuracy: 0.8922
epoch: 69, loss: 255.37, accuracy: 0.8865
epoch: 70, loss: 255.67, accuracy: 0.8908
epoch: 71, loss: 254.67, accuracy: 0.8905
epoch: 72, loss: 254.43, accuracy: 0.8885
epoch: 73, loss: 254.63, accuracy: 0.8914
epoch: 74, loss: 255.17, accuracy: 0.8928
epoch: 75, loss: 241.59, accuracy: 0.8951
epoch: 76, loss: 240.87, accuracy: 0.8948
epoch: 77, loss: 239.66, accuracy: 0.8959
epoch: 78, loss: 239.38, accuracy: 0.8951
epoch: 79, loss: 239.21, accuracy: 0.8968
epoch: 80, loss: 239.54, accuracy: 0.8963
epoch: 81, loss: 239.07, accuracy: 0.897
epoch: 82, loss: 239.15, accuracy: 0.8967
epoch: 83, loss: 238.96, accuracy: 0.8961
epoch: 84, loss: 238.47, accuracy: 0.8964
epoch: 85, loss: 238.45, accuracy: 0.896
epoch: 86, loss: 238.72, accuracy: 0.8947
epoch: 87, loss: 239.03, accuracy: 0.8958
epoch: 88, loss: 238.8, accuracy: 0.898
epoch: 89, loss: 238.28, accuracy: 0.8969
Epoch    90: reducing learning rate of group 0 to 2.5000e-04.
epoch: 90, loss: 238.88, accuracy: 0.896
epoch: 91, loss: 237.63, accuracy: 0.8968
epoch: 92, loss: 237.58, accuracy: 0.8967
epoch: 93, loss: 237.65, accuracy: 0.8957
epoch: 94, loss: 237.76, accuracy: 0.897
epoch: 95, loss: 237.39, accuracy: 0.8963
epoch: 96, loss: 237.35, accuracy: 0.8965
epoch: 97, loss: 238.16, accuracy: 0.8966
epoch: 98, loss: 237.34, accuracy: 0.8972
epoch: 99, loss: 237.52, accuracy: 0.8974
time analysis:
    all 1349.2871 s
    train 1347.9615 s
Accuracy of     0 : 86 %
Accuracy of     1 : 96 %
Accuracy of     2 : 81 %
Accuracy of     3 : 84 %
Accuracy of     4 : 81 %
Accuracy of     5 : 100 %
Accuracy of     6 : 64 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 32, 28, 28]             168
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2di-4            [-1, 8, 14, 14]           1,288
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,266
DAU params: 1,456
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 556.9, accuracy: 0.813
epoch: 1, loss: 403.9, accuracy: 0.8401
epoch: 2, loss: 368.73, accuracy: 0.8428
epoch: 3, loss: 352.97, accuracy: 0.8589
epoch: 4, loss: 339.5, accuracy: 0.8687
epoch: 5, loss: 331.04, accuracy: 0.8647
epoch: 6, loss: 320.34, accuracy: 0.8709
epoch: 7, loss: 317.95, accuracy: 0.8683
epoch: 8, loss: 311.2, accuracy: 0.8707
epoch: 9, loss: 306.18, accuracy: 0.8726
epoch: 10, loss: 302.61, accuracy: 0.8806
epoch: 11, loss: 298.81, accuracy: 0.8815
epoch: 12, loss: 296.0, accuracy: 0.8801
epoch: 13, loss: 294.65, accuracy: 0.8792
epoch: 14, loss: 290.75, accuracy: 0.874
epoch: 15, loss: 286.44, accuracy: 0.8765
epoch: 16, loss: 286.52, accuracy: 0.8799
epoch: 17, loss: 283.02, accuracy: 0.8841
epoch: 18, loss: 281.58, accuracy: 0.8719
epoch: 19, loss: 280.23, accuracy: 0.8835
epoch: 20, loss: 279.15, accuracy: 0.8851
epoch: 21, loss: 279.7, accuracy: 0.8815
epoch: 22, loss: 274.97, accuracy: 0.8798
epoch: 23, loss: 276.4, accuracy: 0.8786
epoch: 24, loss: 275.69, accuracy: 0.8854
epoch: 25, loss: 273.91, accuracy: 0.8809
epoch: 26, loss: 272.16, accuracy: 0.88
epoch: 27, loss: 273.74, accuracy: 0.8831
epoch: 28, loss: 270.32, accuracy: 0.8802
epoch: 29, loss: 271.14, accuracy: 0.8821
epoch: 30, loss: 269.76, accuracy: 0.8793
epoch: 31, loss: 268.03, accuracy: 0.8844
epoch: 32, loss: 268.9, accuracy: 0.8805
epoch: 33, loss: 266.21, accuracy: 0.8836
epoch: 34, loss: 267.43, accuracy: 0.8846
epoch: 35, loss: 266.06, accuracy: 0.8873
epoch: 36, loss: 265.54, accuracy: 0.8906
epoch: 37, loss: 265.12, accuracy: 0.8824
epoch: 38, loss: 262.98, accuracy: 0.8804
epoch: 39, loss: 263.29, accuracy: 0.8847
epoch: 40, loss: 261.67, accuracy: 0.8898
epoch: 41, loss: 262.11, accuracy: 0.8853
epoch: 42, loss: 263.09, accuracy: 0.8881
epoch: 43, loss: 260.78, accuracy: 0.886
epoch: 44, loss: 261.47, accuracy: 0.8873
epoch: 45, loss: 261.92, accuracy: 0.8839
epoch: 46, loss: 259.58, accuracy: 0.8829
epoch: 47, loss: 259.48, accuracy: 0.8856
epoch: 48, loss: 258.35, accuracy: 0.8911
epoch: 49, loss: 257.56, accuracy: 0.8905
epoch: 50, loss: 256.81, accuracy: 0.8849
epoch: 51, loss: 256.04, accuracy: 0.8873
epoch: 52, loss: 254.85, accuracy: 0.8818
epoch: 53, loss: 257.94, accuracy: 0.8908
epoch: 54, loss: 255.78, accuracy: 0.8882
epoch: 55, loss: 253.86, accuracy: 0.8886
epoch: 56, loss: 255.89, accuracy: 0.8776
epoch: 57, loss: 255.13, accuracy: 0.884
epoch: 58, loss: 255.19, accuracy: 0.893
epoch: 59, loss: 254.15, accuracy: 0.8886
epoch: 60, loss: 254.08, accuracy: 0.8892
Epoch    61: reducing learning rate of group 0 to 5.0000e-03.
epoch: 61, loss: 254.51, accuracy: 0.894
epoch: 62, loss: 239.72, accuracy: 0.8938
epoch: 63, loss: 237.94, accuracy: 0.894
epoch: 64, loss: 238.18, accuracy: 0.892
epoch: 65, loss: 237.13, accuracy: 0.8925
epoch: 66, loss: 237.75, accuracy: 0.8935
epoch: 67, loss: 236.83, accuracy: 0.8946
epoch: 68, loss: 236.84, accuracy: 0.8949
epoch: 69, loss: 237.07, accuracy: 0.8912
epoch: 70, loss: 235.53, accuracy: 0.8928
epoch: 71, loss: 236.21, accuracy: 0.8923
epoch: 72, loss: 236.22, accuracy: 0.8947
epoch: 73, loss: 234.07, accuracy: 0.8926
epoch: 74, loss: 234.71, accuracy: 0.8897
epoch: 75, loss: 222.0, accuracy: 0.8981
epoch: 76, loss: 219.95, accuracy: 0.8977
epoch: 77, loss: 219.62, accuracy: 0.8973
epoch: 78, loss: 219.87, accuracy: 0.8979
epoch: 79, loss: 218.93, accuracy: 0.898
epoch: 80, loss: 218.78, accuracy: 0.8985
epoch: 81, loss: 218.83, accuracy: 0.8968
epoch: 82, loss: 218.73, accuracy: 0.8957
epoch: 83, loss: 218.99, accuracy: 0.8986
epoch: 84, loss: 218.52, accuracy: 0.8989
epoch: 85, loss: 218.65, accuracy: 0.8981
epoch: 86, loss: 218.94, accuracy: 0.898
epoch: 87, loss: 218.89, accuracy: 0.8968
epoch: 88, loss: 217.62, accuracy: 0.8968
epoch: 89, loss: 217.93, accuracy: 0.8969
epoch: 90, loss: 218.3, accuracy: 0.897
epoch: 91, loss: 218.14, accuracy: 0.8978
epoch: 92, loss: 218.07, accuracy: 0.8988
epoch: 93, loss: 218.4, accuracy: 0.8979
Epoch    94: reducing learning rate of group 0 to 2.5000e-04.
epoch: 94, loss: 217.99, accuracy: 0.8973
epoch: 95, loss: 217.18, accuracy: 0.8982
epoch: 96, loss: 216.04, accuracy: 0.8986
epoch: 97, loss: 216.69, accuracy: 0.8987
epoch: 98, loss: 216.64, accuracy: 0.8977
epoch: 99, loss: 216.52, accuracy: 0.8991
time analysis:
    all 1585.4199 s
    train 1583.8474 s
Accuracy of     0 : 90 %
Accuracy of     1 : 95 %
Accuracy of     2 : 85 %
Accuracy of     3 : 87 %
Accuracy of     4 : 78 %
Accuracy of     5 : 98 %
Accuracy of     6 : 67 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             224
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             552
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,586
DAU params: 776
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 516.07, accuracy: 0.8292
epoch: 1, loss: 392.31, accuracy: 0.839
epoch: 2, loss: 369.53, accuracy: 0.8401
epoch: 3, loss: 356.86, accuracy: 0.8395
epoch: 4, loss: 350.32, accuracy: 0.8586
epoch: 5, loss: 342.91, accuracy: 0.8574
epoch: 6, loss: 339.38, accuracy: 0.8651
epoch: 7, loss: 334.56, accuracy: 0.8624
epoch: 8, loss: 332.9, accuracy: 0.8621
epoch: 9, loss: 329.69, accuracy: 0.8594
epoch: 10, loss: 325.96, accuracy: 0.8616
epoch: 11, loss: 325.67, accuracy: 0.8625
epoch: 12, loss: 321.93, accuracy: 0.8622
epoch: 13, loss: 321.94, accuracy: 0.8689
epoch: 14, loss: 316.75, accuracy: 0.8686
epoch: 15, loss: 318.47, accuracy: 0.8673
epoch: 16, loss: 316.09, accuracy: 0.8595
epoch: 17, loss: 314.65, accuracy: 0.8725
epoch: 18, loss: 311.76, accuracy: 0.8726
epoch: 19, loss: 311.14, accuracy: 0.8666
epoch: 20, loss: 310.87, accuracy: 0.8684
epoch: 21, loss: 307.82, accuracy: 0.874
epoch: 22, loss: 307.56, accuracy: 0.867
epoch: 23, loss: 307.43, accuracy: 0.8708
epoch: 24, loss: 305.59, accuracy: 0.8674
epoch: 25, loss: 302.85, accuracy: 0.877
epoch: 26, loss: 302.47, accuracy: 0.8751
epoch: 27, loss: 301.12, accuracy: 0.8708
epoch: 28, loss: 302.1, accuracy: 0.8707
epoch: 29, loss: 299.83, accuracy: 0.8745
epoch: 30, loss: 301.4, accuracy: 0.8734
epoch: 31, loss: 300.13, accuracy: 0.8748
epoch: 32, loss: 299.29, accuracy: 0.8721
epoch: 33, loss: 300.14, accuracy: 0.8786
epoch: 34, loss: 296.45, accuracy: 0.878
epoch: 35, loss: 297.18, accuracy: 0.8744
epoch: 36, loss: 297.71, accuracy: 0.8724
epoch: 37, loss: 295.58, accuracy: 0.877
epoch: 38, loss: 294.99, accuracy: 0.8698
epoch: 39, loss: 296.14, accuracy: 0.8788
epoch: 40, loss: 293.15, accuracy: 0.8767
epoch: 41, loss: 294.43, accuracy: 0.8709
epoch: 42, loss: 292.78, accuracy: 0.8724
epoch: 43, loss: 293.81, accuracy: 0.8711
epoch: 44, loss: 292.71, accuracy: 0.8751
epoch: 45, loss: 292.12, accuracy: 0.8755
epoch: 46, loss: 291.74, accuracy: 0.8707
epoch: 47, loss: 290.08, accuracy: 0.875
epoch: 48, loss: 289.91, accuracy: 0.8735
epoch: 49, loss: 287.99, accuracy: 0.8729
epoch: 50, loss: 290.53, accuracy: 0.8771
epoch: 51, loss: 289.12, accuracy: 0.8789
epoch: 52, loss: 290.75, accuracy: 0.8788
epoch: 53, loss: 288.88, accuracy: 0.8786
epoch: 54, loss: 288.4, accuracy: 0.8779
epoch: 55, loss: 286.41, accuracy: 0.8768
epoch: 56, loss: 287.04, accuracy: 0.8767
epoch: 57, loss: 288.83, accuracy: 0.8757
epoch: 58, loss: 286.46, accuracy: 0.879
epoch: 59, loss: 286.59, accuracy: 0.8777
epoch: 60, loss: 286.36, accuracy: 0.8746
epoch: 61, loss: 284.59, accuracy: 0.8797
epoch: 62, loss: 286.27, accuracy: 0.8788
epoch: 63, loss: 285.16, accuracy: 0.8708
epoch: 64, loss: 285.76, accuracy: 0.8752
epoch: 65, loss: 285.39, accuracy: 0.8754
epoch: 66, loss: 283.94, accuracy: 0.8744
epoch: 67, loss: 282.02, accuracy: 0.8757
epoch: 68, loss: 283.89, accuracy: 0.8775
epoch: 69, loss: 284.22, accuracy: 0.8758
epoch: 70, loss: 282.56, accuracy: 0.8808
epoch: 71, loss: 283.24, accuracy: 0.8821
epoch: 72, loss: 282.46, accuracy: 0.8769
epoch: 73, loss: 280.39, accuracy: 0.8788
epoch: 74, loss: 283.38, accuracy: 0.8798
epoch: 75, loss: 260.67, accuracy: 0.8856
epoch: 76, loss: 256.74, accuracy: 0.8861
epoch: 77, loss: 256.24, accuracy: 0.8852
epoch: 78, loss: 255.86, accuracy: 0.8842
epoch: 79, loss: 256.19, accuracy: 0.8853
epoch: 80, loss: 255.25, accuracy: 0.8857
epoch: 81, loss: 254.86, accuracy: 0.8854
epoch: 82, loss: 254.15, accuracy: 0.8846
epoch: 83, loss: 254.63, accuracy: 0.8856
epoch: 84, loss: 254.26, accuracy: 0.8846
epoch: 85, loss: 254.2, accuracy: 0.8841
epoch: 86, loss: 255.02, accuracy: 0.8866
epoch: 87, loss: 253.69, accuracy: 0.886
epoch: 88, loss: 253.66, accuracy: 0.8847
epoch: 89, loss: 253.97, accuracy: 0.8863
epoch: 90, loss: 253.32, accuracy: 0.8837
epoch: 91, loss: 253.18, accuracy: 0.8865
epoch: 92, loss: 253.98, accuracy: 0.8851
epoch: 93, loss: 253.37, accuracy: 0.8844
epoch: 94, loss: 253.19, accuracy: 0.8873
epoch: 95, loss: 252.57, accuracy: 0.8857
epoch: 96, loss: 253.05, accuracy: 0.8853
epoch: 97, loss: 252.98, accuracy: 0.8874
epoch: 98, loss: 252.75, accuracy: 0.8869
epoch: 99, loss: 253.19, accuracy: 0.8873
time analysis:
    all 1834.5536 s
    train 1832.7088 s
Accuracy of     0 : 80 %
Accuracy of     1 : 95 %
Accuracy of     2 : 87 %
Accuracy of     3 : 86 %
Accuracy of     4 : 80 %
Accuracy of     5 : 98 %
Accuracy of     6 : 61 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 96 %
```

## FashionMNIST, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             320
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]             824
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,954
DAU params: 1,144
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 497.12, accuracy: 0.8255
epoch: 1, loss: 383.57, accuracy: 0.8431
epoch: 2, loss: 359.87, accuracy: 0.8492
epoch: 3, loss: 350.38, accuracy: 0.8511
epoch: 4, loss: 336.74, accuracy: 0.8566
epoch: 5, loss: 331.51, accuracy: 0.861
epoch: 6, loss: 328.12, accuracy: 0.8557
epoch: 7, loss: 322.1, accuracy: 0.865
epoch: 8, loss: 317.11, accuracy: 0.8555
epoch: 9, loss: 313.54, accuracy: 0.8727
epoch: 10, loss: 309.66, accuracy: 0.8702
epoch: 11, loss: 304.36, accuracy: 0.8752
epoch: 12, loss: 301.94, accuracy: 0.8635
epoch: 13, loss: 301.15, accuracy: 0.8804
epoch: 14, loss: 298.13, accuracy: 0.8683
epoch: 15, loss: 296.17, accuracy: 0.8794
epoch: 16, loss: 296.04, accuracy: 0.8709
epoch: 17, loss: 291.16, accuracy: 0.8733
epoch: 18, loss: 289.89, accuracy: 0.8683
epoch: 19, loss: 290.69, accuracy: 0.8796
epoch: 20, loss: 287.87, accuracy: 0.8816
epoch: 21, loss: 287.37, accuracy: 0.8789
epoch: 22, loss: 286.84, accuracy: 0.8724
epoch: 23, loss: 283.88, accuracy: 0.8717
epoch: 24, loss: 282.31, accuracy: 0.8764
epoch: 25, loss: 282.53, accuracy: 0.8758
epoch: 26, loss: 281.16, accuracy: 0.884
epoch: 27, loss: 278.74, accuracy: 0.8811
epoch: 28, loss: 279.19, accuracy: 0.883
epoch: 29, loss: 278.31, accuracy: 0.8867
epoch: 30, loss: 277.4, accuracy: 0.8816
epoch: 31, loss: 277.76, accuracy: 0.8881
epoch: 32, loss: 275.04, accuracy: 0.886
epoch: 33, loss: 275.13, accuracy: 0.8877
epoch: 34, loss: 275.43, accuracy: 0.8609
epoch: 35, loss: 276.57, accuracy: 0.8845
epoch: 36, loss: 272.51, accuracy: 0.8872
epoch: 37, loss: 272.47, accuracy: 0.8881
epoch: 38, loss: 270.62, accuracy: 0.8904
epoch: 39, loss: 270.98, accuracy: 0.8839
epoch: 40, loss: 271.69, accuracy: 0.8836
epoch: 41, loss: 271.61, accuracy: 0.8867
epoch: 42, loss: 269.2, accuracy: 0.886
epoch: 43, loss: 270.13, accuracy: 0.8875
epoch: 44, loss: 268.32, accuracy: 0.8757
epoch: 45, loss: 267.22, accuracy: 0.8861
epoch: 46, loss: 268.6, accuracy: 0.8883
epoch: 47, loss: 267.42, accuracy: 0.8818
epoch: 48, loss: 266.57, accuracy: 0.8944
epoch: 49, loss: 267.39, accuracy: 0.8906
epoch: 50, loss: 265.38, accuracy: 0.8813
epoch: 51, loss: 266.85, accuracy: 0.8921
epoch: 52, loss: 266.41, accuracy: 0.8875
epoch: 53, loss: 264.84, accuracy: 0.889
epoch: 54, loss: 265.71, accuracy: 0.8827
epoch: 55, loss: 265.84, accuracy: 0.8872
epoch: 56, loss: 264.39, accuracy: 0.8817
epoch: 57, loss: 264.81, accuracy: 0.8777
epoch: 58, loss: 263.66, accuracy: 0.8897
epoch: 59, loss: 263.65, accuracy: 0.8859
epoch: 60, loss: 262.76, accuracy: 0.8906
epoch: 61, loss: 264.06, accuracy: 0.8938
epoch: 62, loss: 264.7, accuracy: 0.889
epoch: 63, loss: 263.25, accuracy: 0.8928
epoch: 64, loss: 262.35, accuracy: 0.8846
epoch: 65, loss: 262.11, accuracy: 0.8904
epoch: 66, loss: 261.99, accuracy: 0.8849
epoch: 67, loss: 262.63, accuracy: 0.8914
epoch: 68, loss: 261.44, accuracy: 0.8895
epoch: 69, loss: 260.38, accuracy: 0.8874
epoch: 70, loss: 262.96, accuracy: 0.8878
epoch: 71, loss: 260.26, accuracy: 0.8883
epoch: 72, loss: 261.35, accuracy: 0.8915
epoch: 73, loss: 260.14, accuracy: 0.8921
epoch: 74, loss: 261.02, accuracy: 0.8906
epoch: 75, loss: 237.96, accuracy: 0.895
epoch: 76, loss: 234.79, accuracy: 0.8967
epoch: 77, loss: 233.33, accuracy: 0.897
epoch: 78, loss: 234.23, accuracy: 0.8966
epoch: 79, loss: 232.73, accuracy: 0.8954
epoch: 80, loss: 232.91, accuracy: 0.8953
epoch: 81, loss: 232.31, accuracy: 0.8959
epoch: 82, loss: 232.0, accuracy: 0.8968
epoch: 83, loss: 232.39, accuracy: 0.8965
epoch: 84, loss: 230.94, accuracy: 0.8963
epoch: 85, loss: 231.4, accuracy: 0.8975
epoch: 86, loss: 231.56, accuracy: 0.8957
epoch: 87, loss: 231.34, accuracy: 0.8943
epoch: 88, loss: 230.89, accuracy: 0.8975
epoch: 89, loss: 230.89, accuracy: 0.8958
Epoch    90: reducing learning rate of group 0 to 5.0000e-04.
epoch: 90, loss: 230.79, accuracy: 0.8956
epoch: 91, loss: 228.91, accuracy: 0.897
epoch: 92, loss: 228.26, accuracy: 0.8955
epoch: 93, loss: 228.89, accuracy: 0.8969
epoch: 94, loss: 228.54, accuracy: 0.8961
epoch: 95, loss: 228.71, accuracy: 0.8968
epoch: 96, loss: 228.31, accuracy: 0.8965
epoch: 97, loss: 228.63, accuracy: 0.896
epoch: 98, loss: 228.29, accuracy: 0.8953
epoch: 99, loss: 228.28, accuracy: 0.8967
time analysis:
    all 2482.7928 s
    train 2481.5108 s
Accuracy of     0 : 86 %
Accuracy of     1 : 96 %
Accuracy of     2 : 85 %
Accuracy of     3 : 88 %
Accuracy of     4 : 81 %
Accuracy of     5 : 98 %
Accuracy of     6 : 64 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 100 %
```

## FashionMNIST, small_fc, DAUConv2dj, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 32, 28, 28]             416
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
        DAUConv2dj-4            [-1, 8, 14, 14]           1,096
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,322
DAU params: 1,512
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 495.6, accuracy: 0.8504
epoch: 1, loss: 375.9, accuracy: 0.8226
epoch: 2, loss: 353.07, accuracy: 0.8508
epoch: 3, loss: 338.5, accuracy: 0.8587
epoch: 4, loss: 327.65, accuracy: 0.8673
epoch: 5, loss: 322.26, accuracy: 0.8737
epoch: 6, loss: 315.96, accuracy: 0.8705
epoch: 7, loss: 312.16, accuracy: 0.8651
epoch: 8, loss: 305.09, accuracy: 0.8785
epoch: 9, loss: 302.82, accuracy: 0.8661
epoch: 10, loss: 298.83, accuracy: 0.8673
epoch: 11, loss: 297.63, accuracy: 0.88
epoch: 12, loss: 292.55, accuracy: 0.8766
epoch: 13, loss: 291.58, accuracy: 0.8875
epoch: 14, loss: 290.63, accuracy: 0.8788
epoch: 15, loss: 288.53, accuracy: 0.865
epoch: 16, loss: 287.13, accuracy: 0.8799
epoch: 17, loss: 284.75, accuracy: 0.877
epoch: 18, loss: 286.25, accuracy: 0.8869
epoch: 19, loss: 281.44, accuracy: 0.8793
epoch: 20, loss: 282.63, accuracy: 0.8839
epoch: 21, loss: 277.59, accuracy: 0.8818
epoch: 22, loss: 278.73, accuracy: 0.8852
epoch: 23, loss: 277.11, accuracy: 0.8813
epoch: 24, loss: 277.23, accuracy: 0.8876
epoch: 25, loss: 276.73, accuracy: 0.8831
epoch: 26, loss: 276.06, accuracy: 0.88
epoch: 27, loss: 273.16, accuracy: 0.8857
epoch: 28, loss: 274.39, accuracy: 0.8872
epoch: 29, loss: 272.07, accuracy: 0.8778
epoch: 30, loss: 271.35, accuracy: 0.8825
epoch: 31, loss: 271.01, accuracy: 0.8801
epoch: 32, loss: 269.1, accuracy: 0.8808
epoch: 33, loss: 269.42, accuracy: 0.8855
epoch: 34, loss: 268.05, accuracy: 0.8869
epoch: 35, loss: 267.25, accuracy: 0.887
epoch: 36, loss: 265.8, accuracy: 0.8826
epoch: 37, loss: 266.31, accuracy: 0.884
epoch: 38, loss: 265.64, accuracy: 0.887
epoch: 39, loss: 266.11, accuracy: 0.8872
epoch: 40, loss: 263.44, accuracy: 0.8873
epoch: 41, loss: 264.82, accuracy: 0.8855
epoch: 42, loss: 262.81, accuracy: 0.8844
epoch: 43, loss: 262.96, accuracy: 0.8864
epoch: 44, loss: 261.59, accuracy: 0.887
epoch: 45, loss: 262.96, accuracy: 0.8741
epoch: 46, loss: 263.17, accuracy: 0.8864
epoch: 47, loss: 261.21, accuracy: 0.8846
epoch: 48, loss: 262.49, accuracy: 0.8924
epoch: 49, loss: 258.77, accuracy: 0.8792
epoch: 50, loss: 261.83, accuracy: 0.89
epoch: 51, loss: 258.88, accuracy: 0.888
epoch: 52, loss: 259.63, accuracy: 0.881
epoch: 53, loss: 259.26, accuracy: 0.886
epoch: 54, loss: 260.08, accuracy: 0.8758
epoch: 55, loss: 258.21, accuracy: 0.8848
epoch: 56, loss: 257.0, accuracy: 0.8786
epoch: 57, loss: 258.1, accuracy: 0.89
epoch: 58, loss: 257.64, accuracy: 0.8911
epoch: 59, loss: 257.72, accuracy: 0.8823
epoch: 60, loss: 256.91, accuracy: 0.8875
epoch: 61, loss: 258.25, accuracy: 0.8902
epoch: 62, loss: 255.96, accuracy: 0.8878
epoch: 63, loss: 255.3, accuracy: 0.8778
epoch: 64, loss: 256.36, accuracy: 0.89
epoch: 65, loss: 255.62, accuracy: 0.8887
epoch: 66, loss: 253.44, accuracy: 0.8894
epoch: 67, loss: 255.57, accuracy: 0.8869
epoch: 68, loss: 256.19, accuracy: 0.8853
epoch: 69, loss: 254.12, accuracy: 0.8878
epoch: 70, loss: 255.24, accuracy: 0.891
epoch: 71, loss: 253.68, accuracy: 0.8858
Epoch    72: reducing learning rate of group 0 to 5.0000e-03.
epoch: 72, loss: 253.29, accuracy: 0.8937
epoch: 73, loss: 239.21, accuracy: 0.8951
epoch: 74, loss: 236.87, accuracy: 0.8902
epoch: 75, loss: 224.85, accuracy: 0.896
epoch: 76, loss: 223.07, accuracy: 0.8968
epoch: 77, loss: 222.72, accuracy: 0.8975
epoch: 78, loss: 222.93, accuracy: 0.8964
epoch: 79, loss: 222.32, accuracy: 0.8967
epoch: 80, loss: 221.77, accuracy: 0.8977
epoch: 81, loss: 221.78, accuracy: 0.8971
epoch: 82, loss: 221.97, accuracy: 0.8957
epoch: 83, loss: 221.0, accuracy: 0.8971
epoch: 84, loss: 221.46, accuracy: 0.8955
epoch: 85, loss: 221.24, accuracy: 0.8976
epoch: 86, loss: 221.3, accuracy: 0.8969
epoch: 87, loss: 221.08, accuracy: 0.8964
epoch: 88, loss: 221.17, accuracy: 0.8965
Epoch    89: reducing learning rate of group 0 to 2.5000e-04.
epoch: 89, loss: 220.88, accuracy: 0.8959
epoch: 90, loss: 219.64, accuracy: 0.8971
epoch: 91, loss: 219.86, accuracy: 0.8963
epoch: 92, loss: 219.91, accuracy: 0.8971
epoch: 93, loss: 220.11, accuracy: 0.8979
epoch: 94, loss: 219.89, accuracy: 0.8963
epoch: 95, loss: 218.96, accuracy: 0.8976
epoch: 96, loss: 219.52, accuracy: 0.8971
epoch: 97, loss: 220.55, accuracy: 0.8972
epoch: 98, loss: 220.33, accuracy: 0.8973
epoch: 99, loss: 219.57, accuracy: 0.8977
time analysis:
    all 3100.2869 s
    train 3098.7736 s
Accuracy of     0 : 92 %
Accuracy of     1 : 96 %
Accuracy of     2 : 87 %
Accuracy of     3 : 84 %
Accuracy of     4 : 83 %
Accuracy of     5 : 98 %
Accuracy of     6 : 69 %
Accuracy of     7 : 100 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=1, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]              66
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             266
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,142
DAU params: 332
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 682.24, accuracy: 0.789
epoch: 1, loss: 525.07, accuracy: 0.666
epoch: 2, loss: 498.48, accuracy: 0.6542
epoch: 3, loss: 488.16, accuracy: 0.7346
epoch: 4, loss: 479.04, accuracy: 0.7418
epoch: 5, loss: 471.76, accuracy: 0.7941
epoch: 6, loss: 466.1, accuracy: 0.7813
epoch: 7, loss: 458.38, accuracy: 0.7929
epoch: 8, loss: 457.24, accuracy: 0.7735
epoch: 9, loss: 450.73, accuracy: 0.7973
epoch: 10, loss: 447.39, accuracy: 0.6585
epoch: 11, loss: 448.51, accuracy: 0.7895
epoch: 12, loss: 448.04, accuracy: 0.8223
epoch: 13, loss: 445.89, accuracy: 0.8045
epoch: 14, loss: 441.69, accuracy: 0.7917
epoch: 15, loss: 438.35, accuracy: 0.7717
epoch: 16, loss: 437.8, accuracy: 0.7729
epoch: 17, loss: 433.65, accuracy: 0.7906
epoch: 18, loss: 433.21, accuracy: 0.8047
epoch: 19, loss: 433.61, accuracy: 0.7947
epoch: 20, loss: 432.23, accuracy: 0.8161
epoch: 21, loss: 431.29, accuracy: 0.808
epoch: 22, loss: 427.6, accuracy: 0.8052
epoch: 23, loss: 427.18, accuracy: 0.804
epoch: 24, loss: 427.62, accuracy: 0.6951
epoch: 25, loss: 425.79, accuracy: 0.8129
epoch: 26, loss: 423.4, accuracy: 0.819
epoch: 27, loss: 424.52, accuracy: 0.8107
epoch: 28, loss: 424.59, accuracy: 0.8019
epoch: 29, loss: 423.94, accuracy: 0.8178
epoch: 30, loss: 421.5, accuracy: 0.7859
epoch: 31, loss: 422.13, accuracy: 0.7669
epoch: 32, loss: 422.49, accuracy: 0.8203
epoch: 33, loss: 419.49, accuracy: 0.8247
epoch: 34, loss: 420.83, accuracy: 0.8246
epoch: 35, loss: 419.37, accuracy: 0.8247
epoch: 36, loss: 418.36, accuracy: 0.812
epoch: 37, loss: 417.94, accuracy: 0.8182
epoch: 38, loss: 417.37, accuracy: 0.8088
epoch: 39, loss: 417.19, accuracy: 0.7596
epoch: 40, loss: 414.5, accuracy: 0.8232
epoch: 41, loss: 415.27, accuracy: 0.8077
epoch: 42, loss: 416.09, accuracy: 0.819
epoch: 43, loss: 415.67, accuracy: 0.7565
epoch: 44, loss: 416.25, accuracy: 0.8305
epoch: 45, loss: 415.44, accuracy: 0.793
epoch: 46, loss: 413.85, accuracy: 0.814
epoch: 47, loss: 414.36, accuracy: 0.7968
epoch: 48, loss: 413.34, accuracy: 0.7803
epoch: 49, loss: 413.71, accuracy: 0.8049
epoch: 50, loss: 414.92, accuracy: 0.7623
epoch: 51, loss: 412.54, accuracy: 0.8019
epoch: 52, loss: 413.76, accuracy: 0.8327
epoch: 53, loss: 411.82, accuracy: 0.7942
epoch: 54, loss: 412.17, accuracy: 0.7929
epoch: 55, loss: 412.5, accuracy: 0.8271
epoch: 56, loss: 411.51, accuracy: 0.7865
epoch: 57, loss: 410.68, accuracy: 0.8116
epoch: 58, loss: 412.02, accuracy: 0.7832
epoch: 59, loss: 411.68, accuracy: 0.8014
epoch: 60, loss: 410.95, accuracy: 0.8008
epoch: 61, loss: 410.65, accuracy: 0.823
epoch: 62, loss: 413.09, accuracy: 0.8293
Epoch    63: reducing learning rate of group 0 to 5.0000e-03.
epoch: 63, loss: 411.14, accuracy: 0.8076
epoch: 64, loss: 396.75, accuracy: 0.8261
epoch: 65, loss: 396.66, accuracy: 0.819
epoch: 66, loss: 396.42, accuracy: 0.8267
epoch: 67, loss: 395.35, accuracy: 0.8029
epoch: 68, loss: 394.04, accuracy: 0.8236
epoch: 69, loss: 394.13, accuracy: 0.8115
epoch: 70, loss: 395.14, accuracy: 0.8392
epoch: 71, loss: 395.13, accuracy: 0.8402
epoch: 72, loss: 394.32, accuracy: 0.8327
epoch: 73, loss: 395.42, accuracy: 0.8208
Epoch    74: reducing learning rate of group 0 to 2.5000e-03.
epoch: 74, loss: 394.8, accuracy: 0.8316
epoch: 75, loss: 381.36, accuracy: 0.8461
epoch: 76, loss: 380.61, accuracy: 0.8461
epoch: 77, loss: 380.48, accuracy: 0.8465
epoch: 78, loss: 380.44, accuracy: 0.8466
epoch: 79, loss: 380.21, accuracy: 0.8469
epoch: 80, loss: 380.51, accuracy: 0.8474
epoch: 81, loss: 380.7, accuracy: 0.845
epoch: 82, loss: 379.43, accuracy: 0.8463
epoch: 83, loss: 380.41, accuracy: 0.8464
epoch: 84, loss: 379.0, accuracy: 0.8468
epoch: 85, loss: 379.3, accuracy: 0.8472
epoch: 86, loss: 379.31, accuracy: 0.8469
epoch: 87, loss: 379.73, accuracy: 0.846
epoch: 88, loss: 379.08, accuracy: 0.8458
epoch: 89, loss: 379.52, accuracy: 0.8475
Epoch    90: reducing learning rate of group 0 to 1.2500e-04.
epoch: 90, loss: 378.7, accuracy: 0.8468
epoch: 91, loss: 378.64, accuracy: 0.8461
epoch: 92, loss: 379.04, accuracy: 0.8484
epoch: 93, loss: 379.06, accuracy: 0.8474
epoch: 94, loss: 379.32, accuracy: 0.8474
epoch: 95, loss: 378.31, accuracy: 0.8485
epoch: 96, loss: 379.4, accuracy: 0.847
epoch: 97, loss: 378.4, accuracy: 0.8479
epoch: 98, loss: 379.13, accuracy: 0.846
epoch: 99, loss: 379.16, accuracy: 0.8475
time analysis:
    all 975.8929 s
    train 974.2166 s
Accuracy of     0 : 82 %
Accuracy of     1 : 95 %
Accuracy of     2 : 81 %
Accuracy of     3 : 84 %
Accuracy of     4 : 66 %
Accuracy of     5 : 93 %
Accuracy of     6 : 53 %
Accuracy of     7 : 90 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             100
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             524
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,434
DAU params: 624
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 569.41, accuracy: 0.8185
epoch: 1, loss: 443.01, accuracy: 0.8341
epoch: 2, loss: 405.81, accuracy: 0.8352
epoch: 3, loss: 388.42, accuracy: 0.8521
epoch: 4, loss: 371.57, accuracy: 0.8489
epoch: 5, loss: 366.32, accuracy: 0.851
epoch: 6, loss: 360.48, accuracy: 0.8477
epoch: 7, loss: 356.38, accuracy: 0.8476
epoch: 8, loss: 349.56, accuracy: 0.85
epoch: 9, loss: 345.26, accuracy: 0.8496
epoch: 10, loss: 345.18, accuracy: 0.8581
epoch: 11, loss: 341.27, accuracy: 0.8549
epoch: 12, loss: 337.1, accuracy: 0.8606
epoch: 13, loss: 338.38, accuracy: 0.8524
epoch: 14, loss: 335.93, accuracy: 0.8605
epoch: 15, loss: 334.23, accuracy: 0.8666
epoch: 16, loss: 332.07, accuracy: 0.8652
epoch: 17, loss: 329.94, accuracy: 0.8482
epoch: 18, loss: 330.65, accuracy: 0.8644
epoch: 19, loss: 327.55, accuracy: 0.8687
epoch: 20, loss: 327.24, accuracy: 0.8533
epoch: 21, loss: 326.97, accuracy: 0.8657
epoch: 22, loss: 323.95, accuracy: 0.8626
epoch: 23, loss: 322.91, accuracy: 0.8641
epoch: 24, loss: 323.63, accuracy: 0.8634
epoch: 25, loss: 321.53, accuracy: 0.853
epoch: 26, loss: 320.0, accuracy: 0.8714
epoch: 27, loss: 319.1, accuracy: 0.8624
epoch: 28, loss: 319.96, accuracy: 0.8639
epoch: 29, loss: 318.85, accuracy: 0.873
epoch: 30, loss: 317.98, accuracy: 0.8761
epoch: 31, loss: 317.52, accuracy: 0.8698
epoch: 32, loss: 316.39, accuracy: 0.8568
epoch: 33, loss: 313.24, accuracy: 0.8749
epoch: 34, loss: 316.12, accuracy: 0.8625
epoch: 35, loss: 313.53, accuracy: 0.8718
epoch: 36, loss: 311.19, accuracy: 0.8733
epoch: 37, loss: 313.49, accuracy: 0.8633
epoch: 38, loss: 309.47, accuracy: 0.8694
epoch: 39, loss: 311.48, accuracy: 0.8737
epoch: 40, loss: 311.52, accuracy: 0.8757
epoch: 41, loss: 310.12, accuracy: 0.8591
epoch: 42, loss: 311.16, accuracy: 0.8649
epoch: 43, loss: 310.21, accuracy: 0.8687
Epoch    44: reducing learning rate of group 0 to 5.0000e-03.
epoch: 44, loss: 310.58, accuracy: 0.8686
epoch: 45, loss: 295.84, accuracy: 0.8776
epoch: 46, loss: 293.35, accuracy: 0.874
epoch: 47, loss: 293.81, accuracy: 0.88
epoch: 48, loss: 292.59, accuracy: 0.8764
epoch: 49, loss: 294.38, accuracy: 0.8747
epoch: 50, loss: 291.68, accuracy: 0.8789
epoch: 51, loss: 293.99, accuracy: 0.878
epoch: 52, loss: 292.05, accuracy: 0.8775
epoch: 53, loss: 292.16, accuracy: 0.8752
epoch: 54, loss: 293.01, accuracy: 0.8805
epoch: 55, loss: 290.57, accuracy: 0.8804
epoch: 56, loss: 292.31, accuracy: 0.8797
epoch: 57, loss: 291.43, accuracy: 0.8738
epoch: 58, loss: 290.62, accuracy: 0.8731
epoch: 59, loss: 292.15, accuracy: 0.8778
epoch: 60, loss: 290.56, accuracy: 0.88
Epoch    61: reducing learning rate of group 0 to 2.5000e-03.
epoch: 61, loss: 291.87, accuracy: 0.8804
epoch: 62, loss: 282.93, accuracy: 0.8818
epoch: 63, loss: 282.29, accuracy: 0.8792
epoch: 64, loss: 282.07, accuracy: 0.8801
epoch: 65, loss: 281.86, accuracy: 0.8834
epoch: 66, loss: 281.15, accuracy: 0.8803
epoch: 67, loss: 281.08, accuracy: 0.8784
epoch: 68, loss: 280.97, accuracy: 0.8812
epoch: 69, loss: 282.13, accuracy: 0.8816
epoch: 70, loss: 281.13, accuracy: 0.8813
epoch: 71, loss: 281.27, accuracy: 0.877
Epoch    72: reducing learning rate of group 0 to 1.2500e-03.
epoch: 72, loss: 282.08, accuracy: 0.8832
epoch: 73, loss: 276.88, accuracy: 0.8826
epoch: 74, loss: 276.02, accuracy: 0.8825
epoch: 75, loss: 272.29, accuracy: 0.8828
epoch: 76, loss: 271.95, accuracy: 0.882
epoch: 77, loss: 271.75, accuracy: 0.8825
epoch: 78, loss: 271.73, accuracy: 0.8826
epoch: 79, loss: 272.03, accuracy: 0.8822
epoch: 80, loss: 272.38, accuracy: 0.8815
epoch: 81, loss: 272.14, accuracy: 0.8825
epoch: 82, loss: 271.95, accuracy: 0.8825
Epoch    83: reducing learning rate of group 0 to 1.0000e-04.
epoch: 83, loss: 271.89, accuracy: 0.8831
epoch: 84, loss: 271.64, accuracy: 0.8827
epoch: 85, loss: 271.65, accuracy: 0.8819
epoch: 86, loss: 271.91, accuracy: 0.8821
epoch: 87, loss: 271.88, accuracy: 0.8829
epoch: 88, loss: 271.71, accuracy: 0.8827
epoch: 89, loss: 271.62, accuracy: 0.8832
epoch: 90, loss: 271.88, accuracy: 0.8824
epoch: 91, loss: 271.53, accuracy: 0.8831
epoch: 92, loss: 271.73, accuracy: 0.8826
epoch: 93, loss: 270.91, accuracy: 0.8845
epoch: 94, loss: 273.06, accuracy: 0.8846
epoch: 95, loss: 272.4, accuracy: 0.8834
epoch: 96, loss: 271.91, accuracy: 0.8824
epoch: 97, loss: 271.68, accuracy: 0.8831
epoch: 98, loss: 271.49, accuracy: 0.8823
epoch: 99, loss: 271.68, accuracy: 0.8831
time analysis:
    all 1137.8294 s
    train 1136.6852 s
Accuracy of     0 : 84 %
Accuracy of     1 : 95 %
Accuracy of     2 : 82 %
Accuracy of     3 : 84 %
Accuracy of     4 : 80 %
Accuracy of     5 : 100 %
Accuracy of     6 : 64 %
Accuracy of     7 : 98 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             134
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]             782
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,726
DAU params: 916
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 552.89, accuracy: 0.8221
epoch: 1, loss: 401.55, accuracy: 0.8534
epoch: 2, loss: 366.8, accuracy: 0.8586
epoch: 3, loss: 351.38, accuracy: 0.8651
epoch: 4, loss: 340.39, accuracy: 0.8709
epoch: 5, loss: 335.15, accuracy: 0.8684
epoch: 6, loss: 329.34, accuracy: 0.872
epoch: 7, loss: 319.84, accuracy: 0.8686
epoch: 8, loss: 315.55, accuracy: 0.873
epoch: 9, loss: 313.02, accuracy: 0.868
epoch: 10, loss: 311.03, accuracy: 0.8719
epoch: 11, loss: 309.53, accuracy: 0.8777
epoch: 12, loss: 306.28, accuracy: 0.8791
epoch: 13, loss: 305.79, accuracy: 0.8766
epoch: 14, loss: 304.32, accuracy: 0.872
epoch: 15, loss: 301.91, accuracy: 0.8815
epoch: 16, loss: 299.15, accuracy: 0.8767
epoch: 17, loss: 300.49, accuracy: 0.8805
epoch: 18, loss: 296.68, accuracy: 0.8757
epoch: 19, loss: 295.61, accuracy: 0.8825
epoch: 20, loss: 293.0, accuracy: 0.8793
epoch: 21, loss: 293.77, accuracy: 0.8795
epoch: 22, loss: 293.51, accuracy: 0.8801
epoch: 23, loss: 290.67, accuracy: 0.8775
epoch: 24, loss: 292.89, accuracy: 0.8734
epoch: 25, loss: 291.35, accuracy: 0.8827
epoch: 26, loss: 287.95, accuracy: 0.8816
epoch: 27, loss: 289.47, accuracy: 0.8843
epoch: 28, loss: 286.63, accuracy: 0.8819
epoch: 29, loss: 286.93, accuracy: 0.8786
epoch: 30, loss: 286.64, accuracy: 0.8735
epoch: 31, loss: 285.51, accuracy: 0.8733
epoch: 32, loss: 285.28, accuracy: 0.8846
epoch: 33, loss: 284.59, accuracy: 0.8695
epoch: 34, loss: 285.45, accuracy: 0.875
epoch: 35, loss: 285.02, accuracy: 0.8824
epoch: 36, loss: 284.21, accuracy: 0.8854
epoch: 37, loss: 282.58, accuracy: 0.8858
epoch: 38, loss: 284.0, accuracy: 0.8845
epoch: 39, loss: 282.16, accuracy: 0.8855
epoch: 40, loss: 280.56, accuracy: 0.8748
epoch: 41, loss: 280.98, accuracy: 0.8757
epoch: 42, loss: 280.8, accuracy: 0.8861
epoch: 43, loss: 280.19, accuracy: 0.8841
epoch: 44, loss: 280.38, accuracy: 0.8819
epoch: 45, loss: 279.21, accuracy: 0.8828
epoch: 46, loss: 278.11, accuracy: 0.8852
epoch: 47, loss: 278.66, accuracy: 0.8872
epoch: 48, loss: 279.26, accuracy: 0.8832
epoch: 49, loss: 277.95, accuracy: 0.8812
epoch: 50, loss: 280.03, accuracy: 0.8845
epoch: 51, loss: 278.96, accuracy: 0.8868
Epoch    52: reducing learning rate of group 0 to 5.0000e-03.
epoch: 52, loss: 277.9, accuracy: 0.8781
epoch: 53, loss: 264.87, accuracy: 0.8871
epoch: 54, loss: 263.61, accuracy: 0.8884
epoch: 55, loss: 264.23, accuracy: 0.89
epoch: 56, loss: 262.4, accuracy: 0.8872
epoch: 57, loss: 262.76, accuracy: 0.89
epoch: 58, loss: 262.78, accuracy: 0.8893
epoch: 59, loss: 261.79, accuracy: 0.8896
epoch: 60, loss: 261.64, accuracy: 0.8893
epoch: 61, loss: 261.61, accuracy: 0.8879
epoch: 62, loss: 261.15, accuracy: 0.8841
epoch: 63, loss: 260.25, accuracy: 0.8879
epoch: 64, loss: 261.91, accuracy: 0.8858
epoch: 65, loss: 260.52, accuracy: 0.8867
epoch: 66, loss: 260.26, accuracy: 0.89
epoch: 67, loss: 259.11, accuracy: 0.8862
epoch: 68, loss: 259.93, accuracy: 0.8875
epoch: 69, loss: 259.98, accuracy: 0.8886
epoch: 70, loss: 260.16, accuracy: 0.8863
epoch: 71, loss: 258.66, accuracy: 0.8893
epoch: 72, loss: 258.5, accuracy: 0.8866
epoch: 73, loss: 259.32, accuracy: 0.8927
epoch: 74, loss: 260.29, accuracy: 0.8903
epoch: 75, loss: 247.88, accuracy: 0.8926
epoch: 76, loss: 245.48, accuracy: 0.8926
epoch: 77, loss: 244.93, accuracy: 0.894
epoch: 78, loss: 245.07, accuracy: 0.8947
epoch: 79, loss: 245.35, accuracy: 0.8938
epoch: 80, loss: 244.31, accuracy: 0.8933
epoch: 81, loss: 244.11, accuracy: 0.8933
epoch: 82, loss: 244.51, accuracy: 0.8936
epoch: 83, loss: 244.31, accuracy: 0.8962
epoch: 84, loss: 244.72, accuracy: 0.8934
epoch: 85, loss: 244.27, accuracy: 0.8932
Epoch    86: reducing learning rate of group 0 to 2.5000e-04.
epoch: 86, loss: 244.36, accuracy: 0.8944
epoch: 87, loss: 242.99, accuracy: 0.8937
epoch: 88, loss: 243.13, accuracy: 0.8946
epoch: 89, loss: 243.91, accuracy: 0.8938
epoch: 90, loss: 243.27, accuracy: 0.8935
epoch: 91, loss: 243.34, accuracy: 0.8929
epoch: 92, loss: 242.92, accuracy: 0.8939
epoch: 93, loss: 243.49, accuracy: 0.8936
epoch: 94, loss: 242.91, accuracy: 0.8944
epoch: 95, loss: 243.28, accuracy: 0.8934
epoch: 96, loss: 242.64, accuracy: 0.893
epoch: 97, loss: 242.38, accuracy: 0.8941
epoch: 98, loss: 243.53, accuracy: 0.8948
epoch: 99, loss: 242.73, accuracy: 0.895
time analysis:
    all 1365.317 s
    train 1363.961 s
Accuracy of     0 : 88 %
Accuracy of     1 : 95 %
Accuracy of     2 : 85 %
Accuracy of     3 : 86 %
Accuracy of     4 : 83 %
Accuracy of     5 : 98 %
Accuracy of     6 : 70 %
Accuracy of     7 : 94 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             168
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,040
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,018
DAU params: 1,208
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 551.57, accuracy: 0.8253
epoch: 1, loss: 405.3, accuracy: 0.8247
epoch: 2, loss: 374.83, accuracy: 0.8538
epoch: 3, loss: 356.15, accuracy: 0.8583
epoch: 4, loss: 346.62, accuracy: 0.8573
epoch: 5, loss: 337.31, accuracy: 0.8624
epoch: 6, loss: 331.51, accuracy: 0.859
epoch: 7, loss: 325.67, accuracy: 0.8535
epoch: 8, loss: 322.15, accuracy: 0.8668
epoch: 9, loss: 316.33, accuracy: 0.8736
epoch: 10, loss: 314.39, accuracy: 0.8738
epoch: 11, loss: 313.67, accuracy: 0.8709
epoch: 12, loss: 310.67, accuracy: 0.8657
epoch: 13, loss: 308.54, accuracy: 0.8715
epoch: 14, loss: 307.0, accuracy: 0.8713
epoch: 15, loss: 303.9, accuracy: 0.8706
epoch: 16, loss: 302.14, accuracy: 0.8687
epoch: 17, loss: 300.37, accuracy: 0.8749
epoch: 18, loss: 300.97, accuracy: 0.8797
epoch: 19, loss: 297.35, accuracy: 0.8677
epoch: 20, loss: 295.7, accuracy: 0.8709
epoch: 21, loss: 296.84, accuracy: 0.8782
epoch: 22, loss: 293.66, accuracy: 0.8688
epoch: 23, loss: 291.75, accuracy: 0.8783
epoch: 24, loss: 290.83, accuracy: 0.8729
epoch: 25, loss: 290.37, accuracy: 0.8704
epoch: 26, loss: 291.44, accuracy: 0.8734
epoch: 27, loss: 288.79, accuracy: 0.8798
epoch: 28, loss: 287.1, accuracy: 0.8805
epoch: 29, loss: 286.07, accuracy: 0.8843
epoch: 30, loss: 286.85, accuracy: 0.8732
epoch: 31, loss: 285.43, accuracy: 0.8831
epoch: 32, loss: 283.6, accuracy: 0.8652
epoch: 33, loss: 284.04, accuracy: 0.8774
epoch: 34, loss: 283.09, accuracy: 0.8783
epoch: 35, loss: 282.96, accuracy: 0.8819
epoch: 36, loss: 280.96, accuracy: 0.8768
epoch: 37, loss: 280.15, accuracy: 0.8826
epoch: 38, loss: 278.37, accuracy: 0.881
epoch: 39, loss: 279.75, accuracy: 0.8813
epoch: 40, loss: 279.81, accuracy: 0.8812
epoch: 41, loss: 280.05, accuracy: 0.883
epoch: 42, loss: 278.91, accuracy: 0.8784
epoch: 43, loss: 277.51, accuracy: 0.8819
epoch: 44, loss: 275.86, accuracy: 0.8795
epoch: 45, loss: 274.79, accuracy: 0.8856
epoch: 46, loss: 276.27, accuracy: 0.886
epoch: 47, loss: 275.69, accuracy: 0.8832
epoch: 48, loss: 275.99, accuracy: 0.873
epoch: 49, loss: 274.96, accuracy: 0.8791
epoch: 50, loss: 276.43, accuracy: 0.8846
epoch: 51, loss: 274.22, accuracy: 0.8832
epoch: 52, loss: 273.53, accuracy: 0.8841
epoch: 53, loss: 271.91, accuracy: 0.8852
epoch: 54, loss: 272.68, accuracy: 0.8835
epoch: 55, loss: 272.42, accuracy: 0.883
epoch: 56, loss: 274.33, accuracy: 0.8826
epoch: 57, loss: 272.69, accuracy: 0.8853
epoch: 58, loss: 273.08, accuracy: 0.8709
Epoch    59: reducing learning rate of group 0 to 5.0000e-03.
epoch: 59, loss: 272.27, accuracy: 0.8836
epoch: 60, loss: 259.71, accuracy: 0.8885
epoch: 61, loss: 256.86, accuracy: 0.8856
epoch: 62, loss: 256.53, accuracy: 0.8888
epoch: 63, loss: 256.71, accuracy: 0.8908
epoch: 64, loss: 256.02, accuracy: 0.8894
epoch: 65, loss: 257.08, accuracy: 0.8888
epoch: 66, loss: 256.69, accuracy: 0.8887
epoch: 67, loss: 255.46, accuracy: 0.8853
epoch: 68, loss: 256.07, accuracy: 0.8853
epoch: 69, loss: 255.67, accuracy: 0.8842
epoch: 70, loss: 256.39, accuracy: 0.8872
epoch: 71, loss: 254.22, accuracy: 0.885
epoch: 72, loss: 254.98, accuracy: 0.8891
epoch: 73, loss: 254.91, accuracy: 0.89
epoch: 74, loss: 254.16, accuracy: 0.889
epoch: 75, loss: 243.1, accuracy: 0.8921
epoch: 76, loss: 240.51, accuracy: 0.8916
epoch: 77, loss: 240.3, accuracy: 0.8917
epoch: 78, loss: 239.99, accuracy: 0.891
epoch: 79, loss: 239.8, accuracy: 0.8912
epoch: 80, loss: 239.95, accuracy: 0.8925
epoch: 81, loss: 239.68, accuracy: 0.8909
epoch: 82, loss: 239.27, accuracy: 0.8918
epoch: 83, loss: 239.2, accuracy: 0.8923
epoch: 84, loss: 239.12, accuracy: 0.891
epoch: 85, loss: 238.44, accuracy: 0.8917
epoch: 86, loss: 239.15, accuracy: 0.8916
epoch: 87, loss: 239.45, accuracy: 0.8926
epoch: 88, loss: 238.19, accuracy: 0.8904
epoch: 89, loss: 238.72, accuracy: 0.891
epoch: 90, loss: 239.41, accuracy: 0.8908
epoch: 91, loss: 239.22, accuracy: 0.892
epoch: 92, loss: 238.51, accuracy: 0.8917
epoch: 93, loss: 239.2, accuracy: 0.8913
Epoch    94: reducing learning rate of group 0 to 2.5000e-04.
epoch: 94, loss: 238.65, accuracy: 0.8908
epoch: 95, loss: 237.98, accuracy: 0.8913
epoch: 96, loss: 237.41, accuracy: 0.8924
epoch: 97, loss: 237.55, accuracy: 0.8913
epoch: 98, loss: 238.43, accuracy: 0.8911
epoch: 99, loss: 237.58, accuracy: 0.8909
time analysis:
    all 1607.8095 s
    train 1606.234 s
Accuracy of     0 : 84 %
Accuracy of     1 : 95 %
Accuracy of     2 : 82 %
Accuracy of     3 : 83 %
Accuracy of     4 : 78 %
Accuracy of     5 : 96 %
Accuracy of     6 : 67 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=5, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             202
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,298
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,310
DAU params: 1,500
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 548.28, accuracy: 0.823
epoch: 1, loss: 411.63, accuracy: 0.841
epoch: 2, loss: 384.37, accuracy: 0.8484
epoch: 3, loss: 366.84, accuracy: 0.8544
epoch: 4, loss: 358.31, accuracy: 0.8591
epoch: 5, loss: 344.79, accuracy: 0.849
epoch: 6, loss: 339.92, accuracy: 0.8596
epoch: 7, loss: 330.97, accuracy: 0.8637
epoch: 8, loss: 326.89, accuracy: 0.8643
epoch: 9, loss: 323.08, accuracy: 0.8483
epoch: 10, loss: 318.82, accuracy: 0.866
epoch: 11, loss: 313.05, accuracy: 0.8747
epoch: 12, loss: 312.14, accuracy: 0.871
epoch: 13, loss: 309.64, accuracy: 0.8688
epoch: 14, loss: 307.75, accuracy: 0.8713
epoch: 15, loss: 305.76, accuracy: 0.8761
epoch: 16, loss: 301.41, accuracy: 0.8771
epoch: 17, loss: 300.41, accuracy: 0.8803
epoch: 18, loss: 300.52, accuracy: 0.8806
epoch: 19, loss: 298.79, accuracy: 0.8786
epoch: 20, loss: 296.86, accuracy: 0.8798
epoch: 21, loss: 296.09, accuracy: 0.8807
epoch: 22, loss: 293.02, accuracy: 0.8777
epoch: 23, loss: 290.32, accuracy: 0.8784
epoch: 24, loss: 291.07, accuracy: 0.8798
epoch: 25, loss: 287.66, accuracy: 0.8792
epoch: 26, loss: 286.86, accuracy: 0.8699
epoch: 27, loss: 285.97, accuracy: 0.8719
epoch: 28, loss: 284.92, accuracy: 0.8818
epoch: 29, loss: 285.02, accuracy: 0.8823
epoch: 30, loss: 284.08, accuracy: 0.8747
epoch: 31, loss: 281.22, accuracy: 0.8774
epoch: 32, loss: 280.05, accuracy: 0.8839
epoch: 33, loss: 280.67, accuracy: 0.8839
epoch: 34, loss: 279.03, accuracy: 0.8822
epoch: 35, loss: 279.36, accuracy: 0.8761
epoch: 36, loss: 277.96, accuracy: 0.8795
epoch: 37, loss: 276.64, accuracy: 0.8832
epoch: 38, loss: 275.82, accuracy: 0.8879
epoch: 39, loss: 274.98, accuracy: 0.8826
epoch: 40, loss: 275.07, accuracy: 0.8837
epoch: 41, loss: 273.18, accuracy: 0.879
epoch: 42, loss: 272.79, accuracy: 0.8818
epoch: 43, loss: 274.93, accuracy: 0.8824
epoch: 44, loss: 273.39, accuracy: 0.8832
epoch: 45, loss: 273.71, accuracy: 0.8844
epoch: 46, loss: 273.62, accuracy: 0.8807
epoch: 47, loss: 271.03, accuracy: 0.8856
epoch: 48, loss: 271.54, accuracy: 0.8833
epoch: 49, loss: 270.32, accuracy: 0.8746
epoch: 50, loss: 269.82, accuracy: 0.8851
epoch: 51, loss: 267.95, accuracy: 0.8777
epoch: 52, loss: 269.66, accuracy: 0.8853
epoch: 53, loss: 269.67, accuracy: 0.8816
epoch: 54, loss: 266.67, accuracy: 0.8834
epoch: 55, loss: 269.3, accuracy: 0.8873
epoch: 56, loss: 265.75, accuracy: 0.8868
epoch: 57, loss: 268.27, accuracy: 0.8859
epoch: 58, loss: 268.91, accuracy: 0.8862
epoch: 59, loss: 264.15, accuracy: 0.8811
epoch: 60, loss: 265.94, accuracy: 0.8812
epoch: 61, loss: 264.9, accuracy: 0.8783
epoch: 62, loss: 265.96, accuracy: 0.8864
epoch: 63, loss: 264.06, accuracy: 0.8884
epoch: 64, loss: 265.24, accuracy: 0.8841
Epoch    65: reducing learning rate of group 0 to 5.0000e-03.
epoch: 65, loss: 265.92, accuracy: 0.8857
epoch: 66, loss: 250.37, accuracy: 0.8869
epoch: 67, loss: 250.06, accuracy: 0.8918
epoch: 68, loss: 249.07, accuracy: 0.8916
epoch: 69, loss: 248.96, accuracy: 0.8914
epoch: 70, loss: 248.14, accuracy: 0.8892
epoch: 71, loss: 249.23, accuracy: 0.8945
epoch: 72, loss: 248.26, accuracy: 0.8889
epoch: 73, loss: 248.69, accuracy: 0.8893
epoch: 74, loss: 246.51, accuracy: 0.8859
epoch: 75, loss: 236.51, accuracy: 0.8948
epoch: 76, loss: 234.16, accuracy: 0.8953
epoch: 77, loss: 234.57, accuracy: 0.8938
epoch: 78, loss: 233.95, accuracy: 0.8941
epoch: 79, loss: 233.83, accuracy: 0.8946
epoch: 80, loss: 233.82, accuracy: 0.8944
epoch: 81, loss: 233.25, accuracy: 0.8957
epoch: 82, loss: 233.12, accuracy: 0.8962
epoch: 83, loss: 233.36, accuracy: 0.895
epoch: 84, loss: 233.24, accuracy: 0.8959
epoch: 85, loss: 233.14, accuracy: 0.895
epoch: 86, loss: 233.34, accuracy: 0.896
Epoch    87: reducing learning rate of group 0 to 2.5000e-04.
epoch: 87, loss: 233.23, accuracy: 0.8941
epoch: 88, loss: 232.84, accuracy: 0.8953
epoch: 89, loss: 232.85, accuracy: 0.8957
epoch: 90, loss: 232.58, accuracy: 0.8957
epoch: 91, loss: 232.16, accuracy: 0.8972
epoch: 92, loss: 232.25, accuracy: 0.896
epoch: 93, loss: 232.3, accuracy: 0.8963
epoch: 94, loss: 231.96, accuracy: 0.897
epoch: 95, loss: 232.18, accuracy: 0.8954
epoch: 96, loss: 232.08, accuracy: 0.8952
epoch: 97, loss: 232.26, accuracy: 0.8961
epoch: 98, loss: 231.7, accuracy: 0.8946
epoch: 99, loss: 232.11, accuracy: 0.8954
time analysis:
    all 1851.4202 s
    train 1849.5581 s
Accuracy of     0 : 82 %
Accuracy of     1 : 93 %
Accuracy of     2 : 87 %
Accuracy of     3 : 87 %
Accuracy of     4 : 81 %
Accuracy of     5 : 98 %
Accuracy of     6 : 70 %
Accuracy of     7 : 100 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, small_fc, DAUConv2dOneMu, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=32, no_units=6, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=32, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 32, 28, 28]             236
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
    DAUConv2dOneMu-4            [-1, 8, 14, 14]           1,556
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 2,602
DAU params: 1,792
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 539.28, accuracy: 0.8394
epoch: 1, loss: 387.51, accuracy: 0.8348
epoch: 2, loss: 355.74, accuracy: 0.8672
epoch: 3, loss: 339.99, accuracy: 0.863
epoch: 4, loss: 326.72, accuracy: 0.8687
epoch: 5, loss: 320.5, accuracy: 0.8766
epoch: 6, loss: 315.11, accuracy: 0.8695
epoch: 7, loss: 307.1, accuracy: 0.8771
epoch: 8, loss: 302.96, accuracy: 0.8787
epoch: 9, loss: 298.73, accuracy: 0.8718
epoch: 10, loss: 292.61, accuracy: 0.8719
epoch: 11, loss: 290.28, accuracy: 0.8794
epoch: 12, loss: 287.98, accuracy: 0.8824
epoch: 13, loss: 285.06, accuracy: 0.8865
epoch: 14, loss: 282.41, accuracy: 0.8666
epoch: 15, loss: 281.78, accuracy: 0.8809
epoch: 16, loss: 278.33, accuracy: 0.8735
epoch: 17, loss: 279.74, accuracy: 0.8851
epoch: 18, loss: 276.15, accuracy: 0.8855
epoch: 19, loss: 275.36, accuracy: 0.8881
epoch: 20, loss: 275.1, accuracy: 0.8778
epoch: 21, loss: 270.86, accuracy: 0.8802
epoch: 22, loss: 270.58, accuracy: 0.8882
epoch: 23, loss: 269.48, accuracy: 0.8902
epoch: 24, loss: 269.89, accuracy: 0.8877
epoch: 25, loss: 268.67, accuracy: 0.8875
epoch: 26, loss: 268.34, accuracy: 0.8787
epoch: 27, loss: 267.04, accuracy: 0.8879
epoch: 28, loss: 266.04, accuracy: 0.8912
epoch: 29, loss: 265.69, accuracy: 0.8826
epoch: 30, loss: 265.14, accuracy: 0.8891
epoch: 31, loss: 264.09, accuracy: 0.8913
epoch: 32, loss: 262.35, accuracy: 0.89
epoch: 33, loss: 262.28, accuracy: 0.8803
epoch: 34, loss: 261.08, accuracy: 0.8828
epoch: 35, loss: 261.77, accuracy: 0.8826
epoch: 36, loss: 261.51, accuracy: 0.8893
epoch: 37, loss: 260.83, accuracy: 0.8892
epoch: 38, loss: 260.16, accuracy: 0.8878
epoch: 39, loss: 258.13, accuracy: 0.8923
epoch: 40, loss: 256.89, accuracy: 0.8862
epoch: 41, loss: 258.54, accuracy: 0.8866
epoch: 42, loss: 256.53, accuracy: 0.8816
epoch: 43, loss: 255.81, accuracy: 0.8853
epoch: 44, loss: 256.45, accuracy: 0.8801
epoch: 45, loss: 254.76, accuracy: 0.8873
epoch: 46, loss: 254.76, accuracy: 0.8899
epoch: 47, loss: 253.42, accuracy: 0.8871
epoch: 48, loss: 253.97, accuracy: 0.8891
epoch: 49, loss: 254.17, accuracy: 0.8936
epoch: 50, loss: 255.13, accuracy: 0.8897
epoch: 51, loss: 252.28, accuracy: 0.8894
epoch: 52, loss: 251.11, accuracy: 0.8904
epoch: 53, loss: 252.35, accuracy: 0.888
epoch: 54, loss: 251.05, accuracy: 0.8843
epoch: 55, loss: 251.4, accuracy: 0.8832
epoch: 56, loss: 251.38, accuracy: 0.8939
epoch: 57, loss: 250.9, accuracy: 0.8821
Epoch    58: reducing learning rate of group 0 to 5.0000e-03.
epoch: 58, loss: 251.3, accuracy: 0.8912
epoch: 59, loss: 237.06, accuracy: 0.8957
epoch: 60, loss: 234.23, accuracy: 0.8909
epoch: 61, loss: 235.36, accuracy: 0.8927
epoch: 62, loss: 232.77, accuracy: 0.8954
epoch: 63, loss: 232.59, accuracy: 0.8983
epoch: 64, loss: 233.65, accuracy: 0.8887
epoch: 65, loss: 232.58, accuracy: 0.8935
epoch: 66, loss: 233.14, accuracy: 0.8931
epoch: 67, loss: 233.41, accuracy: 0.8941
epoch: 68, loss: 232.4, accuracy: 0.8935
epoch: 69, loss: 231.6, accuracy: 0.8938
epoch: 70, loss: 231.74, accuracy: 0.8898
epoch: 71, loss: 231.15, accuracy: 0.8939
epoch: 72, loss: 231.92, accuracy: 0.8942
epoch: 73, loss: 231.1, accuracy: 0.8932
epoch: 74, loss: 230.64, accuracy: 0.8907
epoch: 75, loss: 218.59, accuracy: 0.899
epoch: 76, loss: 216.5, accuracy: 0.8987
epoch: 77, loss: 216.59, accuracy: 0.899
epoch: 78, loss: 216.74, accuracy: 0.8977
epoch: 79, loss: 216.65, accuracy: 0.8983
epoch: 80, loss: 216.15, accuracy: 0.8989
epoch: 81, loss: 216.42, accuracy: 0.8988
epoch: 82, loss: 216.05, accuracy: 0.8991
epoch: 83, loss: 215.54, accuracy: 0.8993
epoch: 84, loss: 215.71, accuracy: 0.899
epoch: 85, loss: 215.99, accuracy: 0.8984
epoch: 86, loss: 215.63, accuracy: 0.8979
epoch: 87, loss: 215.39, accuracy: 0.8991
epoch: 88, loss: 215.17, accuracy: 0.8979
epoch: 89, loss: 215.7, accuracy: 0.8985
epoch: 90, loss: 215.11, accuracy: 0.8981
epoch: 91, loss: 216.0, accuracy: 0.8989
epoch: 92, loss: 214.89, accuracy: 0.8973
epoch: 93, loss: 215.43, accuracy: 0.8967
epoch: 94, loss: 215.27, accuracy: 0.8985
epoch: 95, loss: 215.25, accuracy: 0.8979
epoch: 96, loss: 215.05, accuracy: 0.8973
epoch: 97, loss: 215.08, accuracy: 0.8985
Epoch    98: reducing learning rate of group 0 to 2.5000e-04.
epoch: 98, loss: 214.97, accuracy: 0.8985
epoch: 99, loss: 214.25, accuracy: 0.8982
time analysis:
    all 2140.7841 s
    train 2138.6876 s
Accuracy of     0 : 88 %
Accuracy of     1 : 93 %
Accuracy of     2 : 87 %
Accuracy of     3 : 86 %
Accuracy of     4 : 83 %
Accuracy of     5 : 98 %
Accuracy of     6 : 74 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 96 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 655.81, accuracy: 0.5347
epoch: 1, loss: 524.88, accuracy: 0.7591
epoch: 2, loss: 501.72, accuracy: 0.7543
epoch: 3, loss: 490.96, accuracy: 0.7968
epoch: 4, loss: 482.16, accuracy: 0.7784
epoch: 5, loss: 474.34, accuracy: 0.7531
epoch: 6, loss: 468.55, accuracy: 0.8119
epoch: 7, loss: 459.53, accuracy: 0.7853
epoch: 8, loss: 453.87, accuracy: 0.8159
epoch: 9, loss: 451.89, accuracy: 0.7972
epoch: 10, loss: 447.25, accuracy: 0.806
epoch: 11, loss: 444.77, accuracy: 0.7583
epoch: 12, loss: 443.57, accuracy: 0.8031
epoch: 13, loss: 440.34, accuracy: 0.8094
epoch: 14, loss: 439.55, accuracy: 0.8148
epoch: 15, loss: 439.31, accuracy: 0.8216
epoch: 16, loss: 436.93, accuracy: 0.812
epoch: 17, loss: 433.98, accuracy: 0.7939
epoch: 18, loss: 434.42, accuracy: 0.7947
epoch: 19, loss: 429.09, accuracy: 0.8028
epoch: 20, loss: 430.44, accuracy: 0.7868
epoch: 21, loss: 430.39, accuracy: 0.8125
epoch: 22, loss: 429.28, accuracy: 0.7613
epoch: 23, loss: 427.89, accuracy: 0.8005
epoch: 24, loss: 427.53, accuracy: 0.8258
epoch: 25, loss: 427.14, accuracy: 0.7481
epoch: 26, loss: 427.65, accuracy: 0.7766
epoch: 27, loss: 424.38, accuracy: 0.8188
epoch: 28, loss: 424.76, accuracy: 0.8277
epoch: 29, loss: 423.23, accuracy: 0.7627
epoch: 30, loss: 420.72, accuracy: 0.7835
epoch: 31, loss: 423.29, accuracy: 0.764
epoch: 32, loss: 422.78, accuracy: 0.8169
epoch: 33, loss: 424.11, accuracy: 0.8093
epoch: 34, loss: 421.69, accuracy: 0.7519
epoch: 35, loss: 422.16, accuracy: 0.8269
Epoch    36: reducing learning rate of group 0 to 5.0000e-03.
epoch: 36, loss: 422.0, accuracy: 0.7645
epoch: 37, loss: 408.79, accuracy: 0.8206
epoch: 38, loss: 408.78, accuracy: 0.836
epoch: 39, loss: 406.61, accuracy: 0.8242
epoch: 40, loss: 405.92, accuracy: 0.8066
epoch: 41, loss: 406.13, accuracy: 0.8319
epoch: 42, loss: 406.06, accuracy: 0.8381
epoch: 43, loss: 404.84, accuracy: 0.834
epoch: 44, loss: 406.54, accuracy: 0.7969
epoch: 45, loss: 405.77, accuracy: 0.8275
epoch: 46, loss: 404.53, accuracy: 0.825
epoch: 47, loss: 404.87, accuracy: 0.8306
epoch: 48, loss: 406.59, accuracy: 0.8298
epoch: 49, loss: 404.43, accuracy: 0.8075
epoch: 50, loss: 403.89, accuracy: 0.831
epoch: 51, loss: 405.65, accuracy: 0.8086
epoch: 52, loss: 406.34, accuracy: 0.839
epoch: 53, loss: 404.65, accuracy: 0.8291
epoch: 54, loss: 404.99, accuracy: 0.8323
epoch: 55, loss: 404.38, accuracy: 0.821
epoch: 56, loss: 403.17, accuracy: 0.8296
epoch: 57, loss: 401.73, accuracy: 0.8263
epoch: 58, loss: 403.5, accuracy: 0.8338
epoch: 59, loss: 402.6, accuracy: 0.8101
epoch: 60, loss: 402.92, accuracy: 0.818
epoch: 61, loss: 402.8, accuracy: 0.7983
epoch: 62, loss: 403.6, accuracy: 0.8093
Epoch    63: reducing learning rate of group 0 to 2.5000e-03.
epoch: 63, loss: 403.66, accuracy: 0.8353
epoch: 64, loss: 394.93, accuracy: 0.8357
epoch: 65, loss: 394.22, accuracy: 0.8375
epoch: 66, loss: 394.93, accuracy: 0.8386
epoch: 67, loss: 395.12, accuracy: 0.84
epoch: 68, loss: 396.59, accuracy: 0.8379
epoch: 69, loss: 394.3, accuracy: 0.8267
epoch: 70, loss: 394.07, accuracy: 0.839
epoch: 71, loss: 394.11, accuracy: 0.8384
epoch: 72, loss: 395.7, accuracy: 0.8402
epoch: 73, loss: 393.46, accuracy: 0.8324
epoch: 74, loss: 395.1, accuracy: 0.835
epoch: 75, loss: 387.53, accuracy: 0.8412
epoch: 76, loss: 386.59, accuracy: 0.8436
epoch: 77, loss: 387.61, accuracy: 0.8425
epoch: 78, loss: 387.76, accuracy: 0.8427
epoch: 79, loss: 387.37, accuracy: 0.8434
epoch: 80, loss: 387.11, accuracy: 0.844
epoch: 81, loss: 386.22, accuracy: 0.8438
Epoch    82: reducing learning rate of group 0 to 1.2500e-04.
epoch: 82, loss: 386.42, accuracy: 0.8433
epoch: 83, loss: 385.71, accuracy: 0.8429
epoch: 84, loss: 386.47, accuracy: 0.8434
epoch: 85, loss: 385.42, accuracy: 0.8445
epoch: 86, loss: 386.19, accuracy: 0.8443
epoch: 87, loss: 385.14, accuracy: 0.8431
epoch: 88, loss: 385.6, accuracy: 0.8436
epoch: 89, loss: 385.62, accuracy: 0.8431
epoch: 90, loss: 386.59, accuracy: 0.8424
epoch: 91, loss: 386.73, accuracy: 0.8448
epoch: 92, loss: 385.91, accuracy: 0.8445
Epoch    93: reducing learning rate of group 0 to 1.0000e-04.
epoch: 93, loss: 385.78, accuracy: 0.845
epoch: 94, loss: 384.69, accuracy: 0.8435
epoch: 95, loss: 386.31, accuracy: 0.8431
epoch: 96, loss: 385.08, accuracy: 0.8448
epoch: 97, loss: 384.8, accuracy: 0.8434
epoch: 98, loss: 385.88, accuracy: 0.8438
epoch: 99, loss: 386.93, accuracy: 0.8436
time analysis:
    all 731.7257 s
    train 729.3591 s
Accuracy of     0 : 82 %
Accuracy of     1 : 93 %
Accuracy of     2 : 78 %
Accuracy of     3 : 86 %
Accuracy of     4 : 71 %
Accuracy of     5 : 93 %
Accuracy of     6 : 54 %
Accuracy of     7 : 94 %
Accuracy of     8 : 98 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=1)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=1)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 660.16, accuracy: 0.7629
epoch: 1, loss: 531.62, accuracy: 0.783
epoch: 2, loss: 506.53, accuracy: 0.8077
epoch: 3, loss: 493.96, accuracy: 0.7983
epoch: 4, loss: 485.74, accuracy: 0.7994
epoch: 5, loss: 478.6, accuracy: 0.7897
epoch: 6, loss: 473.19, accuracy: 0.7753
epoch: 7, loss: 467.36, accuracy: 0.8014
epoch: 8, loss: 461.37, accuracy: 0.8094
epoch: 9, loss: 461.35, accuracy: 0.8087
epoch: 10, loss: 457.98, accuracy: 0.7682
epoch: 11, loss: 455.32, accuracy: 0.7968
epoch: 12, loss: 453.12, accuracy: 0.8171
epoch: 13, loss: 451.08, accuracy: 0.8
epoch: 14, loss: 449.42, accuracy: 0.8136
epoch: 15, loss: 448.26, accuracy: 0.8219
epoch: 16, loss: 446.81, accuracy: 0.7943
epoch: 17, loss: 445.41, accuracy: 0.8129
epoch: 18, loss: 442.05, accuracy: 0.8046
epoch: 19, loss: 443.13, accuracy: 0.7956
epoch: 20, loss: 441.62, accuracy: 0.7718
epoch: 21, loss: 442.3, accuracy: 0.8037
epoch: 22, loss: 440.51, accuracy: 0.8142
epoch: 23, loss: 440.36, accuracy: 0.8068
epoch: 24, loss: 437.97, accuracy: 0.7979
epoch: 25, loss: 437.14, accuracy: 0.7893
epoch: 26, loss: 434.42, accuracy: 0.8188
epoch: 27, loss: 437.69, accuracy: 0.8067
epoch: 28, loss: 438.37, accuracy: 0.8252
epoch: 29, loss: 432.85, accuracy: 0.7787
epoch: 30, loss: 434.87, accuracy: 0.7725
epoch: 31, loss: 436.03, accuracy: 0.7762
epoch: 32, loss: 434.32, accuracy: 0.8053
epoch: 33, loss: 432.74, accuracy: 0.8212
epoch: 34, loss: 431.03, accuracy: 0.8105
epoch: 35, loss: 431.89, accuracy: 0.8138
epoch: 36, loss: 433.33, accuracy: 0.8217
epoch: 37, loss: 432.03, accuracy: 0.79
epoch: 38, loss: 431.12, accuracy: 0.8035
epoch: 39, loss: 429.21, accuracy: 0.6554
epoch: 40, loss: 432.13, accuracy: 0.811
epoch: 41, loss: 428.91, accuracy: 0.7985
epoch: 42, loss: 430.27, accuracy: 0.8093
epoch: 43, loss: 428.09, accuracy: 0.8084
epoch: 44, loss: 430.0, accuracy: 0.7873
epoch: 45, loss: 427.52, accuracy: 0.794
epoch: 46, loss: 429.42, accuracy: 0.819
epoch: 47, loss: 427.15, accuracy: 0.82
epoch: 48, loss: 424.79, accuracy: 0.8226
epoch: 49, loss: 427.61, accuracy: 0.8167
epoch: 50, loss: 428.82, accuracy: 0.7635
epoch: 51, loss: 427.76, accuracy: 0.8147
epoch: 52, loss: 428.55, accuracy: 0.8167
epoch: 53, loss: 425.98, accuracy: 0.8031
Epoch    54: reducing learning rate of group 0 to 5.0000e-03.
epoch: 54, loss: 427.51, accuracy: 0.7925
epoch: 55, loss: 413.39, accuracy: 0.8211
epoch: 56, loss: 412.14, accuracy: 0.7799
epoch: 57, loss: 411.62, accuracy: 0.8269
epoch: 58, loss: 411.86, accuracy: 0.8102
epoch: 59, loss: 410.82, accuracy: 0.8246
epoch: 60, loss: 412.18, accuracy: 0.8257
epoch: 61, loss: 410.71, accuracy: 0.8278
epoch: 62, loss: 410.7, accuracy: 0.8168
epoch: 63, loss: 410.51, accuracy: 0.8288
epoch: 64, loss: 411.26, accuracy: 0.8328
epoch: 65, loss: 409.7, accuracy: 0.8134
epoch: 66, loss: 410.31, accuracy: 0.8259
epoch: 67, loss: 410.23, accuracy: 0.8249
epoch: 68, loss: 409.46, accuracy: 0.8228
epoch: 69, loss: 409.48, accuracy: 0.8159
epoch: 70, loss: 409.99, accuracy: 0.817
Epoch    71: reducing learning rate of group 0 to 2.5000e-03.
epoch: 71, loss: 410.41, accuracy: 0.8263
epoch: 72, loss: 401.74, accuracy: 0.8296
epoch: 73, loss: 402.14, accuracy: 0.8302
epoch: 74, loss: 401.71, accuracy: 0.8336
epoch: 75, loss: 396.27, accuracy: 0.8334
epoch: 76, loss: 395.56, accuracy: 0.8339
epoch: 77, loss: 394.96, accuracy: 0.8349
epoch: 78, loss: 395.82, accuracy: 0.8338
epoch: 79, loss: 395.23, accuracy: 0.8341
epoch: 80, loss: 395.79, accuracy: 0.8345
epoch: 81, loss: 396.13, accuracy: 0.8329
epoch: 82, loss: 395.83, accuracy: 0.8349
Epoch    83: reducing learning rate of group 0 to 1.2500e-04.
epoch: 83, loss: 394.6, accuracy: 0.8356
epoch: 84, loss: 393.9, accuracy: 0.8341
epoch: 85, loss: 393.85, accuracy: 0.8339
epoch: 86, loss: 394.06, accuracy: 0.8346
epoch: 87, loss: 394.28, accuracy: 0.8334
epoch: 88, loss: 394.69, accuracy: 0.8351
epoch: 89, loss: 395.23, accuracy: 0.8341
epoch: 90, loss: 394.52, accuracy: 0.8357
epoch: 91, loss: 393.71, accuracy: 0.8328
epoch: 92, loss: 394.91, accuracy: 0.8342
epoch: 93, loss: 394.32, accuracy: 0.8357
Epoch    94: reducing learning rate of group 0 to 1.0000e-04.
epoch: 94, loss: 394.4, accuracy: 0.8356
epoch: 95, loss: 394.45, accuracy: 0.8349
epoch: 96, loss: 393.88, accuracy: 0.8336
epoch: 97, loss: 393.07, accuracy: 0.8335
epoch: 98, loss: 394.16, accuracy: 0.8359
epoch: 99, loss: 395.5, accuracy: 0.8355
time analysis:
    all 730.4341 s
    train 729.3975 s
Accuracy of     0 : 78 %
Accuracy of     1 : 90 %
Accuracy of     2 : 72 %
Accuracy of     3 : 80 %
Accuracy of     4 : 66 %
Accuracy of     5 : 93 %
Accuracy of     6 : 52 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 96 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=1.5)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=1.5)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 704.07, accuracy: 0.717
epoch: 1, loss: 587.95, accuracy: 0.74
epoch: 2, loss: 555.23, accuracy: 0.7544
epoch: 3, loss: 536.56, accuracy: 0.7477
epoch: 4, loss: 522.02, accuracy: 0.5509
epoch: 5, loss: 511.47, accuracy: 0.7279
epoch: 6, loss: 505.97, accuracy: 0.7945
epoch: 7, loss: 499.76, accuracy: 0.7577
epoch: 8, loss: 496.17, accuracy: 0.724
epoch: 9, loss: 494.18, accuracy: 0.755
epoch: 10, loss: 491.92, accuracy: 0.7188
epoch: 11, loss: 483.43, accuracy: 0.7858
epoch: 12, loss: 481.06, accuracy: 0.7755
epoch: 13, loss: 486.33, accuracy: 0.5785
epoch: 14, loss: 480.6, accuracy: 0.733
epoch: 15, loss: 478.57, accuracy: 0.7597
epoch: 16, loss: 476.96, accuracy: 0.8014
epoch: 17, loss: 475.6, accuracy: 0.7836
epoch: 18, loss: 471.33, accuracy: 0.7581
epoch: 19, loss: 473.08, accuracy: 0.792
epoch: 20, loss: 472.58, accuracy: 0.7275
epoch: 21, loss: 470.97, accuracy: 0.7577
epoch: 22, loss: 471.32, accuracy: 0.7747
epoch: 23, loss: 468.58, accuracy: 0.7963
epoch: 24, loss: 468.41, accuracy: 0.7889
epoch: 25, loss: 467.86, accuracy: 0.8078
epoch: 26, loss: 464.84, accuracy: 0.7933
epoch: 27, loss: 463.86, accuracy: 0.7997
epoch: 28, loss: 465.01, accuracy: 0.7711
epoch: 29, loss: 462.18, accuracy: 0.7046
epoch: 30, loss: 466.03, accuracy: 0.7886
epoch: 31, loss: 463.6, accuracy: 0.7825
epoch: 32, loss: 462.31, accuracy: 0.7803
epoch: 33, loss: 464.22, accuracy: 0.7912
epoch: 34, loss: 461.62, accuracy: 0.7962
epoch: 35, loss: 462.06, accuracy: 0.6565
epoch: 36, loss: 459.87, accuracy: 0.7854
epoch: 37, loss: 461.6, accuracy: 0.7613
epoch: 38, loss: 457.53, accuracy: 0.8045
epoch: 39, loss: 459.23, accuracy: 0.7408
epoch: 40, loss: 458.37, accuracy: 0.7987
epoch: 41, loss: 459.97, accuracy: 0.7856
epoch: 42, loss: 457.96, accuracy: 0.7884
epoch: 43, loss: 458.91, accuracy: 0.7743
Epoch    44: reducing learning rate of group 0 to 5.0000e-03.
epoch: 44, loss: 457.57, accuracy: 0.793
epoch: 45, loss: 442.04, accuracy: 0.7368
epoch: 46, loss: 442.51, accuracy: 0.7842
epoch: 47, loss: 440.23, accuracy: 0.7957
epoch: 48, loss: 441.22, accuracy: 0.7913
epoch: 49, loss: 441.33, accuracy: 0.8162
epoch: 50, loss: 439.63, accuracy: 0.7728
epoch: 51, loss: 440.97, accuracy: 0.8118
epoch: 52, loss: 439.59, accuracy: 0.8108
epoch: 53, loss: 439.95, accuracy: 0.8134
epoch: 54, loss: 439.25, accuracy: 0.8152
epoch: 55, loss: 439.0, accuracy: 0.8106
epoch: 56, loss: 437.36, accuracy: 0.8068
epoch: 57, loss: 438.3, accuracy: 0.8037
epoch: 58, loss: 437.92, accuracy: 0.8111
epoch: 59, loss: 438.54, accuracy: 0.8169
epoch: 60, loss: 438.15, accuracy: 0.7973
epoch: 61, loss: 437.83, accuracy: 0.8031
epoch: 62, loss: 436.88, accuracy: 0.7946
epoch: 63, loss: 437.76, accuracy: 0.8007
epoch: 64, loss: 437.14, accuracy: 0.8072
epoch: 65, loss: 436.98, accuracy: 0.7997
epoch: 66, loss: 437.89, accuracy: 0.7995
epoch: 67, loss: 437.94, accuracy: 0.8166
Epoch    68: reducing learning rate of group 0 to 2.5000e-03.
epoch: 68, loss: 436.87, accuracy: 0.7994
epoch: 69, loss: 428.81, accuracy: 0.8162
epoch: 70, loss: 426.55, accuracy: 0.8134
epoch: 71, loss: 428.15, accuracy: 0.8228
epoch: 72, loss: 427.82, accuracy: 0.8209
epoch: 73, loss: 426.92, accuracy: 0.8241
epoch: 74, loss: 426.6, accuracy: 0.8211
epoch: 75, loss: 419.01, accuracy: 0.8274
epoch: 76, loss: 418.87, accuracy: 0.8273
epoch: 77, loss: 418.92, accuracy: 0.8278
epoch: 78, loss: 418.74, accuracy: 0.8264
epoch: 79, loss: 418.49, accuracy: 0.8264
epoch: 80, loss: 418.77, accuracy: 0.8252
epoch: 81, loss: 418.64, accuracy: 0.8261
epoch: 82, loss: 418.93, accuracy: 0.8276
epoch: 83, loss: 418.47, accuracy: 0.827
epoch: 84, loss: 418.68, accuracy: 0.8252
Epoch    85: reducing learning rate of group 0 to 1.2500e-04.
epoch: 85, loss: 418.8, accuracy: 0.8268
epoch: 86, loss: 418.65, accuracy: 0.8273
epoch: 87, loss: 418.13, accuracy: 0.8266
epoch: 88, loss: 418.59, accuracy: 0.8256
epoch: 89, loss: 418.27, accuracy: 0.8269
epoch: 90, loss: 417.63, accuracy: 0.8276
epoch: 91, loss: 418.86, accuracy: 0.8279
epoch: 92, loss: 418.28, accuracy: 0.8265
epoch: 93, loss: 417.92, accuracy: 0.8281
epoch: 94, loss: 417.48, accuracy: 0.8281
epoch: 95, loss: 418.27, accuracy: 0.8261
Epoch    96: reducing learning rate of group 0 to 1.0000e-04.
epoch: 96, loss: 417.33, accuracy: 0.8277
epoch: 97, loss: 417.21, accuracy: 0.8273
epoch: 98, loss: 418.24, accuracy: 0.8279
epoch: 99, loss: 417.85, accuracy: 0.8268
time analysis:
    all 734.5662 s
    train 733.5415 s
Accuracy of     0 : 78 %
Accuracy of     1 : 86 %
Accuracy of     2 : 75 %
Accuracy of     3 : 76 %
Accuracy of     4 : 66 %
Accuracy of     5 : 93 %
Accuracy of     6 : 54 %
Accuracy of     7 : 98 %
Accuracy of     8 : 95 %
Accuracy of     9 : 94 %
```

## FashionMNIST, small_fc, DAUConv2dZeroMu, units: 1, sigma: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=32, no_units=1, sigma=2)
  (conv2): DAUConv2dZeroMu(in_channels=32, out_channels=8, no_units=1, sigma=2)
  (norm1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (pool4): MaxPool2d(kernel_size=4, stride=4, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 32, 28, 28]              64
       BatchNorm2d-2           [-1, 32, 28, 28]              64
         MaxPool2d-3           [-1, 32, 14, 14]               0
   DAUConv2dZeroMu-4            [-1, 8, 14, 14]             264
       BatchNorm2d-5            [-1, 8, 14, 14]              16
         MaxPool2d-6              [-1, 8, 3, 3]               0
            Linear-7                   [-1, 10]             730
================================================================
total params: 1,138
DAU params: 328
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 739.03, accuracy: 0.7239
epoch: 1, loss: 632.03, accuracy: 0.7451
epoch: 2, loss: 605.77, accuracy: 0.7427
epoch: 3, loss: 592.76, accuracy: 0.7098
epoch: 4, loss: 582.17, accuracy: 0.7159
epoch: 5, loss: 578.47, accuracy: 0.5879
epoch: 6, loss: 567.38, accuracy: 0.7492
epoch: 7, loss: 563.41, accuracy: 0.7381
epoch: 8, loss: 560.69, accuracy: 0.7433
epoch: 9, loss: 555.37, accuracy: 0.6693
epoch: 10, loss: 554.01, accuracy: 0.7135
epoch: 11, loss: 550.76, accuracy: 0.7201
epoch: 12, loss: 548.42, accuracy: 0.7643
epoch: 13, loss: 542.63, accuracy: 0.7523
epoch: 14, loss: 542.1, accuracy: 0.7567
epoch: 15, loss: 541.72, accuracy: 0.7634
epoch: 16, loss: 535.81, accuracy: 0.7299
epoch: 17, loss: 534.4, accuracy: 0.7435
epoch: 18, loss: 534.28, accuracy: 0.7669
epoch: 19, loss: 532.66, accuracy: 0.7751
epoch: 20, loss: 529.97, accuracy: 0.7761
epoch: 21, loss: 526.62, accuracy: 0.7742
epoch: 22, loss: 525.8, accuracy: 0.7537
epoch: 23, loss: 523.55, accuracy: 0.756
epoch: 24, loss: 520.84, accuracy: 0.7793
epoch: 25, loss: 518.88, accuracy: 0.7026
epoch: 26, loss: 516.91, accuracy: 0.7217
epoch: 27, loss: 517.12, accuracy: 0.7406
epoch: 28, loss: 514.16, accuracy: 0.7392
epoch: 29, loss: 513.71, accuracy: 0.784
epoch: 30, loss: 513.9, accuracy: 0.7147
epoch: 31, loss: 510.94, accuracy: 0.7836
epoch: 32, loss: 509.59, accuracy: 0.7807
epoch: 33, loss: 511.13, accuracy: 0.7889
epoch: 34, loss: 510.56, accuracy: 0.7429
epoch: 35, loss: 505.78, accuracy: 0.778
epoch: 36, loss: 507.26, accuracy: 0.7859
epoch: 37, loss: 504.9, accuracy: 0.795
epoch: 38, loss: 505.29, accuracy: 0.7948
epoch: 39, loss: 504.85, accuracy: 0.7218
epoch: 40, loss: 502.08, accuracy: 0.7687
epoch: 41, loss: 501.42, accuracy: 0.7336
epoch: 42, loss: 501.96, accuracy: 0.7729
epoch: 43, loss: 502.65, accuracy: 0.7922
epoch: 44, loss: 502.07, accuracy: 0.782
epoch: 45, loss: 499.19, accuracy: 0.7867
epoch: 46, loss: 501.91, accuracy: 0.783
epoch: 47, loss: 500.74, accuracy: 0.7559
epoch: 48, loss: 499.01, accuracy: 0.7778
epoch: 49, loss: 498.44, accuracy: 0.7928
epoch: 50, loss: 498.04, accuracy: 0.7404
epoch: 51, loss: 499.53, accuracy: 0.7945
epoch: 52, loss: 497.64, accuracy: 0.7695
epoch: 53, loss: 498.01, accuracy: 0.7592
epoch: 54, loss: 497.01, accuracy: 0.7773
epoch: 55, loss: 495.79, accuracy: 0.7656
epoch: 56, loss: 496.88, accuracy: 0.7614
epoch: 57, loss: 493.15, accuracy: 0.7732
epoch: 58, loss: 497.93, accuracy: 0.7322
epoch: 59, loss: 494.22, accuracy: 0.7658
epoch: 60, loss: 494.44, accuracy: 0.715
epoch: 61, loss: 492.79, accuracy: 0.7527
epoch: 62, loss: 496.96, accuracy: 0.7803
epoch: 63, loss: 492.29, accuracy: 0.7899
epoch: 64, loss: 492.37, accuracy: 0.7338
epoch: 65, loss: 492.81, accuracy: 0.7682
epoch: 66, loss: 492.01, accuracy: 0.7679
epoch: 67, loss: 492.41, accuracy: 0.788
epoch: 68, loss: 490.31, accuracy: 0.7983
epoch: 69, loss: 492.64, accuracy: 0.7936
epoch: 70, loss: 491.81, accuracy: 0.7545
epoch: 71, loss: 489.58, accuracy: 0.7691
epoch: 72, loss: 490.16, accuracy: 0.7636
epoch: 73, loss: 490.51, accuracy: 0.7295
epoch: 74, loss: 489.99, accuracy: 0.7575
epoch: 75, loss: 461.72, accuracy: 0.8111
epoch: 76, loss: 460.04, accuracy: 0.8106
epoch: 77, loss: 460.13, accuracy: 0.8129
epoch: 78, loss: 458.42, accuracy: 0.8111
epoch: 79, loss: 458.24, accuracy: 0.8134
epoch: 80, loss: 458.51, accuracy: 0.8123
epoch: 81, loss: 457.43, accuracy: 0.8155
epoch: 82, loss: 457.87, accuracy: 0.8115
epoch: 83, loss: 458.02, accuracy: 0.8123
epoch: 84, loss: 457.5, accuracy: 0.8107
epoch: 85, loss: 457.55, accuracy: 0.8089
epoch: 86, loss: 459.01, accuracy: 0.8125
Epoch    87: reducing learning rate of group 0 to 5.0000e-04.
epoch: 87, loss: 457.77, accuracy: 0.8105
epoch: 88, loss: 453.74, accuracy: 0.8149
epoch: 89, loss: 455.27, accuracy: 0.813
epoch: 90, loss: 455.22, accuracy: 0.8145
epoch: 91, loss: 454.38, accuracy: 0.8159
epoch: 92, loss: 455.54, accuracy: 0.8134
epoch: 93, loss: 454.55, accuracy: 0.8122
epoch: 94, loss: 453.98, accuracy: 0.8121
epoch: 95, loss: 455.49, accuracy: 0.8149
epoch: 96, loss: 454.15, accuracy: 0.8147
epoch: 97, loss: 454.89, accuracy: 0.8152
Epoch    98: reducing learning rate of group 0 to 2.5000e-04.
epoch: 98, loss: 454.02, accuracy: 0.8147
epoch: 99, loss: 453.33, accuracy: 0.8152
time analysis:
    all 764.5235 s
    train 763.449 s
Accuracy of     0 : 76 %
Accuracy of     1 : 86 %
Accuracy of     2 : 72 %
Accuracy of     3 : 81 %
Accuracy of     4 : 63 %
Accuracy of     5 : 96 %
Accuracy of     6 : 47 %
Accuracy of     7 : 92 %
Accuracy of     8 : 92 %
Accuracy of     9 : 94 %
```

## FashionMNIST, 3_layers, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(1, 16, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(16, 16, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv3): Conv2d(16, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 16, 28, 28]             160
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
            Conv2d-4           [-1, 16, 14, 14]           2,320
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
            Conv2d-7              [-1, 8, 7, 7]           1,160
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 4,450
DAU params: 0
other params: 4,450
----------------------------------------------------------------
epoch: 0, loss: 422.43, accuracy: 0.8573
epoch: 1, loss: 311.03, accuracy: 0.8817
epoch: 2, loss: 284.74, accuracy: 0.8852
epoch: 3, loss: 272.3, accuracy: 0.8852
epoch: 4, loss: 262.4, accuracy: 0.8932
epoch: 5, loss: 254.85, accuracy: 0.8939
epoch: 6, loss: 247.71, accuracy: 0.8958
epoch: 7, loss: 243.33, accuracy: 0.8956
epoch: 8, loss: 239.76, accuracy: 0.8884
epoch: 9, loss: 233.34, accuracy: 0.898
epoch: 10, loss: 231.85, accuracy: 0.8892
epoch: 11, loss: 227.53, accuracy: 0.8941
epoch: 12, loss: 224.84, accuracy: 0.8906
epoch: 13, loss: 222.18, accuracy: 0.8992
epoch: 14, loss: 220.99, accuracy: 0.8978
epoch: 15, loss: 218.36, accuracy: 0.893
epoch: 16, loss: 215.07, accuracy: 0.8875
epoch: 17, loss: 215.0, accuracy: 0.8958
epoch: 18, loss: 213.74, accuracy: 0.8918
epoch: 19, loss: 211.35, accuracy: 0.9033
epoch: 20, loss: 210.72, accuracy: 0.901
epoch: 21, loss: 208.17, accuracy: 0.9034
epoch: 22, loss: 206.07, accuracy: 0.8918
epoch: 23, loss: 205.07, accuracy: 0.892
epoch: 24, loss: 203.2, accuracy: 0.9026
epoch: 25, loss: 203.26, accuracy: 0.9018
epoch: 26, loss: 202.16, accuracy: 0.9045
epoch: 27, loss: 200.04, accuracy: 0.8993
epoch: 28, loss: 200.79, accuracy: 0.9049
epoch: 29, loss: 199.21, accuracy: 0.8941
epoch: 30, loss: 199.05, accuracy: 0.9021
epoch: 31, loss: 197.53, accuracy: 0.8954
epoch: 32, loss: 198.38, accuracy: 0.9032
epoch: 33, loss: 197.25, accuracy: 0.8973
epoch: 34, loss: 194.16, accuracy: 0.9027
epoch: 35, loss: 194.41, accuracy: 0.9027
epoch: 36, loss: 194.23, accuracy: 0.9017
epoch: 37, loss: 196.06, accuracy: 0.9012
epoch: 38, loss: 192.0, accuracy: 0.8998
epoch: 39, loss: 193.73, accuracy: 0.9026
epoch: 40, loss: 189.57, accuracy: 0.8938
epoch: 41, loss: 191.36, accuracy: 0.9036
epoch: 42, loss: 189.45, accuracy: 0.9003
epoch: 43, loss: 189.75, accuracy: 0.8996
epoch: 44, loss: 189.82, accuracy: 0.8943
epoch: 45, loss: 188.55, accuracy: 0.8968
epoch: 46, loss: 187.42, accuracy: 0.9022
epoch: 47, loss: 189.29, accuracy: 0.8997
epoch: 48, loss: 186.76, accuracy: 0.9016
epoch: 49, loss: 186.3, accuracy: 0.8976
epoch: 50, loss: 187.18, accuracy: 0.8994
epoch: 51, loss: 184.32, accuracy: 0.9033
epoch: 52, loss: 185.61, accuracy: 0.9005
epoch: 53, loss: 185.03, accuracy: 0.8995
epoch: 54, loss: 185.57, accuracy: 0.9015
epoch: 55, loss: 185.52, accuracy: 0.9001
epoch: 56, loss: 184.71, accuracy: 0.8998
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 184.44, accuracy: 0.9016
epoch: 58, loss: 169.21, accuracy: 0.9013
epoch: 59, loss: 166.29, accuracy: 0.9037
epoch: 60, loss: 166.26, accuracy: 0.9036
epoch: 61, loss: 165.41, accuracy: 0.8988
epoch: 62, loss: 164.99, accuracy: 0.905
epoch: 63, loss: 164.36, accuracy: 0.9026
epoch: 64, loss: 165.03, accuracy: 0.9014
epoch: 65, loss: 163.99, accuracy: 0.902
epoch: 66, loss: 163.74, accuracy: 0.9047
epoch: 67, loss: 164.09, accuracy: 0.9013
epoch: 68, loss: 163.36, accuracy: 0.9017
epoch: 69, loss: 162.26, accuracy: 0.9048
epoch: 70, loss: 162.32, accuracy: 0.9012
epoch: 71, loss: 163.22, accuracy: 0.9035
epoch: 72, loss: 162.13, accuracy: 0.9026
epoch: 73, loss: 162.59, accuracy: 0.9021
epoch: 74, loss: 161.74, accuracy: 0.9029
epoch: 75, loss: 148.23, accuracy: 0.903
epoch: 76, loss: 146.39, accuracy: 0.9037
epoch: 77, loss: 145.42, accuracy: 0.9033
epoch: 78, loss: 144.4, accuracy: 0.9038
epoch: 79, loss: 144.68, accuracy: 0.9033
epoch: 80, loss: 145.28, accuracy: 0.9037
epoch: 81, loss: 144.11, accuracy: 0.9035
epoch: 82, loss: 143.75, accuracy: 0.9034
epoch: 83, loss: 144.09, accuracy: 0.9035
epoch: 84, loss: 144.49, accuracy: 0.9017
epoch: 85, loss: 143.75, accuracy: 0.9029
epoch: 86, loss: 143.71, accuracy: 0.9034
epoch: 87, loss: 144.01, accuracy: 0.9047
epoch: 88, loss: 143.56, accuracy: 0.9037
epoch: 89, loss: 143.57, accuracy: 0.9029
epoch: 90, loss: 143.93, accuracy: 0.9036
epoch: 91, loss: 143.97, accuracy: 0.9036
epoch: 92, loss: 143.22, accuracy: 0.9025
epoch: 93, loss: 143.46, accuracy: 0.9033
epoch: 94, loss: 143.27, accuracy: 0.9042
epoch: 95, loss: 143.75, accuracy: 0.9033
epoch: 96, loss: 143.41, accuracy: 0.9029
epoch: 97, loss: 144.21, accuracy: 0.9028
Epoch    98: reducing learning rate of group 0 to 2.5000e-04.
epoch: 98, loss: 143.31, accuracy: 0.9034
epoch: 99, loss: 142.47, accuracy: 0.9016
time analysis:
    all 682.6773 s
    train 681.6145 s
Accuracy of     0 : 84 %
Accuracy of     1 : 100 %
Accuracy of     2 : 78 %
Accuracy of     3 : 86 %
Accuracy of     4 : 83 %
Accuracy of     5 : 98 %
Accuracy of     6 : 69 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=16, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=16, out_channels=16, no_units=2, sigma=0.5)
  (conv3): DAUConv2d(in_channels=16, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 16, 28, 28]             112
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
         DAUConv2d-4           [-1, 16, 14, 14]           1,552
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
         DAUConv2d-7              [-1, 8, 7, 7]             776
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 3,250
DAU params: 2,440
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 505.98, accuracy: 0.8216
epoch: 1, loss: 370.38, accuracy: 0.8472
epoch: 2, loss: 341.02, accuracy: 0.8609
epoch: 3, loss: 329.61, accuracy: 0.8622
epoch: 4, loss: 318.05, accuracy: 0.8713
epoch: 5, loss: 310.36, accuracy: 0.863
epoch: 6, loss: 303.42, accuracy: 0.8686
epoch: 7, loss: 301.59, accuracy: 0.8806
epoch: 8, loss: 294.79, accuracy: 0.8665
epoch: 9, loss: 291.97, accuracy: 0.8641
epoch: 10, loss: 288.67, accuracy: 0.8859
epoch: 11, loss: 284.14, accuracy: 0.8767
epoch: 12, loss: 279.19, accuracy: 0.8801
epoch: 13, loss: 276.04, accuracy: 0.885
epoch: 14, loss: 274.67, accuracy: 0.8773
epoch: 15, loss: 272.23, accuracy: 0.8817
epoch: 16, loss: 269.07, accuracy: 0.8869
epoch: 17, loss: 268.75, accuracy: 0.8864
epoch: 18, loss: 265.48, accuracy: 0.8815
epoch: 19, loss: 264.35, accuracy: 0.8845
epoch: 20, loss: 260.92, accuracy: 0.877
epoch: 21, loss: 260.18, accuracy: 0.8913
epoch: 22, loss: 257.52, accuracy: 0.8843
epoch: 23, loss: 257.71, accuracy: 0.8871
epoch: 24, loss: 256.18, accuracy: 0.8932
epoch: 25, loss: 253.33, accuracy: 0.8911
epoch: 26, loss: 253.87, accuracy: 0.8835
epoch: 27, loss: 253.57, accuracy: 0.8923
epoch: 28, loss: 252.08, accuracy: 0.8949
epoch: 29, loss: 251.53, accuracy: 0.8952
epoch: 30, loss: 250.41, accuracy: 0.8865
epoch: 31, loss: 251.72, accuracy: 0.8926
epoch: 32, loss: 249.08, accuracy: 0.8884
epoch: 33, loss: 248.7, accuracy: 0.8883
epoch: 34, loss: 248.5, accuracy: 0.8957
epoch: 35, loss: 248.73, accuracy: 0.8813
epoch: 36, loss: 247.34, accuracy: 0.8921
epoch: 37, loss: 246.79, accuracy: 0.8915
epoch: 38, loss: 245.13, accuracy: 0.8833
epoch: 39, loss: 246.09, accuracy: 0.8949
epoch: 40, loss: 245.66, accuracy: 0.8895
epoch: 41, loss: 245.04, accuracy: 0.8959
epoch: 42, loss: 244.56, accuracy: 0.8903
epoch: 43, loss: 243.12, accuracy: 0.8975
epoch: 44, loss: 243.52, accuracy: 0.8911
epoch: 45, loss: 242.69, accuracy: 0.8976
epoch: 46, loss: 241.88, accuracy: 0.8955
epoch: 47, loss: 241.63, accuracy: 0.8979
epoch: 48, loss: 242.76, accuracy: 0.8915
epoch: 49, loss: 242.44, accuracy: 0.8956
epoch: 50, loss: 240.47, accuracy: 0.8915
epoch: 51, loss: 240.8, accuracy: 0.8958
epoch: 52, loss: 239.11, accuracy: 0.8935
epoch: 53, loss: 239.63, accuracy: 0.8914
epoch: 54, loss: 241.12, accuracy: 0.8948
epoch: 55, loss: 238.04, accuracy: 0.8946
epoch: 56, loss: 239.69, accuracy: 0.8951
epoch: 57, loss: 239.42, accuracy: 0.8946
epoch: 58, loss: 238.3, accuracy: 0.9007
epoch: 59, loss: 237.86, accuracy: 0.898
epoch: 60, loss: 237.5, accuracy: 0.8942
epoch: 61, loss: 238.69, accuracy: 0.8968
epoch: 62, loss: 238.43, accuracy: 0.8846
epoch: 63, loss: 237.24, accuracy: 0.8973
epoch: 64, loss: 237.07, accuracy: 0.8976
epoch: 65, loss: 234.89, accuracy: 0.8899
epoch: 66, loss: 237.26, accuracy: 0.8964
epoch: 67, loss: 235.86, accuracy: 0.8953
epoch: 68, loss: 234.82, accuracy: 0.8926
epoch: 69, loss: 237.01, accuracy: 0.8989
epoch: 70, loss: 236.16, accuracy: 0.898
Epoch    71: reducing learning rate of group 0 to 5.0000e-03.
epoch: 71, loss: 235.11, accuracy: 0.8949
epoch: 72, loss: 217.52, accuracy: 0.8998
epoch: 73, loss: 216.07, accuracy: 0.9022
epoch: 74, loss: 213.84, accuracy: 0.8961
epoch: 75, loss: 197.99, accuracy: 0.9078
epoch: 76, loss: 195.76, accuracy: 0.9076
epoch: 77, loss: 194.97, accuracy: 0.908
epoch: 78, loss: 193.72, accuracy: 0.9068
epoch: 79, loss: 194.73, accuracy: 0.9075
epoch: 80, loss: 192.57, accuracy: 0.9074
epoch: 81, loss: 193.11, accuracy: 0.9084
epoch: 82, loss: 192.35, accuracy: 0.9084
epoch: 83, loss: 192.44, accuracy: 0.907
epoch: 84, loss: 192.26, accuracy: 0.9086
epoch: 85, loss: 191.6, accuracy: 0.9087
epoch: 86, loss: 192.12, accuracy: 0.9081
epoch: 87, loss: 191.0, accuracy: 0.9082
epoch: 88, loss: 191.56, accuracy: 0.9063
epoch: 89, loss: 191.49, accuracy: 0.9078
epoch: 90, loss: 190.09, accuracy: 0.9072
epoch: 91, loss: 190.88, accuracy: 0.9084
epoch: 92, loss: 190.71, accuracy: 0.9076
epoch: 93, loss: 190.42, accuracy: 0.907
epoch: 94, loss: 189.83, accuracy: 0.9071
epoch: 95, loss: 190.4, accuracy: 0.9088
epoch: 96, loss: 190.5, accuracy: 0.9086
epoch: 97, loss: 189.21, accuracy: 0.9084
epoch: 98, loss: 190.14, accuracy: 0.9074
epoch: 99, loss: 189.66, accuracy: 0.9076
time analysis:
    all 1787.7295 s
    train 1786.7717 s
Accuracy of     0 : 76 %
Accuracy of     1 : 96 %
Accuracy of     2 : 88 %
Accuracy of     3 : 93 %
Accuracy of     4 : 85 %
Accuracy of     5 : 100 %
Accuracy of     6 : 64 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=16, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2d(in_channels=16, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 16, 28, 28]             160
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
         DAUConv2d-4           [-1, 16, 14, 14]           2,320
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
         DAUConv2d-7              [-1, 8, 7, 7]           1,160
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 4,450
DAU params: 3,640
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 477.6, accuracy: 0.8529
epoch: 1, loss: 347.72, accuracy: 0.8303
epoch: 2, loss: 322.33, accuracy: 0.8609
epoch: 3, loss: 305.47, accuracy: 0.8802
epoch: 4, loss: 294.76, accuracy: 0.8602
epoch: 5, loss: 288.77, accuracy: 0.8881
epoch: 6, loss: 282.4, accuracy: 0.8895
epoch: 7, loss: 277.52, accuracy: 0.8844
epoch: 8, loss: 274.36, accuracy: 0.8656
epoch: 9, loss: 270.14, accuracy: 0.8956
epoch: 10, loss: 269.88, accuracy: 0.8823
epoch: 11, loss: 267.23, accuracy: 0.8908
epoch: 12, loss: 263.36, accuracy: 0.8871
epoch: 13, loss: 262.1, accuracy: 0.889
epoch: 14, loss: 261.14, accuracy: 0.8804
epoch: 15, loss: 257.93, accuracy: 0.894
epoch: 16, loss: 256.21, accuracy: 0.8956
epoch: 17, loss: 256.99, accuracy: 0.8977
epoch: 18, loss: 254.22, accuracy: 0.8961
epoch: 19, loss: 254.5, accuracy: 0.8898
epoch: 20, loss: 250.05, accuracy: 0.8953
epoch: 21, loss: 247.96, accuracy: 0.8999
epoch: 22, loss: 249.17, accuracy: 0.8934
epoch: 23, loss: 244.3, accuracy: 0.8921
epoch: 24, loss: 246.66, accuracy: 0.8921
epoch: 25, loss: 245.57, accuracy: 0.893
epoch: 26, loss: 242.17, accuracy: 0.8885
epoch: 27, loss: 241.24, accuracy: 0.8984
epoch: 28, loss: 240.7, accuracy: 0.894
epoch: 29, loss: 238.5, accuracy: 0.8975
epoch: 30, loss: 239.5, accuracy: 0.8967
epoch: 31, loss: 236.72, accuracy: 0.8966
epoch: 32, loss: 237.69, accuracy: 0.8996
epoch: 33, loss: 236.78, accuracy: 0.898
epoch: 34, loss: 235.84, accuracy: 0.8846
epoch: 35, loss: 235.14, accuracy: 0.8932
epoch: 36, loss: 234.32, accuracy: 0.8962
epoch: 37, loss: 234.15, accuracy: 0.8949
epoch: 38, loss: 232.39, accuracy: 0.902
epoch: 39, loss: 232.59, accuracy: 0.8972
epoch: 40, loss: 231.72, accuracy: 0.8933
epoch: 41, loss: 231.99, accuracy: 0.8964
epoch: 42, loss: 232.64, accuracy: 0.892
epoch: 43, loss: 230.68, accuracy: 0.9022
epoch: 44, loss: 232.11, accuracy: 0.8984
epoch: 45, loss: 230.02, accuracy: 0.9007
epoch: 46, loss: 229.99, accuracy: 0.8979
epoch: 47, loss: 229.84, accuracy: 0.8987
epoch: 48, loss: 227.45, accuracy: 0.8982
epoch: 49, loss: 227.22, accuracy: 0.9045
epoch: 50, loss: 228.68, accuracy: 0.8979
epoch: 51, loss: 228.04, accuracy: 0.9007
epoch: 52, loss: 224.87, accuracy: 0.9013
epoch: 53, loss: 226.02, accuracy: 0.8928
epoch: 54, loss: 226.66, accuracy: 0.8999
epoch: 55, loss: 225.71, accuracy: 0.8968
epoch: 56, loss: 227.76, accuracy: 0.9005
epoch: 57, loss: 225.82, accuracy: 0.8996
Epoch    58: reducing learning rate of group 0 to 5.0000e-03.
epoch: 58, loss: 224.76, accuracy: 0.8951
epoch: 59, loss: 206.18, accuracy: 0.9065
epoch: 60, loss: 203.16, accuracy: 0.9064
epoch: 61, loss: 203.75, accuracy: 0.9067
epoch: 62, loss: 202.87, accuracy: 0.9022
epoch: 63, loss: 200.27, accuracy: 0.9085
epoch: 64, loss: 201.53, accuracy: 0.9058
epoch: 65, loss: 199.58, accuracy: 0.9067
epoch: 66, loss: 200.36, accuracy: 0.9055
epoch: 67, loss: 199.78, accuracy: 0.9036
epoch: 68, loss: 199.17, accuracy: 0.9066
epoch: 69, loss: 198.86, accuracy: 0.9064
epoch: 70, loss: 198.19, accuracy: 0.903
epoch: 71, loss: 197.82, accuracy: 0.9077
epoch: 72, loss: 196.9, accuracy: 0.9071
epoch: 73, loss: 197.14, accuracy: 0.8998
epoch: 74, loss: 196.36, accuracy: 0.9094
epoch: 75, loss: 180.05, accuracy: 0.912
epoch: 76, loss: 177.09, accuracy: 0.9099
epoch: 77, loss: 175.67, accuracy: 0.9112
epoch: 78, loss: 174.74, accuracy: 0.9097
epoch: 79, loss: 174.04, accuracy: 0.9106
epoch: 80, loss: 173.68, accuracy: 0.9098
epoch: 81, loss: 173.39, accuracy: 0.9113
epoch: 82, loss: 173.77, accuracy: 0.9107
epoch: 83, loss: 173.16, accuracy: 0.9115
epoch: 84, loss: 172.85, accuracy: 0.9101
epoch: 85, loss: 172.88, accuracy: 0.9116
epoch: 86, loss: 172.35, accuracy: 0.9111
epoch: 87, loss: 172.57, accuracy: 0.909
epoch: 88, loss: 172.13, accuracy: 0.9114
epoch: 89, loss: 172.38, accuracy: 0.9096
epoch: 90, loss: 171.83, accuracy: 0.9114
epoch: 91, loss: 172.21, accuracy: 0.911
epoch: 92, loss: 172.05, accuracy: 0.9109
epoch: 93, loss: 171.3, accuracy: 0.9109
epoch: 94, loss: 171.9, accuracy: 0.9109
epoch: 95, loss: 170.97, accuracy: 0.9101
epoch: 96, loss: 170.95, accuracy: 0.9112
epoch: 97, loss: 170.97, accuracy: 0.9118
epoch: 98, loss: 170.38, accuracy: 0.9108
epoch: 99, loss: 171.21, accuracy: 0.9107
time analysis:
    all 2366.0564 s
    train 2364.7429 s
Accuracy of     0 : 80 %
Accuracy of     1 : 95 %
Accuracy of     2 : 78 %
Accuracy of     3 : 87 %
Accuracy of     4 : 90 %
Accuracy of     5 : 96 %
Accuracy of     6 : 73 %
Accuracy of     7 : 100 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2d, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2d(in_channels=16, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2d(in_channels=16, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 16, 28, 28]             208
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
         DAUConv2d-4           [-1, 16, 14, 14]           3,088
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
         DAUConv2d-7              [-1, 8, 7, 7]           1,544
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 5,650
DAU params: 4,840
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 477.46, accuracy: 0.8214
epoch: 1, loss: 346.94, accuracy: 0.8567
epoch: 2, loss: 319.42, accuracy: 0.8738
epoch: 3, loss: 306.03, accuracy: 0.8682
epoch: 4, loss: 296.4, accuracy: 0.8724
epoch: 5, loss: 289.68, accuracy: 0.8815
epoch: 6, loss: 281.13, accuracy: 0.8662
epoch: 7, loss: 276.73, accuracy: 0.8905
epoch: 8, loss: 273.63, accuracy: 0.8858
epoch: 9, loss: 268.32, accuracy: 0.8731
epoch: 10, loss: 262.79, accuracy: 0.8845
epoch: 11, loss: 260.36, accuracy: 0.884
epoch: 12, loss: 259.01, accuracy: 0.8897
epoch: 13, loss: 257.52, accuracy: 0.8915
epoch: 14, loss: 252.07, accuracy: 0.8815
epoch: 15, loss: 251.09, accuracy: 0.8845
epoch: 16, loss: 248.73, accuracy: 0.8923
epoch: 17, loss: 246.74, accuracy: 0.8936
epoch: 18, loss: 243.1, accuracy: 0.8945
epoch: 19, loss: 243.3, accuracy: 0.889
epoch: 20, loss: 241.97, accuracy: 0.8973
epoch: 21, loss: 240.83, accuracy: 0.8949
epoch: 22, loss: 239.56, accuracy: 0.8936
epoch: 23, loss: 237.53, accuracy: 0.8982
epoch: 24, loss: 236.38, accuracy: 0.9004
epoch: 25, loss: 236.46, accuracy: 0.8927
epoch: 26, loss: 234.29, accuracy: 0.8949
epoch: 27, loss: 235.28, accuracy: 0.8879
epoch: 28, loss: 232.75, accuracy: 0.899
epoch: 29, loss: 231.36, accuracy: 0.869
epoch: 30, loss: 232.52, accuracy: 0.8951
epoch: 31, loss: 229.98, accuracy: 0.8887
epoch: 32, loss: 228.85, accuracy: 0.8984
epoch: 33, loss: 230.29, accuracy: 0.8953
epoch: 34, loss: 227.86, accuracy: 0.8925
epoch: 35, loss: 228.64, accuracy: 0.898
epoch: 36, loss: 227.94, accuracy: 0.8929
epoch: 37, loss: 226.07, accuracy: 0.9014
epoch: 38, loss: 225.55, accuracy: 0.898
epoch: 39, loss: 226.88, accuracy: 0.8953
epoch: 40, loss: 225.66, accuracy: 0.8988
epoch: 41, loss: 225.3, accuracy: 0.8968
epoch: 42, loss: 224.01, accuracy: 0.9006
epoch: 43, loss: 224.39, accuracy: 0.8962
epoch: 44, loss: 223.07, accuracy: 0.8997
epoch: 45, loss: 223.58, accuracy: 0.8962
epoch: 46, loss: 222.05, accuracy: 0.9
epoch: 47, loss: 222.09, accuracy: 0.901
epoch: 48, loss: 223.13, accuracy: 0.8997
epoch: 49, loss: 220.36, accuracy: 0.8993
epoch: 50, loss: 220.37, accuracy: 0.899
epoch: 51, loss: 220.85, accuracy: 0.9039
epoch: 52, loss: 219.39, accuracy: 0.9003
epoch: 53, loss: 219.4, accuracy: 0.9041
epoch: 54, loss: 218.84, accuracy: 0.9003
epoch: 55, loss: 218.18, accuracy: 0.8983
epoch: 56, loss: 218.45, accuracy: 0.8999
epoch: 57, loss: 218.25, accuracy: 0.902
epoch: 58, loss: 216.96, accuracy: 0.8951
epoch: 59, loss: 218.78, accuracy: 0.9055
epoch: 60, loss: 217.77, accuracy: 0.9008
epoch: 61, loss: 217.11, accuracy: 0.8965
epoch: 62, loss: 218.65, accuracy: 0.9008
epoch: 63, loss: 216.94, accuracy: 0.9058
epoch: 64, loss: 215.75, accuracy: 0.8985
epoch: 65, loss: 218.23, accuracy: 0.8934
epoch: 66, loss: 216.43, accuracy: 0.8968
epoch: 67, loss: 216.13, accuracy: 0.9015
epoch: 68, loss: 215.2, accuracy: 0.9048
epoch: 69, loss: 214.25, accuracy: 0.9032
epoch: 70, loss: 214.95, accuracy: 0.9024
epoch: 71, loss: 214.08, accuracy: 0.8983
epoch: 72, loss: 214.75, accuracy: 0.9045
epoch: 73, loss: 215.94, accuracy: 0.9051
epoch: 74, loss: 213.49, accuracy: 0.898
epoch: 75, loss: 186.92, accuracy: 0.9082
epoch: 76, loss: 178.73, accuracy: 0.9103
epoch: 77, loss: 177.3, accuracy: 0.912
epoch: 78, loss: 176.52, accuracy: 0.9118
epoch: 79, loss: 175.07, accuracy: 0.9103
epoch: 80, loss: 174.1, accuracy: 0.9091
epoch: 81, loss: 173.27, accuracy: 0.9108
epoch: 82, loss: 173.12, accuracy: 0.9113
epoch: 83, loss: 171.92, accuracy: 0.9104
epoch: 84, loss: 172.62, accuracy: 0.911
epoch: 85, loss: 171.92, accuracy: 0.9121
epoch: 86, loss: 171.06, accuracy: 0.9101
epoch: 87, loss: 169.87, accuracy: 0.9107
epoch: 88, loss: 170.36, accuracy: 0.9104
epoch: 89, loss: 170.52, accuracy: 0.9109
epoch: 90, loss: 170.05, accuracy: 0.9099
epoch: 91, loss: 169.68, accuracy: 0.9082
epoch: 92, loss: 169.21, accuracy: 0.91
epoch: 93, loss: 169.0, accuracy: 0.9102
epoch: 94, loss: 169.78, accuracy: 0.9098
epoch: 95, loss: 168.29, accuracy: 0.9108
epoch: 96, loss: 168.07, accuracy: 0.9103
epoch: 97, loss: 168.32, accuracy: 0.911
epoch: 98, loss: 167.97, accuracy: 0.9118
epoch: 99, loss: 167.73, accuracy: 0.9108
time analysis:
    all 2955.5695 s
    train 2954.0817 s
Accuracy of     0 : 82 %
Accuracy of     1 : 93 %
Accuracy of     2 : 88 %
Accuracy of     3 : 91 %
Accuracy of     4 : 86 %
Accuracy of     5 : 96 %
Accuracy of     6 : 73 %
Accuracy of     7 : 100 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2d, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=16, no_units=5, sigma=0.5)
  (conv2): DAUConv2d(in_channels=16, out_channels=16, no_units=5, sigma=0.5)
  (conv3): DAUConv2d(in_channels=16, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 16, 28, 28]             256
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
         DAUConv2d-4           [-1, 16, 14, 14]           3,856
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
         DAUConv2d-7              [-1, 8, 7, 7]           1,928
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 6,850
DAU params: 6,040
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 482.7, accuracy: 0.86
epoch: 1, loss: 348.21, accuracy: 0.8653
epoch: 2, loss: 320.73, accuracy: 0.8682
epoch: 3, loss: 310.98, accuracy: 0.8666
epoch: 4, loss: 297.78, accuracy: 0.8815
epoch: 5, loss: 289.64, accuracy: 0.8782
epoch: 6, loss: 281.92, accuracy: 0.8623
epoch: 7, loss: 277.22, accuracy: 0.8874
epoch: 8, loss: 271.09, accuracy: 0.8916
epoch: 9, loss: 269.27, accuracy: 0.8921
epoch: 10, loss: 264.71, accuracy: 0.8812
epoch: 11, loss: 260.85, accuracy: 0.8776
epoch: 12, loss: 257.3, accuracy: 0.8941
epoch: 13, loss: 254.89, accuracy: 0.8727
epoch: 14, loss: 252.36, accuracy: 0.885
epoch: 15, loss: 250.26, accuracy: 0.8971
epoch: 16, loss: 248.33, accuracy: 0.8967
epoch: 17, loss: 248.95, accuracy: 0.8826
epoch: 18, loss: 246.13, accuracy: 0.895
epoch: 19, loss: 246.44, accuracy: 0.8869
epoch: 20, loss: 241.8, accuracy: 0.8984
epoch: 21, loss: 241.23, accuracy: 0.9
epoch: 22, loss: 240.24, accuracy: 0.8744
epoch: 23, loss: 236.65, accuracy: 0.9005
epoch: 24, loss: 238.4, accuracy: 0.9014
epoch: 25, loss: 234.89, accuracy: 0.9033
epoch: 26, loss: 233.7, accuracy: 0.9053
epoch: 27, loss: 233.55, accuracy: 0.8933
epoch: 28, loss: 233.08, accuracy: 0.9031
epoch: 29, loss: 232.85, accuracy: 0.8927
epoch: 30, loss: 228.93, accuracy: 0.9022
epoch: 31, loss: 229.92, accuracy: 0.9008
epoch: 32, loss: 229.69, accuracy: 0.904
epoch: 33, loss: 226.82, accuracy: 0.9008
epoch: 34, loss: 227.53, accuracy: 0.8945
epoch: 35, loss: 227.26, accuracy: 0.8996
epoch: 36, loss: 225.33, accuracy: 0.9003
epoch: 37, loss: 224.76, accuracy: 0.8955
epoch: 38, loss: 223.54, accuracy: 0.8995
epoch: 39, loss: 224.03, accuracy: 0.9004
epoch: 40, loss: 219.85, accuracy: 0.9012
epoch: 41, loss: 219.82, accuracy: 0.9037
epoch: 42, loss: 223.05, accuracy: 0.8999
epoch: 43, loss: 223.13, accuracy: 0.8993
epoch: 44, loss: 218.78, accuracy: 0.901
epoch: 45, loss: 220.67, accuracy: 0.8963
epoch: 46, loss: 218.9, accuracy: 0.9002
epoch: 47, loss: 218.0, accuracy: 0.9004
epoch: 48, loss: 218.3, accuracy: 0.9058
epoch: 49, loss: 219.07, accuracy: 0.8971
epoch: 50, loss: 218.91, accuracy: 0.9004
epoch: 51, loss: 217.45, accuracy: 0.9007
epoch: 52, loss: 219.03, accuracy: 0.905
epoch: 53, loss: 216.97, accuracy: 0.9019
epoch: 54, loss: 217.17, accuracy: 0.8977
epoch: 55, loss: 214.14, accuracy: 0.8931
epoch: 56, loss: 215.03, accuracy: 0.8992
epoch: 57, loss: 215.49, accuracy: 0.9025
epoch: 58, loss: 212.9, accuracy: 0.8992
epoch: 59, loss: 215.89, accuracy: 0.9013
epoch: 60, loss: 212.95, accuracy: 0.9047
epoch: 61, loss: 213.01, accuracy: 0.9051
epoch: 62, loss: 211.06, accuracy: 0.901
epoch: 63, loss: 213.37, accuracy: 0.9063
epoch: 64, loss: 211.31, accuracy: 0.9052
epoch: 65, loss: 212.38, accuracy: 0.9019
epoch: 66, loss: 210.5, accuracy: 0.9042
epoch: 67, loss: 212.52, accuracy: 0.9055
epoch: 68, loss: 210.49, accuracy: 0.9022
epoch: 69, loss: 210.77, accuracy: 0.9044
epoch: 70, loss: 212.24, accuracy: 0.9065
epoch: 71, loss: 210.58, accuracy: 0.9037
epoch: 72, loss: 209.08, accuracy: 0.9065
epoch: 73, loss: 209.26, accuracy: 0.8997
epoch: 74, loss: 210.04, accuracy: 0.9052
epoch: 75, loss: 179.51, accuracy: 0.9148
epoch: 76, loss: 172.85, accuracy: 0.9126
epoch: 77, loss: 170.34, accuracy: 0.9167
epoch: 78, loss: 168.41, accuracy: 0.9152
epoch: 79, loss: 167.84, accuracy: 0.9149
epoch: 80, loss: 166.87, accuracy: 0.9152
epoch: 81, loss: 166.45, accuracy: 0.9163
epoch: 82, loss: 165.72, accuracy: 0.9155
epoch: 83, loss: 164.87, accuracy: 0.9121
epoch: 84, loss: 164.32, accuracy: 0.9153
epoch: 85, loss: 163.86, accuracy: 0.9156
epoch: 86, loss: 163.14, accuracy: 0.9157
epoch: 87, loss: 162.36, accuracy: 0.9142
epoch: 88, loss: 163.21, accuracy: 0.9153
epoch: 89, loss: 161.89, accuracy: 0.9156
epoch: 90, loss: 161.9, accuracy: 0.9167
epoch: 91, loss: 161.92, accuracy: 0.9164
epoch: 92, loss: 161.47, accuracy: 0.9135
epoch: 93, loss: 160.91, accuracy: 0.9145
epoch: 94, loss: 161.27, accuracy: 0.9171
epoch: 95, loss: 160.32, accuracy: 0.914
epoch: 96, loss: 160.78, accuracy: 0.9139
epoch: 97, loss: 160.57, accuracy: 0.9161
epoch: 98, loss: 159.94, accuracy: 0.9155
epoch: 99, loss: 159.37, accuracy: 0.9144
time analysis:
    all 3557.04 s
    train 3555.4137 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 85 %
Accuracy of     3 : 86 %
Accuracy of     4 : 81 %
Accuracy of     5 : 96 %
Accuracy of     6 : 74 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2d, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=1, out_channels=16, no_units=6, sigma=0.5)
  (conv2): DAUConv2d(in_channels=16, out_channels=16, no_units=6, sigma=0.5)
  (conv3): DAUConv2d(in_channels=16, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 16, 28, 28]             304
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
         DAUConv2d-4           [-1, 16, 14, 14]           4,624
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
         DAUConv2d-7              [-1, 8, 7, 7]           2,312
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 8,050
DAU params: 7,240
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 486.44, accuracy: 0.836
epoch: 1, loss: 353.79, accuracy: 0.8596
epoch: 2, loss: 328.71, accuracy: 0.8427
epoch: 3, loss: 317.19, accuracy: 0.8735
epoch: 4, loss: 304.5, accuracy: 0.871
epoch: 5, loss: 296.0, accuracy: 0.8584
epoch: 6, loss: 292.36, accuracy: 0.8774
epoch: 7, loss: 288.45, accuracy: 0.8872
epoch: 8, loss: 283.2, accuracy: 0.8881
epoch: 9, loss: 278.65, accuracy: 0.8798
epoch: 10, loss: 275.07, accuracy: 0.8843
epoch: 11, loss: 273.09, accuracy: 0.8877
epoch: 12, loss: 268.86, accuracy: 0.8868
epoch: 13, loss: 266.51, accuracy: 0.8859
epoch: 14, loss: 266.05, accuracy: 0.889
epoch: 15, loss: 262.47, accuracy: 0.8954
epoch: 16, loss: 261.92, accuracy: 0.8902
epoch: 17, loss: 258.76, accuracy: 0.8858
epoch: 18, loss: 259.91, accuracy: 0.8865
epoch: 19, loss: 255.0, accuracy: 0.8782
epoch: 20, loss: 255.44, accuracy: 0.8847
epoch: 21, loss: 251.79, accuracy: 0.8889
epoch: 22, loss: 253.39, accuracy: 0.8858
epoch: 23, loss: 252.13, accuracy: 0.8856
epoch: 24, loss: 251.03, accuracy: 0.9002
epoch: 25, loss: 250.33, accuracy: 0.8962
epoch: 26, loss: 248.44, accuracy: 0.8875
epoch: 27, loss: 247.74, accuracy: 0.8928
epoch: 28, loss: 246.85, accuracy: 0.8942
epoch: 29, loss: 246.94, accuracy: 0.8967
epoch: 30, loss: 242.78, accuracy: 0.8934
epoch: 31, loss: 244.5, accuracy: 0.8981
epoch: 32, loss: 245.92, accuracy: 0.897
epoch: 33, loss: 242.92, accuracy: 0.8946
epoch: 34, loss: 243.29, accuracy: 0.8984
epoch: 35, loss: 243.53, accuracy: 0.8898
epoch: 36, loss: 241.22, accuracy: 0.8935
epoch: 37, loss: 242.4, accuracy: 0.889
epoch: 38, loss: 239.81, accuracy: 0.8988
epoch: 39, loss: 239.87, accuracy: 0.8962
epoch: 40, loss: 238.7, accuracy: 0.8908
epoch: 41, loss: 238.24, accuracy: 0.8922
epoch: 42, loss: 237.95, accuracy: 0.8894
epoch: 43, loss: 237.95, accuracy: 0.8892
epoch: 44, loss: 238.21, accuracy: 0.893
epoch: 45, loss: 237.83, accuracy: 0.8957
epoch: 46, loss: 234.79, accuracy: 0.8808
epoch: 47, loss: 237.07, accuracy: 0.8983
epoch: 48, loss: 236.22, accuracy: 0.8917
epoch: 49, loss: 234.58, accuracy: 0.8891
epoch: 50, loss: 235.24, accuracy: 0.8971
epoch: 51, loss: 233.95, accuracy: 0.9008
epoch: 52, loss: 233.51, accuracy: 0.8925
epoch: 53, loss: 232.63, accuracy: 0.8944
epoch: 54, loss: 234.98, accuracy: 0.8963
epoch: 55, loss: 232.43, accuracy: 0.9017
epoch: 56, loss: 231.98, accuracy: 0.897
epoch: 57, loss: 232.96, accuracy: 0.8965
epoch: 58, loss: 231.12, accuracy: 0.8948
epoch: 59, loss: 232.17, accuracy: 0.8947
epoch: 60, loss: 231.14, accuracy: 0.8989
epoch: 61, loss: 228.87, accuracy: 0.887
epoch: 62, loss: 231.62, accuracy: 0.8975
epoch: 63, loss: 230.39, accuracy: 0.8994
epoch: 64, loss: 230.63, accuracy: 0.8939
epoch: 65, loss: 228.81, accuracy: 0.8935
epoch: 66, loss: 229.82, accuracy: 0.8875
Epoch    67: reducing learning rate of group 0 to 5.0000e-03.
epoch: 67, loss: 229.82, accuracy: 0.8994
epoch: 68, loss: 209.67, accuracy: 0.9051
epoch: 69, loss: 208.07, accuracy: 0.9032
epoch: 70, loss: 206.09, accuracy: 0.9007
epoch: 71, loss: 205.86, accuracy: 0.9057
epoch: 72, loss: 204.3, accuracy: 0.9025
epoch: 73, loss: 204.8, accuracy: 0.9041
epoch: 74, loss: 204.63, accuracy: 0.9045
epoch: 75, loss: 187.72, accuracy: 0.9081
epoch: 76, loss: 184.15, accuracy: 0.9086
epoch: 77, loss: 182.61, accuracy: 0.9081
epoch: 78, loss: 182.99, accuracy: 0.9075
epoch: 79, loss: 181.63, accuracy: 0.9076
epoch: 80, loss: 181.2, accuracy: 0.907
epoch: 81, loss: 181.09, accuracy: 0.9081
epoch: 82, loss: 181.19, accuracy: 0.9082
epoch: 83, loss: 181.28, accuracy: 0.907
epoch: 84, loss: 180.33, accuracy: 0.9073
epoch: 85, loss: 180.9, accuracy: 0.9066
epoch: 86, loss: 180.08, accuracy: 0.9082
epoch: 87, loss: 179.53, accuracy: 0.9085
epoch: 88, loss: 178.86, accuracy: 0.9082
epoch: 89, loss: 179.07, accuracy: 0.9075
epoch: 90, loss: 179.62, accuracy: 0.9077
epoch: 91, loss: 179.14, accuracy: 0.9076
epoch: 92, loss: 178.95, accuracy: 0.9067
epoch: 93, loss: 178.76, accuracy: 0.9073
Epoch    94: reducing learning rate of group 0 to 2.5000e-04.
epoch: 94, loss: 179.07, accuracy: 0.9066
epoch: 95, loss: 176.94, accuracy: 0.9083
epoch: 96, loss: 177.03, accuracy: 0.908
epoch: 97, loss: 176.95, accuracy: 0.9071
epoch: 98, loss: 176.84, accuracy: 0.9071
epoch: 99, loss: 177.44, accuracy: 0.9084
time analysis:
    all 4153.8374 s
    train 4152.0384 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 80 %
Accuracy of     3 : 88 %
Accuracy of     4 : 86 %
Accuracy of     5 : 98 %
Accuracy of     6 : 74 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=16, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=16, no_units=2, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 28, 28]              52
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2di-4           [-1, 16, 14, 14]             592
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2di-7              [-1, 8, 7, 7]             328
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,782
DAU params: 972
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 516.78, accuracy: 0.8222
epoch: 1, loss: 390.62, accuracy: 0.8254
epoch: 2, loss: 358.71, accuracy: 0.8545
epoch: 3, loss: 345.33, accuracy: 0.8353
epoch: 4, loss: 334.66, accuracy: 0.8651
epoch: 5, loss: 326.64, accuracy: 0.8752
epoch: 6, loss: 319.33, accuracy: 0.8587
epoch: 7, loss: 317.1, accuracy: 0.874
epoch: 8, loss: 309.45, accuracy: 0.8794
epoch: 9, loss: 306.73, accuracy: 0.8794
epoch: 10, loss: 303.01, accuracy: 0.8699
epoch: 11, loss: 301.76, accuracy: 0.8727
epoch: 12, loss: 296.97, accuracy: 0.8728
epoch: 13, loss: 294.1, accuracy: 0.8639
epoch: 14, loss: 293.15, accuracy: 0.8697
epoch: 15, loss: 291.85, accuracy: 0.8672
epoch: 16, loss: 292.42, accuracy: 0.8845
epoch: 17, loss: 290.39, accuracy: 0.876
epoch: 18, loss: 290.33, accuracy: 0.8682
epoch: 19, loss: 287.29, accuracy: 0.881
epoch: 20, loss: 283.75, accuracy: 0.8696
epoch: 21, loss: 282.63, accuracy: 0.8776
epoch: 22, loss: 282.44, accuracy: 0.8819
epoch: 23, loss: 281.71, accuracy: 0.882
epoch: 24, loss: 278.82, accuracy: 0.8837
epoch: 25, loss: 280.97, accuracy: 0.8769
epoch: 26, loss: 279.2, accuracy: 0.8843
epoch: 27, loss: 278.72, accuracy: 0.8789
epoch: 28, loss: 276.24, accuracy: 0.8856
epoch: 29, loss: 275.47, accuracy: 0.875
epoch: 30, loss: 276.74, accuracy: 0.8827
epoch: 31, loss: 274.44, accuracy: 0.8852
epoch: 32, loss: 274.8, accuracy: 0.8882
epoch: 33, loss: 273.74, accuracy: 0.8879
epoch: 34, loss: 272.14, accuracy: 0.8877
epoch: 35, loss: 272.18, accuracy: 0.888
epoch: 36, loss: 270.04, accuracy: 0.8843
epoch: 37, loss: 270.3, accuracy: 0.886
epoch: 38, loss: 272.0, accuracy: 0.8859
epoch: 39, loss: 269.74, accuracy: 0.8746
epoch: 40, loss: 270.9, accuracy: 0.8867
epoch: 41, loss: 269.27, accuracy: 0.8831
epoch: 42, loss: 269.07, accuracy: 0.8895
epoch: 43, loss: 268.08, accuracy: 0.886
epoch: 44, loss: 268.4, accuracy: 0.8813
epoch: 45, loss: 268.77, accuracy: 0.8838
epoch: 46, loss: 267.0, accuracy: 0.8885
epoch: 47, loss: 268.9, accuracy: 0.8816
epoch: 48, loss: 264.65, accuracy: 0.8858
epoch: 49, loss: 267.24, accuracy: 0.88
epoch: 50, loss: 266.09, accuracy: 0.8766
epoch: 51, loss: 264.12, accuracy: 0.8875
epoch: 52, loss: 265.67, accuracy: 0.8915
epoch: 53, loss: 265.62, accuracy: 0.8935
epoch: 54, loss: 264.0, accuracy: 0.8884
epoch: 55, loss: 264.76, accuracy: 0.8847
epoch: 56, loss: 264.28, accuracy: 0.8844
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 264.85, accuracy: 0.8857
epoch: 58, loss: 249.76, accuracy: 0.8895
epoch: 59, loss: 245.16, accuracy: 0.8952
epoch: 60, loss: 247.37, accuracy: 0.8893
epoch: 61, loss: 246.49, accuracy: 0.8941
epoch: 62, loss: 245.16, accuracy: 0.8876
epoch: 63, loss: 245.04, accuracy: 0.8882
epoch: 64, loss: 244.27, accuracy: 0.8924
epoch: 65, loss: 244.88, accuracy: 0.8946
epoch: 66, loss: 244.99, accuracy: 0.8949
epoch: 67, loss: 243.06, accuracy: 0.8896
epoch: 68, loss: 244.5, accuracy: 0.8964
epoch: 69, loss: 244.62, accuracy: 0.8983
epoch: 70, loss: 243.68, accuracy: 0.893
epoch: 71, loss: 243.87, accuracy: 0.897
epoch: 72, loss: 243.44, accuracy: 0.8979
Epoch    73: reducing learning rate of group 0 to 2.5000e-03.
epoch: 73, loss: 244.73, accuracy: 0.8897
epoch: 74, loss: 234.57, accuracy: 0.8967
epoch: 75, loss: 226.58, accuracy: 0.8984
epoch: 76, loss: 225.78, accuracy: 0.899
epoch: 77, loss: 225.12, accuracy: 0.9004
epoch: 78, loss: 224.98, accuracy: 0.9002
epoch: 79, loss: 224.85, accuracy: 0.899
epoch: 80, loss: 224.74, accuracy: 0.8999
epoch: 81, loss: 224.72, accuracy: 0.899
epoch: 82, loss: 224.18, accuracy: 0.9005
epoch: 83, loss: 223.74, accuracy: 0.8989
epoch: 84, loss: 224.06, accuracy: 0.8989
epoch: 85, loss: 224.37, accuracy: 0.9005
epoch: 86, loss: 224.23, accuracy: 0.8989
epoch: 87, loss: 224.22, accuracy: 0.8983
epoch: 88, loss: 224.94, accuracy: 0.8983
Epoch    89: reducing learning rate of group 0 to 1.2500e-04.
epoch: 89, loss: 223.62, accuracy: 0.8985
epoch: 90, loss: 223.66, accuracy: 0.8994
epoch: 91, loss: 223.48, accuracy: 0.8994
epoch: 92, loss: 223.17, accuracy: 0.8984
epoch: 93, loss: 223.32, accuracy: 0.8989
epoch: 94, loss: 223.12, accuracy: 0.8991
epoch: 95, loss: 223.8, accuracy: 0.8992
epoch: 96, loss: 223.08, accuracy: 0.9007
epoch: 97, loss: 223.22, accuracy: 0.9
epoch: 98, loss: 222.57, accuracy: 0.8988
epoch: 99, loss: 223.48, accuracy: 0.8991
time analysis:
    all 1107.093 s
    train 1105.1077 s
Accuracy of     0 : 88 %
Accuracy of     1 : 93 %
Accuracy of     2 : 85 %
Accuracy of     3 : 87 %
Accuracy of     4 : 81 %
Accuracy of     5 : 100 %
Accuracy of     6 : 63 %
Accuracy of     7 : 98 %
Accuracy of     8 : 95 %
Accuracy of     9 : 96 %
```

## FashionMNIST, 3_layers, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 28, 28]              70
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2di-4           [-1, 16, 14, 14]             880
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2di-7              [-1, 8, 7, 7]             488
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,248
DAU params: 1,438
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 507.41, accuracy: 0.8355
epoch: 1, loss: 376.25, accuracy: 0.8523
epoch: 2, loss: 349.26, accuracy: 0.8643
epoch: 3, loss: 329.99, accuracy: 0.863
epoch: 4, loss: 318.82, accuracy: 0.8749
epoch: 5, loss: 312.17, accuracy: 0.8762
epoch: 6, loss: 305.45, accuracy: 0.8654
epoch: 7, loss: 299.46, accuracy: 0.8751
epoch: 8, loss: 293.0, accuracy: 0.8685
epoch: 9, loss: 290.45, accuracy: 0.8592
epoch: 10, loss: 288.96, accuracy: 0.8722
epoch: 11, loss: 282.34, accuracy: 0.871
epoch: 12, loss: 280.0, accuracy: 0.8854
epoch: 13, loss: 279.25, accuracy: 0.8404
epoch: 14, loss: 276.39, accuracy: 0.8889
epoch: 15, loss: 275.22, accuracy: 0.8775
epoch: 16, loss: 273.77, accuracy: 0.875
epoch: 17, loss: 273.46, accuracy: 0.8814
epoch: 18, loss: 271.81, accuracy: 0.8889
epoch: 19, loss: 268.65, accuracy: 0.8782
epoch: 20, loss: 269.37, accuracy: 0.8874
epoch: 21, loss: 267.84, accuracy: 0.8837
epoch: 22, loss: 266.43, accuracy: 0.8854
epoch: 23, loss: 265.55, accuracy: 0.8833
epoch: 24, loss: 266.59, accuracy: 0.8853
epoch: 25, loss: 264.54, accuracy: 0.8887
epoch: 26, loss: 264.22, accuracy: 0.8905
epoch: 27, loss: 263.76, accuracy: 0.8851
epoch: 28, loss: 263.79, accuracy: 0.8803
epoch: 29, loss: 260.51, accuracy: 0.8874
epoch: 30, loss: 261.69, accuracy: 0.8877
epoch: 31, loss: 261.92, accuracy: 0.8877
epoch: 32, loss: 259.55, accuracy: 0.8909
epoch: 33, loss: 259.72, accuracy: 0.8903
epoch: 34, loss: 260.12, accuracy: 0.8859
epoch: 35, loss: 258.71, accuracy: 0.8911
epoch: 36, loss: 256.95, accuracy: 0.8766
epoch: 37, loss: 259.4, accuracy: 0.8871
epoch: 38, loss: 257.31, accuracy: 0.8818
epoch: 39, loss: 258.49, accuracy: 0.8857
epoch: 40, loss: 257.75, accuracy: 0.8864
epoch: 41, loss: 257.62, accuracy: 0.8888
epoch: 42, loss: 256.37, accuracy: 0.8904
epoch: 43, loss: 255.53, accuracy: 0.8921
epoch: 44, loss: 256.14, accuracy: 0.8902
epoch: 45, loss: 255.72, accuracy: 0.8928
epoch: 46, loss: 255.34, accuracy: 0.8914
epoch: 47, loss: 252.56, accuracy: 0.8932
epoch: 48, loss: 254.21, accuracy: 0.8884
epoch: 49, loss: 254.54, accuracy: 0.8916
epoch: 50, loss: 253.78, accuracy: 0.8849
epoch: 51, loss: 254.51, accuracy: 0.8878
epoch: 52, loss: 253.34, accuracy: 0.8859
Epoch    53: reducing learning rate of group 0 to 5.0000e-03.
epoch: 53, loss: 253.01, accuracy: 0.8925
epoch: 54, loss: 238.96, accuracy: 0.8971
epoch: 55, loss: 237.4, accuracy: 0.8917
epoch: 56, loss: 236.77, accuracy: 0.8957
epoch: 57, loss: 235.7, accuracy: 0.8961
epoch: 58, loss: 235.04, accuracy: 0.8932
epoch: 59, loss: 234.18, accuracy: 0.8962
epoch: 60, loss: 235.45, accuracy: 0.8932
epoch: 61, loss: 234.54, accuracy: 0.8969
epoch: 62, loss: 233.52, accuracy: 0.8926
epoch: 63, loss: 234.48, accuracy: 0.8886
epoch: 64, loss: 233.26, accuracy: 0.8982
epoch: 65, loss: 233.66, accuracy: 0.8992
epoch: 66, loss: 233.54, accuracy: 0.8964
epoch: 67, loss: 233.83, accuracy: 0.898
epoch: 68, loss: 232.19, accuracy: 0.8948
epoch: 69, loss: 232.82, accuracy: 0.8969
epoch: 70, loss: 232.5, accuracy: 0.8958
epoch: 71, loss: 232.79, accuracy: 0.8993
epoch: 72, loss: 233.3, accuracy: 0.8936
epoch: 73, loss: 232.7, accuracy: 0.8968
Epoch    74: reducing learning rate of group 0 to 2.5000e-03.
epoch: 74, loss: 232.67, accuracy: 0.899
epoch: 75, loss: 219.66, accuracy: 0.8995
epoch: 76, loss: 215.94, accuracy: 0.8992
epoch: 77, loss: 216.18, accuracy: 0.8996
epoch: 78, loss: 215.29, accuracy: 0.9001
epoch: 79, loss: 215.47, accuracy: 0.901
epoch: 80, loss: 214.9, accuracy: 0.9005
epoch: 81, loss: 214.71, accuracy: 0.9004
epoch: 82, loss: 215.11, accuracy: 0.8997
epoch: 83, loss: 215.23, accuracy: 0.8996
epoch: 84, loss: 214.65, accuracy: 0.8996
epoch: 85, loss: 214.12, accuracy: 0.8998
epoch: 86, loss: 213.97, accuracy: 0.8991
epoch: 87, loss: 214.53, accuracy: 0.9007
epoch: 88, loss: 214.18, accuracy: 0.9
epoch: 89, loss: 214.14, accuracy: 0.8998
epoch: 90, loss: 213.86, accuracy: 0.8993
epoch: 91, loss: 213.71, accuracy: 0.8987
epoch: 92, loss: 215.3, accuracy: 0.8982
epoch: 93, loss: 214.23, accuracy: 0.899
epoch: 94, loss: 213.81, accuracy: 0.8994
epoch: 95, loss: 214.4, accuracy: 0.8978
Epoch    96: reducing learning rate of group 0 to 1.2500e-04.
epoch: 96, loss: 214.41, accuracy: 0.8999
epoch: 97, loss: 213.49, accuracy: 0.8985
epoch: 98, loss: 213.12, accuracy: 0.9006
epoch: 99, loss: 212.99, accuracy: 0.8994
time analysis:
    all 1192.0691 s
    train 1190.7965 s
Accuracy of     0 : 82 %
Accuracy of     1 : 93 %
Accuracy of     2 : 84 %
Accuracy of     3 : 87 %
Accuracy of     4 : 85 %
Accuracy of     5 : 100 %
Accuracy of     6 : 69 %
Accuracy of     7 : 100 %
Accuracy of     8 : 96 %
Accuracy of     9 : 96 %
```

## FashionMNIST, 3_layers, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=1, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=16, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2di(in_channels=16, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 16, 28, 28]              88
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2di-4           [-1, 16, 14, 14]           1,168
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2di-7              [-1, 8, 7, 7]             648
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,714
DAU params: 1,904
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 524.48, accuracy: 0.8224
epoch: 1, loss: 377.3, accuracy: 0.855
epoch: 2, loss: 354.37, accuracy: 0.8543
epoch: 3, loss: 336.34, accuracy: 0.8647
epoch: 4, loss: 325.43, accuracy: 0.8572
epoch: 5, loss: 315.41, accuracy: 0.8675
epoch: 6, loss: 306.76, accuracy: 0.8682
epoch: 7, loss: 297.99, accuracy: 0.8792
epoch: 8, loss: 292.43, accuracy: 0.8791
epoch: 9, loss: 291.46, accuracy: 0.8816
epoch: 10, loss: 285.52, accuracy: 0.8761
epoch: 11, loss: 280.46, accuracy: 0.8893
epoch: 12, loss: 278.45, accuracy: 0.8804
epoch: 13, loss: 272.99, accuracy: 0.8827
epoch: 14, loss: 270.94, accuracy: 0.8889
epoch: 15, loss: 266.07, accuracy: 0.8873
epoch: 16, loss: 266.08, accuracy: 0.8844
epoch: 17, loss: 264.23, accuracy: 0.8856
epoch: 18, loss: 260.74, accuracy: 0.8922
epoch: 19, loss: 261.06, accuracy: 0.8931
epoch: 20, loss: 257.78, accuracy: 0.8945
epoch: 21, loss: 259.18, accuracy: 0.8886
epoch: 22, loss: 255.62, accuracy: 0.8839
epoch: 23, loss: 255.78, accuracy: 0.8948
epoch: 24, loss: 254.15, accuracy: 0.8891
epoch: 25, loss: 251.94, accuracy: 0.8943
epoch: 26, loss: 252.0, accuracy: 0.8967
epoch: 27, loss: 250.39, accuracy: 0.8904
epoch: 28, loss: 251.2, accuracy: 0.8916
epoch: 29, loss: 250.84, accuracy: 0.8914
epoch: 30, loss: 250.48, accuracy: 0.8725
epoch: 31, loss: 249.57, accuracy: 0.8938
epoch: 32, loss: 247.77, accuracy: 0.8928
epoch: 33, loss: 246.51, accuracy: 0.8904
epoch: 34, loss: 245.91, accuracy: 0.8886
epoch: 35, loss: 246.4, accuracy: 0.8905
epoch: 36, loss: 246.04, accuracy: 0.8947
epoch: 37, loss: 243.91, accuracy: 0.8947
epoch: 38, loss: 244.69, accuracy: 0.8881
epoch: 39, loss: 241.78, accuracy: 0.8974
epoch: 40, loss: 242.34, accuracy: 0.8925
epoch: 41, loss: 242.49, accuracy: 0.8918
epoch: 42, loss: 240.57, accuracy: 0.8941
epoch: 43, loss: 240.67, accuracy: 0.8933
epoch: 44, loss: 242.62, accuracy: 0.8917
epoch: 45, loss: 240.69, accuracy: 0.8969
epoch: 46, loss: 241.75, accuracy: 0.8942
epoch: 47, loss: 238.3, accuracy: 0.8921
epoch: 48, loss: 238.18, accuracy: 0.8923
epoch: 49, loss: 240.31, accuracy: 0.894
epoch: 50, loss: 236.31, accuracy: 0.8879
epoch: 51, loss: 236.09, accuracy: 0.897
epoch: 52, loss: 237.64, accuracy: 0.896
epoch: 53, loss: 236.73, accuracy: 0.8908
epoch: 54, loss: 236.88, accuracy: 0.8864
epoch: 55, loss: 236.74, accuracy: 0.8975
Epoch    56: reducing learning rate of group 0 to 5.0000e-03.
epoch: 56, loss: 236.66, accuracy: 0.8997
epoch: 57, loss: 220.44, accuracy: 0.9007
epoch: 58, loss: 217.75, accuracy: 0.8966
epoch: 59, loss: 217.36, accuracy: 0.8986
epoch: 60, loss: 216.71, accuracy: 0.9033
epoch: 61, loss: 216.09, accuracy: 0.9023
epoch: 62, loss: 216.25, accuracy: 0.8938
epoch: 63, loss: 216.62, accuracy: 0.9027
epoch: 64, loss: 215.54, accuracy: 0.9009
epoch: 65, loss: 214.25, accuracy: 0.8995
epoch: 66, loss: 215.25, accuracy: 0.8998
epoch: 67, loss: 215.06, accuracy: 0.8975
epoch: 68, loss: 215.26, accuracy: 0.9009
epoch: 69, loss: 213.72, accuracy: 0.901
epoch: 70, loss: 214.36, accuracy: 0.9036
epoch: 71, loss: 213.57, accuracy: 0.8982
epoch: 72, loss: 213.37, accuracy: 0.9017
epoch: 73, loss: 213.17, accuracy: 0.8997
epoch: 74, loss: 213.5, accuracy: 0.8964
epoch: 75, loss: 199.26, accuracy: 0.905
epoch: 76, loss: 196.9, accuracy: 0.9041
epoch: 77, loss: 196.01, accuracy: 0.9039
epoch: 78, loss: 195.78, accuracy: 0.9055
epoch: 79, loss: 194.94, accuracy: 0.9063
epoch: 80, loss: 195.37, accuracy: 0.9046
epoch: 81, loss: 195.07, accuracy: 0.9042
epoch: 82, loss: 194.64, accuracy: 0.9034
epoch: 83, loss: 194.37, accuracy: 0.903
epoch: 84, loss: 194.6, accuracy: 0.9036
epoch: 85, loss: 193.84, accuracy: 0.9041
epoch: 86, loss: 194.81, accuracy: 0.9048
epoch: 87, loss: 194.3, accuracy: 0.9049
epoch: 88, loss: 193.92, accuracy: 0.9039
epoch: 89, loss: 194.84, accuracy: 0.9049
epoch: 90, loss: 193.73, accuracy: 0.906
Epoch    91: reducing learning rate of group 0 to 2.5000e-04.
epoch: 91, loss: 193.68, accuracy: 0.9046
epoch: 92, loss: 192.49, accuracy: 0.9049
epoch: 93, loss: 192.34, accuracy: 0.9037
epoch: 94, loss: 192.45, accuracy: 0.9038
epoch: 95, loss: 192.43, accuracy: 0.9049
epoch: 96, loss: 192.95, accuracy: 0.9042
epoch: 97, loss: 192.64, accuracy: 0.905
epoch: 98, loss: 191.75, accuracy: 0.9037
epoch: 99, loss: 192.87, accuracy: 0.9031
time analysis:
    all 1273.5289 s
    train 1272.1919 s
Accuracy of     0 : 88 %
Accuracy of     1 : 96 %
Accuracy of     2 : 82 %
Accuracy of     3 : 90 %
Accuracy of     4 : 86 %
Accuracy of     5 : 96 %
Accuracy of     6 : 67 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 96 %
```

## FashionMNIST, 3_layers, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=16, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=16, out_channels=16, no_units=2, sigma=0.5)
  (conv3): DAUConv2dj(in_channels=16, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 16, 28, 28]             112
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2dj-4           [-1, 16, 14, 14]             592
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2dj-7              [-1, 8, 7, 7]             296
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,810
DAU params: 1,000
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 546.01, accuracy: 0.811
epoch: 1, loss: 400.87, accuracy: 0.8361
epoch: 2, loss: 360.7, accuracy: 0.8604
epoch: 3, loss: 338.62, accuracy: 0.8606
epoch: 4, loss: 329.02, accuracy: 0.851
epoch: 5, loss: 323.35, accuracy: 0.8518
epoch: 6, loss: 314.2, accuracy: 0.8708
epoch: 7, loss: 309.12, accuracy: 0.8773
epoch: 8, loss: 306.24, accuracy: 0.8592
epoch: 9, loss: 300.74, accuracy: 0.8791
epoch: 10, loss: 297.33, accuracy: 0.8825
epoch: 11, loss: 294.71, accuracy: 0.8772
epoch: 12, loss: 293.81, accuracy: 0.8664
epoch: 13, loss: 290.77, accuracy: 0.872
epoch: 14, loss: 288.21, accuracy: 0.8802
epoch: 15, loss: 286.88, accuracy: 0.8782
epoch: 16, loss: 285.69, accuracy: 0.8779
epoch: 17, loss: 282.17, accuracy: 0.8856
epoch: 18, loss: 281.91, accuracy: 0.877
epoch: 19, loss: 279.83, accuracy: 0.8658
epoch: 20, loss: 278.9, accuracy: 0.8867
epoch: 21, loss: 275.76, accuracy: 0.8827
epoch: 22, loss: 273.17, accuracy: 0.8901
epoch: 23, loss: 273.4, accuracy: 0.8859
epoch: 24, loss: 271.5, accuracy: 0.8837
epoch: 25, loss: 273.2, accuracy: 0.885
epoch: 26, loss: 269.55, accuracy: 0.8802
epoch: 27, loss: 270.18, accuracy: 0.8887
epoch: 28, loss: 269.9, accuracy: 0.882
epoch: 29, loss: 265.06, accuracy: 0.8933
epoch: 30, loss: 268.34, accuracy: 0.8924
epoch: 31, loss: 265.81, accuracy: 0.8831
epoch: 32, loss: 264.75, accuracy: 0.8869
epoch: 33, loss: 263.47, accuracy: 0.8875
epoch: 34, loss: 263.94, accuracy: 0.8936
epoch: 35, loss: 263.07, accuracy: 0.8856
epoch: 36, loss: 263.59, accuracy: 0.8912
epoch: 37, loss: 261.91, accuracy: 0.8842
epoch: 38, loss: 262.32, accuracy: 0.8838
epoch: 39, loss: 260.61, accuracy: 0.8874
epoch: 40, loss: 260.89, accuracy: 0.8916
epoch: 41, loss: 259.39, accuracy: 0.8875
epoch: 42, loss: 260.04, accuracy: 0.8908
epoch: 43, loss: 259.13, accuracy: 0.893
epoch: 44, loss: 257.01, accuracy: 0.8914
epoch: 45, loss: 259.69, accuracy: 0.8893
epoch: 46, loss: 257.07, accuracy: 0.8891
epoch: 47, loss: 258.16, accuracy: 0.8887
epoch: 48, loss: 257.24, accuracy: 0.8913
epoch: 49, loss: 257.71, accuracy: 0.8835
epoch: 50, loss: 256.21, accuracy: 0.8929
epoch: 51, loss: 255.97, accuracy: 0.8857
epoch: 52, loss: 256.02, accuracy: 0.8867
epoch: 53, loss: 255.1, accuracy: 0.88
epoch: 54, loss: 256.24, accuracy: 0.8814
epoch: 55, loss: 255.1, accuracy: 0.8806
epoch: 56, loss: 256.11, accuracy: 0.8864
epoch: 57, loss: 257.09, accuracy: 0.8898
epoch: 58, loss: 254.79, accuracy: 0.8961
epoch: 59, loss: 253.5, accuracy: 0.8883
epoch: 60, loss: 254.09, accuracy: 0.8892
epoch: 61, loss: 255.52, accuracy: 0.8798
epoch: 62, loss: 253.94, accuracy: 0.8898
epoch: 63, loss: 256.36, accuracy: 0.8876
epoch: 64, loss: 254.31, accuracy: 0.8908
Epoch    65: reducing learning rate of group 0 to 5.0000e-03.
epoch: 65, loss: 253.86, accuracy: 0.8936
epoch: 66, loss: 238.43, accuracy: 0.8954
epoch: 67, loss: 236.97, accuracy: 0.8972
epoch: 68, loss: 236.66, accuracy: 0.8957
epoch: 69, loss: 237.32, accuracy: 0.8987
epoch: 70, loss: 236.45, accuracy: 0.8975
epoch: 71, loss: 235.62, accuracy: 0.8899
epoch: 72, loss: 235.93, accuracy: 0.8946
epoch: 73, loss: 234.91, accuracy: 0.8984
epoch: 74, loss: 235.55, accuracy: 0.8992
epoch: 75, loss: 222.19, accuracy: 0.9016
epoch: 76, loss: 219.46, accuracy: 0.901
epoch: 77, loss: 218.43, accuracy: 0.9016
epoch: 78, loss: 218.87, accuracy: 0.9021
epoch: 79, loss: 218.68, accuracy: 0.9015
epoch: 80, loss: 218.0, accuracy: 0.901
epoch: 81, loss: 218.25, accuracy: 0.9028
epoch: 82, loss: 218.12, accuracy: 0.9018
epoch: 83, loss: 217.88, accuracy: 0.9019
epoch: 84, loss: 217.96, accuracy: 0.902
epoch: 85, loss: 217.78, accuracy: 0.9009
epoch: 86, loss: 216.96, accuracy: 0.9017
epoch: 87, loss: 217.71, accuracy: 0.9015
epoch: 88, loss: 217.9, accuracy: 0.9009
epoch: 89, loss: 216.68, accuracy: 0.9022
epoch: 90, loss: 217.76, accuracy: 0.9016
epoch: 91, loss: 216.99, accuracy: 0.9014
epoch: 92, loss: 216.52, accuracy: 0.9009
epoch: 93, loss: 217.08, accuracy: 0.9007
epoch: 94, loss: 216.84, accuracy: 0.9011
Epoch    95: reducing learning rate of group 0 to 2.5000e-04.
epoch: 95, loss: 217.02, accuracy: 0.9022
epoch: 96, loss: 215.84, accuracy: 0.9017
epoch: 97, loss: 215.49, accuracy: 0.9022
epoch: 98, loss: 216.16, accuracy: 0.903
epoch: 99, loss: 215.37, accuracy: 0.9023
time analysis:
    all 1808.0348 s
    train 1806.5987 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 87 %
Accuracy of     3 : 90 %
Accuracy of     4 : 81 %
Accuracy of     5 : 100 %
Accuracy of     6 : 69 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=16, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2dj(in_channels=16, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 16, 28, 28]             160
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2dj-4           [-1, 16, 14, 14]             880
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2dj-7              [-1, 8, 7, 7]             440
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,290
DAU params: 1,480
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 523.82, accuracy: 0.8333
epoch: 1, loss: 390.81, accuracy: 0.8163
epoch: 2, loss: 361.36, accuracy: 0.8536
epoch: 3, loss: 350.02, accuracy: 0.8521
epoch: 4, loss: 338.45, accuracy: 0.8683
epoch: 5, loss: 331.01, accuracy: 0.8672
epoch: 6, loss: 323.29, accuracy: 0.8487
epoch: 7, loss: 317.89, accuracy: 0.8578
epoch: 8, loss: 314.81, accuracy: 0.8718
epoch: 9, loss: 309.11, accuracy: 0.8716
epoch: 10, loss: 304.57, accuracy: 0.8754
epoch: 11, loss: 302.13, accuracy: 0.8689
epoch: 12, loss: 297.88, accuracy: 0.878
epoch: 13, loss: 295.85, accuracy: 0.8765
epoch: 14, loss: 293.38, accuracy: 0.8825
epoch: 15, loss: 291.79, accuracy: 0.875
epoch: 16, loss: 291.9, accuracy: 0.8736
epoch: 17, loss: 287.16, accuracy: 0.8809
epoch: 18, loss: 288.11, accuracy: 0.8825
epoch: 19, loss: 284.97, accuracy: 0.8797
epoch: 20, loss: 283.19, accuracy: 0.8816
epoch: 21, loss: 282.21, accuracy: 0.8719
epoch: 22, loss: 280.89, accuracy: 0.8834
epoch: 23, loss: 281.7, accuracy: 0.8743
epoch: 24, loss: 277.57, accuracy: 0.8799
epoch: 25, loss: 277.2, accuracy: 0.8809
epoch: 26, loss: 277.63, accuracy: 0.8809
epoch: 27, loss: 274.26, accuracy: 0.8857
epoch: 28, loss: 273.57, accuracy: 0.8804
epoch: 29, loss: 273.44, accuracy: 0.8814
epoch: 30, loss: 272.37, accuracy: 0.8799
epoch: 31, loss: 270.5, accuracy: 0.8843
epoch: 32, loss: 269.31, accuracy: 0.8823
epoch: 33, loss: 267.84, accuracy: 0.8881
epoch: 34, loss: 269.0, accuracy: 0.8854
epoch: 35, loss: 268.63, accuracy: 0.8929
epoch: 36, loss: 266.43, accuracy: 0.8893
epoch: 37, loss: 265.26, accuracy: 0.8797
epoch: 38, loss: 266.79, accuracy: 0.8864
epoch: 39, loss: 265.81, accuracy: 0.8934
epoch: 40, loss: 266.03, accuracy: 0.8886
epoch: 41, loss: 266.89, accuracy: 0.8875
epoch: 42, loss: 264.14, accuracy: 0.8836
epoch: 43, loss: 265.02, accuracy: 0.8903
epoch: 44, loss: 264.01, accuracy: 0.8899
epoch: 45, loss: 264.36, accuracy: 0.8875
epoch: 46, loss: 263.27, accuracy: 0.8867
epoch: 47, loss: 261.36, accuracy: 0.8863
epoch: 48, loss: 263.59, accuracy: 0.882
epoch: 49, loss: 262.99, accuracy: 0.8854
epoch: 50, loss: 260.38, accuracy: 0.8854
epoch: 51, loss: 261.98, accuracy: 0.8865
epoch: 52, loss: 261.33, accuracy: 0.8928
epoch: 53, loss: 259.53, accuracy: 0.8909
epoch: 54, loss: 260.76, accuracy: 0.8899
epoch: 55, loss: 260.57, accuracy: 0.8948
epoch: 56, loss: 260.8, accuracy: 0.8923
epoch: 57, loss: 259.2, accuracy: 0.8959
epoch: 58, loss: 259.1, accuracy: 0.8966
epoch: 59, loss: 257.28, accuracy: 0.8845
epoch: 60, loss: 257.55, accuracy: 0.8886
epoch: 61, loss: 258.17, accuracy: 0.8896
epoch: 62, loss: 257.53, accuracy: 0.8914
epoch: 63, loss: 256.67, accuracy: 0.8896
epoch: 64, loss: 256.88, accuracy: 0.8946
epoch: 65, loss: 257.13, accuracy: 0.8889
epoch: 66, loss: 256.66, accuracy: 0.8886
epoch: 67, loss: 256.32, accuracy: 0.8807
epoch: 68, loss: 257.02, accuracy: 0.8886
epoch: 69, loss: 257.35, accuracy: 0.8887
epoch: 70, loss: 255.02, accuracy: 0.8849
epoch: 71, loss: 256.88, accuracy: 0.8932
epoch: 72, loss: 257.11, accuracy: 0.8846
epoch: 73, loss: 255.4, accuracy: 0.8876
epoch: 74, loss: 255.07, accuracy: 0.8891
epoch: 75, loss: 232.15, accuracy: 0.8976
epoch: 76, loss: 228.09, accuracy: 0.8978
epoch: 77, loss: 226.53, accuracy: 0.8972
epoch: 78, loss: 224.77, accuracy: 0.9001
epoch: 79, loss: 225.38, accuracy: 0.8981
epoch: 80, loss: 224.41, accuracy: 0.8998
epoch: 81, loss: 224.25, accuracy: 0.8989
epoch: 82, loss: 223.34, accuracy: 0.9004
epoch: 83, loss: 223.2, accuracy: 0.9002
epoch: 84, loss: 223.05, accuracy: 0.8994
epoch: 85, loss: 223.23, accuracy: 0.8985
epoch: 86, loss: 222.67, accuracy: 0.8979
epoch: 87, loss: 223.25, accuracy: 0.8982
epoch: 88, loss: 222.77, accuracy: 0.8983
epoch: 89, loss: 222.94, accuracy: 0.8994
epoch: 90, loss: 222.82, accuracy: 0.9004
epoch: 91, loss: 221.53, accuracy: 0.8988
epoch: 92, loss: 222.51, accuracy: 0.8992
epoch: 93, loss: 221.68, accuracy: 0.8982
epoch: 94, loss: 222.18, accuracy: 0.8989
epoch: 95, loss: 222.22, accuracy: 0.9004
epoch: 96, loss: 221.36, accuracy: 0.8989
epoch: 97, loss: 220.45, accuracy: 0.8982
epoch: 98, loss: 220.52, accuracy: 0.9014
epoch: 99, loss: 221.09, accuracy: 0.8977
time analysis:
    all 2401.5659 s
    train 2400.2738 s
Accuracy of     0 : 88 %
Accuracy of     1 : 95 %
Accuracy of     2 : 82 %
Accuracy of     3 : 86 %
Accuracy of     4 : 80 %
Accuracy of     5 : 98 %
Accuracy of     6 : 60 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 100 %
```

## FashionMNIST, 3_layers, DAUConv2dj, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=1, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=16, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2dj(in_channels=16, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 16, 28, 28]             208
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
        DAUConv2dj-4           [-1, 16, 14, 14]           1,168
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
        DAUConv2dj-7              [-1, 8, 7, 7]             584
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,770
DAU params: 1,960
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 529.55, accuracy: 0.8291
epoch: 1, loss: 375.54, accuracy: 0.8556
epoch: 2, loss: 337.2, accuracy: 0.8661
epoch: 3, loss: 322.21, accuracy: 0.8784
epoch: 4, loss: 309.84, accuracy: 0.8574
epoch: 5, loss: 303.8, accuracy: 0.8813
epoch: 6, loss: 297.13, accuracy: 0.8807
epoch: 7, loss: 295.86, accuracy: 0.8707
epoch: 8, loss: 286.63, accuracy: 0.8771
epoch: 9, loss: 283.15, accuracy: 0.8843
epoch: 10, loss: 281.04, accuracy: 0.8726
epoch: 11, loss: 275.67, accuracy: 0.8727
epoch: 12, loss: 274.74, accuracy: 0.89
epoch: 13, loss: 272.02, accuracy: 0.8791
epoch: 14, loss: 271.19, accuracy: 0.8861
epoch: 15, loss: 268.37, accuracy: 0.8795
epoch: 16, loss: 266.0, accuracy: 0.8833
epoch: 17, loss: 264.45, accuracy: 0.8829
epoch: 18, loss: 263.98, accuracy: 0.8809
epoch: 19, loss: 260.97, accuracy: 0.8909
epoch: 20, loss: 261.24, accuracy: 0.8818
epoch: 21, loss: 258.28, accuracy: 0.8827
epoch: 22, loss: 258.04, accuracy: 0.8878
epoch: 23, loss: 255.87, accuracy: 0.8855
epoch: 24, loss: 255.02, accuracy: 0.8853
epoch: 25, loss: 252.13, accuracy: 0.8837
epoch: 26, loss: 251.82, accuracy: 0.8924
epoch: 27, loss: 253.25, accuracy: 0.8896
epoch: 28, loss: 250.59, accuracy: 0.8833
epoch: 29, loss: 249.63, accuracy: 0.8883
epoch: 30, loss: 246.01, accuracy: 0.8919
epoch: 31, loss: 247.75, accuracy: 0.895
epoch: 32, loss: 246.69, accuracy: 0.8849
epoch: 33, loss: 242.45, accuracy: 0.8923
epoch: 34, loss: 244.56, accuracy: 0.8975
epoch: 35, loss: 242.82, accuracy: 0.8983
epoch: 36, loss: 243.49, accuracy: 0.894
epoch: 37, loss: 241.07, accuracy: 0.8898
epoch: 38, loss: 242.61, accuracy: 0.8948
epoch: 39, loss: 240.98, accuracy: 0.8963
epoch: 40, loss: 239.63, accuracy: 0.8918
epoch: 41, loss: 239.6, accuracy: 0.8918
epoch: 42, loss: 238.4, accuracy: 0.8944
epoch: 43, loss: 237.59, accuracy: 0.8943
epoch: 44, loss: 237.89, accuracy: 0.8959
epoch: 45, loss: 236.15, accuracy: 0.8977
epoch: 46, loss: 238.77, accuracy: 0.8718
epoch: 47, loss: 236.92, accuracy: 0.896
epoch: 48, loss: 235.6, accuracy: 0.8966
epoch: 49, loss: 236.82, accuracy: 0.8991
epoch: 50, loss: 234.09, accuracy: 0.8911
epoch: 51, loss: 232.66, accuracy: 0.8943
epoch: 52, loss: 234.19, accuracy: 0.8915
epoch: 53, loss: 233.34, accuracy: 0.8921
epoch: 54, loss: 232.35, accuracy: 0.896
epoch: 55, loss: 232.55, accuracy: 0.8997
epoch: 56, loss: 232.14, accuracy: 0.8986
epoch: 57, loss: 230.99, accuracy: 0.9008
epoch: 58, loss: 230.98, accuracy: 0.8945
epoch: 59, loss: 231.86, accuracy: 0.8932
epoch: 60, loss: 229.64, accuracy: 0.8986
epoch: 61, loss: 231.93, accuracy: 0.8974
epoch: 62, loss: 230.94, accuracy: 0.901
epoch: 63, loss: 230.83, accuracy: 0.8963
epoch: 64, loss: 229.6, accuracy: 0.8905
epoch: 65, loss: 229.4, accuracy: 0.8963
epoch: 66, loss: 230.0, accuracy: 0.8954
epoch: 67, loss: 229.21, accuracy: 0.9002
epoch: 68, loss: 228.86, accuracy: 0.8999
epoch: 69, loss: 227.98, accuracy: 0.894
epoch: 70, loss: 227.06, accuracy: 0.8952
epoch: 71, loss: 226.77, accuracy: 0.9001
epoch: 72, loss: 227.15, accuracy: 0.8939
epoch: 73, loss: 226.12, accuracy: 0.8992
epoch: 74, loss: 226.36, accuracy: 0.8981
epoch: 75, loss: 201.35, accuracy: 0.9047
epoch: 76, loss: 196.85, accuracy: 0.9053
epoch: 77, loss: 195.46, accuracy: 0.9058
epoch: 78, loss: 194.24, accuracy: 0.9059
epoch: 79, loss: 193.43, accuracy: 0.9053
epoch: 80, loss: 193.35, accuracy: 0.9057
epoch: 81, loss: 193.46, accuracy: 0.9059
epoch: 82, loss: 193.1, accuracy: 0.9047
epoch: 83, loss: 193.66, accuracy: 0.9056
epoch: 84, loss: 192.35, accuracy: 0.9066
epoch: 85, loss: 191.66, accuracy: 0.906
epoch: 86, loss: 191.51, accuracy: 0.9059
epoch: 87, loss: 191.88, accuracy: 0.9064
epoch: 88, loss: 192.09, accuracy: 0.9056
epoch: 89, loss: 191.2, accuracy: 0.9061
epoch: 90, loss: 191.4, accuracy: 0.9069
epoch: 91, loss: 190.86, accuracy: 0.9067
epoch: 92, loss: 190.16, accuracy: 0.9067
epoch: 93, loss: 189.88, accuracy: 0.9042
epoch: 94, loss: 189.86, accuracy: 0.9066
epoch: 95, loss: 190.5, accuracy: 0.9068
epoch: 96, loss: 189.8, accuracy: 0.906
epoch: 97, loss: 189.43, accuracy: 0.9056
epoch: 98, loss: 189.2, accuracy: 0.9072
epoch: 99, loss: 188.82, accuracy: 0.9056
time analysis:
    all 3021.2265 s
    train 3019.766 s
Accuracy of     0 : 86 %
Accuracy of     1 : 96 %
Accuracy of     2 : 84 %
Accuracy of     3 : 90 %
Accuracy of     4 : 88 %
Accuracy of     5 : 96 %
Accuracy of     6 : 66 %
Accuracy of     7 : 98 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=1, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=1, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]              34
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]             274
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             138
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,256
DAU params: 446
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 588.69, accuracy: 0.7263
epoch: 1, loss: 475.89, accuracy: 0.6613
epoch: 2, loss: 456.81, accuracy: 0.8095
epoch: 3, loss: 442.12, accuracy: 0.8042
epoch: 4, loss: 435.72, accuracy: 0.7792
epoch: 5, loss: 430.1, accuracy: 0.7137
epoch: 6, loss: 423.42, accuracy: 0.6706
epoch: 7, loss: 417.2, accuracy: 0.8231
epoch: 8, loss: 414.03, accuracy: 0.5808
epoch: 9, loss: 411.63, accuracy: 0.725
epoch: 10, loss: 406.03, accuracy: 0.7547
epoch: 11, loss: 401.64, accuracy: 0.7511
epoch: 12, loss: 397.45, accuracy: 0.743
epoch: 13, loss: 396.11, accuracy: 0.8386
epoch: 14, loss: 395.46, accuracy: 0.7677
epoch: 15, loss: 390.66, accuracy: 0.7709
epoch: 16, loss: 391.38, accuracy: 0.7992
epoch: 17, loss: 389.73, accuracy: 0.6749
epoch: 18, loss: 389.29, accuracy: 0.8396
epoch: 19, loss: 385.22, accuracy: 0.7775
epoch: 20, loss: 384.7, accuracy: 0.8038
epoch: 21, loss: 383.08, accuracy: 0.8219
epoch: 22, loss: 380.97, accuracy: 0.7713
epoch: 23, loss: 378.94, accuracy: 0.7059
epoch: 24, loss: 377.29, accuracy: 0.8144
epoch: 25, loss: 373.9, accuracy: 0.844
epoch: 26, loss: 374.13, accuracy: 0.7601
epoch: 27, loss: 372.13, accuracy: 0.783
epoch: 28, loss: 370.88, accuracy: 0.6688
epoch: 29, loss: 368.59, accuracy: 0.7571
epoch: 30, loss: 368.33, accuracy: 0.6112
epoch: 31, loss: 366.25, accuracy: 0.7789
epoch: 32, loss: 366.36, accuracy: 0.6798
epoch: 33, loss: 365.36, accuracy: 0.5716
epoch: 34, loss: 365.84, accuracy: 0.8307
epoch: 35, loss: 364.05, accuracy: 0.5587
epoch: 36, loss: 363.98, accuracy: 0.82
epoch: 37, loss: 361.99, accuracy: 0.7408
epoch: 38, loss: 362.6, accuracy: 0.8346
epoch: 39, loss: 362.95, accuracy: 0.8195
epoch: 40, loss: 361.19, accuracy: 0.8292
epoch: 41, loss: 358.87, accuracy: 0.8464
epoch: 42, loss: 359.39, accuracy: 0.5612
epoch: 43, loss: 360.84, accuracy: 0.5931
epoch: 44, loss: 358.05, accuracy: 0.8276
epoch: 45, loss: 358.93, accuracy: 0.8159
epoch: 46, loss: 359.13, accuracy: 0.6733
epoch: 47, loss: 356.88, accuracy: 0.8356
epoch: 48, loss: 358.94, accuracy: 0.692
epoch: 49, loss: 358.5, accuracy: 0.7682
epoch: 50, loss: 355.59, accuracy: 0.8287
epoch: 51, loss: 356.47, accuracy: 0.7125
epoch: 52, loss: 357.02, accuracy: 0.822
epoch: 53, loss: 356.55, accuracy: 0.8301
epoch: 54, loss: 357.4, accuracy: 0.8163
epoch: 55, loss: 356.24, accuracy: 0.8268
epoch: 56, loss: 353.17, accuracy: 0.828
epoch: 57, loss: 355.1, accuracy: 0.801
epoch: 58, loss: 356.15, accuracy: 0.8477
epoch: 59, loss: 354.39, accuracy: 0.8359
epoch: 60, loss: 354.87, accuracy: 0.8077
epoch: 61, loss: 352.03, accuracy: 0.69
epoch: 62, loss: 353.41, accuracy: 0.8294
epoch: 63, loss: 351.19, accuracy: 0.7324
epoch: 64, loss: 353.59, accuracy: 0.8107
epoch: 65, loss: 350.07, accuracy: 0.8391
epoch: 66, loss: 351.23, accuracy: 0.7134
epoch: 67, loss: 350.56, accuracy: 0.8316
epoch: 68, loss: 351.07, accuracy: 0.843
epoch: 69, loss: 351.96, accuracy: 0.8518
epoch: 70, loss: 348.35, accuracy: 0.8236
epoch: 71, loss: 350.49, accuracy: 0.4567
epoch: 72, loss: 352.49, accuracy: 0.7414
epoch: 73, loss: 350.25, accuracy: 0.7958
epoch: 74, loss: 349.08, accuracy: 0.8322
epoch: 75, loss: 327.68, accuracy: 0.8656
epoch: 76, loss: 324.23, accuracy: 0.8668
epoch: 77, loss: 324.28, accuracy: 0.8653
epoch: 78, loss: 323.2, accuracy: 0.867
epoch: 79, loss: 323.35, accuracy: 0.866
epoch: 80, loss: 322.62, accuracy: 0.8678
epoch: 81, loss: 322.82, accuracy: 0.869
epoch: 82, loss: 321.66, accuracy: 0.8663
epoch: 83, loss: 322.05, accuracy: 0.8678
epoch: 84, loss: 323.21, accuracy: 0.8569
epoch: 85, loss: 322.22, accuracy: 0.8643
epoch: 86, loss: 321.87, accuracy: 0.8548
epoch: 87, loss: 320.33, accuracy: 0.8635
epoch: 88, loss: 321.46, accuracy: 0.8664
epoch: 89, loss: 321.8, accuracy: 0.8626
epoch: 90, loss: 321.23, accuracy: 0.8655
epoch: 91, loss: 321.28, accuracy: 0.8649
epoch: 92, loss: 320.78, accuracy: 0.8662
Epoch    93: reducing learning rate of group 0 to 5.0000e-04.
epoch: 93, loss: 320.02, accuracy: 0.8676
epoch: 94, loss: 319.6, accuracy: 0.8704
epoch: 95, loss: 318.65, accuracy: 0.8701
epoch: 96, loss: 318.26, accuracy: 0.8693
epoch: 97, loss: 318.05, accuracy: 0.8707
epoch: 98, loss: 318.28, accuracy: 0.8678
epoch: 99, loss: 318.46, accuracy: 0.8699
time analysis:
    all 1112.4544 s
    train 1110.8108 s
Accuracy of     0 : 80 %
Accuracy of     1 : 93 %
Accuracy of     2 : 80 %
Accuracy of     3 : 87 %
Accuracy of     4 : 71 %
Accuracy of     5 : 98 %
Accuracy of     6 : 60 %
Accuracy of     7 : 96 %
Accuracy of     8 : 98 %
Accuracy of     9 : 96 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=2, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]              52
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]             532
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             268
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,662
DAU params: 852
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 560.74, accuracy: 0.8359
epoch: 1, loss: 406.74, accuracy: 0.8359
epoch: 2, loss: 377.17, accuracy: 0.8575
epoch: 3, loss: 359.29, accuracy: 0.8575
epoch: 4, loss: 352.29, accuracy: 0.858
epoch: 5, loss: 343.88, accuracy: 0.8656
epoch: 6, loss: 338.75, accuracy: 0.8474
epoch: 7, loss: 331.75, accuracy: 0.8587
epoch: 8, loss: 332.76, accuracy: 0.8652
epoch: 9, loss: 327.28, accuracy: 0.8633
epoch: 10, loss: 323.52, accuracy: 0.87
epoch: 11, loss: 323.2, accuracy: 0.8674
epoch: 12, loss: 322.66, accuracy: 0.8627
epoch: 13, loss: 318.08, accuracy: 0.867
epoch: 14, loss: 315.52, accuracy: 0.8661
epoch: 15, loss: 314.98, accuracy: 0.8619
epoch: 16, loss: 314.13, accuracy: 0.8656
epoch: 17, loss: 311.19, accuracy: 0.8666
epoch: 18, loss: 313.29, accuracy: 0.865
epoch: 19, loss: 312.07, accuracy: 0.8716
epoch: 20, loss: 310.95, accuracy: 0.8621
epoch: 21, loss: 310.32, accuracy: 0.8734
epoch: 22, loss: 308.07, accuracy: 0.8736
epoch: 23, loss: 305.93, accuracy: 0.8545
epoch: 24, loss: 306.62, accuracy: 0.8696
epoch: 25, loss: 308.14, accuracy: 0.8714
epoch: 26, loss: 306.5, accuracy: 0.8746
epoch: 27, loss: 304.44, accuracy: 0.8571
epoch: 28, loss: 302.71, accuracy: 0.8793
epoch: 29, loss: 304.7, accuracy: 0.8754
epoch: 30, loss: 302.51, accuracy: 0.8725
epoch: 31, loss: 301.58, accuracy: 0.8512
epoch: 32, loss: 302.97, accuracy: 0.8754
epoch: 33, loss: 300.22, accuracy: 0.8765
epoch: 34, loss: 299.3, accuracy: 0.8696
epoch: 35, loss: 301.52, accuracy: 0.8677
epoch: 36, loss: 299.52, accuracy: 0.851
epoch: 37, loss: 297.66, accuracy: 0.876
epoch: 38, loss: 298.67, accuracy: 0.8718
epoch: 39, loss: 298.38, accuracy: 0.877
epoch: 40, loss: 297.13, accuracy: 0.8786
epoch: 41, loss: 296.61, accuracy: 0.8819
epoch: 42, loss: 296.66, accuracy: 0.8814
epoch: 43, loss: 295.38, accuracy: 0.8712
epoch: 44, loss: 296.19, accuracy: 0.8652
epoch: 45, loss: 296.25, accuracy: 0.8821
epoch: 46, loss: 293.46, accuracy: 0.873
epoch: 47, loss: 293.94, accuracy: 0.8704
epoch: 48, loss: 292.79, accuracy: 0.8805
epoch: 49, loss: 291.39, accuracy: 0.8745
epoch: 50, loss: 293.4, accuracy: 0.8767
epoch: 51, loss: 295.35, accuracy: 0.8718
epoch: 52, loss: 291.73, accuracy: 0.8758
epoch: 53, loss: 289.33, accuracy: 0.8774
epoch: 54, loss: 291.14, accuracy: 0.8744
epoch: 55, loss: 289.04, accuracy: 0.8765
epoch: 56, loss: 289.37, accuracy: 0.8708
epoch: 57, loss: 289.37, accuracy: 0.8827
epoch: 58, loss: 287.39, accuracy: 0.865
epoch: 59, loss: 287.72, accuracy: 0.878
epoch: 60, loss: 287.6, accuracy: 0.8793
epoch: 61, loss: 287.48, accuracy: 0.8753
epoch: 62, loss: 286.89, accuracy: 0.8731
epoch: 63, loss: 285.43, accuracy: 0.8737
epoch: 64, loss: 286.36, accuracy: 0.8798
epoch: 65, loss: 286.67, accuracy: 0.8764
epoch: 66, loss: 286.1, accuracy: 0.881
epoch: 67, loss: 286.53, accuracy: 0.8841
epoch: 68, loss: 283.74, accuracy: 0.8765
epoch: 69, loss: 285.13, accuracy: 0.8831
epoch: 70, loss: 283.85, accuracy: 0.875
epoch: 71, loss: 285.51, accuracy: 0.8783
epoch: 72, loss: 284.21, accuracy: 0.8782
epoch: 73, loss: 283.96, accuracy: 0.8722
Epoch    74: reducing learning rate of group 0 to 5.0000e-03.
epoch: 74, loss: 284.81, accuracy: 0.8747
epoch: 75, loss: 263.38, accuracy: 0.8894
epoch: 76, loss: 258.74, accuracy: 0.8899
epoch: 77, loss: 258.4, accuracy: 0.8905
epoch: 78, loss: 258.22, accuracy: 0.8896
epoch: 79, loss: 257.05, accuracy: 0.8911
epoch: 80, loss: 256.6, accuracy: 0.889
epoch: 81, loss: 257.8, accuracy: 0.8897
epoch: 82, loss: 256.9, accuracy: 0.8892
epoch: 83, loss: 256.6, accuracy: 0.8901
epoch: 84, loss: 256.69, accuracy: 0.8882
epoch: 85, loss: 257.16, accuracy: 0.889
epoch: 86, loss: 256.05, accuracy: 0.8897
epoch: 87, loss: 256.44, accuracy: 0.8906
epoch: 88, loss: 256.42, accuracy: 0.8895
epoch: 89, loss: 256.2, accuracy: 0.8896
epoch: 90, loss: 255.52, accuracy: 0.8895
epoch: 91, loss: 255.86, accuracy: 0.8911
epoch: 92, loss: 255.76, accuracy: 0.8893
epoch: 93, loss: 256.05, accuracy: 0.8892
epoch: 94, loss: 255.67, accuracy: 0.8886
epoch: 95, loss: 255.49, accuracy: 0.8897
Epoch    96: reducing learning rate of group 0 to 2.5000e-04.
epoch: 96, loss: 255.7, accuracy: 0.8904
epoch: 97, loss: 254.69, accuracy: 0.8901
epoch: 98, loss: 254.1, accuracy: 0.8896
epoch: 99, loss: 254.73, accuracy: 0.8908
time analysis:
    all 1098.4008 s
    train 1097.2774 s
Accuracy of     0 : 92 %
Accuracy of     1 : 93 %
Accuracy of     2 : 85 %
Accuracy of     3 : 86 %
Accuracy of     4 : 80 %
Accuracy of     5 : 98 %
Accuracy of     6 : 66 %
Accuracy of     7 : 98 %
Accuracy of     8 : 95 %
Accuracy of     9 : 96 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=3, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]              70
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]             790
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             398
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,068
DAU params: 1,258
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 508.36, accuracy: 0.829
epoch: 1, loss: 396.82, accuracy: 0.7856
epoch: 2, loss: 378.16, accuracy: 0.8463
epoch: 3, loss: 358.38, accuracy: 0.8121
epoch: 4, loss: 351.98, accuracy: 0.8584
epoch: 5, loss: 342.74, accuracy: 0.8489
epoch: 6, loss: 339.3, accuracy: 0.8624
epoch: 7, loss: 331.03, accuracy: 0.8578
epoch: 8, loss: 328.91, accuracy: 0.8498
epoch: 9, loss: 325.05, accuracy: 0.8603
epoch: 10, loss: 319.99, accuracy: 0.86
epoch: 11, loss: 316.46, accuracy: 0.8634
epoch: 12, loss: 313.07, accuracy: 0.8715
epoch: 13, loss: 309.68, accuracy: 0.8776
epoch: 14, loss: 306.94, accuracy: 0.8706
epoch: 15, loss: 305.18, accuracy: 0.8549
epoch: 16, loss: 302.61, accuracy: 0.8678
epoch: 17, loss: 300.24, accuracy: 0.8711
epoch: 18, loss: 298.82, accuracy: 0.8774
epoch: 19, loss: 295.61, accuracy: 0.8694
epoch: 20, loss: 293.21, accuracy: 0.8792
epoch: 21, loss: 292.39, accuracy: 0.874
epoch: 22, loss: 290.8, accuracy: 0.8715
epoch: 23, loss: 288.03, accuracy: 0.8772
epoch: 24, loss: 285.51, accuracy: 0.881
epoch: 25, loss: 285.17, accuracy: 0.8749
epoch: 26, loss: 281.81, accuracy: 0.8836
epoch: 27, loss: 282.39, accuracy: 0.8798
epoch: 28, loss: 279.36, accuracy: 0.8709
epoch: 29, loss: 279.51, accuracy: 0.88
epoch: 30, loss: 277.67, accuracy: 0.8777
epoch: 31, loss: 277.16, accuracy: 0.8791
epoch: 32, loss: 277.56, accuracy: 0.8855
epoch: 33, loss: 274.34, accuracy: 0.8835
epoch: 34, loss: 273.03, accuracy: 0.8839
epoch: 35, loss: 272.95, accuracy: 0.8829
epoch: 36, loss: 272.47, accuracy: 0.8844
epoch: 37, loss: 270.1, accuracy: 0.885
epoch: 38, loss: 270.2, accuracy: 0.8833
epoch: 39, loss: 270.97, accuracy: 0.8858
epoch: 40, loss: 269.44, accuracy: 0.8858
epoch: 41, loss: 268.53, accuracy: 0.8712
epoch: 42, loss: 268.65, accuracy: 0.8872
epoch: 43, loss: 266.66, accuracy: 0.879
epoch: 44, loss: 266.84, accuracy: 0.8871
epoch: 45, loss: 266.37, accuracy: 0.8886
epoch: 46, loss: 264.37, accuracy: 0.8862
epoch: 47, loss: 263.41, accuracy: 0.8889
epoch: 48, loss: 263.56, accuracy: 0.8811
epoch: 49, loss: 265.2, accuracy: 0.8868
epoch: 50, loss: 263.77, accuracy: 0.8846
epoch: 51, loss: 265.65, accuracy: 0.8822
epoch: 52, loss: 264.54, accuracy: 0.8906
epoch: 53, loss: 262.65, accuracy: 0.8836
epoch: 54, loss: 262.39, accuracy: 0.8914
epoch: 55, loss: 262.73, accuracy: 0.888
epoch: 56, loss: 260.3, accuracy: 0.8883
epoch: 57, loss: 260.14, accuracy: 0.8814
epoch: 58, loss: 261.53, accuracy: 0.8903
epoch: 59, loss: 259.93, accuracy: 0.8887
epoch: 60, loss: 260.98, accuracy: 0.8918
epoch: 61, loss: 259.85, accuracy: 0.89
epoch: 62, loss: 260.58, accuracy: 0.8901
epoch: 63, loss: 259.93, accuracy: 0.8885
epoch: 64, loss: 259.31, accuracy: 0.8879
epoch: 65, loss: 259.86, accuracy: 0.8859
epoch: 66, loss: 258.31, accuracy: 0.8866
epoch: 67, loss: 259.63, accuracy: 0.8883
epoch: 68, loss: 257.74, accuracy: 0.8907
epoch: 69, loss: 257.73, accuracy: 0.8841
epoch: 70, loss: 258.54, accuracy: 0.8793
epoch: 71, loss: 257.71, accuracy: 0.8872
epoch: 72, loss: 257.72, accuracy: 0.8894
epoch: 73, loss: 258.58, accuracy: 0.8873
epoch: 74, loss: 256.87, accuracy: 0.8893
epoch: 75, loss: 235.61, accuracy: 0.8955
epoch: 76, loss: 233.16, accuracy: 0.8967
epoch: 77, loss: 232.22, accuracy: 0.899
epoch: 78, loss: 231.17, accuracy: 0.8986
epoch: 79, loss: 231.47, accuracy: 0.898
epoch: 80, loss: 231.08, accuracy: 0.896
epoch: 81, loss: 230.41, accuracy: 0.9007
epoch: 82, loss: 231.0, accuracy: 0.8976
epoch: 83, loss: 230.23, accuracy: 0.8976
epoch: 84, loss: 229.66, accuracy: 0.8995
epoch: 85, loss: 229.71, accuracy: 0.899
epoch: 86, loss: 230.1, accuracy: 0.8984
epoch: 87, loss: 229.89, accuracy: 0.899
epoch: 88, loss: 229.75, accuracy: 0.8985
epoch: 89, loss: 228.77, accuracy: 0.8981
epoch: 90, loss: 228.48, accuracy: 0.8983
epoch: 91, loss: 229.41, accuracy: 0.8984
epoch: 92, loss: 228.77, accuracy: 0.8984
epoch: 93, loss: 228.96, accuracy: 0.8983
epoch: 94, loss: 228.46, accuracy: 0.899
epoch: 95, loss: 228.19, accuracy: 0.8985
epoch: 96, loss: 228.05, accuracy: 0.9005
epoch: 97, loss: 227.41, accuracy: 0.8976
epoch: 98, loss: 227.87, accuracy: 0.8982
epoch: 99, loss: 227.88, accuracy: 0.8992
time analysis:
    all 1200.5321 s
    train 1199.2397 s
Accuracy of     0 : 82 %
Accuracy of     1 : 93 %
Accuracy of     2 : 85 %
Accuracy of     3 : 86 %
Accuracy of     4 : 78 %
Accuracy of     5 : 96 %
Accuracy of     6 : 69 %
Accuracy of     7 : 100 %
Accuracy of     8 : 98 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=4, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]              88
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]           1,048
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             528
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,474
DAU params: 1,664
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 521.36, accuracy: 0.8349
epoch: 1, loss: 387.78, accuracy: 0.8447
epoch: 2, loss: 355.55, accuracy: 0.8623
epoch: 3, loss: 337.07, accuracy: 0.8305
epoch: 4, loss: 323.21, accuracy: 0.8693
epoch: 5, loss: 312.65, accuracy: 0.8742
epoch: 6, loss: 307.19, accuracy: 0.8616
epoch: 7, loss: 298.84, accuracy: 0.8681
epoch: 8, loss: 294.02, accuracy: 0.8775
epoch: 9, loss: 288.23, accuracy: 0.8832
epoch: 10, loss: 286.94, accuracy: 0.8772
epoch: 11, loss: 282.74, accuracy: 0.8816
epoch: 12, loss: 279.79, accuracy: 0.8883
epoch: 13, loss: 279.73, accuracy: 0.8663
epoch: 14, loss: 276.28, accuracy: 0.8764
epoch: 15, loss: 273.4, accuracy: 0.8773
epoch: 16, loss: 271.64, accuracy: 0.8918
epoch: 17, loss: 271.6, accuracy: 0.8895
epoch: 18, loss: 268.36, accuracy: 0.8908
epoch: 19, loss: 267.17, accuracy: 0.8915
epoch: 20, loss: 267.08, accuracy: 0.8887
epoch: 21, loss: 262.5, accuracy: 0.8953
epoch: 22, loss: 264.65, accuracy: 0.8906
epoch: 23, loss: 262.35, accuracy: 0.8937
epoch: 24, loss: 262.96, accuracy: 0.8813
epoch: 25, loss: 261.93, accuracy: 0.8882
epoch: 26, loss: 258.83, accuracy: 0.8919
epoch: 27, loss: 258.81, accuracy: 0.8915
epoch: 28, loss: 258.3, accuracy: 0.8908
epoch: 29, loss: 256.63, accuracy: 0.8863
epoch: 30, loss: 256.05, accuracy: 0.8918
epoch: 31, loss: 253.94, accuracy: 0.8769
epoch: 32, loss: 256.11, accuracy: 0.8926
epoch: 33, loss: 253.66, accuracy: 0.8932
epoch: 34, loss: 255.12, accuracy: 0.8919
epoch: 35, loss: 252.25, accuracy: 0.8905
epoch: 36, loss: 251.26, accuracy: 0.8921
epoch: 37, loss: 251.85, accuracy: 0.889
epoch: 38, loss: 249.78, accuracy: 0.8826
epoch: 39, loss: 249.38, accuracy: 0.8944
epoch: 40, loss: 247.36, accuracy: 0.8895
epoch: 41, loss: 250.1, accuracy: 0.8913
epoch: 42, loss: 249.0, accuracy: 0.8854
epoch: 43, loss: 248.77, accuracy: 0.8912
epoch: 44, loss: 247.58, accuracy: 0.8912
epoch: 45, loss: 249.19, accuracy: 0.8901
Epoch    46: reducing learning rate of group 0 to 5.0000e-03.
epoch: 46, loss: 247.51, accuracy: 0.8954
epoch: 47, loss: 231.21, accuracy: 0.8978
epoch: 48, loss: 230.68, accuracy: 0.8987
epoch: 49, loss: 229.51, accuracy: 0.8981
epoch: 50, loss: 229.99, accuracy: 0.8939
epoch: 51, loss: 230.01, accuracy: 0.8962
epoch: 52, loss: 228.01, accuracy: 0.893
epoch: 53, loss: 230.12, accuracy: 0.8968
epoch: 54, loss: 227.9, accuracy: 0.8952
epoch: 55, loss: 227.24, accuracy: 0.899
epoch: 56, loss: 227.07, accuracy: 0.8988
epoch: 57, loss: 227.4, accuracy: 0.8989
epoch: 58, loss: 227.71, accuracy: 0.891
epoch: 59, loss: 226.77, accuracy: 0.8971
epoch: 60, loss: 225.73, accuracy: 0.8954
epoch: 61, loss: 226.45, accuracy: 0.8991
epoch: 62, loss: 225.62, accuracy: 0.898
epoch: 63, loss: 225.49, accuracy: 0.8958
epoch: 64, loss: 223.57, accuracy: 0.9003
epoch: 65, loss: 224.72, accuracy: 0.896
epoch: 66, loss: 224.95, accuracy: 0.8991
epoch: 67, loss: 225.5, accuracy: 0.8998
epoch: 68, loss: 223.26, accuracy: 0.9012
epoch: 69, loss: 225.2, accuracy: 0.8977
epoch: 70, loss: 224.0, accuracy: 0.8966
epoch: 71, loss: 224.36, accuracy: 0.8979
epoch: 72, loss: 223.74, accuracy: 0.8913
epoch: 73, loss: 224.01, accuracy: 0.8961
Epoch    74: reducing learning rate of group 0 to 2.5000e-03.
epoch: 74, loss: 224.81, accuracy: 0.8966
epoch: 75, loss: 210.89, accuracy: 0.9002
epoch: 76, loss: 208.61, accuracy: 0.9009
epoch: 77, loss: 207.47, accuracy: 0.9011
epoch: 78, loss: 207.47, accuracy: 0.9014
epoch: 79, loss: 206.21, accuracy: 0.9011
epoch: 80, loss: 205.87, accuracy: 0.9004
epoch: 81, loss: 206.42, accuracy: 0.9018
epoch: 82, loss: 207.39, accuracy: 0.9028
epoch: 83, loss: 206.71, accuracy: 0.9007
epoch: 84, loss: 206.89, accuracy: 0.9022
epoch: 85, loss: 205.98, accuracy: 0.9016
Epoch    86: reducing learning rate of group 0 to 1.2500e-04.
epoch: 86, loss: 206.1, accuracy: 0.9013
epoch: 87, loss: 204.76, accuracy: 0.9023
epoch: 88, loss: 205.16, accuracy: 0.9011
epoch: 89, loss: 205.18, accuracy: 0.9019
epoch: 90, loss: 205.16, accuracy: 0.9016
epoch: 91, loss: 205.8, accuracy: 0.9019
epoch: 92, loss: 205.24, accuracy: 0.9017
epoch: 93, loss: 204.42, accuracy: 0.9014
epoch: 94, loss: 204.63, accuracy: 0.9016
epoch: 95, loss: 205.71, accuracy: 0.9022
epoch: 96, loss: 205.3, accuracy: 0.9014
epoch: 97, loss: 205.67, accuracy: 0.9015
epoch: 98, loss: 204.95, accuracy: 0.9012
Epoch    99: reducing learning rate of group 0 to 1.0000e-04.
epoch: 99, loss: 205.39, accuracy: 0.9011
time analysis:
    all 1288.2717 s
    train 1286.9355 s
Accuracy of     0 : 80 %
Accuracy of     1 : 89 %
Accuracy of     2 : 82 %
Accuracy of     3 : 86 %
Accuracy of     4 : 78 %
Accuracy of     5 : 100 %
Accuracy of     6 : 66 %
Accuracy of     7 : 100 %
Accuracy of     8 : 98 %
Accuracy of     9 : 100 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=5, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=5, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]             106
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]           1,306
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             658
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 2,880
DAU params: 2,070
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 543.64, accuracy: 0.7856
epoch: 1, loss: 376.32, accuracy: 0.8193
epoch: 2, loss: 347.11, accuracy: 0.8607
epoch: 3, loss: 328.34, accuracy: 0.863
epoch: 4, loss: 319.13, accuracy: 0.8577
epoch: 5, loss: 308.31, accuracy: 0.8704
epoch: 6, loss: 301.1, accuracy: 0.8709
epoch: 7, loss: 295.55, accuracy: 0.8788
epoch: 8, loss: 286.96, accuracy: 0.8731
epoch: 9, loss: 283.99, accuracy: 0.8854
epoch: 10, loss: 281.32, accuracy: 0.8856
epoch: 11, loss: 277.09, accuracy: 0.8804
epoch: 12, loss: 277.67, accuracy: 0.8868
epoch: 13, loss: 274.86, accuracy: 0.8876
epoch: 14, loss: 271.33, accuracy: 0.8887
epoch: 15, loss: 267.77, accuracy: 0.8781
epoch: 16, loss: 266.55, accuracy: 0.8891
epoch: 17, loss: 264.48, accuracy: 0.8808
epoch: 18, loss: 261.37, accuracy: 0.8825
epoch: 19, loss: 260.71, accuracy: 0.8911
epoch: 20, loss: 258.46, accuracy: 0.8858
epoch: 21, loss: 257.24, accuracy: 0.8908
epoch: 22, loss: 257.37, accuracy: 0.8931
epoch: 23, loss: 255.12, accuracy: 0.8894
epoch: 24, loss: 256.89, accuracy: 0.8931
epoch: 25, loss: 255.64, accuracy: 0.893
epoch: 26, loss: 253.46, accuracy: 0.8938
epoch: 27, loss: 252.1, accuracy: 0.8868
epoch: 28, loss: 251.03, accuracy: 0.8827
epoch: 29, loss: 253.49, accuracy: 0.8838
epoch: 30, loss: 249.02, accuracy: 0.8909
epoch: 31, loss: 249.63, accuracy: 0.8949
epoch: 32, loss: 249.31, accuracy: 0.8924
epoch: 33, loss: 246.91, accuracy: 0.8866
epoch: 34, loss: 248.51, accuracy: 0.8885
epoch: 35, loss: 246.61, accuracy: 0.8922
epoch: 36, loss: 245.65, accuracy: 0.8921
epoch: 37, loss: 245.79, accuracy: 0.8935
epoch: 38, loss: 245.42, accuracy: 0.891
epoch: 39, loss: 244.2, accuracy: 0.8891
epoch: 40, loss: 243.37, accuracy: 0.8899
epoch: 41, loss: 242.85, accuracy: 0.8911
epoch: 42, loss: 243.76, accuracy: 0.8935
epoch: 43, loss: 243.0, accuracy: 0.8896
epoch: 44, loss: 241.81, accuracy: 0.8862
epoch: 45, loss: 240.63, accuracy: 0.8948
epoch: 46, loss: 241.6, accuracy: 0.8878
epoch: 47, loss: 240.07, accuracy: 0.8963
epoch: 48, loss: 237.79, accuracy: 0.8896
epoch: 49, loss: 238.94, accuracy: 0.895
epoch: 50, loss: 239.52, accuracy: 0.8814
epoch: 51, loss: 237.02, accuracy: 0.8946
epoch: 52, loss: 238.9, accuracy: 0.8927
epoch: 53, loss: 236.89, accuracy: 0.8953
epoch: 54, loss: 236.13, accuracy: 0.8829
epoch: 55, loss: 238.01, accuracy: 0.8943
epoch: 56, loss: 235.49, accuracy: 0.8924
epoch: 57, loss: 235.95, accuracy: 0.8928
epoch: 58, loss: 235.61, accuracy: 0.8937
epoch: 59, loss: 236.36, accuracy: 0.8957
epoch: 60, loss: 234.24, accuracy: 0.8957
epoch: 61, loss: 235.01, accuracy: 0.893
epoch: 62, loss: 234.71, accuracy: 0.8972
epoch: 63, loss: 234.72, accuracy: 0.8922
epoch: 64, loss: 234.85, accuracy: 0.8936
epoch: 65, loss: 232.8, accuracy: 0.8949
epoch: 66, loss: 231.78, accuracy: 0.897
epoch: 67, loss: 231.95, accuracy: 0.8966
epoch: 68, loss: 232.14, accuracy: 0.8886
epoch: 69, loss: 231.3, accuracy: 0.8909
epoch: 70, loss: 230.84, accuracy: 0.8913
epoch: 71, loss: 231.32, accuracy: 0.8958
epoch: 72, loss: 229.44, accuracy: 0.8914
epoch: 73, loss: 230.61, accuracy: 0.8961
epoch: 74, loss: 229.79, accuracy: 0.8954
epoch: 75, loss: 208.31, accuracy: 0.9016
epoch: 76, loss: 205.51, accuracy: 0.9018
epoch: 77, loss: 203.37, accuracy: 0.9028
epoch: 78, loss: 202.71, accuracy: 0.8998
epoch: 79, loss: 203.14, accuracy: 0.902
epoch: 80, loss: 202.14, accuracy: 0.9018
epoch: 81, loss: 202.17, accuracy: 0.9031
epoch: 82, loss: 201.35, accuracy: 0.9033
epoch: 83, loss: 201.88, accuracy: 0.9023
epoch: 84, loss: 202.11, accuracy: 0.9011
epoch: 85, loss: 200.84, accuracy: 0.9019
epoch: 86, loss: 200.65, accuracy: 0.9027
epoch: 87, loss: 201.21, accuracy: 0.9035
epoch: 88, loss: 200.53, accuracy: 0.9014
epoch: 89, loss: 200.53, accuracy: 0.9035
epoch: 90, loss: 200.28, accuracy: 0.9031
epoch: 91, loss: 200.61, accuracy: 0.9025
epoch: 92, loss: 200.52, accuracy: 0.9011
epoch: 93, loss: 199.53, accuracy: 0.9036
epoch: 94, loss: 199.27, accuracy: 0.9018
epoch: 95, loss: 199.14, accuracy: 0.9009
epoch: 96, loss: 199.58, accuracy: 0.9027
epoch: 97, loss: 199.37, accuracy: 0.9018
epoch: 98, loss: 199.28, accuracy: 0.9032
epoch: 99, loss: 199.89, accuracy: 0.9028
time analysis:
    all 1471.0614 s
    train 1469.6699 s
Accuracy of     0 : 86 %
Accuracy of     1 : 95 %
Accuracy of     2 : 80 %
Accuracy of     3 : 87 %
Accuracy of     4 : 85 %
Accuracy of     5 : 98 %
Accuracy of     6 : 71 %
Accuracy of     7 : 100 %
Accuracy of     8 : 96 %
Accuracy of     9 : 100 %
```

## FashionMNIST, 3_layers, DAUConv2dOneMu, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=1, out_channels=16, no_units=6, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=16, out_channels=16, no_units=6, sigma=0.5)
  (conv3): DAUConv2dOneMu(in_channels=16, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 16, 28, 28]             124
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
    DAUConv2dOneMu-4           [-1, 16, 14, 14]           1,564
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
    DAUConv2dOneMu-7              [-1, 8, 7, 7]             788
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 3,286
DAU params: 2,476
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 560.93, accuracy: 0.7637
epoch: 1, loss: 397.22, accuracy: 0.8264
epoch: 2, loss: 355.29, accuracy: 0.8717
epoch: 3, loss: 335.97, accuracy: 0.8655
epoch: 4, loss: 322.23, accuracy: 0.8178
epoch: 5, loss: 314.9, accuracy: 0.869
epoch: 6, loss: 305.47, accuracy: 0.8592
epoch: 7, loss: 299.84, accuracy: 0.8805
epoch: 8, loss: 293.97, accuracy: 0.8749
epoch: 9, loss: 290.57, accuracy: 0.8739
epoch: 10, loss: 286.46, accuracy: 0.8819
epoch: 11, loss: 279.65, accuracy: 0.8855
epoch: 12, loss: 275.79, accuracy: 0.8837
epoch: 13, loss: 275.81, accuracy: 0.879
epoch: 14, loss: 272.11, accuracy: 0.8818
epoch: 15, loss: 270.42, accuracy: 0.8675
epoch: 16, loss: 266.65, accuracy: 0.8879
epoch: 17, loss: 264.74, accuracy: 0.8873
epoch: 18, loss: 264.26, accuracy: 0.8914
epoch: 19, loss: 262.9, accuracy: 0.8889
epoch: 20, loss: 259.96, accuracy: 0.8869
epoch: 21, loss: 256.48, accuracy: 0.8894
epoch: 22, loss: 257.55, accuracy: 0.8861
epoch: 23, loss: 255.06, accuracy: 0.8806
epoch: 24, loss: 253.13, accuracy: 0.8819
epoch: 25, loss: 252.3, accuracy: 0.8942
epoch: 26, loss: 251.81, accuracy: 0.8932
epoch: 27, loss: 249.38, accuracy: 0.8867
epoch: 28, loss: 250.08, accuracy: 0.8914
epoch: 29, loss: 247.75, accuracy: 0.89
epoch: 30, loss: 247.01, accuracy: 0.8949
epoch: 31, loss: 247.67, accuracy: 0.889
epoch: 32, loss: 246.59, accuracy: 0.8948
epoch: 33, loss: 243.48, accuracy: 0.8949
epoch: 34, loss: 243.03, accuracy: 0.885
epoch: 35, loss: 242.07, accuracy: 0.8886
epoch: 36, loss: 240.88, accuracy: 0.893
epoch: 37, loss: 242.64, accuracy: 0.8946
epoch: 38, loss: 239.21, accuracy: 0.8928
epoch: 39, loss: 240.36, accuracy: 0.886
epoch: 40, loss: 240.88, accuracy: 0.8901
epoch: 41, loss: 237.49, accuracy: 0.8927
epoch: 42, loss: 236.74, accuracy: 0.8881
epoch: 43, loss: 237.28, accuracy: 0.8936
epoch: 44, loss: 238.44, accuracy: 0.8998
epoch: 45, loss: 236.12, accuracy: 0.8936
epoch: 46, loss: 234.94, accuracy: 0.896
epoch: 47, loss: 235.31, accuracy: 0.8927
epoch: 48, loss: 234.21, accuracy: 0.8969
epoch: 49, loss: 232.62, accuracy: 0.8882
epoch: 50, loss: 233.06, accuracy: 0.897
epoch: 51, loss: 232.26, accuracy: 0.8988
epoch: 52, loss: 232.43, accuracy: 0.8951
epoch: 53, loss: 232.95, accuracy: 0.8938
epoch: 54, loss: 231.52, accuracy: 0.8909
epoch: 55, loss: 231.76, accuracy: 0.8952
epoch: 56, loss: 231.58, accuracy: 0.8942
epoch: 57, loss: 230.79, accuracy: 0.8965
epoch: 58, loss: 231.26, accuracy: 0.8982
epoch: 59, loss: 227.58, accuracy: 0.8967
epoch: 60, loss: 230.74, accuracy: 0.8994
epoch: 61, loss: 230.03, accuracy: 0.8956
epoch: 62, loss: 229.31, accuracy: 0.895
epoch: 63, loss: 230.08, accuracy: 0.9002
epoch: 64, loss: 229.24, accuracy: 0.8971
Epoch    65: reducing learning rate of group 0 to 5.0000e-03.
epoch: 65, loss: 228.09, accuracy: 0.8932
epoch: 66, loss: 214.05, accuracy: 0.9033
epoch: 67, loss: 212.38, accuracy: 0.9013
epoch: 68, loss: 211.18, accuracy: 0.9007
epoch: 69, loss: 210.8, accuracy: 0.899
epoch: 70, loss: 210.31, accuracy: 0.9023
epoch: 71, loss: 212.17, accuracy: 0.903
epoch: 72, loss: 210.74, accuracy: 0.9019
epoch: 73, loss: 210.7, accuracy: 0.9013
epoch: 74, loss: 209.5, accuracy: 0.8989
epoch: 75, loss: 197.18, accuracy: 0.9064
epoch: 76, loss: 195.17, accuracy: 0.9047
epoch: 77, loss: 194.4, accuracy: 0.9051
epoch: 78, loss: 193.82, accuracy: 0.9073
epoch: 79, loss: 193.6, accuracy: 0.9049
epoch: 80, loss: 193.66, accuracy: 0.9068
epoch: 81, loss: 193.88, accuracy: 0.9071
epoch: 82, loss: 193.22, accuracy: 0.9078
epoch: 83, loss: 193.77, accuracy: 0.9073
epoch: 84, loss: 193.13, accuracy: 0.9073
epoch: 85, loss: 193.17, accuracy: 0.9068
epoch: 86, loss: 193.34, accuracy: 0.9072
epoch: 87, loss: 193.1, accuracy: 0.9071
Epoch    88: reducing learning rate of group 0 to 2.5000e-04.
epoch: 88, loss: 193.5, accuracy: 0.906
epoch: 89, loss: 191.77, accuracy: 0.9062
epoch: 90, loss: 192.06, accuracy: 0.9065
epoch: 91, loss: 191.78, accuracy: 0.907
epoch: 92, loss: 191.49, accuracy: 0.9068
epoch: 93, loss: 192.49, accuracy: 0.9069
epoch: 94, loss: 191.89, accuracy: 0.9069
epoch: 95, loss: 192.21, accuracy: 0.9087
epoch: 96, loss: 191.56, accuracy: 0.9062
epoch: 97, loss: 191.77, accuracy: 0.9079
epoch: 98, loss: 191.15, accuracy: 0.9064
epoch: 99, loss: 190.83, accuracy: 0.9074
time analysis:
    all 1615.1443 s
    train 1613.5649 s
Accuracy of     0 : 88 %
Accuracy of     1 : 92 %
Accuracy of     2 : 90 %
Accuracy of     3 : 83 %
Accuracy of     4 : 81 %
Accuracy of     5 : 100 %
Accuracy of     6 : 71 %
Accuracy of     7 : 100 %
Accuracy of     8 : 96 %
Accuracy of     9 : 100 %
```

## FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=16, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=16, out_channels=16, no_units=1, sigma=0.5)
  (conv3): DAUConv2dZeroMu(in_channels=16, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 16, 28, 28]              32
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
   DAUConv2dZeroMu-4           [-1, 16, 14, 14]             272
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
   DAUConv2dZeroMu-7              [-1, 8, 7, 7]             136
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,250
DAU params: 440
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 598.04, accuracy: 0.729
epoch: 1, loss: 481.18, accuracy: 0.7367
epoch: 2, loss: 462.2, accuracy: 0.396
epoch: 3, loss: 450.15, accuracy: 0.4825
epoch: 4, loss: 441.58, accuracy: 0.513
epoch: 5, loss: 436.55, accuracy: 0.793
epoch: 6, loss: 432.12, accuracy: 0.7881
epoch: 7, loss: 425.66, accuracy: 0.8172
epoch: 8, loss: 422.75, accuracy: 0.7927
epoch: 9, loss: 416.69, accuracy: 0.6651
epoch: 10, loss: 416.04, accuracy: 0.8106
epoch: 11, loss: 413.2, accuracy: 0.6012
epoch: 12, loss: 410.58, accuracy: 0.8061
epoch: 13, loss: 409.41, accuracy: 0.5698
epoch: 14, loss: 407.02, accuracy: 0.7678
epoch: 15, loss: 406.92, accuracy: 0.8057
epoch: 16, loss: 404.1, accuracy: 0.8244
epoch: 17, loss: 402.84, accuracy: 0.7916
epoch: 18, loss: 401.1, accuracy: 0.8182
epoch: 19, loss: 399.47, accuracy: 0.6605
epoch: 20, loss: 400.05, accuracy: 0.7462
epoch: 21, loss: 397.38, accuracy: 0.796
epoch: 22, loss: 398.6, accuracy: 0.8178
epoch: 23, loss: 396.85, accuracy: 0.7962
epoch: 24, loss: 397.3, accuracy: 0.727
epoch: 25, loss: 395.2, accuracy: 0.7962
epoch: 26, loss: 394.17, accuracy: 0.7868
epoch: 27, loss: 394.23, accuracy: 0.8287
epoch: 28, loss: 393.56, accuracy: 0.7561
epoch: 29, loss: 392.14, accuracy: 0.8258
epoch: 30, loss: 392.87, accuracy: 0.8036
epoch: 31, loss: 392.74, accuracy: 0.5862
epoch: 32, loss: 391.48, accuracy: 0.7206
epoch: 33, loss: 390.66, accuracy: 0.8241
epoch: 34, loss: 389.38, accuracy: 0.7225
epoch: 35, loss: 386.4, accuracy: 0.8042
epoch: 36, loss: 389.92, accuracy: 0.6943
epoch: 37, loss: 388.23, accuracy: 0.6559
epoch: 38, loss: 385.86, accuracy: 0.8244
epoch: 39, loss: 386.52, accuracy: 0.7289
epoch: 40, loss: 384.12, accuracy: 0.5794
epoch: 41, loss: 386.5, accuracy: 0.8163
epoch: 42, loss: 384.52, accuracy: 0.829
epoch: 43, loss: 385.76, accuracy: 0.7527
epoch: 44, loss: 382.88, accuracy: 0.8149
epoch: 45, loss: 383.76, accuracy: 0.8192
epoch: 46, loss: 381.31, accuracy: 0.7312
epoch: 47, loss: 380.25, accuracy: 0.8195
epoch: 48, loss: 380.99, accuracy: 0.8223
epoch: 49, loss: 377.45, accuracy: 0.8271
epoch: 50, loss: 380.04, accuracy: 0.8408
epoch: 51, loss: 377.59, accuracy: 0.7663
epoch: 52, loss: 378.78, accuracy: 0.7577
epoch: 53, loss: 380.58, accuracy: 0.8186
epoch: 54, loss: 375.83, accuracy: 0.5895
epoch: 55, loss: 374.54, accuracy: 0.7311
epoch: 56, loss: 375.42, accuracy: 0.6918
epoch: 57, loss: 376.38, accuracy: 0.7252
epoch: 58, loss: 376.76, accuracy: 0.8246
epoch: 59, loss: 373.56, accuracy: 0.729
epoch: 60, loss: 376.08, accuracy: 0.722
epoch: 61, loss: 374.2, accuracy: 0.8198
epoch: 62, loss: 374.7, accuracy: 0.8413
epoch: 63, loss: 374.23, accuracy: 0.6489
epoch: 64, loss: 372.81, accuracy: 0.7885
epoch: 65, loss: 372.54, accuracy: 0.7884
epoch: 66, loss: 371.53, accuracy: 0.8467
epoch: 67, loss: 372.05, accuracy: 0.8089
epoch: 68, loss: 372.36, accuracy: 0.8122
epoch: 69, loss: 371.63, accuracy: 0.5516
epoch: 70, loss: 370.22, accuracy: 0.8416
epoch: 71, loss: 371.8, accuracy: 0.8175
epoch: 72, loss: 369.6, accuracy: 0.7702
epoch: 73, loss: 369.2, accuracy: 0.8265
epoch: 74, loss: 369.08, accuracy: 0.8328
epoch: 75, loss: 348.28, accuracy: 0.8518
epoch: 76, loss: 346.91, accuracy: 0.8545
epoch: 77, loss: 346.42, accuracy: 0.8562
epoch: 78, loss: 346.01, accuracy: 0.8521
epoch: 79, loss: 345.46, accuracy: 0.8562
epoch: 80, loss: 345.74, accuracy: 0.8528
epoch: 81, loss: 345.86, accuracy: 0.8546
epoch: 82, loss: 345.03, accuracy: 0.8514
epoch: 83, loss: 344.5, accuracy: 0.8575
epoch: 84, loss: 344.56, accuracy: 0.8559
epoch: 85, loss: 344.71, accuracy: 0.8526
epoch: 86, loss: 344.7, accuracy: 0.8522
epoch: 87, loss: 343.99, accuracy: 0.8542
epoch: 88, loss: 344.7, accuracy: 0.8525
epoch: 89, loss: 343.68, accuracy: 0.8575
epoch: 90, loss: 344.28, accuracy: 0.8554
epoch: 91, loss: 343.64, accuracy: 0.8525
epoch: 92, loss: 343.66, accuracy: 0.8558
epoch: 93, loss: 343.89, accuracy: 0.8533
epoch: 94, loss: 343.27, accuracy: 0.8539
epoch: 95, loss: 342.94, accuracy: 0.8532
epoch: 96, loss: 343.78, accuracy: 0.8561
epoch: 97, loss: 344.17, accuracy: 0.8534
epoch: 98, loss: 343.05, accuracy: 0.854
epoch: 99, loss: 341.74, accuracy: 0.8545
time analysis:
    all 760.5484 s
    train 758.8264 s
Accuracy of     0 : 76 %
Accuracy of     1 : 95 %
Accuracy of     2 : 81 %
Accuracy of     3 : 84 %
Accuracy of     4 : 66 %
Accuracy of     5 : 100 %
Accuracy of     6 : 60 %
Accuracy of     7 : 94 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=16, no_units=1, sigma=1)
  (conv2): DAUConv2dZeroMu(in_channels=16, out_channels=16, no_units=1, sigma=1)
  (conv3): DAUConv2dZeroMu(in_channels=16, out_channels=8, no_units=1, sigma=1)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 16, 28, 28]              32
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
   DAUConv2dZeroMu-4           [-1, 16, 14, 14]             272
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
   DAUConv2dZeroMu-7              [-1, 8, 7, 7]             136
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,250
DAU params: 440
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 671.55, accuracy: 0.5095
epoch: 1, loss: 550.02, accuracy: 0.7698
epoch: 2, loss: 523.11, accuracy: 0.6884
epoch: 3, loss: 502.83, accuracy: 0.6732
epoch: 4, loss: 492.61, accuracy: 0.7549
epoch: 5, loss: 477.36, accuracy: 0.5933
epoch: 6, loss: 475.38, accuracy: 0.6764
epoch: 7, loss: 465.31, accuracy: 0.7331
epoch: 8, loss: 462.14, accuracy: 0.532
epoch: 9, loss: 456.59, accuracy: 0.6914
epoch: 10, loss: 455.88, accuracy: 0.6798
epoch: 11, loss: 451.83, accuracy: 0.6871
epoch: 12, loss: 449.7, accuracy: 0.729
epoch: 13, loss: 446.58, accuracy: 0.752
epoch: 14, loss: 446.01, accuracy: 0.7428
epoch: 15, loss: 443.8, accuracy: 0.7353
epoch: 16, loss: 444.29, accuracy: 0.6569
epoch: 17, loss: 441.97, accuracy: 0.8026
epoch: 18, loss: 441.21, accuracy: 0.764
epoch: 19, loss: 439.19, accuracy: 0.8039
epoch: 20, loss: 437.34, accuracy: 0.5964
epoch: 21, loss: 437.75, accuracy: 0.7097
epoch: 22, loss: 436.0, accuracy: 0.6735
epoch: 23, loss: 435.18, accuracy: 0.7054
epoch: 24, loss: 431.76, accuracy: 0.6679
epoch: 25, loss: 431.7, accuracy: 0.6146
epoch: 26, loss: 431.58, accuracy: 0.8105
epoch: 27, loss: 428.48, accuracy: 0.7835
epoch: 28, loss: 428.72, accuracy: 0.7759
epoch: 29, loss: 426.85, accuracy: 0.7537
epoch: 30, loss: 426.57, accuracy: 0.5926
epoch: 31, loss: 428.3, accuracy: 0.8082
epoch: 32, loss: 425.55, accuracy: 0.7056
epoch: 33, loss: 426.59, accuracy: 0.5408
epoch: 34, loss: 424.48, accuracy: 0.7411
epoch: 35, loss: 423.72, accuracy: 0.7614
epoch: 36, loss: 421.94, accuracy: 0.7741
epoch: 37, loss: 421.85, accuracy: 0.657
epoch: 38, loss: 421.56, accuracy: 0.7907
epoch: 39, loss: 421.04, accuracy: 0.71
epoch: 40, loss: 421.42, accuracy: 0.6791
epoch: 41, loss: 418.9, accuracy: 0.804
epoch: 42, loss: 420.47, accuracy: 0.7705
epoch: 43, loss: 419.88, accuracy: 0.7991
epoch: 44, loss: 418.94, accuracy: 0.8033
epoch: 45, loss: 417.03, accuracy: 0.7872
epoch: 46, loss: 416.33, accuracy: 0.7985
epoch: 47, loss: 415.64, accuracy: 0.7202
epoch: 48, loss: 415.7, accuracy: 0.7971
epoch: 49, loss: 418.91, accuracy: 0.8124
epoch: 50, loss: 416.53, accuracy: 0.7865
epoch: 51, loss: 415.65, accuracy: 0.7808
epoch: 52, loss: 414.1, accuracy: 0.7421
epoch: 53, loss: 415.89, accuracy: 0.7399
epoch: 54, loss: 416.15, accuracy: 0.8101
epoch: 55, loss: 414.26, accuracy: 0.7055
epoch: 56, loss: 416.01, accuracy: 0.7393
epoch: 57, loss: 413.21, accuracy: 0.62
epoch: 58, loss: 413.24, accuracy: 0.6023
epoch: 59, loss: 413.65, accuracy: 0.7977
epoch: 60, loss: 413.12, accuracy: 0.7968
epoch: 61, loss: 413.71, accuracy: 0.7524
epoch: 62, loss: 412.75, accuracy: 0.7071
epoch: 63, loss: 412.56, accuracy: 0.8315
epoch: 64, loss: 411.85, accuracy: 0.7219
epoch: 65, loss: 411.99, accuracy: 0.5888
epoch: 66, loss: 410.83, accuracy: 0.8122
epoch: 67, loss: 409.83, accuracy: 0.7251
epoch: 68, loss: 410.52, accuracy: 0.7581
epoch: 69, loss: 411.26, accuracy: 0.7818
epoch: 70, loss: 407.88, accuracy: 0.7697
epoch: 71, loss: 407.39, accuracy: 0.8082
epoch: 72, loss: 409.66, accuracy: 0.7833
epoch: 73, loss: 408.0, accuracy: 0.3989
epoch: 74, loss: 407.55, accuracy: 0.6931
epoch: 75, loss: 384.7, accuracy: 0.8426
epoch: 76, loss: 383.29, accuracy: 0.8389
epoch: 77, loss: 382.38, accuracy: 0.8395
epoch: 78, loss: 382.09, accuracy: 0.8438
epoch: 79, loss: 380.88, accuracy: 0.8364
epoch: 80, loss: 381.2, accuracy: 0.8439
epoch: 81, loss: 381.91, accuracy: 0.832
epoch: 82, loss: 379.49, accuracy: 0.8394
epoch: 83, loss: 381.17, accuracy: 0.8332
epoch: 84, loss: 379.49, accuracy: 0.8425
epoch: 85, loss: 380.41, accuracy: 0.8399
epoch: 86, loss: 379.94, accuracy: 0.8439
epoch: 87, loss: 378.96, accuracy: 0.8443
epoch: 88, loss: 380.29, accuracy: 0.8433
epoch: 89, loss: 379.12, accuracy: 0.8426
epoch: 90, loss: 378.54, accuracy: 0.842
epoch: 91, loss: 379.34, accuracy: 0.8432
epoch: 92, loss: 379.0, accuracy: 0.842
epoch: 93, loss: 378.93, accuracy: 0.8398
epoch: 94, loss: 378.7, accuracy: 0.841
epoch: 95, loss: 378.91, accuracy: 0.8434
epoch: 96, loss: 377.21, accuracy: 0.8416
epoch: 97, loss: 378.55, accuracy: 0.842
epoch: 98, loss: 377.92, accuracy: 0.8382
epoch: 99, loss: 378.63, accuracy: 0.8428
time analysis:
    all 757.4996 s
    train 756.5731 s
Accuracy of     0 : 78 %
Accuracy of     1 : 93 %
Accuracy of     2 : 72 %
Accuracy of     3 : 79 %
Accuracy of     4 : 71 %
Accuracy of     5 : 91 %
Accuracy of     6 : 47 %
Accuracy of     7 : 96 %
Accuracy of     8 : 96 %
Accuracy of     9 : 98 %
```

## FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 1.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=16, no_units=1, sigma=1.5)
  (conv2): DAUConv2dZeroMu(in_channels=16, out_channels=16, no_units=1, sigma=1.5)
  (conv3): DAUConv2dZeroMu(in_channels=16, out_channels=8, no_units=1, sigma=1.5)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 16, 28, 28]              32
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
   DAUConv2dZeroMu-4           [-1, 16, 14, 14]             272
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
   DAUConv2dZeroMu-7              [-1, 8, 7, 7]             136
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,250
DAU params: 440
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 761.12, accuracy: 0.6446
epoch: 1, loss: 619.77, accuracy: 0.7204
epoch: 2, loss: 577.08, accuracy: 0.5831
epoch: 3, loss: 552.34, accuracy: 0.7464
epoch: 4, loss: 542.68, accuracy: 0.6478
epoch: 5, loss: 528.5, accuracy: 0.6837
epoch: 6, loss: 518.75, accuracy: 0.7531
epoch: 7, loss: 515.04, accuracy: 0.7295
epoch: 8, loss: 509.97, accuracy: 0.6077
epoch: 9, loss: 505.84, accuracy: 0.7106
epoch: 10, loss: 504.39, accuracy: 0.7429
epoch: 11, loss: 502.6, accuracy: 0.6849
epoch: 12, loss: 498.98, accuracy: 0.7534
epoch: 13, loss: 492.35, accuracy: 0.6771
epoch: 14, loss: 496.27, accuracy: 0.7799
epoch: 15, loss: 491.42, accuracy: 0.6442
epoch: 16, loss: 490.76, accuracy: 0.7456
epoch: 17, loss: 487.68, accuracy: 0.7549
epoch: 18, loss: 485.52, accuracy: 0.7548
epoch: 19, loss: 483.63, accuracy: 0.7627
epoch: 20, loss: 483.98, accuracy: 0.7904
epoch: 21, loss: 481.65, accuracy: 0.6334
epoch: 22, loss: 482.2, accuracy: 0.7611
epoch: 23, loss: 477.38, accuracy: 0.7628
epoch: 24, loss: 477.51, accuracy: 0.7689
epoch: 25, loss: 475.65, accuracy: 0.771
epoch: 26, loss: 477.86, accuracy: 0.7435
epoch: 27, loss: 474.64, accuracy: 0.6647
epoch: 28, loss: 474.94, accuracy: 0.8036
epoch: 29, loss: 473.28, accuracy: 0.533
epoch: 30, loss: 473.73, accuracy: 0.7842
epoch: 31, loss: 471.03, accuracy: 0.7524
epoch: 32, loss: 470.41, accuracy: 0.704
epoch: 33, loss: 470.44, accuracy: 0.7107
epoch: 34, loss: 468.99, accuracy: 0.593
epoch: 35, loss: 469.36, accuracy: 0.7084
epoch: 36, loss: 468.32, accuracy: 0.7607
epoch: 37, loss: 468.83, accuracy: 0.7614
epoch: 38, loss: 465.55, accuracy: 0.7061
epoch: 39, loss: 466.33, accuracy: 0.7163
epoch: 40, loss: 466.7, accuracy: 0.7432
epoch: 41, loss: 465.54, accuracy: 0.7259
epoch: 42, loss: 466.57, accuracy: 0.6728
epoch: 43, loss: 466.46, accuracy: 0.6997
epoch: 44, loss: 462.26, accuracy: 0.766
epoch: 45, loss: 463.04, accuracy: 0.7088
epoch: 46, loss: 462.12, accuracy: 0.4559
epoch: 47, loss: 461.54, accuracy: 0.6636
epoch: 48, loss: 461.73, accuracy: 0.5819
epoch: 49, loss: 461.3, accuracy: 0.4897
epoch: 50, loss: 461.83, accuracy: 0.6456
epoch: 51, loss: 462.54, accuracy: 0.7673
epoch: 52, loss: 458.47, accuracy: 0.777
epoch: 53, loss: 460.27, accuracy: 0.7395
epoch: 54, loss: 458.15, accuracy: 0.6462
epoch: 55, loss: 460.52, accuracy: 0.7898
epoch: 56, loss: 459.7, accuracy: 0.5352
epoch: 57, loss: 458.99, accuracy: 0.648
Epoch    58: reducing learning rate of group 0 to 5.0000e-03.
epoch: 58, loss: 459.73, accuracy: 0.5756
epoch: 59, loss: 441.99, accuracy: 0.6324
epoch: 60, loss: 443.58, accuracy: 0.7807
epoch: 61, loss: 439.43, accuracy: 0.7397
epoch: 62, loss: 441.33, accuracy: 0.745
epoch: 63, loss: 442.2, accuracy: 0.7932
epoch: 64, loss: 440.5, accuracy: 0.7099
epoch: 65, loss: 441.33, accuracy: 0.5462
epoch: 66, loss: 440.78, accuracy: 0.6709
epoch: 67, loss: 441.33, accuracy: 0.8017
epoch: 68, loss: 439.24, accuracy: 0.8063
Epoch    69: reducing learning rate of group 0 to 2.5000e-03.
epoch: 69, loss: 440.87, accuracy: 0.7989
epoch: 70, loss: 429.66, accuracy: 0.8169
epoch: 71, loss: 429.63, accuracy: 0.733
epoch: 72, loss: 429.63, accuracy: 0.8205
epoch: 73, loss: 429.72, accuracy: 0.8147
epoch: 74, loss: 428.79, accuracy: 0.7986
epoch: 75, loss: 420.55, accuracy: 0.8258
epoch: 76, loss: 420.53, accuracy: 0.8262
epoch: 77, loss: 421.72, accuracy: 0.8259
epoch: 78, loss: 420.47, accuracy: 0.8255
epoch: 79, loss: 420.3, accuracy: 0.8248
epoch: 80, loss: 419.82, accuracy: 0.8241
epoch: 81, loss: 421.33, accuracy: 0.824
epoch: 82, loss: 421.59, accuracy: 0.8256
epoch: 83, loss: 420.96, accuracy: 0.8252
epoch: 84, loss: 420.0, accuracy: 0.8241
epoch: 85, loss: 420.16, accuracy: 0.8273
Epoch    86: reducing learning rate of group 0 to 1.2500e-04.
epoch: 86, loss: 421.33, accuracy: 0.8248
epoch: 87, loss: 420.16, accuracy: 0.8257
epoch: 88, loss: 420.2, accuracy: 0.8263
epoch: 89, loss: 419.52, accuracy: 0.8269
epoch: 90, loss: 418.48, accuracy: 0.8266
epoch: 91, loss: 419.3, accuracy: 0.8264
epoch: 92, loss: 419.55, accuracy: 0.8263
epoch: 93, loss: 418.33, accuracy: 0.8251
epoch: 94, loss: 419.36, accuracy: 0.8278
epoch: 95, loss: 418.58, accuracy: 0.8249
epoch: 96, loss: 419.99, accuracy: 0.8263
Epoch    97: reducing learning rate of group 0 to 1.0000e-04.
epoch: 97, loss: 419.51, accuracy: 0.8243
epoch: 98, loss: 419.4, accuracy: 0.825
epoch: 99, loss: 418.7, accuracy: 0.8254
time analysis:
    all 779.1767 s
    train 778.2604 s
Accuracy of     0 : 76 %
Accuracy of     1 : 92 %
Accuracy of     2 : 74 %
Accuracy of     3 : 77 %
Accuracy of     4 : 56 %
Accuracy of     5 : 95 %
Accuracy of     6 : 43 %
Accuracy of     7 : 94 %
Accuracy of     8 : 95 %
Accuracy of     9 : 93 %
```

## FashionMNIST, 3_layers, DAUConv2dZeroMu, units: 1, sigma: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=1, out_channels=16, no_units=1, sigma=2)
  (conv2): DAUConv2dZeroMu(in_channels=16, out_channels=16, no_units=1, sigma=2)
  (conv3): DAUConv2dZeroMu(in_channels=16, out_channels=8, no_units=1, sigma=2)
  (norm1): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm3): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool2): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc): Linear(in_features=72, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 16, 28, 28]              32
       BatchNorm2d-2           [-1, 16, 28, 28]              32
         MaxPool2d-3           [-1, 16, 14, 14]               0
   DAUConv2dZeroMu-4           [-1, 16, 14, 14]             272
       BatchNorm2d-5           [-1, 16, 14, 14]              32
         MaxPool2d-6             [-1, 16, 7, 7]               0
   DAUConv2dZeroMu-7              [-1, 8, 7, 7]             136
       BatchNorm2d-8              [-1, 8, 7, 7]              16
         MaxPool2d-9              [-1, 8, 3, 3]               0
           Linear-10                   [-1, 10]             730
================================================================
total params: 1,250
DAU params: 440
other params: 810
----------------------------------------------------------------
epoch: 0, loss: 866.92, accuracy: 0.6909
epoch: 1, loss: 679.65, accuracy: 0.6785
epoch: 2, loss: 649.17, accuracy: 0.3784
epoch: 3, loss: 628.16, accuracy: 0.6898
epoch: 4, loss: 611.55, accuracy: 0.6818
epoch: 5, loss: 602.45, accuracy: 0.6214
epoch: 6, loss: 595.83, accuracy: 0.7367
epoch: 7, loss: 589.59, accuracy: 0.6618
epoch: 8, loss: 584.53, accuracy: 0.7297
epoch: 9, loss: 581.63, accuracy: 0.7489
epoch: 10, loss: 580.65, accuracy: 0.678
epoch: 11, loss: 574.22, accuracy: 0.7457
epoch: 12, loss: 570.61, accuracy: 0.6628
epoch: 13, loss: 570.3, accuracy: 0.6545
epoch: 14, loss: 564.96, accuracy: 0.5888
epoch: 15, loss: 564.87, accuracy: 0.6513
epoch: 16, loss: 560.87, accuracy: 0.7045
epoch: 17, loss: 561.09, accuracy: 0.7012
epoch: 18, loss: 559.29, accuracy: 0.7301
epoch: 19, loss: 556.36, accuracy: 0.5233
epoch: 20, loss: 554.49, accuracy: 0.7209
epoch: 21, loss: 555.23, accuracy: 0.7233
epoch: 22, loss: 550.01, accuracy: 0.6095
epoch: 23, loss: 546.04, accuracy: 0.6767
epoch: 24, loss: 548.23, accuracy: 0.7033
epoch: 25, loss: 545.38, accuracy: 0.6035
epoch: 26, loss: 547.45, accuracy: 0.7224
epoch: 27, loss: 543.15, accuracy: 0.5918
epoch: 28, loss: 544.45, accuracy: 0.6198
epoch: 29, loss: 540.66, accuracy: 0.6867
epoch: 30, loss: 540.67, accuracy: 0.7679
epoch: 31, loss: 539.6, accuracy: 0.547
epoch: 32, loss: 542.06, accuracy: 0.7485
epoch: 33, loss: 539.98, accuracy: 0.6912
epoch: 34, loss: 537.56, accuracy: 0.7319
epoch: 35, loss: 539.25, accuracy: 0.6932
epoch: 36, loss: 538.93, accuracy: 0.7403
epoch: 37, loss: 536.96, accuracy: 0.7119
epoch: 38, loss: 535.75, accuracy: 0.7251
epoch: 39, loss: 534.71, accuracy: 0.7131
epoch: 40, loss: 532.27, accuracy: 0.7353
epoch: 41, loss: 531.4, accuracy: 0.7542
epoch: 42, loss: 534.46, accuracy: 0.68
epoch: 43, loss: 530.77, accuracy: 0.7534
epoch: 44, loss: 530.15, accuracy: 0.7685
epoch: 45, loss: 530.39, accuracy: 0.76
epoch: 46, loss: 530.63, accuracy: 0.7093
epoch: 47, loss: 530.03, accuracy: 0.7549
epoch: 48, loss: 525.78, accuracy: 0.6731
epoch: 49, loss: 528.14, accuracy: 0.6493
epoch: 50, loss: 528.77, accuracy: 0.7657
epoch: 51, loss: 527.17, accuracy: 0.7155
epoch: 52, loss: 528.89, accuracy: 0.6469
epoch: 53, loss: 526.51, accuracy: 0.6392
epoch: 54, loss: 524.92, accuracy: 0.7364
epoch: 55, loss: 521.95, accuracy: 0.7248
epoch: 56, loss: 523.89, accuracy: 0.7684
epoch: 57, loss: 524.91, accuracy: 0.592
epoch: 58, loss: 522.75, accuracy: 0.5799
epoch: 59, loss: 522.37, accuracy: 0.5414
epoch: 60, loss: 522.54, accuracy: 0.718
epoch: 61, loss: 520.49, accuracy: 0.7162
epoch: 62, loss: 522.45, accuracy: 0.719
epoch: 63, loss: 522.35, accuracy: 0.3974
epoch: 64, loss: 520.75, accuracy: 0.7499
epoch: 65, loss: 520.46, accuracy: 0.7654
epoch: 66, loss: 519.9, accuracy: 0.7226
epoch: 67, loss: 519.07, accuracy: 0.7041
epoch: 68, loss: 519.31, accuracy: 0.7238
epoch: 69, loss: 518.78, accuracy: 0.6562
epoch: 70, loss: 518.57, accuracy: 0.7437
epoch: 71, loss: 518.45, accuracy: 0.7265
epoch: 72, loss: 519.52, accuracy: 0.6219
epoch: 73, loss: 516.49, accuracy: 0.734
epoch: 74, loss: 515.48, accuracy: 0.7006
epoch: 75, loss: 490.72, accuracy: 0.7967
epoch: 76, loss: 489.7, accuracy: 0.7982
epoch: 77, loss: 488.09, accuracy: 0.7995
epoch: 78, loss: 486.93, accuracy: 0.794
epoch: 79, loss: 486.63, accuracy: 0.8024
epoch: 80, loss: 486.65, accuracy: 0.797
epoch: 81, loss: 487.41, accuracy: 0.7938
epoch: 82, loss: 486.54, accuracy: 0.8007
epoch: 83, loss: 486.26, accuracy: 0.7968
epoch: 84, loss: 484.79, accuracy: 0.7989
epoch: 85, loss: 486.6, accuracy: 0.8014
epoch: 86, loss: 485.28, accuracy: 0.8002
epoch: 87, loss: 486.0, accuracy: 0.8
epoch: 88, loss: 483.94, accuracy: 0.7999
epoch: 89, loss: 486.96, accuracy: 0.7964
epoch: 90, loss: 484.53, accuracy: 0.8006
epoch: 91, loss: 484.81, accuracy: 0.8007
epoch: 92, loss: 484.5, accuracy: 0.7973
epoch: 93, loss: 485.81, accuracy: 0.7899
Epoch    94: reducing learning rate of group 0 to 5.0000e-04.
epoch: 94, loss: 484.8, accuracy: 0.7975
epoch: 95, loss: 482.48, accuracy: 0.8006
epoch: 96, loss: 482.55, accuracy: 0.8016
epoch: 97, loss: 483.49, accuracy: 0.8039
epoch: 98, loss: 482.09, accuracy: 0.8014
epoch: 99, loss: 483.07, accuracy: 0.8012
time analysis:
    all 783.2448 s
    train 782.2913 s
Accuracy of     0 : 78 %
Accuracy of     1 : 92 %
Accuracy of     2 : 70 %
Accuracy of     3 : 77 %
Accuracy of     4 : 58 %
Accuracy of     5 : 86 %
Accuracy of     6 : 38 %
Accuracy of     7 : 87 %
Accuracy of     8 : 93 %
Accuracy of     9 : 94 %
```

## CIFAR10, small_fc, Conv2d
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (conv2): Conv2d(64, 8, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
            Conv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
            Conv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 0
other params: 11,682
----------------------------------------------------------------
epoch: 0, loss: 1099.59, accuracy: 0.5566
epoch: 1, loss: 908.49, accuracy: 0.5912
epoch: 2, loss: 852.76, accuracy: 0.6087
epoch: 3, loss: 824.05, accuracy: 0.6354
epoch: 4, loss: 804.5, accuracy: 0.6364
epoch: 5, loss: 782.81, accuracy: 0.6301
epoch: 6, loss: 770.98, accuracy: 0.6264
epoch: 7, loss: 758.9, accuracy: 0.6313
epoch: 8, loss: 757.3, accuracy: 0.6568
epoch: 9, loss: 742.82, accuracy: 0.6403
epoch: 10, loss: 734.06, accuracy: 0.6546
epoch: 11, loss: 728.59, accuracy: 0.6507
epoch: 12, loss: 722.84, accuracy: 0.6523
epoch: 13, loss: 718.54, accuracy: 0.6517
epoch: 14, loss: 714.14, accuracy: 0.6455
epoch: 15, loss: 711.07, accuracy: 0.6509
epoch: 16, loss: 704.02, accuracy: 0.6693
epoch: 17, loss: 699.34, accuracy: 0.6654
epoch: 18, loss: 696.84, accuracy: 0.6493
epoch: 19, loss: 689.09, accuracy: 0.6632
epoch: 20, loss: 693.87, accuracy: 0.662
epoch: 21, loss: 685.37, accuracy: 0.6714
epoch: 22, loss: 684.56, accuracy: 0.6693
epoch: 23, loss: 677.54, accuracy: 0.6558
epoch: 24, loss: 677.33, accuracy: 0.6725
epoch: 25, loss: 674.32, accuracy: 0.6591
epoch: 26, loss: 675.26, accuracy: 0.6587
epoch: 27, loss: 668.87, accuracy: 0.6655
epoch: 28, loss: 664.3, accuracy: 0.6499
epoch: 29, loss: 664.11, accuracy: 0.6745
epoch: 30, loss: 662.35, accuracy: 0.6673
epoch: 31, loss: 660.34, accuracy: 0.6809
epoch: 32, loss: 660.12, accuracy: 0.6505
epoch: 33, loss: 656.85, accuracy: 0.6687
epoch: 34, loss: 656.24, accuracy: 0.6747
epoch: 35, loss: 656.63, accuracy: 0.6845
epoch: 36, loss: 656.16, accuracy: 0.669
epoch: 37, loss: 653.35, accuracy: 0.6719
epoch: 38, loss: 650.5, accuracy: 0.6777
epoch: 39, loss: 649.26, accuracy: 0.6731
epoch: 40, loss: 647.15, accuracy: 0.6826
epoch: 41, loss: 646.48, accuracy: 0.67
epoch: 42, loss: 644.28, accuracy: 0.6557
epoch: 43, loss: 648.46, accuracy: 0.6704
epoch: 44, loss: 643.56, accuracy: 0.6656
epoch: 45, loss: 642.94, accuracy: 0.6649
epoch: 46, loss: 639.17, accuracy: 0.6693
epoch: 47, loss: 640.41, accuracy: 0.6742
epoch: 48, loss: 636.9, accuracy: 0.6812
epoch: 49, loss: 637.22, accuracy: 0.6838
epoch: 50, loss: 635.54, accuracy: 0.6775
epoch: 51, loss: 635.77, accuracy: 0.6827
epoch: 52, loss: 634.61, accuracy: 0.6765
epoch: 53, loss: 633.28, accuracy: 0.6715
epoch: 54, loss: 635.38, accuracy: 0.6743
epoch: 55, loss: 631.34, accuracy: 0.6706
epoch: 56, loss: 630.18, accuracy: 0.6531
epoch: 57, loss: 631.06, accuracy: 0.6663
epoch: 58, loss: 629.82, accuracy: 0.676
epoch: 59, loss: 634.73, accuracy: 0.6813
epoch: 60, loss: 629.37, accuracy: 0.6757
epoch: 61, loss: 623.63, accuracy: 0.6759
epoch: 62, loss: 621.83, accuracy: 0.6824
epoch: 63, loss: 621.23, accuracy: 0.6819
epoch: 64, loss: 622.95, accuracy: 0.6704
epoch: 65, loss: 623.37, accuracy: 0.6741
epoch: 66, loss: 619.35, accuracy: 0.674
epoch: 67, loss: 623.51, accuracy: 0.6731
epoch: 68, loss: 623.32, accuracy: 0.6743
epoch: 69, loss: 620.25, accuracy: 0.6709
epoch: 70, loss: 621.18, accuracy: 0.6606
epoch: 71, loss: 617.15, accuracy: 0.6736
epoch: 72, loss: 624.13, accuracy: 0.6756
epoch: 73, loss: 617.22, accuracy: 0.6761
epoch: 74, loss: 615.35, accuracy: 0.6732
epoch: 75, loss: 555.48, accuracy: 0.6956
epoch: 76, loss: 549.07, accuracy: 0.6947
epoch: 77, loss: 546.32, accuracy: 0.6967
epoch: 78, loss: 545.3, accuracy: 0.6955
epoch: 79, loss: 544.09, accuracy: 0.6981
epoch: 80, loss: 543.97, accuracy: 0.6962
epoch: 81, loss: 542.17, accuracy: 0.695
epoch: 82, loss: 543.59, accuracy: 0.6966
epoch: 83, loss: 541.38, accuracy: 0.6963
epoch: 84, loss: 541.87, accuracy: 0.695
epoch: 85, loss: 541.93, accuracy: 0.6934
epoch: 86, loss: 540.78, accuracy: 0.6946
epoch: 87, loss: 539.98, accuracy: 0.6942
epoch: 88, loss: 539.6, accuracy: 0.6968
epoch: 89, loss: 540.01, accuracy: 0.6964
epoch: 90, loss: 539.58, accuracy: 0.6932
epoch: 91, loss: 538.38, accuracy: 0.6937
epoch: 92, loss: 538.33, accuracy: 0.6948
epoch: 93, loss: 538.15, accuracy: 0.6934
epoch: 94, loss: 536.78, accuracy: 0.6957
epoch: 95, loss: 538.3, accuracy: 0.6896
epoch: 96, loss: 537.03, accuracy: 0.6921
epoch: 97, loss: 537.46, accuracy: 0.6955
epoch: 98, loss: 538.26, accuracy: 0.694
epoch: 99, loss: 536.62, accuracy: 0.6951
time analysis:
    all 552.7974 s
    train 551.0335 s
Accuracy of     0 : 66 %
Accuracy of     1 : 78 %
Accuracy of     2 : 56 %
Accuracy of     3 : 53 %
Accuracy of     4 : 54 %
Accuracy of     5 : 61 %
Accuracy of     6 : 78 %
Accuracy of     7 : 70 %
Accuracy of     8 : 84 %
Accuracy of     9 : 75 %
```

## CIFAR10, small_fc, DAUConv2d, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,216
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           3,080
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 9,570
DAU params: 4,296
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1135.41, accuracy: 0.5426
epoch: 1, loss: 966.62, accuracy: 0.5714
epoch: 2, loss: 906.56, accuracy: 0.5806
epoch: 3, loss: 863.67, accuracy: 0.5954
epoch: 4, loss: 835.25, accuracy: 0.6189
epoch: 5, loss: 817.19, accuracy: 0.6216
epoch: 6, loss: 801.52, accuracy: 0.6241
epoch: 7, loss: 786.68, accuracy: 0.64
epoch: 8, loss: 777.14, accuracy: 0.6226
epoch: 9, loss: 770.67, accuracy: 0.6415
epoch: 10, loss: 755.74, accuracy: 0.6479
epoch: 11, loss: 752.19, accuracy: 0.6402
epoch: 12, loss: 741.52, accuracy: 0.638
epoch: 13, loss: 741.6, accuracy: 0.642
epoch: 14, loss: 734.27, accuracy: 0.6546
epoch: 15, loss: 727.22, accuracy: 0.6479
epoch: 16, loss: 725.14, accuracy: 0.6618
epoch: 17, loss: 718.48, accuracy: 0.657
epoch: 18, loss: 716.45, accuracy: 0.6696
epoch: 19, loss: 711.35, accuracy: 0.6387
epoch: 20, loss: 711.49, accuracy: 0.6578
epoch: 21, loss: 708.92, accuracy: 0.6674
epoch: 22, loss: 708.78, accuracy: 0.6624
epoch: 23, loss: 704.6, accuracy: 0.6674
epoch: 24, loss: 703.38, accuracy: 0.6533
epoch: 25, loss: 700.69, accuracy: 0.665
epoch: 26, loss: 695.57, accuracy: 0.6344
epoch: 27, loss: 696.94, accuracy: 0.6667
epoch: 28, loss: 695.27, accuracy: 0.6604
epoch: 29, loss: 693.84, accuracy: 0.6571
epoch: 30, loss: 689.0, accuracy: 0.6517
epoch: 31, loss: 690.48, accuracy: 0.6735
epoch: 32, loss: 687.98, accuracy: 0.6607
epoch: 33, loss: 684.97, accuracy: 0.6691
epoch: 34, loss: 682.7, accuracy: 0.6715
epoch: 35, loss: 681.27, accuracy: 0.6715
epoch: 36, loss: 679.21, accuracy: 0.6502
epoch: 37, loss: 682.11, accuracy: 0.6532
epoch: 38, loss: 679.15, accuracy: 0.6661
epoch: 39, loss: 677.8, accuracy: 0.6729
epoch: 40, loss: 677.14, accuracy: 0.6557
epoch: 41, loss: 671.99, accuracy: 0.6673
epoch: 42, loss: 671.39, accuracy: 0.677
epoch: 43, loss: 675.09, accuracy: 0.6656
epoch: 44, loss: 673.19, accuracy: 0.68
epoch: 45, loss: 671.11, accuracy: 0.6657
epoch: 46, loss: 670.91, accuracy: 0.6752
epoch: 47, loss: 672.97, accuracy: 0.6754
epoch: 48, loss: 668.76, accuracy: 0.6699
epoch: 49, loss: 666.48, accuracy: 0.6736
epoch: 50, loss: 667.14, accuracy: 0.6776
epoch: 51, loss: 667.48, accuracy: 0.6717
epoch: 52, loss: 665.85, accuracy: 0.6731
epoch: 53, loss: 667.32, accuracy: 0.6814
epoch: 54, loss: 662.25, accuracy: 0.6721
epoch: 55, loss: 662.99, accuracy: 0.6704
epoch: 56, loss: 659.99, accuracy: 0.6612
epoch: 57, loss: 663.06, accuracy: 0.6712
epoch: 58, loss: 663.73, accuracy: 0.6906
epoch: 59, loss: 660.42, accuracy: 0.6662
epoch: 60, loss: 657.6, accuracy: 0.6665
epoch: 61, loss: 658.47, accuracy: 0.6754
epoch: 62, loss: 656.9, accuracy: 0.6813
epoch: 63, loss: 656.0, accuracy: 0.6688
epoch: 64, loss: 656.88, accuracy: 0.6745
epoch: 65, loss: 657.27, accuracy: 0.6769
epoch: 66, loss: 654.95, accuracy: 0.6657
epoch: 67, loss: 655.65, accuracy: 0.6794
epoch: 68, loss: 654.01, accuracy: 0.6818
epoch: 69, loss: 656.02, accuracy: 0.6667
epoch: 70, loss: 651.94, accuracy: 0.6789
epoch: 71, loss: 653.86, accuracy: 0.6731
epoch: 72, loss: 650.16, accuracy: 0.6735
epoch: 73, loss: 649.03, accuracy: 0.6657
epoch: 74, loss: 649.96, accuracy: 0.6659
epoch: 75, loss: 586.86, accuracy: 0.6956
epoch: 76, loss: 576.6, accuracy: 0.6982
epoch: 77, loss: 573.32, accuracy: 0.6978
epoch: 78, loss: 571.51, accuracy: 0.6995
epoch: 79, loss: 570.07, accuracy: 0.697
epoch: 80, loss: 569.63, accuracy: 0.6966
epoch: 81, loss: 568.53, accuracy: 0.6983
epoch: 82, loss: 568.03, accuracy: 0.6962
epoch: 83, loss: 568.47, accuracy: 0.6977
epoch: 84, loss: 566.39, accuracy: 0.6948
epoch: 85, loss: 567.08, accuracy: 0.6986
epoch: 86, loss: 565.98, accuracy: 0.6989
epoch: 87, loss: 566.04, accuracy: 0.7027
epoch: 88, loss: 563.91, accuracy: 0.6969
epoch: 89, loss: 563.38, accuracy: 0.7002
epoch: 90, loss: 562.82, accuracy: 0.6964
epoch: 91, loss: 565.13, accuracy: 0.6977
epoch: 92, loss: 563.97, accuracy: 0.6993
epoch: 93, loss: 563.46, accuracy: 0.6949
epoch: 94, loss: 562.58, accuracy: 0.6959
epoch: 95, loss: 563.26, accuracy: 0.6947
epoch: 96, loss: 562.25, accuracy: 0.6987
epoch: 97, loss: 561.75, accuracy: 0.698
epoch: 98, loss: 562.11, accuracy: 0.6991
epoch: 99, loss: 561.7, accuracy: 0.6991
time analysis:
    all 5537.2555 s
    train 5535.5064 s
Accuracy of     0 : 73 %
Accuracy of     1 : 86 %
Accuracy of     2 : 64 %
Accuracy of     3 : 43 %
Accuracy of     4 : 60 %
Accuracy of     5 : 57 %
Accuracy of     6 : 75 %
Accuracy of     7 : 73 %
Accuracy of     8 : 84 %
Accuracy of     9 : 83 %
```

## CIFAR10, small_fc, DAUConv2d, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           1,792
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           4,616
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 11,682
DAU params: 6,408
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1123.88, accuracy: 0.5487
epoch: 1, loss: 932.62, accuracy: 0.5765
epoch: 2, loss: 872.9, accuracy: 0.6018
epoch: 3, loss: 830.89, accuracy: 0.6315
epoch: 4, loss: 809.78, accuracy: 0.6392
epoch: 5, loss: 790.07, accuracy: 0.6426
epoch: 6, loss: 778.04, accuracy: 0.6479
epoch: 7, loss: 764.42, accuracy: 0.6473
epoch: 8, loss: 752.86, accuracy: 0.6476
epoch: 9, loss: 742.42, accuracy: 0.6468
epoch: 10, loss: 738.11, accuracy: 0.657
epoch: 11, loss: 731.55, accuracy: 0.6628
epoch: 12, loss: 726.19, accuracy: 0.6414
epoch: 13, loss: 718.4, accuracy: 0.6735
epoch: 14, loss: 714.79, accuracy: 0.6642
epoch: 15, loss: 708.92, accuracy: 0.6554
epoch: 16, loss: 708.43, accuracy: 0.6151
epoch: 17, loss: 704.78, accuracy: 0.657
epoch: 18, loss: 703.14, accuracy: 0.6504
epoch: 19, loss: 697.22, accuracy: 0.6701
epoch: 20, loss: 696.0, accuracy: 0.6723
epoch: 21, loss: 689.88, accuracy: 0.6613
epoch: 22, loss: 689.37, accuracy: 0.665
epoch: 23, loss: 685.67, accuracy: 0.6727
epoch: 24, loss: 688.34, accuracy: 0.6607
epoch: 25, loss: 684.65, accuracy: 0.6712
epoch: 26, loss: 681.12, accuracy: 0.6575
epoch: 27, loss: 681.41, accuracy: 0.6798
epoch: 28, loss: 676.45, accuracy: 0.6791
epoch: 29, loss: 678.62, accuracy: 0.6799
epoch: 30, loss: 675.12, accuracy: 0.6688
epoch: 31, loss: 673.09, accuracy: 0.6785
epoch: 32, loss: 672.15, accuracy: 0.6732
epoch: 33, loss: 668.3, accuracy: 0.6792
epoch: 34, loss: 672.53, accuracy: 0.6733
epoch: 35, loss: 672.58, accuracy: 0.6652
epoch: 36, loss: 671.26, accuracy: 0.6788
epoch: 37, loss: 667.71, accuracy: 0.6665
epoch: 38, loss: 662.13, accuracy: 0.6738
epoch: 39, loss: 662.36, accuracy: 0.6791
epoch: 40, loss: 663.4, accuracy: 0.6748
epoch: 41, loss: 659.87, accuracy: 0.6769
epoch: 42, loss: 655.1, accuracy: 0.6798
epoch: 43, loss: 662.81, accuracy: 0.6667
epoch: 44, loss: 660.71, accuracy: 0.6742
epoch: 45, loss: 661.7, accuracy: 0.6777
epoch: 46, loss: 659.32, accuracy: 0.6762
epoch: 47, loss: 657.61, accuracy: 0.6784
Epoch    48: reducing learning rate of group 0 to 5.0000e-03.
epoch: 48, loss: 655.63, accuracy: 0.6721
epoch: 49, loss: 614.58, accuracy: 0.6853
epoch: 50, loss: 610.23, accuracy: 0.6792
epoch: 51, loss: 606.67, accuracy: 0.6891
epoch: 52, loss: 609.28, accuracy: 0.6821
epoch: 53, loss: 606.1, accuracy: 0.6892
epoch: 54, loss: 607.36, accuracy: 0.6834
epoch: 55, loss: 604.98, accuracy: 0.677
epoch: 56, loss: 603.01, accuracy: 0.6878
epoch: 57, loss: 602.77, accuracy: 0.6877
epoch: 58, loss: 602.91, accuracy: 0.6836
epoch: 59, loss: 602.07, accuracy: 0.6903
epoch: 60, loss: 602.14, accuracy: 0.6893
epoch: 61, loss: 600.01, accuracy: 0.6891
epoch: 62, loss: 598.02, accuracy: 0.6869
epoch: 63, loss: 602.3, accuracy: 0.6815
epoch: 64, loss: 597.69, accuracy: 0.6828
epoch: 65, loss: 598.12, accuracy: 0.6848
epoch: 66, loss: 596.78, accuracy: 0.6868
epoch: 67, loss: 599.44, accuracy: 0.6921
epoch: 68, loss: 598.5, accuracy: 0.6882
epoch: 69, loss: 597.78, accuracy: 0.6893
epoch: 70, loss: 596.29, accuracy: 0.6723
epoch: 71, loss: 596.12, accuracy: 0.6904
epoch: 72, loss: 594.41, accuracy: 0.6899
epoch: 73, loss: 594.12, accuracy: 0.6812
epoch: 74, loss: 593.31, accuracy: 0.686
epoch: 75, loss: 553.32, accuracy: 0.6962
epoch: 76, loss: 548.27, accuracy: 0.6949
epoch: 77, loss: 547.25, accuracy: 0.6996
epoch: 78, loss: 547.17, accuracy: 0.697
epoch: 79, loss: 545.77, accuracy: 0.6977
epoch: 80, loss: 546.21, accuracy: 0.6979
epoch: 81, loss: 544.97, accuracy: 0.6949
epoch: 82, loss: 545.68, accuracy: 0.6941
epoch: 83, loss: 544.18, accuracy: 0.6944
epoch: 84, loss: 544.76, accuracy: 0.6959
epoch: 85, loss: 545.06, accuracy: 0.6963
epoch: 86, loss: 542.3, accuracy: 0.6952
epoch: 87, loss: 544.62, accuracy: 0.6957
epoch: 88, loss: 544.36, accuracy: 0.6948
epoch: 89, loss: 542.11, accuracy: 0.6972
epoch: 90, loss: 544.0, accuracy: 0.6958
epoch: 91, loss: 542.39, accuracy: 0.6947
Epoch    92: reducing learning rate of group 0 to 2.5000e-04.
epoch: 92, loss: 543.69, accuracy: 0.6979
epoch: 93, loss: 539.33, accuracy: 0.6957
epoch: 94, loss: 540.69, accuracy: 0.6962
epoch: 95, loss: 538.86, accuracy: 0.6967
epoch: 96, loss: 540.49, accuracy: 0.6953
epoch: 97, loss: 538.47, accuracy: 0.6956
epoch: 98, loss: 539.79, accuracy: 0.6944
epoch: 99, loss: 538.58, accuracy: 0.6957
time analysis:
    all 8367.4582 s
    train 8363.8397 s
Accuracy of     0 : 75 %
Accuracy of     1 : 86 %
Accuracy of     2 : 51 %
Accuracy of     3 : 38 %
Accuracy of     4 : 63 %
Accuracy of     5 : 55 %
Accuracy of     6 : 76 %
Accuracy of     7 : 64 %
Accuracy of     8 : 81 %
Accuracy of     9 : 76 %
```

## CIFAR10, small_fc, DAUConv2d, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           2,368
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           6,152
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 13,794
DAU params: 8,520
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1154.04, accuracy: 0.5278
epoch: 1, loss: 976.61, accuracy: 0.5738
epoch: 2, loss: 918.48, accuracy: 0.5679
epoch: 3, loss: 888.71, accuracy: 0.5848
epoch: 4, loss: 866.37, accuracy: 0.5998
epoch: 5, loss: 844.09, accuracy: 0.5992
epoch: 6, loss: 830.7, accuracy: 0.6133
epoch: 7, loss: 819.93, accuracy: 0.6293
epoch: 8, loss: 805.18, accuracy: 0.6203
epoch: 9, loss: 801.38, accuracy: 0.6237
epoch: 10, loss: 786.89, accuracy: 0.6285
epoch: 11, loss: 782.09, accuracy: 0.6429
epoch: 12, loss: 773.28, accuracy: 0.6444
epoch: 13, loss: 768.68, accuracy: 0.6202
epoch: 14, loss: 764.73, accuracy: 0.6512
epoch: 15, loss: 757.18, accuracy: 0.65
epoch: 16, loss: 753.06, accuracy: 0.6399
epoch: 17, loss: 751.42, accuracy: 0.6417
epoch: 18, loss: 747.22, accuracy: 0.6425
epoch: 19, loss: 743.98, accuracy: 0.6379
epoch: 20, loss: 740.59, accuracy: 0.6487
epoch: 21, loss: 733.86, accuracy: 0.652
epoch: 22, loss: 733.69, accuracy: 0.6477
epoch: 23, loss: 733.06, accuracy: 0.6376
epoch: 24, loss: 726.5, accuracy: 0.6542
epoch: 25, loss: 724.02, accuracy: 0.6483
epoch: 26, loss: 719.74, accuracy: 0.6575
epoch: 27, loss: 719.01, accuracy: 0.6593
epoch: 28, loss: 718.34, accuracy: 0.6568
epoch: 29, loss: 715.97, accuracy: 0.662
epoch: 30, loss: 714.31, accuracy: 0.6529
epoch: 31, loss: 712.69, accuracy: 0.6501
epoch: 32, loss: 708.81, accuracy: 0.6524
epoch: 33, loss: 711.42, accuracy: 0.6596
epoch: 34, loss: 703.73, accuracy: 0.6631
epoch: 35, loss: 705.8, accuracy: 0.6273
epoch: 36, loss: 704.82, accuracy: 0.6572
epoch: 37, loss: 700.78, accuracy: 0.6644
epoch: 38, loss: 703.24, accuracy: 0.6576
epoch: 39, loss: 698.18, accuracy: 0.6668
epoch: 40, loss: 697.71, accuracy: 0.66
epoch: 41, loss: 695.89, accuracy: 0.6553
epoch: 42, loss: 695.87, accuracy: 0.6721
epoch: 43, loss: 690.44, accuracy: 0.6722
epoch: 44, loss: 690.83, accuracy: 0.6638
epoch: 45, loss: 690.63, accuracy: 0.6528
epoch: 46, loss: 689.62, accuracy: 0.6567
epoch: 47, loss: 686.11, accuracy: 0.6731
epoch: 48, loss: 689.65, accuracy: 0.6647
epoch: 49, loss: 683.04, accuracy: 0.6563
epoch: 50, loss: 687.08, accuracy: 0.6538
epoch: 51, loss: 681.21, accuracy: 0.6631
epoch: 52, loss: 681.61, accuracy: 0.6696
epoch: 53, loss: 680.21, accuracy: 0.6673
epoch: 54, loss: 680.66, accuracy: 0.6668
epoch: 55, loss: 681.5, accuracy: 0.663
epoch: 56, loss: 679.76, accuracy: 0.6568
epoch: 57, loss: 675.59, accuracy: 0.6676
epoch: 58, loss: 675.81, accuracy: 0.6728
epoch: 59, loss: 675.6, accuracy: 0.6465
epoch: 60, loss: 675.79, accuracy: 0.6767
epoch: 61, loss: 674.71, accuracy: 0.6595
epoch: 62, loss: 669.74, accuracy: 0.6586
epoch: 63, loss: 674.2, accuracy: 0.6539
epoch: 64, loss: 668.36, accuracy: 0.6564
epoch: 65, loss: 667.28, accuracy: 0.6668
epoch: 66, loss: 669.26, accuracy: 0.6739
epoch: 67, loss: 668.01, accuracy: 0.6753
epoch: 68, loss: 666.38, accuracy: 0.6757
epoch: 69, loss: 664.65, accuracy: 0.6755
epoch: 70, loss: 663.42, accuracy: 0.6756
epoch: 71, loss: 667.69, accuracy: 0.6734
epoch: 72, loss: 662.28, accuracy: 0.6695
epoch: 73, loss: 665.46, accuracy: 0.6704
epoch: 74, loss: 662.2, accuracy: 0.6734
epoch: 75, loss: 596.34, accuracy: 0.6905
epoch: 76, loss: 585.64, accuracy: 0.6891
epoch: 77, loss: 583.82, accuracy: 0.6891
epoch: 78, loss: 582.11, accuracy: 0.6906
epoch: 79, loss: 581.63, accuracy: 0.6926
epoch: 80, loss: 579.75, accuracy: 0.6933
epoch: 81, loss: 578.91, accuracy: 0.6921
epoch: 82, loss: 578.7, accuracy: 0.689
epoch: 83, loss: 577.18, accuracy: 0.6874
epoch: 84, loss: 577.74, accuracy: 0.6877
epoch: 85, loss: 577.14, accuracy: 0.691
epoch: 86, loss: 575.08, accuracy: 0.6893
epoch: 87, loss: 574.14, accuracy: 0.6917
epoch: 88, loss: 576.05, accuracy: 0.6917
epoch: 89, loss: 573.88, accuracy: 0.6898
epoch: 90, loss: 573.45, accuracy: 0.6908
epoch: 91, loss: 572.62, accuracy: 0.687
epoch: 92, loss: 572.92, accuracy: 0.6901
epoch: 93, loss: 572.23, accuracy: 0.69
epoch: 94, loss: 571.2, accuracy: 0.687
epoch: 95, loss: 571.03, accuracy: 0.6897
epoch: 96, loss: 570.54, accuracy: 0.6871
epoch: 97, loss: 570.71, accuracy: 0.6919
epoch: 98, loss: 570.19, accuracy: 0.6893
epoch: 99, loss: 571.06, accuracy: 0.6925
time analysis:
    all 11355.718 s
    train 11351.2515 s
Accuracy of     0 : 76 %
Accuracy of     1 : 94 %
Accuracy of     2 : 59 %
Accuracy of     3 : 43 %
Accuracy of     4 : 54 %
Accuracy of     5 : 50 %
Accuracy of     6 : 87 %
Accuracy of     7 : 71 %
Accuracy of     8 : 77 %
Accuracy of     9 : 69 %
```

## CIFAR10, small_fc, DAUConv2d, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=5, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           2,944
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           7,688
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 15,906
DAU params: 10,632
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1131.32, accuracy: 0.5621
epoch: 1, loss: 936.52, accuracy: 0.5856
epoch: 2, loss: 866.36, accuracy: 0.6165
epoch: 3, loss: 827.1, accuracy: 0.6319
epoch: 4, loss: 803.53, accuracy: 0.6354
epoch: 5, loss: 786.88, accuracy: 0.6108
epoch: 6, loss: 776.5, accuracy: 0.6198
epoch: 7, loss: 764.52, accuracy: 0.6393
epoch: 8, loss: 753.25, accuracy: 0.6448
epoch: 9, loss: 747.58, accuracy: 0.6468
epoch: 10, loss: 742.19, accuracy: 0.6473
epoch: 11, loss: 731.34, accuracy: 0.6447
epoch: 12, loss: 727.52, accuracy: 0.6607
epoch: 13, loss: 722.8, accuracy: 0.6532
epoch: 14, loss: 717.13, accuracy: 0.6355
epoch: 15, loss: 711.44, accuracy: 0.6674
epoch: 16, loss: 708.78, accuracy: 0.6679
epoch: 17, loss: 704.2, accuracy: 0.6619
epoch: 18, loss: 701.02, accuracy: 0.6449
epoch: 19, loss: 697.24, accuracy: 0.6577
epoch: 20, loss: 696.89, accuracy: 0.669
epoch: 21, loss: 692.24, accuracy: 0.6624
epoch: 22, loss: 688.22, accuracy: 0.6721
epoch: 23, loss: 687.15, accuracy: 0.6602
epoch: 24, loss: 683.5, accuracy: 0.6584
epoch: 25, loss: 684.28, accuracy: 0.6694
epoch: 26, loss: 679.81, accuracy: 0.6665
epoch: 27, loss: 676.85, accuracy: 0.6522
epoch: 28, loss: 674.98, accuracy: 0.6489
epoch: 29, loss: 672.47, accuracy: 0.6778
epoch: 30, loss: 668.79, accuracy: 0.6682
epoch: 31, loss: 669.5, accuracy: 0.6764
epoch: 32, loss: 667.18, accuracy: 0.6549
epoch: 33, loss: 668.12, accuracy: 0.667
epoch: 34, loss: 663.91, accuracy: 0.6711
epoch: 35, loss: 657.31, accuracy: 0.6702
epoch: 36, loss: 661.36, accuracy: 0.6741
epoch: 37, loss: 662.22, accuracy: 0.6627
epoch: 38, loss: 658.96, accuracy: 0.6684
epoch: 39, loss: 657.37, accuracy: 0.6794
epoch: 40, loss: 653.36, accuracy: 0.6784
epoch: 41, loss: 652.72, accuracy: 0.6808
epoch: 42, loss: 650.01, accuracy: 0.6791
epoch: 43, loss: 649.99, accuracy: 0.6726
epoch: 44, loss: 652.38, accuracy: 0.6727
epoch: 45, loss: 646.3, accuracy: 0.6745
epoch: 46, loss: 649.3, accuracy: 0.6823
epoch: 47, loss: 641.9, accuracy: 0.6679
epoch: 48, loss: 637.57, accuracy: 0.6846
epoch: 49, loss: 639.67, accuracy: 0.6698
epoch: 50, loss: 642.75, accuracy: 0.6711
epoch: 51, loss: 634.75, accuracy: 0.6732
epoch: 52, loss: 636.41, accuracy: 0.6678
epoch: 53, loss: 637.24, accuracy: 0.6777
epoch: 54, loss: 634.06, accuracy: 0.6653
epoch: 55, loss: 633.0, accuracy: 0.6759
epoch: 56, loss: 633.42, accuracy: 0.6653
epoch: 57, loss: 632.08, accuracy: 0.6619
epoch: 58, loss: 630.44, accuracy: 0.6781
epoch: 59, loss: 625.15, accuracy: 0.6683
epoch: 60, loss: 624.87, accuracy: 0.6723
epoch: 61, loss: 628.68, accuracy: 0.6866
epoch: 62, loss: 625.1, accuracy: 0.6842
epoch: 63, loss: 627.85, accuracy: 0.6796
epoch: 64, loss: 625.56, accuracy: 0.6793
epoch: 65, loss: 622.21, accuracy: 0.6856
epoch: 66, loss: 623.23, accuracy: 0.6917
epoch: 67, loss: 620.91, accuracy: 0.6864
epoch: 68, loss: 616.71, accuracy: 0.6642
epoch: 69, loss: 620.9, accuracy: 0.6832
epoch: 70, loss: 618.27, accuracy: 0.6713
epoch: 71, loss: 618.61, accuracy: 0.6711
epoch: 72, loss: 618.9, accuracy: 0.6679
epoch: 73, loss: 616.81, accuracy: 0.6867
epoch: 74, loss: 614.32, accuracy: 0.6665
epoch: 75, loss: 544.85, accuracy: 0.704
epoch: 76, loss: 532.33, accuracy: 0.7061
epoch: 77, loss: 526.49, accuracy: 0.7049
epoch: 78, loss: 524.65, accuracy: 0.7064
epoch: 79, loss: 523.09, accuracy: 0.7021
epoch: 80, loss: 521.63, accuracy: 0.7054
epoch: 81, loss: 520.3, accuracy: 0.7046
epoch: 82, loss: 520.17, accuracy: 0.7028
epoch: 83, loss: 519.01, accuracy: 0.7039
epoch: 84, loss: 518.24, accuracy: 0.706
epoch: 85, loss: 516.94, accuracy: 0.7032
epoch: 86, loss: 517.62, accuracy: 0.7037
epoch: 87, loss: 516.15, accuracy: 0.7072
epoch: 88, loss: 515.98, accuracy: 0.7032
epoch: 89, loss: 516.49, accuracy: 0.7001
epoch: 90, loss: 514.68, accuracy: 0.7022
epoch: 91, loss: 514.33, accuracy: 0.7025
epoch: 92, loss: 514.76, accuracy: 0.7016
epoch: 93, loss: 514.17, accuracy: 0.7071
epoch: 94, loss: 512.92, accuracy: 0.7052
epoch: 95, loss: 512.93, accuracy: 0.704
epoch: 96, loss: 512.68, accuracy: 0.7066
epoch: 97, loss: 512.83, accuracy: 0.7054
epoch: 98, loss: 512.72, accuracy: 0.7036
epoch: 99, loss: 511.05, accuracy: 0.7046
time analysis:
    all 14571.2436 s
    train 14565.8425 s
Accuracy of     0 : 76 %
Accuracy of     1 : 80 %
Accuracy of     2 : 46 %
Accuracy of     3 : 45 %
Accuracy of     4 : 56 %
Accuracy of     5 : 57 %
Accuracy of     6 : 75 %
Accuracy of     7 : 75 %
Accuracy of     8 : 81 %
Accuracy of     9 : 79 %
```

## CIFAR10, small_fc, DAUConv2d, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2d(in_channels=3, out_channels=64, no_units=6, sigma=0.5)
  (conv2): DAUConv2d(in_channels=64, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
         DAUConv2d-1           [-1, 64, 32, 32]           3,520
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
         DAUConv2d-4            [-1, 8, 16, 16]           9,224
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 18,018
DAU params: 12,744
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1158.53, accuracy: 0.5336
epoch: 1, loss: 975.33, accuracy: 0.5727
epoch: 2, loss: 902.3, accuracy: 0.5653
epoch: 3, loss: 862.19, accuracy: 0.6172
epoch: 4, loss: 833.31, accuracy: 0.6099
epoch: 5, loss: 811.29, accuracy: 0.6042
epoch: 6, loss: 798.17, accuracy: 0.6378
epoch: 7, loss: 788.02, accuracy: 0.6115
epoch: 8, loss: 777.53, accuracy: 0.6346
epoch: 9, loss: 769.81, accuracy: 0.6429
epoch: 10, loss: 755.99, accuracy: 0.6423
epoch: 11, loss: 749.72, accuracy: 0.6315
epoch: 12, loss: 743.47, accuracy: 0.6537
epoch: 13, loss: 736.05, accuracy: 0.6335
epoch: 14, loss: 729.48, accuracy: 0.6527
epoch: 15, loss: 727.46, accuracy: 0.6378
epoch: 16, loss: 718.79, accuracy: 0.66
epoch: 17, loss: 715.14, accuracy: 0.6471
epoch: 18, loss: 712.47, accuracy: 0.6616
epoch: 19, loss: 706.89, accuracy: 0.6498
epoch: 20, loss: 705.23, accuracy: 0.6636
epoch: 21, loss: 700.17, accuracy: 0.6637
epoch: 22, loss: 700.48, accuracy: 0.6706
epoch: 23, loss: 698.82, accuracy: 0.6657
epoch: 24, loss: 693.27, accuracy: 0.6653
epoch: 25, loss: 693.52, accuracy: 0.6542
epoch: 26, loss: 686.33, accuracy: 0.6645
epoch: 27, loss: 687.47, accuracy: 0.6653
epoch: 28, loss: 687.51, accuracy: 0.6676
epoch: 29, loss: 685.86, accuracy: 0.657
epoch: 30, loss: 681.95, accuracy: 0.6584
epoch: 31, loss: 678.69, accuracy: 0.6602
epoch: 32, loss: 679.82, accuracy: 0.6649
epoch: 33, loss: 672.98, accuracy: 0.6691
epoch: 34, loss: 672.0, accuracy: 0.6478
epoch: 35, loss: 669.24, accuracy: 0.654
epoch: 36, loss: 673.02, accuracy: 0.6728
epoch: 37, loss: 665.83, accuracy: 0.676
epoch: 38, loss: 665.93, accuracy: 0.6817
epoch: 39, loss: 662.25, accuracy: 0.6738
epoch: 40, loss: 660.82, accuracy: 0.6844
epoch: 41, loss: 661.11, accuracy: 0.6738
epoch: 42, loss: 655.91, accuracy: 0.6714
epoch: 43, loss: 658.88, accuracy: 0.6746
epoch: 44, loss: 657.86, accuracy: 0.6542
epoch: 45, loss: 652.99, accuracy: 0.666
epoch: 46, loss: 654.74, accuracy: 0.6875
epoch: 47, loss: 647.58, accuracy: 0.6792
epoch: 48, loss: 648.57, accuracy: 0.6818
epoch: 49, loss: 649.25, accuracy: 0.6819
epoch: 50, loss: 646.6, accuracy: 0.6801
epoch: 51, loss: 647.0, accuracy: 0.6768
epoch: 52, loss: 643.17, accuracy: 0.6718
epoch: 53, loss: 643.62, accuracy: 0.6608
epoch: 54, loss: 641.37, accuracy: 0.6883
epoch: 55, loss: 640.9, accuracy: 0.6813
epoch: 56, loss: 640.93, accuracy: 0.6829
epoch: 57, loss: 638.48, accuracy: 0.6671
epoch: 58, loss: 636.0, accuracy: 0.6772
epoch: 59, loss: 636.16, accuracy: 0.6843
epoch: 60, loss: 634.28, accuracy: 0.6825
epoch: 61, loss: 633.56, accuracy: 0.6885
epoch: 62, loss: 632.76, accuracy: 0.6798
epoch: 63, loss: 637.7, accuracy: 0.6733
epoch: 64, loss: 632.04, accuracy: 0.6872
epoch: 65, loss: 635.34, accuracy: 0.6797
epoch: 66, loss: 630.49, accuracy: 0.6919
epoch: 67, loss: 629.47, accuracy: 0.6895
epoch: 68, loss: 629.74, accuracy: 0.6852
epoch: 69, loss: 628.2, accuracy: 0.6808
epoch: 70, loss: 628.87, accuracy: 0.6895
epoch: 71, loss: 627.62, accuracy: 0.6849
epoch: 72, loss: 627.27, accuracy: 0.6957
epoch: 73, loss: 622.45, accuracy: 0.6923
epoch: 74, loss: 623.75, accuracy: 0.6878
epoch: 75, loss: 552.39, accuracy: 0.7151
epoch: 76, loss: 543.49, accuracy: 0.7105
epoch: 77, loss: 540.38, accuracy: 0.7129
epoch: 78, loss: 537.79, accuracy: 0.7138
epoch: 79, loss: 536.77, accuracy: 0.713
epoch: 80, loss: 535.54, accuracy: 0.7126
epoch: 81, loss: 532.67, accuracy: 0.7115
epoch: 82, loss: 532.62, accuracy: 0.7139
epoch: 83, loss: 531.45, accuracy: 0.7129
epoch: 84, loss: 531.52, accuracy: 0.7126
epoch: 85, loss: 530.79, accuracy: 0.7104
epoch: 86, loss: 529.97, accuracy: 0.7116
epoch: 87, loss: 530.22, accuracy: 0.7114
epoch: 88, loss: 528.47, accuracy: 0.7141
epoch: 89, loss: 529.11, accuracy: 0.7098
epoch: 90, loss: 528.82, accuracy: 0.7146
epoch: 91, loss: 527.89, accuracy: 0.7133
epoch: 92, loss: 527.21, accuracy: 0.7166
epoch: 93, loss: 527.56, accuracy: 0.7137
epoch: 94, loss: 527.98, accuracy: 0.7113
epoch: 95, loss: 527.14, accuracy: 0.7097
epoch: 96, loss: 527.37, accuracy: 0.7123
epoch: 97, loss: 525.86, accuracy: 0.7124
epoch: 98, loss: 524.97, accuracy: 0.7128
epoch: 99, loss: 526.01, accuracy: 0.7119
time analysis:
    all 18060.819 s
    train 18054.5368 s
Accuracy of     0 : 78 %
Accuracy of     1 : 86 %
Accuracy of     2 : 59 %
Accuracy of     3 : 46 %
Accuracy of     4 : 58 %
Accuracy of     5 : 62 %
Accuracy of     6 : 73 %
Accuracy of     7 : 71 %
Accuracy of     8 : 75 %
Accuracy of     9 : 75 %
```

## CIFAR10, small_fc, DAUConv2di, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             460
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,288
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,022
DAU params: 1,748
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1168.79, accuracy: 0.5138
epoch: 1, loss: 1013.59, accuracy: 0.527
epoch: 2, loss: 966.67, accuracy: 0.5149
epoch: 3, loss: 938.35, accuracy: 0.5271
epoch: 4, loss: 914.46, accuracy: 0.5668
epoch: 5, loss: 898.94, accuracy: 0.5878
epoch: 6, loss: 884.93, accuracy: 0.5814
epoch: 7, loss: 879.91, accuracy: 0.5902
epoch: 8, loss: 864.46, accuracy: 0.5881
epoch: 9, loss: 858.64, accuracy: 0.5964
epoch: 10, loss: 850.95, accuracy: 0.6126
epoch: 11, loss: 845.21, accuracy: 0.602
epoch: 12, loss: 840.2, accuracy: 0.5987
epoch: 13, loss: 833.22, accuracy: 0.5886
epoch: 14, loss: 832.22, accuracy: 0.6089
epoch: 15, loss: 828.1, accuracy: 0.6169
epoch: 16, loss: 828.06, accuracy: 0.6037
epoch: 17, loss: 820.63, accuracy: 0.6071
epoch: 18, loss: 818.71, accuracy: 0.5978
epoch: 19, loss: 816.53, accuracy: 0.601
epoch: 20, loss: 809.66, accuracy: 0.6124
epoch: 21, loss: 807.67, accuracy: 0.6225
epoch: 22, loss: 806.9, accuracy: 0.6095
epoch: 23, loss: 807.51, accuracy: 0.6112
epoch: 24, loss: 805.48, accuracy: 0.5977
epoch: 25, loss: 804.68, accuracy: 0.6242
epoch: 26, loss: 799.88, accuracy: 0.6063
epoch: 27, loss: 799.87, accuracy: 0.5981
epoch: 28, loss: 798.46, accuracy: 0.626
epoch: 29, loss: 795.47, accuracy: 0.6151
epoch: 30, loss: 795.12, accuracy: 0.6238
epoch: 31, loss: 794.11, accuracy: 0.6304
epoch: 32, loss: 790.77, accuracy: 0.6143
epoch: 33, loss: 789.93, accuracy: 0.6001
epoch: 34, loss: 787.82, accuracy: 0.6015
epoch: 35, loss: 790.9, accuracy: 0.6287
epoch: 36, loss: 787.73, accuracy: 0.6087
epoch: 37, loss: 784.52, accuracy: 0.5933
epoch: 38, loss: 783.03, accuracy: 0.6104
epoch: 39, loss: 784.88, accuracy: 0.6195
epoch: 40, loss: 781.98, accuracy: 0.6257
epoch: 41, loss: 785.46, accuracy: 0.6171
epoch: 42, loss: 782.76, accuracy: 0.6119
epoch: 43, loss: 779.19, accuracy: 0.6215
epoch: 44, loss: 781.02, accuracy: 0.6191
epoch: 45, loss: 778.55, accuracy: 0.6193
epoch: 46, loss: 779.33, accuracy: 0.6097
epoch: 47, loss: 780.09, accuracy: 0.6193
epoch: 48, loss: 779.07, accuracy: 0.6283
epoch: 49, loss: 775.52, accuracy: 0.6261
epoch: 50, loss: 778.44, accuracy: 0.6015
epoch: 51, loss: 776.08, accuracy: 0.6308
epoch: 52, loss: 775.86, accuracy: 0.6276
epoch: 53, loss: 770.57, accuracy: 0.621
epoch: 54, loss: 776.17, accuracy: 0.6213
epoch: 55, loss: 771.72, accuracy: 0.6045
epoch: 56, loss: 770.38, accuracy: 0.6356
epoch: 57, loss: 771.13, accuracy: 0.6093
epoch: 58, loss: 770.1, accuracy: 0.6209
Epoch    59: reducing learning rate of group 0 to 5.0000e-03.
epoch: 59, loss: 771.98, accuracy: 0.593
epoch: 60, loss: 734.89, accuracy: 0.6385
epoch: 61, loss: 735.13, accuracy: 0.6362
epoch: 62, loss: 734.99, accuracy: 0.6389
epoch: 63, loss: 731.84, accuracy: 0.6384
epoch: 64, loss: 732.68, accuracy: 0.6387
epoch: 65, loss: 731.52, accuracy: 0.6431
epoch: 66, loss: 731.72, accuracy: 0.6469
epoch: 67, loss: 729.78, accuracy: 0.6472
epoch: 68, loss: 727.9, accuracy: 0.6418
epoch: 69, loss: 731.59, accuracy: 0.6396
epoch: 70, loss: 729.03, accuracy: 0.6461
epoch: 71, loss: 727.5, accuracy: 0.6468
epoch: 72, loss: 726.69, accuracy: 0.6393
epoch: 73, loss: 728.08, accuracy: 0.643
epoch: 74, loss: 728.48, accuracy: 0.6216
epoch: 75, loss: 694.95, accuracy: 0.6497
epoch: 76, loss: 691.66, accuracy: 0.653
epoch: 77, loss: 690.89, accuracy: 0.6522
epoch: 78, loss: 690.01, accuracy: 0.6526
epoch: 79, loss: 689.04, accuracy: 0.6531
epoch: 80, loss: 690.51, accuracy: 0.6529
epoch: 81, loss: 688.69, accuracy: 0.654
epoch: 82, loss: 690.7, accuracy: 0.6537
epoch: 83, loss: 689.02, accuracy: 0.656
epoch: 84, loss: 688.07, accuracy: 0.654
epoch: 85, loss: 690.55, accuracy: 0.6526
epoch: 86, loss: 687.9, accuracy: 0.6544
epoch: 87, loss: 689.08, accuracy: 0.6527
epoch: 88, loss: 688.46, accuracy: 0.6543
epoch: 89, loss: 688.89, accuracy: 0.6542
Epoch    90: reducing learning rate of group 0 to 2.5000e-04.
epoch: 90, loss: 688.74, accuracy: 0.6542
epoch: 91, loss: 686.84, accuracy: 0.6529
epoch: 92, loss: 686.98, accuracy: 0.6566
epoch: 93, loss: 685.89, accuracy: 0.6552
epoch: 94, loss: 683.67, accuracy: 0.6539
epoch: 95, loss: 685.66, accuracy: 0.6573
epoch: 96, loss: 685.89, accuracy: 0.6562
epoch: 97, loss: 685.08, accuracy: 0.6553
epoch: 98, loss: 686.15, accuracy: 0.6562
epoch: 99, loss: 685.33, accuracy: 0.6557
time analysis:
    all 1721.0813 s
    train 1713.9895 s
Accuracy of     0 : 71 %
Accuracy of     1 : 78 %
Accuracy of     2 : 46 %
Accuracy of     3 : 41 %
Accuracy of     4 : 43 %
Accuracy of     5 : 61 %
Accuracy of     6 : 78 %
Accuracy of     7 : 76 %
Accuracy of     8 : 77 %
Accuracy of     9 : 67 %
```

## CIFAR10, small_fc, DAUConv2di, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             658
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           1,928
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,860
DAU params: 2,586
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1209.85, accuracy: 0.501
epoch: 1, loss: 1029.99, accuracy: 0.5329
epoch: 2, loss: 972.79, accuracy: 0.554
epoch: 3, loss: 938.22, accuracy: 0.5773
epoch: 4, loss: 919.68, accuracy: 0.5814
epoch: 5, loss: 898.54, accuracy: 0.5873
epoch: 6, loss: 880.81, accuracy: 0.5983
epoch: 7, loss: 869.02, accuracy: 0.6012
epoch: 8, loss: 859.56, accuracy: 0.5803
epoch: 9, loss: 850.39, accuracy: 0.6132
epoch: 10, loss: 842.1, accuracy: 0.607
epoch: 11, loss: 838.05, accuracy: 0.6097
epoch: 12, loss: 835.58, accuracy: 0.6104
epoch: 13, loss: 828.61, accuracy: 0.6094
epoch: 14, loss: 828.19, accuracy: 0.6128
epoch: 15, loss: 819.59, accuracy: 0.5968
epoch: 16, loss: 816.97, accuracy: 0.6269
epoch: 17, loss: 811.36, accuracy: 0.6227
epoch: 18, loss: 807.08, accuracy: 0.6253
epoch: 19, loss: 808.17, accuracy: 0.6153
epoch: 20, loss: 801.18, accuracy: 0.597
epoch: 21, loss: 802.16, accuracy: 0.6231
epoch: 22, loss: 799.8, accuracy: 0.6102
epoch: 23, loss: 798.34, accuracy: 0.6343
epoch: 24, loss: 794.77, accuracy: 0.6246
epoch: 25, loss: 797.69, accuracy: 0.6185
epoch: 26, loss: 796.17, accuracy: 0.6302
epoch: 27, loss: 792.41, accuracy: 0.6239
epoch: 28, loss: 788.44, accuracy: 0.6137
epoch: 29, loss: 784.62, accuracy: 0.6185
epoch: 30, loss: 785.6, accuracy: 0.6319
epoch: 31, loss: 784.92, accuracy: 0.6264
epoch: 32, loss: 784.72, accuracy: 0.6281
epoch: 33, loss: 782.29, accuracy: 0.6295
epoch: 34, loss: 779.03, accuracy: 0.6321
epoch: 35, loss: 778.24, accuracy: 0.5833
epoch: 36, loss: 779.05, accuracy: 0.6312
epoch: 37, loss: 778.54, accuracy: 0.6349
epoch: 38, loss: 775.5, accuracy: 0.632
epoch: 39, loss: 778.04, accuracy: 0.6316
epoch: 40, loss: 775.45, accuracy: 0.6358
epoch: 41, loss: 775.39, accuracy: 0.6132
epoch: 42, loss: 772.15, accuracy: 0.6203
epoch: 43, loss: 772.89, accuracy: 0.6221
epoch: 44, loss: 771.95, accuracy: 0.627
epoch: 45, loss: 769.59, accuracy: 0.6428
epoch: 46, loss: 770.41, accuracy: 0.6396
epoch: 47, loss: 767.52, accuracy: 0.6469
epoch: 48, loss: 767.41, accuracy: 0.6422
epoch: 49, loss: 764.52, accuracy: 0.634
epoch: 50, loss: 768.9, accuracy: 0.6156
epoch: 51, loss: 771.01, accuracy: 0.64
epoch: 52, loss: 762.62, accuracy: 0.6238
epoch: 53, loss: 765.52, accuracy: 0.6301
epoch: 54, loss: 765.71, accuracy: 0.6076
epoch: 55, loss: 764.67, accuracy: 0.6099
epoch: 56, loss: 764.12, accuracy: 0.6263
epoch: 57, loss: 761.58, accuracy: 0.6312
epoch: 58, loss: 761.73, accuracy: 0.6409
epoch: 59, loss: 762.13, accuracy: 0.6337
epoch: 60, loss: 759.89, accuracy: 0.6418
epoch: 61, loss: 760.03, accuracy: 0.6305
epoch: 62, loss: 755.76, accuracy: 0.6286
epoch: 63, loss: 757.62, accuracy: 0.6341
epoch: 64, loss: 753.96, accuracy: 0.628
epoch: 65, loss: 758.41, accuracy: 0.6298
epoch: 66, loss: 757.0, accuracy: 0.637
epoch: 67, loss: 758.96, accuracy: 0.6434
epoch: 68, loss: 754.65, accuracy: 0.63
epoch: 69, loss: 755.63, accuracy: 0.6336
Epoch    70: reducing learning rate of group 0 to 5.0000e-03.
epoch: 70, loss: 754.01, accuracy: 0.636
epoch: 71, loss: 721.79, accuracy: 0.6538
epoch: 72, loss: 719.16, accuracy: 0.6486
epoch: 73, loss: 720.47, accuracy: 0.6537
epoch: 74, loss: 716.97, accuracy: 0.6466
epoch: 75, loss: 686.6, accuracy: 0.6604
epoch: 76, loss: 682.18, accuracy: 0.6625
epoch: 77, loss: 683.43, accuracy: 0.6623
epoch: 78, loss: 682.0, accuracy: 0.66
epoch: 79, loss: 681.18, accuracy: 0.6619
epoch: 80, loss: 679.82, accuracy: 0.6632
epoch: 81, loss: 680.9, accuracy: 0.6607
epoch: 82, loss: 680.98, accuracy: 0.6603
epoch: 83, loss: 679.45, accuracy: 0.6627
epoch: 84, loss: 681.51, accuracy: 0.6623
epoch: 85, loss: 678.35, accuracy: 0.6621
epoch: 86, loss: 679.36, accuracy: 0.6625
epoch: 87, loss: 680.14, accuracy: 0.6611
epoch: 88, loss: 680.73, accuracy: 0.662
epoch: 89, loss: 679.14, accuracy: 0.6612
epoch: 90, loss: 679.91, accuracy: 0.6614
Epoch    91: reducing learning rate of group 0 to 2.5000e-04.
epoch: 91, loss: 679.41, accuracy: 0.6635
epoch: 92, loss: 677.56, accuracy: 0.662
epoch: 93, loss: 676.79, accuracy: 0.6634
epoch: 94, loss: 675.75, accuracy: 0.6639
epoch: 95, loss: 676.05, accuracy: 0.6629
epoch: 96, loss: 676.27, accuracy: 0.6634
epoch: 97, loss: 676.13, accuracy: 0.661
epoch: 98, loss: 676.57, accuracy: 0.6648
epoch: 99, loss: 674.38, accuracy: 0.6633
time analysis:
    all 2306.9905 s
    train 2304.0564 s
Accuracy of     0 : 80 %
Accuracy of     1 : 82 %
Accuracy of     2 : 54 %
Accuracy of     3 : 46 %
Accuracy of     4 : 58 %
Accuracy of     5 : 61 %
Accuracy of     6 : 67 %
Accuracy of     7 : 65 %
Accuracy of     8 : 70 %
Accuracy of     9 : 71 %
```

## CIFAR10, small_fc, DAUConv2di, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2di(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2di(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2di-1           [-1, 64, 32, 32]             856
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2di-4            [-1, 8, 16, 16]           2,568
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 8,698
DAU params: 3,424
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1220.27, accuracy: 0.4956
epoch: 1, loss: 1036.12, accuracy: 0.5213
epoch: 2, loss: 972.09, accuracy: 0.5762
epoch: 3, loss: 931.57, accuracy: 0.5756
epoch: 4, loss: 903.52, accuracy: 0.5868
epoch: 5, loss: 880.13, accuracy: 0.5935
epoch: 6, loss: 862.16, accuracy: 0.6102
epoch: 7, loss: 847.21, accuracy: 0.6107
epoch: 8, loss: 839.14, accuracy: 0.5996
epoch: 9, loss: 831.76, accuracy: 0.6229
epoch: 10, loss: 822.34, accuracy: 0.6149
epoch: 11, loss: 811.58, accuracy: 0.6237
epoch: 12, loss: 804.33, accuracy: 0.6141
epoch: 13, loss: 807.44, accuracy: 0.6161
epoch: 14, loss: 796.82, accuracy: 0.6258
epoch: 15, loss: 790.73, accuracy: 0.63
epoch: 16, loss: 788.37, accuracy: 0.6067
epoch: 17, loss: 781.54, accuracy: 0.6384
epoch: 18, loss: 774.29, accuracy: 0.6279
epoch: 19, loss: 773.33, accuracy: 0.6168
epoch: 20, loss: 772.66, accuracy: 0.641
epoch: 21, loss: 768.44, accuracy: 0.637
epoch: 22, loss: 762.17, accuracy: 0.6378
epoch: 23, loss: 765.53, accuracy: 0.6423
epoch: 24, loss: 763.55, accuracy: 0.6345
epoch: 25, loss: 759.78, accuracy: 0.6368
epoch: 26, loss: 757.57, accuracy: 0.6357
epoch: 27, loss: 755.03, accuracy: 0.6518
epoch: 28, loss: 752.3, accuracy: 0.645
epoch: 29, loss: 749.89, accuracy: 0.6488
epoch: 30, loss: 752.45, accuracy: 0.6417
epoch: 31, loss: 745.6, accuracy: 0.6417
epoch: 32, loss: 747.03, accuracy: 0.6384
epoch: 33, loss: 742.5, accuracy: 0.6447
epoch: 34, loss: 742.37, accuracy: 0.6437
epoch: 35, loss: 738.61, accuracy: 0.6483
epoch: 36, loss: 739.63, accuracy: 0.6354
epoch: 37, loss: 736.48, accuracy: 0.6334
epoch: 38, loss: 734.28, accuracy: 0.644
epoch: 39, loss: 735.86, accuracy: 0.6254
epoch: 40, loss: 732.31, accuracy: 0.6342
epoch: 41, loss: 728.28, accuracy: 0.645
epoch: 42, loss: 731.78, accuracy: 0.6494
epoch: 43, loss: 728.93, accuracy: 0.6418
epoch: 44, loss: 729.63, accuracy: 0.65
epoch: 45, loss: 727.64, accuracy: 0.6366
epoch: 46, loss: 724.37, accuracy: 0.6418
epoch: 47, loss: 726.38, accuracy: 0.621
epoch: 48, loss: 722.99, accuracy: 0.6529
epoch: 49, loss: 720.32, accuracy: 0.6569
epoch: 50, loss: 721.51, accuracy: 0.6534
epoch: 51, loss: 722.13, accuracy: 0.6525
epoch: 52, loss: 717.07, accuracy: 0.66
epoch: 53, loss: 719.31, accuracy: 0.6564
epoch: 54, loss: 714.32, accuracy: 0.6379
epoch: 55, loss: 714.83, accuracy: 0.6266
epoch: 56, loss: 719.64, accuracy: 0.6436
epoch: 57, loss: 714.86, accuracy: 0.6605
epoch: 58, loss: 713.67, accuracy: 0.6571
epoch: 59, loss: 715.13, accuracy: 0.6358
epoch: 60, loss: 711.29, accuracy: 0.6331
epoch: 61, loss: 712.35, accuracy: 0.646
epoch: 62, loss: 710.34, accuracy: 0.6491
epoch: 63, loss: 711.32, accuracy: 0.6497
epoch: 64, loss: 710.59, accuracy: 0.6531
epoch: 65, loss: 708.4, accuracy: 0.6614
epoch: 66, loss: 708.4, accuracy: 0.6576
epoch: 67, loss: 708.05, accuracy: 0.6624
epoch: 68, loss: 705.49, accuracy: 0.6539
epoch: 69, loss: 709.31, accuracy: 0.6478
epoch: 70, loss: 705.55, accuracy: 0.6476
epoch: 71, loss: 705.67, accuracy: 0.6527
epoch: 72, loss: 703.58, accuracy: 0.6521
epoch: 73, loss: 702.92, accuracy: 0.6629
epoch: 74, loss: 702.51, accuracy: 0.6456
epoch: 75, loss: 641.51, accuracy: 0.6752
epoch: 76, loss: 635.47, accuracy: 0.6753
epoch: 77, loss: 631.47, accuracy: 0.6779
epoch: 78, loss: 629.07, accuracy: 0.6779
epoch: 79, loss: 628.47, accuracy: 0.6784
epoch: 80, loss: 628.52, accuracy: 0.6759
epoch: 81, loss: 627.82, accuracy: 0.6771
epoch: 82, loss: 627.82, accuracy: 0.6808
epoch: 83, loss: 626.49, accuracy: 0.6744
epoch: 84, loss: 626.56, accuracy: 0.6739
epoch: 85, loss: 623.85, accuracy: 0.6797
epoch: 86, loss: 625.05, accuracy: 0.6757
epoch: 87, loss: 625.93, accuracy: 0.6794
epoch: 88, loss: 623.82, accuracy: 0.6765
epoch: 89, loss: 622.83, accuracy: 0.6773
epoch: 90, loss: 622.78, accuracy: 0.6773
epoch: 91, loss: 623.65, accuracy: 0.6743
epoch: 92, loss: 623.31, accuracy: 0.6778
epoch: 93, loss: 622.42, accuracy: 0.6775
epoch: 94, loss: 622.56, accuracy: 0.6779
epoch: 95, loss: 621.44, accuracy: 0.6778
epoch: 96, loss: 621.6, accuracy: 0.6753
epoch: 97, loss: 622.38, accuracy: 0.677
epoch: 98, loss: 620.98, accuracy: 0.6748
epoch: 99, loss: 620.93, accuracy: 0.6773
time analysis:
    all 2932.261 s
    train 2928.5202 s
Accuracy of     0 : 64 %
Accuracy of     1 : 84 %
Accuracy of     2 : 49 %
Accuracy of     3 : 46 %
Accuracy of     4 : 60 %
Accuracy of     5 : 55 %
Accuracy of     6 : 80 %
Accuracy of     7 : 64 %
Accuracy of     8 : 81 %
Accuracy of     9 : 73 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]             704
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,042
DAU params: 1,768
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1138.87, accuracy: 0.5594
epoch: 1, loss: 966.04, accuracy: 0.5722
epoch: 2, loss: 921.26, accuracy: 0.5725
epoch: 3, loss: 897.7, accuracy: 0.5967
epoch: 4, loss: 874.63, accuracy: 0.61
epoch: 5, loss: 859.24, accuracy: 0.6116
epoch: 6, loss: 844.63, accuracy: 0.6113
epoch: 7, loss: 832.24, accuracy: 0.622
epoch: 8, loss: 821.29, accuracy: 0.6237
epoch: 9, loss: 817.08, accuracy: 0.626
epoch: 10, loss: 808.06, accuracy: 0.6241
epoch: 11, loss: 799.04, accuracy: 0.6294
epoch: 12, loss: 792.89, accuracy: 0.6122
epoch: 13, loss: 784.82, accuracy: 0.6257
epoch: 14, loss: 781.02, accuracy: 0.6415
epoch: 15, loss: 775.6, accuracy: 0.6333
epoch: 16, loss: 773.78, accuracy: 0.6278
epoch: 17, loss: 769.01, accuracy: 0.6474
epoch: 18, loss: 767.68, accuracy: 0.6337
epoch: 19, loss: 764.28, accuracy: 0.6483
epoch: 20, loss: 763.12, accuracy: 0.6388
epoch: 21, loss: 758.43, accuracy: 0.6453
epoch: 22, loss: 754.22, accuracy: 0.6433
epoch: 23, loss: 756.82, accuracy: 0.6509
epoch: 24, loss: 750.11, accuracy: 0.6391
epoch: 25, loss: 747.2, accuracy: 0.6532
epoch: 26, loss: 748.73, accuracy: 0.6463
epoch: 27, loss: 744.75, accuracy: 0.6579
epoch: 28, loss: 745.62, accuracy: 0.6527
epoch: 29, loss: 740.54, accuracy: 0.6393
epoch: 30, loss: 737.0, accuracy: 0.6416
epoch: 31, loss: 737.66, accuracy: 0.6471
epoch: 32, loss: 737.47, accuracy: 0.6378
epoch: 33, loss: 734.66, accuracy: 0.6474
epoch: 34, loss: 736.26, accuracy: 0.6509
epoch: 35, loss: 732.21, accuracy: 0.6586
epoch: 36, loss: 731.85, accuracy: 0.6514
epoch: 37, loss: 727.71, accuracy: 0.6456
epoch: 38, loss: 727.51, accuracy: 0.6516
epoch: 39, loss: 728.11, accuracy: 0.6516
epoch: 40, loss: 727.24, accuracy: 0.6382
epoch: 41, loss: 724.34, accuracy: 0.6507
epoch: 42, loss: 727.21, accuracy: 0.6485
epoch: 43, loss: 723.26, accuracy: 0.652
epoch: 44, loss: 723.15, accuracy: 0.6555
epoch: 45, loss: 723.94, accuracy: 0.6553
epoch: 46, loss: 718.44, accuracy: 0.6521
epoch: 47, loss: 717.62, accuracy: 0.6504
epoch: 48, loss: 720.08, accuracy: 0.6553
epoch: 49, loss: 720.89, accuracy: 0.6589
epoch: 50, loss: 720.46, accuracy: 0.6584
epoch: 51, loss: 717.15, accuracy: 0.6571
epoch: 52, loss: 715.55, accuracy: 0.6518
epoch: 53, loss: 716.97, accuracy: 0.653
epoch: 54, loss: 721.65, accuracy: 0.6652
epoch: 55, loss: 714.76, accuracy: 0.6594
epoch: 56, loss: 713.19, accuracy: 0.6631
epoch: 57, loss: 714.01, accuracy: 0.651
epoch: 58, loss: 714.9, accuracy: 0.6577
epoch: 59, loss: 710.58, accuracy: 0.6588
epoch: 60, loss: 714.76, accuracy: 0.6534
epoch: 61, loss: 711.39, accuracy: 0.6564
epoch: 62, loss: 711.12, accuracy: 0.6594
epoch: 63, loss: 714.48, accuracy: 0.6262
epoch: 64, loss: 712.41, accuracy: 0.6659
epoch: 65, loss: 709.18, accuracy: 0.6616
epoch: 66, loss: 707.38, accuracy: 0.659
epoch: 67, loss: 708.87, accuracy: 0.655
epoch: 68, loss: 710.23, accuracy: 0.6566
epoch: 69, loss: 708.2, accuracy: 0.655
epoch: 70, loss: 707.26, accuracy: 0.6199
epoch: 71, loss: 708.01, accuracy: 0.6557
Epoch    72: reducing learning rate of group 0 to 5.0000e-03.
epoch: 72, loss: 710.45, accuracy: 0.643
epoch: 73, loss: 673.62, accuracy: 0.6583
epoch: 74, loss: 672.74, accuracy: 0.6715
epoch: 75, loss: 639.62, accuracy: 0.6819
epoch: 76, loss: 637.17, accuracy: 0.6824
epoch: 77, loss: 636.32, accuracy: 0.6825
epoch: 78, loss: 634.98, accuracy: 0.6825
epoch: 79, loss: 635.37, accuracy: 0.6826
epoch: 80, loss: 634.19, accuracy: 0.682
epoch: 81, loss: 635.0, accuracy: 0.6831
epoch: 82, loss: 633.36, accuracy: 0.6832
epoch: 83, loss: 632.88, accuracy: 0.6832
epoch: 84, loss: 634.64, accuracy: 0.6817
epoch: 85, loss: 632.89, accuracy: 0.6823
epoch: 86, loss: 632.14, accuracy: 0.682
epoch: 87, loss: 633.62, accuracy: 0.6825
epoch: 88, loss: 632.33, accuracy: 0.6824
epoch: 89, loss: 631.17, accuracy: 0.6827
epoch: 90, loss: 632.03, accuracy: 0.6829
epoch: 91, loss: 631.32, accuracy: 0.683
epoch: 92, loss: 632.15, accuracy: 0.6806
epoch: 93, loss: 631.95, accuracy: 0.6781
epoch: 94, loss: 631.31, accuracy: 0.6808
Epoch    95: reducing learning rate of group 0 to 2.5000e-04.
epoch: 95, loss: 631.79, accuracy: 0.682
epoch: 96, loss: 629.95, accuracy: 0.6832
epoch: 97, loss: 629.89, accuracy: 0.6832
epoch: 98, loss: 628.58, accuracy: 0.6845
epoch: 99, loss: 629.67, accuracy: 0.6831
time analysis:
    all 5566.6878 s
    train 5562.2131 s
Accuracy of     0 : 69 %
Accuracy of     1 : 80 %
Accuracy of     2 : 46 %
Accuracy of     3 : 36 %
Accuracy of     4 : 63 %
Accuracy of     5 : 55 %
Accuracy of     6 : 75 %
Accuracy of     7 : 68 %
Accuracy of     8 : 75 %
Accuracy of     9 : 79 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,024
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           1,592
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,890
DAU params: 2,616
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1161.89, accuracy: 0.5032
epoch: 1, loss: 965.92, accuracy: 0.5488
epoch: 2, loss: 910.37, accuracy: 0.565
epoch: 3, loss: 874.0, accuracy: 0.5964
epoch: 4, loss: 856.36, accuracy: 0.6141
epoch: 5, loss: 838.99, accuracy: 0.6087
epoch: 6, loss: 824.92, accuracy: 0.6187
epoch: 7, loss: 810.51, accuracy: 0.6128
epoch: 8, loss: 801.53, accuracy: 0.6272
epoch: 9, loss: 795.6, accuracy: 0.624
epoch: 10, loss: 788.48, accuracy: 0.6202
epoch: 11, loss: 780.8, accuracy: 0.6343
epoch: 12, loss: 775.45, accuracy: 0.6148
epoch: 13, loss: 768.26, accuracy: 0.6432
epoch: 14, loss: 763.73, accuracy: 0.64
epoch: 15, loss: 757.75, accuracy: 0.6327
epoch: 16, loss: 758.13, accuracy: 0.6449
epoch: 17, loss: 755.85, accuracy: 0.6207
epoch: 18, loss: 749.99, accuracy: 0.6469
epoch: 19, loss: 746.47, accuracy: 0.6441
epoch: 20, loss: 744.16, accuracy: 0.6327
epoch: 21, loss: 737.85, accuracy: 0.6536
epoch: 22, loss: 739.18, accuracy: 0.6529
epoch: 23, loss: 737.27, accuracy: 0.6219
epoch: 24, loss: 734.8, accuracy: 0.6462
epoch: 25, loss: 732.96, accuracy: 0.6492
epoch: 26, loss: 731.43, accuracy: 0.6473
epoch: 27, loss: 726.2, accuracy: 0.6543
epoch: 28, loss: 726.27, accuracy: 0.6576
epoch: 29, loss: 727.56, accuracy: 0.6245
epoch: 30, loss: 724.13, accuracy: 0.6404
epoch: 31, loss: 719.71, accuracy: 0.6629
epoch: 32, loss: 717.13, accuracy: 0.6546
epoch: 33, loss: 717.29, accuracy: 0.6542
epoch: 34, loss: 717.79, accuracy: 0.6523
epoch: 35, loss: 715.64, accuracy: 0.6548
epoch: 36, loss: 717.47, accuracy: 0.6536
epoch: 37, loss: 716.42, accuracy: 0.6536
epoch: 38, loss: 711.05, accuracy: 0.6622
epoch: 39, loss: 713.0, accuracy: 0.6558
epoch: 40, loss: 709.35, accuracy: 0.658
epoch: 41, loss: 709.75, accuracy: 0.6591
epoch: 42, loss: 713.22, accuracy: 0.645
epoch: 43, loss: 705.46, accuracy: 0.6652
epoch: 44, loss: 704.64, accuracy: 0.6336
epoch: 45, loss: 705.49, accuracy: 0.6627
epoch: 46, loss: 704.79, accuracy: 0.6582
epoch: 47, loss: 703.5, accuracy: 0.6611
epoch: 48, loss: 704.22, accuracy: 0.665
epoch: 49, loss: 698.08, accuracy: 0.6553
epoch: 50, loss: 704.19, accuracy: 0.6432
epoch: 51, loss: 696.03, accuracy: 0.6494
epoch: 52, loss: 697.71, accuracy: 0.6565
epoch: 53, loss: 697.97, accuracy: 0.6608
epoch: 54, loss: 696.02, accuracy: 0.6629
epoch: 55, loss: 696.48, accuracy: 0.6589
epoch: 56, loss: 700.25, accuracy: 0.6473
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 697.36, accuracy: 0.6681
epoch: 58, loss: 660.9, accuracy: 0.6742
epoch: 59, loss: 655.75, accuracy: 0.6656
epoch: 60, loss: 658.17, accuracy: 0.6753
epoch: 61, loss: 655.18, accuracy: 0.6665
epoch: 62, loss: 655.56, accuracy: 0.6687
epoch: 63, loss: 652.33, accuracy: 0.668
epoch: 64, loss: 652.46, accuracy: 0.6625
epoch: 65, loss: 650.53, accuracy: 0.671
epoch: 66, loss: 655.22, accuracy: 0.6647
epoch: 67, loss: 653.07, accuracy: 0.6745
epoch: 68, loss: 653.42, accuracy: 0.6736
epoch: 69, loss: 651.83, accuracy: 0.6782
epoch: 70, loss: 648.99, accuracy: 0.6765
epoch: 71, loss: 648.99, accuracy: 0.6654
epoch: 72, loss: 649.65, accuracy: 0.6718
epoch: 73, loss: 649.23, accuracy: 0.6686
epoch: 74, loss: 648.03, accuracy: 0.6793
epoch: 75, loss: 613.01, accuracy: 0.6883
epoch: 76, loss: 608.78, accuracy: 0.6853
epoch: 77, loss: 610.31, accuracy: 0.6887
epoch: 78, loss: 608.51, accuracy: 0.6869
epoch: 79, loss: 607.95, accuracy: 0.6866
epoch: 80, loss: 607.5, accuracy: 0.6873
epoch: 81, loss: 607.28, accuracy: 0.6865
epoch: 82, loss: 607.9, accuracy: 0.6885
epoch: 83, loss: 605.81, accuracy: 0.6862
epoch: 84, loss: 607.16, accuracy: 0.6878
epoch: 85, loss: 606.08, accuracy: 0.687
epoch: 86, loss: 606.33, accuracy: 0.6867
epoch: 87, loss: 606.27, accuracy: 0.6861
epoch: 88, loss: 606.66, accuracy: 0.6886
Epoch    89: reducing learning rate of group 0 to 2.5000e-04.
epoch: 89, loss: 606.16, accuracy: 0.6862
epoch: 90, loss: 604.54, accuracy: 0.6886
epoch: 91, loss: 603.29, accuracy: 0.6879
epoch: 92, loss: 603.58, accuracy: 0.6852
epoch: 93, loss: 602.74, accuracy: 0.6874
epoch: 94, loss: 603.15, accuracy: 0.6879
epoch: 95, loss: 602.57, accuracy: 0.6875
epoch: 96, loss: 603.6, accuracy: 0.6885
epoch: 97, loss: 602.5, accuracy: 0.6879
epoch: 98, loss: 602.72, accuracy: 0.6888
epoch: 99, loss: 603.53, accuracy: 0.6864
time analysis:
    all 8375.6195 s
    train 8372.0032 s
Accuracy of     0 : 66 %
Accuracy of     1 : 88 %
Accuracy of     2 : 51 %
Accuracy of     3 : 43 %
Accuracy of     4 : 52 %
Accuracy of     5 : 52 %
Accuracy of     6 : 75 %
Accuracy of     7 : 62 %
Accuracy of     8 : 81 %
Accuracy of     9 : 79 %
```

## CIFAR10, small_fc, DAUConv2dj, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dj(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dj(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
        DAUConv2dj-1           [-1, 64, 32, 32]           1,344
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
        DAUConv2dj-4            [-1, 8, 16, 16]           2,120
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 8,738
DAU params: 3,464
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1174.38, accuracy: 0.5174
epoch: 1, loss: 992.46, accuracy: 0.5514
epoch: 2, loss: 943.82, accuracy: 0.582
epoch: 3, loss: 911.75, accuracy: 0.5738
epoch: 4, loss: 882.13, accuracy: 0.5904
epoch: 5, loss: 863.16, accuracy: 0.5458
epoch: 6, loss: 844.93, accuracy: 0.6094
epoch: 7, loss: 831.39, accuracy: 0.6047
epoch: 8, loss: 815.29, accuracy: 0.6233
epoch: 9, loss: 808.82, accuracy: 0.6358
epoch: 10, loss: 804.1, accuracy: 0.6222
epoch: 11, loss: 791.9, accuracy: 0.6261
epoch: 12, loss: 788.79, accuracy: 0.6347
epoch: 13, loss: 779.74, accuracy: 0.6104
epoch: 14, loss: 778.43, accuracy: 0.6456
epoch: 15, loss: 773.26, accuracy: 0.6251
epoch: 16, loss: 768.79, accuracy: 0.644
epoch: 17, loss: 764.26, accuracy: 0.6519
epoch: 18, loss: 760.02, accuracy: 0.6451
epoch: 19, loss: 760.64, accuracy: 0.6499
epoch: 20, loss: 754.84, accuracy: 0.6547
epoch: 21, loss: 747.1, accuracy: 0.6537
epoch: 22, loss: 751.24, accuracy: 0.6429
epoch: 23, loss: 746.56, accuracy: 0.6433
epoch: 24, loss: 739.52, accuracy: 0.6589
epoch: 25, loss: 742.48, accuracy: 0.6547
epoch: 26, loss: 738.77, accuracy: 0.6323
epoch: 27, loss: 737.47, accuracy: 0.6533
epoch: 28, loss: 738.18, accuracy: 0.6553
epoch: 29, loss: 725.84, accuracy: 0.6448
epoch: 30, loss: 729.06, accuracy: 0.6572
epoch: 31, loss: 729.04, accuracy: 0.6436
epoch: 32, loss: 725.56, accuracy: 0.658
epoch: 33, loss: 724.38, accuracy: 0.6658
epoch: 34, loss: 721.28, accuracy: 0.6706
epoch: 35, loss: 718.93, accuracy: 0.6477
epoch: 36, loss: 715.68, accuracy: 0.6657
epoch: 37, loss: 714.49, accuracy: 0.6652
epoch: 38, loss: 716.2, accuracy: 0.6648
epoch: 39, loss: 709.74, accuracy: 0.6689
epoch: 40, loss: 713.73, accuracy: 0.6525
epoch: 41, loss: 712.26, accuracy: 0.6461
epoch: 42, loss: 705.98, accuracy: 0.6521
epoch: 43, loss: 709.3, accuracy: 0.669
epoch: 44, loss: 706.05, accuracy: 0.663
epoch: 45, loss: 704.74, accuracy: 0.6563
epoch: 46, loss: 703.73, accuracy: 0.6695
epoch: 47, loss: 706.2, accuracy: 0.6597
epoch: 48, loss: 701.89, accuracy: 0.6654
epoch: 49, loss: 705.39, accuracy: 0.6632
epoch: 50, loss: 700.57, accuracy: 0.6583
epoch: 51, loss: 701.38, accuracy: 0.6665
epoch: 52, loss: 701.17, accuracy: 0.6599
epoch: 53, loss: 702.22, accuracy: 0.6649
epoch: 54, loss: 696.3, accuracy: 0.6562
epoch: 55, loss: 697.77, accuracy: 0.653
epoch: 56, loss: 693.34, accuracy: 0.6695
epoch: 57, loss: 697.95, accuracy: 0.6576
epoch: 58, loss: 695.72, accuracy: 0.6628
epoch: 59, loss: 693.85, accuracy: 0.6587
epoch: 60, loss: 693.52, accuracy: 0.6777
epoch: 61, loss: 692.2, accuracy: 0.6567
epoch: 62, loss: 692.53, accuracy: 0.6743
epoch: 63, loss: 692.83, accuracy: 0.6753
epoch: 64, loss: 689.53, accuracy: 0.669
epoch: 65, loss: 691.71, accuracy: 0.6574
epoch: 66, loss: 692.39, accuracy: 0.6692
epoch: 67, loss: 685.77, accuracy: 0.6609
epoch: 68, loss: 688.82, accuracy: 0.6685
epoch: 69, loss: 688.07, accuracy: 0.6731
epoch: 70, loss: 688.01, accuracy: 0.6597
epoch: 71, loss: 684.02, accuracy: 0.675
epoch: 72, loss: 685.82, accuracy: 0.6676
epoch: 73, loss: 685.58, accuracy: 0.6646
epoch: 74, loss: 687.56, accuracy: 0.6704
epoch: 75, loss: 625.67, accuracy: 0.6928
epoch: 76, loss: 619.5, accuracy: 0.6908
epoch: 77, loss: 618.22, accuracy: 0.6905
epoch: 78, loss: 616.96, accuracy: 0.6928
epoch: 79, loss: 614.6, accuracy: 0.694
epoch: 80, loss: 614.02, accuracy: 0.6926
epoch: 81, loss: 614.67, accuracy: 0.692
epoch: 82, loss: 613.41, accuracy: 0.6951
epoch: 83, loss: 613.64, accuracy: 0.6883
epoch: 84, loss: 612.93, accuracy: 0.6936
epoch: 85, loss: 611.97, accuracy: 0.694
epoch: 86, loss: 613.66, accuracy: 0.6915
epoch: 87, loss: 612.4, accuracy: 0.6911
epoch: 88, loss: 612.47, accuracy: 0.6919
epoch: 89, loss: 612.18, accuracy: 0.6916
epoch: 90, loss: 610.18, accuracy: 0.6915
epoch: 91, loss: 610.34, accuracy: 0.6929
epoch: 92, loss: 610.4, accuracy: 0.6902
epoch: 93, loss: 610.74, accuracy: 0.6898
epoch: 94, loss: 609.66, accuracy: 0.6927
epoch: 95, loss: 610.66, accuracy: 0.6926
epoch: 96, loss: 609.35, accuracy: 0.6901
epoch: 97, loss: 610.68, accuracy: 0.6915
epoch: 98, loss: 609.35, accuracy: 0.6906
epoch: 99, loss: 608.64, accuracy: 0.6904
time analysis:
    all 11428.5254 s
    train 11424.0077 s
Accuracy of     0 : 78 %
Accuracy of     1 : 88 %
Accuracy of     2 : 56 %
Accuracy of     3 : 34 %
Accuracy of     4 : 65 %
Accuracy of     5 : 57 %
Accuracy of     6 : 69 %
Accuracy of     7 : 70 %
Accuracy of     8 : 77 %
Accuracy of     9 : 76 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=1, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             258
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]             522
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,054
DAU params: 780
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1223.11, accuracy: 0.4756
epoch: 1, loss: 1110.48, accuracy: 0.5163
epoch: 2, loss: 1089.26, accuracy: 0.512
epoch: 3, loss: 1083.96, accuracy: 0.4961
epoch: 4, loss: 1073.67, accuracy: 0.5146
epoch: 5, loss: 1063.47, accuracy: 0.5179
epoch: 6, loss: 1060.43, accuracy: 0.5063
epoch: 7, loss: 1053.93, accuracy: 0.5196
epoch: 8, loss: 1052.23, accuracy: 0.5153
epoch: 9, loss: 1050.37, accuracy: 0.514
epoch: 10, loss: 1045.54, accuracy: 0.5253
epoch: 11, loss: 1043.88, accuracy: 0.5206
epoch: 12, loss: 1041.06, accuracy: 0.5206
epoch: 13, loss: 1041.14, accuracy: 0.5214
epoch: 14, loss: 1039.41, accuracy: 0.5298
epoch: 15, loss: 1037.45, accuracy: 0.5198
epoch: 16, loss: 1035.81, accuracy: 0.5302
epoch: 17, loss: 1032.25, accuracy: 0.5253
epoch: 18, loss: 1031.42, accuracy: 0.5193
epoch: 19, loss: 1028.58, accuracy: 0.5221
epoch: 20, loss: 1025.5, accuracy: 0.5192
epoch: 21, loss: 1020.94, accuracy: 0.5204
epoch: 22, loss: 1020.15, accuracy: 0.5265
epoch: 23, loss: 1017.36, accuracy: 0.5244
epoch: 24, loss: 1015.99, accuracy: 0.5024
epoch: 25, loss: 1011.03, accuracy: 0.5135
epoch: 26, loss: 1012.74, accuracy: 0.5296
epoch: 27, loss: 1008.63, accuracy: 0.5085
epoch: 28, loss: 1007.69, accuracy: 0.5258
epoch: 29, loss: 1008.81, accuracy: 0.5329
epoch: 30, loss: 1007.07, accuracy: 0.5134
epoch: 31, loss: 1001.27, accuracy: 0.5066
epoch: 32, loss: 1000.89, accuracy: 0.5277
epoch: 33, loss: 1000.59, accuracy: 0.5346
epoch: 34, loss: 998.12, accuracy: 0.4995
epoch: 35, loss: 1000.97, accuracy: 0.5222
epoch: 36, loss: 998.22, accuracy: 0.5302
epoch: 37, loss: 998.46, accuracy: 0.5126
epoch: 38, loss: 999.97, accuracy: 0.5232
epoch: 39, loss: 997.96, accuracy: 0.5206
Epoch    40: reducing learning rate of group 0 to 5.0000e-03.
epoch: 40, loss: 998.59, accuracy: 0.5235
epoch: 41, loss: 964.81, accuracy: 0.5379
epoch: 42, loss: 964.5, accuracy: 0.5389
epoch: 43, loss: 962.83, accuracy: 0.538
epoch: 44, loss: 961.81, accuracy: 0.5392
epoch: 45, loss: 960.02, accuracy: 0.5409
epoch: 46, loss: 961.9, accuracy: 0.5483
epoch: 47, loss: 956.48, accuracy: 0.5412
epoch: 48, loss: 959.77, accuracy: 0.5485
epoch: 49, loss: 960.03, accuracy: 0.5484
epoch: 50, loss: 955.88, accuracy: 0.5304
epoch: 51, loss: 960.49, accuracy: 0.5429
epoch: 52, loss: 960.38, accuracy: 0.5416
Epoch    53: reducing learning rate of group 0 to 2.5000e-03.
epoch: 53, loss: 958.5, accuracy: 0.5348
epoch: 54, loss: 942.2, accuracy: 0.5438
epoch: 55, loss: 941.21, accuracy: 0.5462
epoch: 56, loss: 941.67, accuracy: 0.5455
epoch: 57, loss: 938.06, accuracy: 0.5496
epoch: 58, loss: 939.26, accuracy: 0.5473
epoch: 59, loss: 940.09, accuracy: 0.5496
epoch: 60, loss: 938.09, accuracy: 0.5466
epoch: 61, loss: 938.5, accuracy: 0.5463
epoch: 62, loss: 936.94, accuracy: 0.5452
epoch: 63, loss: 937.69, accuracy: 0.5465
epoch: 64, loss: 937.22, accuracy: 0.5517
epoch: 65, loss: 938.09, accuracy: 0.5473
epoch: 66, loss: 935.18, accuracy: 0.5483
epoch: 67, loss: 935.84, accuracy: 0.5459
epoch: 68, loss: 936.66, accuracy: 0.5478
epoch: 69, loss: 937.5, accuracy: 0.5507
epoch: 70, loss: 935.39, accuracy: 0.5463
epoch: 71, loss: 937.4, accuracy: 0.5439
Epoch    72: reducing learning rate of group 0 to 1.2500e-03.
epoch: 72, loss: 934.94, accuracy: 0.5444
epoch: 73, loss: 926.99, accuracy: 0.5506
epoch: 74, loss: 925.96, accuracy: 0.5523
epoch: 75, loss: 917.78, accuracy: 0.5535
epoch: 76, loss: 917.73, accuracy: 0.553
epoch: 77, loss: 916.33, accuracy: 0.5519
epoch: 78, loss: 916.76, accuracy: 0.5519
epoch: 79, loss: 918.52, accuracy: 0.5553
epoch: 80, loss: 917.09, accuracy: 0.5532
epoch: 81, loss: 916.27, accuracy: 0.5543
epoch: 82, loss: 914.81, accuracy: 0.5523
epoch: 83, loss: 916.46, accuracy: 0.5543
epoch: 84, loss: 915.38, accuracy: 0.5517
epoch: 85, loss: 915.68, accuracy: 0.5543
epoch: 86, loss: 917.35, accuracy: 0.554
epoch: 87, loss: 916.35, accuracy: 0.554
Epoch    88: reducing learning rate of group 0 to 1.0000e-04.
epoch: 88, loss: 916.67, accuracy: 0.5533
epoch: 89, loss: 915.43, accuracy: 0.552
epoch: 90, loss: 914.8, accuracy: 0.5546
epoch: 91, loss: 915.85, accuracy: 0.5523
epoch: 92, loss: 915.11, accuracy: 0.5534
epoch: 93, loss: 915.46, accuracy: 0.5512
epoch: 94, loss: 916.39, accuracy: 0.5527
epoch: 95, loss: 914.67, accuracy: 0.5522
epoch: 96, loss: 915.12, accuracy: 0.556
epoch: 97, loss: 914.26, accuracy: 0.5528
epoch: 98, loss: 915.55, accuracy: 0.554
epoch: 99, loss: 915.32, accuracy: 0.5508
time analysis:
    all 1355.2516 s
    train 1349.8285 s
Accuracy of     0 : 66 %
Accuracy of     1 : 64 %
Accuracy of     2 : 39 %
Accuracy of     3 : 39 %
Accuracy of     4 : 49 %
Accuracy of     5 : 47 %
Accuracy of     6 : 64 %
Accuracy of     7 : 54 %
Accuracy of     8 : 70 %
Accuracy of     9 : 58 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=2, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=2, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             452
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,036
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,762
DAU params: 1,488
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1221.21, accuracy: 0.4845
epoch: 1, loss: 1076.3, accuracy: 0.5161
epoch: 2, loss: 1018.3, accuracy: 0.5523
epoch: 3, loss: 995.07, accuracy: 0.5596
epoch: 4, loss: 979.1, accuracy: 0.5206
epoch: 5, loss: 966.33, accuracy: 0.5479
epoch: 6, loss: 961.12, accuracy: 0.5331
epoch: 7, loss: 949.46, accuracy: 0.5651
epoch: 8, loss: 945.35, accuracy: 0.549
epoch: 9, loss: 941.95, accuracy: 0.5621
epoch: 10, loss: 934.01, accuracy: 0.5614
epoch: 11, loss: 926.66, accuracy: 0.5615
epoch: 12, loss: 928.34, accuracy: 0.566
epoch: 13, loss: 923.78, accuracy: 0.5668
epoch: 14, loss: 920.48, accuracy: 0.5711
epoch: 15, loss: 915.82, accuracy: 0.5538
epoch: 16, loss: 916.96, accuracy: 0.5739
epoch: 17, loss: 911.34, accuracy: 0.5589
epoch: 18, loss: 912.77, accuracy: 0.5665
epoch: 19, loss: 909.15, accuracy: 0.5629
epoch: 20, loss: 906.37, accuracy: 0.5702
epoch: 21, loss: 904.9, accuracy: 0.5739
epoch: 22, loss: 899.98, accuracy: 0.5626
epoch: 23, loss: 899.06, accuracy: 0.5771
epoch: 24, loss: 899.55, accuracy: 0.5762
epoch: 25, loss: 892.56, accuracy: 0.57
epoch: 26, loss: 897.04, accuracy: 0.5873
epoch: 27, loss: 890.41, accuracy: 0.5655
epoch: 28, loss: 890.76, accuracy: 0.5731
epoch: 29, loss: 888.5, accuracy: 0.5753
epoch: 30, loss: 891.64, accuracy: 0.5808
epoch: 31, loss: 887.26, accuracy: 0.589
epoch: 32, loss: 883.58, accuracy: 0.5648
epoch: 33, loss: 884.81, accuracy: 0.5764
epoch: 34, loss: 880.06, accuracy: 0.5836
epoch: 35, loss: 882.09, accuracy: 0.585
epoch: 36, loss: 881.36, accuracy: 0.5748
epoch: 37, loss: 881.27, accuracy: 0.5801
epoch: 38, loss: 876.93, accuracy: 0.5854
epoch: 39, loss: 877.41, accuracy: 0.557
epoch: 40, loss: 876.32, accuracy: 0.5931
epoch: 41, loss: 875.73, accuracy: 0.5765
epoch: 42, loss: 871.6, accuracy: 0.5673
epoch: 43, loss: 873.18, accuracy: 0.5714
epoch: 44, loss: 873.18, accuracy: 0.5805
epoch: 45, loss: 873.44, accuracy: 0.5774
epoch: 46, loss: 870.69, accuracy: 0.5697
epoch: 47, loss: 872.56, accuracy: 0.5761
epoch: 48, loss: 869.3, accuracy: 0.582
epoch: 49, loss: 869.93, accuracy: 0.5689
epoch: 50, loss: 866.61, accuracy: 0.5762
epoch: 51, loss: 868.12, accuracy: 0.5931
epoch: 52, loss: 866.35, accuracy: 0.5859
epoch: 53, loss: 868.03, accuracy: 0.5734
epoch: 54, loss: 863.34, accuracy: 0.5861
epoch: 55, loss: 862.65, accuracy: 0.5873
epoch: 56, loss: 863.92, accuracy: 0.597
epoch: 57, loss: 865.76, accuracy: 0.5663
epoch: 58, loss: 865.37, accuracy: 0.5584
epoch: 59, loss: 865.37, accuracy: 0.578
Epoch    60: reducing learning rate of group 0 to 5.0000e-03.
epoch: 60, loss: 868.62, accuracy: 0.5722
epoch: 61, loss: 831.25, accuracy: 0.6024
epoch: 62, loss: 830.54, accuracy: 0.5889
epoch: 63, loss: 831.07, accuracy: 0.589
epoch: 64, loss: 829.42, accuracy: 0.5869
epoch: 65, loss: 830.05, accuracy: 0.592
epoch: 66, loss: 829.44, accuracy: 0.5963
epoch: 67, loss: 828.49, accuracy: 0.5909
epoch: 68, loss: 828.73, accuracy: 0.5935
epoch: 69, loss: 827.34, accuracy: 0.6043
epoch: 70, loss: 824.09, accuracy: 0.5994
epoch: 71, loss: 825.59, accuracy: 0.5991
epoch: 72, loss: 826.76, accuracy: 0.5961
epoch: 73, loss: 827.41, accuracy: 0.5943
epoch: 74, loss: 824.43, accuracy: 0.593
epoch: 75, loss: 795.88, accuracy: 0.6051
epoch: 76, loss: 794.04, accuracy: 0.6096
epoch: 77, loss: 791.24, accuracy: 0.6072
epoch: 78, loss: 791.55, accuracy: 0.6101
epoch: 79, loss: 792.16, accuracy: 0.6111
epoch: 80, loss: 790.92, accuracy: 0.6097
epoch: 81, loss: 790.74, accuracy: 0.6088
epoch: 82, loss: 790.72, accuracy: 0.609
epoch: 83, loss: 790.33, accuracy: 0.6065
epoch: 84, loss: 790.56, accuracy: 0.6104
epoch: 85, loss: 790.58, accuracy: 0.6096
epoch: 86, loss: 790.76, accuracy: 0.6087
epoch: 87, loss: 789.98, accuracy: 0.6105
epoch: 88, loss: 790.39, accuracy: 0.6111
epoch: 89, loss: 788.87, accuracy: 0.6067
epoch: 90, loss: 790.4, accuracy: 0.609
epoch: 91, loss: 790.15, accuracy: 0.6075
epoch: 92, loss: 789.97, accuracy: 0.6094
epoch: 93, loss: 789.37, accuracy: 0.6089
epoch: 94, loss: 789.01, accuracy: 0.6117
Epoch    95: reducing learning rate of group 0 to 2.5000e-04.
epoch: 95, loss: 790.79, accuracy: 0.6097
epoch: 96, loss: 788.67, accuracy: 0.61
epoch: 97, loss: 786.01, accuracy: 0.6109
epoch: 98, loss: 787.16, accuracy: 0.6106
epoch: 99, loss: 786.54, accuracy: 0.6081
time analysis:
    all 1758.2209 s
    train 1755.8775 s
Accuracy of     0 : 67 %
Accuracy of     1 : 78 %
Accuracy of     2 : 48 %
Accuracy of     3 : 42 %
Accuracy of     4 : 54 %
Accuracy of     5 : 59 %
Accuracy of     6 : 78 %
Accuracy of     7 : 57 %
Accuracy of     8 : 63 %
Accuracy of     9 : 62 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 3
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=3, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=3, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             646
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           1,550
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 7,470
DAU params: 2,196
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1229.63, accuracy: 0.4966
epoch: 1, loss: 1040.09, accuracy: 0.5192
epoch: 2, loss: 978.3, accuracy: 0.5579
epoch: 3, loss: 939.07, accuracy: 0.566
epoch: 4, loss: 914.88, accuracy: 0.5783
epoch: 5, loss: 896.11, accuracy: 0.5921
epoch: 6, loss: 880.51, accuracy: 0.5928
epoch: 7, loss: 874.82, accuracy: 0.5766
epoch: 8, loss: 862.34, accuracy: 0.5913
epoch: 9, loss: 856.04, accuracy: 0.5962
epoch: 10, loss: 847.86, accuracy: 0.6031
epoch: 11, loss: 843.86, accuracy: 0.5976
epoch: 12, loss: 840.94, accuracy: 0.614
epoch: 13, loss: 833.85, accuracy: 0.5987
epoch: 14, loss: 830.24, accuracy: 0.6125
epoch: 15, loss: 825.42, accuracy: 0.6068
epoch: 16, loss: 825.07, accuracy: 0.6149
epoch: 17, loss: 821.67, accuracy: 0.6103
epoch: 18, loss: 814.78, accuracy: 0.6168
epoch: 19, loss: 816.95, accuracy: 0.6124
epoch: 20, loss: 812.38, accuracy: 0.606
epoch: 21, loss: 808.06, accuracy: 0.6103
epoch: 22, loss: 804.36, accuracy: 0.6189
epoch: 23, loss: 805.86, accuracy: 0.6176
epoch: 24, loss: 801.75, accuracy: 0.6231
epoch: 25, loss: 801.09, accuracy: 0.6278
epoch: 26, loss: 797.12, accuracy: 0.6194
epoch: 27, loss: 794.54, accuracy: 0.63
epoch: 28, loss: 795.16, accuracy: 0.6268
epoch: 29, loss: 790.62, accuracy: 0.6339
epoch: 30, loss: 788.95, accuracy: 0.6275
epoch: 31, loss: 787.24, accuracy: 0.608
epoch: 32, loss: 786.31, accuracy: 0.6082
epoch: 33, loss: 783.44, accuracy: 0.6295
epoch: 34, loss: 781.0, accuracy: 0.6074
epoch: 35, loss: 780.68, accuracy: 0.6227
epoch: 36, loss: 784.06, accuracy: 0.6204
epoch: 37, loss: 780.49, accuracy: 0.6373
epoch: 38, loss: 779.53, accuracy: 0.5924
epoch: 39, loss: 773.18, accuracy: 0.6211
epoch: 40, loss: 775.05, accuracy: 0.6292
epoch: 41, loss: 775.65, accuracy: 0.6011
epoch: 42, loss: 775.45, accuracy: 0.6178
epoch: 43, loss: 773.7, accuracy: 0.6233
epoch: 44, loss: 771.79, accuracy: 0.6284
epoch: 45, loss: 770.74, accuracy: 0.6345
epoch: 46, loss: 765.32, accuracy: 0.624
epoch: 47, loss: 771.06, accuracy: 0.6305
epoch: 48, loss: 770.12, accuracy: 0.6302
epoch: 49, loss: 766.81, accuracy: 0.6352
epoch: 50, loss: 764.28, accuracy: 0.6171
epoch: 51, loss: 760.92, accuracy: 0.6403
epoch: 52, loss: 766.6, accuracy: 0.6162
epoch: 53, loss: 765.31, accuracy: 0.6246
epoch: 54, loss: 762.21, accuracy: 0.6424
epoch: 55, loss: 763.22, accuracy: 0.6294
epoch: 56, loss: 760.98, accuracy: 0.614
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 760.33, accuracy: 0.6322
epoch: 58, loss: 728.6, accuracy: 0.6472
epoch: 59, loss: 726.01, accuracy: 0.6464
epoch: 60, loss: 728.49, accuracy: 0.6432
epoch: 61, loss: 725.43, accuracy: 0.643
epoch: 62, loss: 722.42, accuracy: 0.6454
epoch: 63, loss: 722.8, accuracy: 0.6471
epoch: 64, loss: 725.33, accuracy: 0.6481
epoch: 65, loss: 722.07, accuracy: 0.6472
epoch: 66, loss: 720.95, accuracy: 0.6443
epoch: 67, loss: 722.17, accuracy: 0.6535
epoch: 68, loss: 722.65, accuracy: 0.6423
epoch: 69, loss: 719.41, accuracy: 0.651
epoch: 70, loss: 721.61, accuracy: 0.6447
epoch: 71, loss: 720.97, accuracy: 0.6446
epoch: 72, loss: 717.99, accuracy: 0.6453
epoch: 73, loss: 717.13, accuracy: 0.6462
epoch: 74, loss: 719.62, accuracy: 0.6456
epoch: 75, loss: 687.23, accuracy: 0.6562
epoch: 76, loss: 684.27, accuracy: 0.6579
epoch: 77, loss: 684.24, accuracy: 0.6569
epoch: 78, loss: 684.46, accuracy: 0.6589
epoch: 79, loss: 682.39, accuracy: 0.6586
epoch: 80, loss: 683.92, accuracy: 0.6552
epoch: 81, loss: 682.86, accuracy: 0.6577
epoch: 82, loss: 683.09, accuracy: 0.6568
epoch: 83, loss: 681.91, accuracy: 0.6576
epoch: 84, loss: 683.01, accuracy: 0.6544
Epoch    85: reducing learning rate of group 0 to 2.5000e-04.
epoch: 85, loss: 682.16, accuracy: 0.6564
epoch: 86, loss: 679.07, accuracy: 0.66
epoch: 87, loss: 680.94, accuracy: 0.6596
epoch: 88, loss: 678.77, accuracy: 0.6573
epoch: 89, loss: 679.0, accuracy: 0.6576
epoch: 90, loss: 680.22, accuracy: 0.6569
epoch: 91, loss: 679.64, accuracy: 0.6555
epoch: 92, loss: 679.15, accuracy: 0.6608
epoch: 93, loss: 680.33, accuracy: 0.6602
epoch: 94, loss: 678.93, accuracy: 0.6584
epoch: 95, loss: 680.25, accuracy: 0.6573
Epoch    96: reducing learning rate of group 0 to 1.2500e-04.
epoch: 96, loss: 679.59, accuracy: 0.6576
epoch: 97, loss: 678.9, accuracy: 0.6574
epoch: 98, loss: 678.72, accuracy: 0.6585
epoch: 99, loss: 678.92, accuracy: 0.6588
time analysis:
    all 2325.8894 s
    train 2322.95 s
Accuracy of     0 : 71 %
Accuracy of     1 : 82 %
Accuracy of     2 : 46 %
Accuracy of     3 : 38 %
Accuracy of     4 : 52 %
Accuracy of     5 : 55 %
Accuracy of     6 : 78 %
Accuracy of     7 : 60 %
Accuracy of     8 : 74 %
Accuracy of     9 : 73 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 4
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=4, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=4, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]             840
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           2,064
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 8,178
DAU params: 2,904
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1239.0, accuracy: 0.5029
epoch: 1, loss: 1070.56, accuracy: 0.518
epoch: 2, loss: 997.6, accuracy: 0.5311
epoch: 3, loss: 970.74, accuracy: 0.5533
epoch: 4, loss: 952.3, accuracy: 0.5537
epoch: 5, loss: 944.58, accuracy: 0.5424
epoch: 6, loss: 935.18, accuracy: 0.5512
epoch: 7, loss: 927.87, accuracy: 0.5774
epoch: 8, loss: 922.8, accuracy: 0.5656
epoch: 9, loss: 914.65, accuracy: 0.565
epoch: 10, loss: 907.82, accuracy: 0.5793
epoch: 11, loss: 905.12, accuracy: 0.5492
epoch: 12, loss: 901.02, accuracy: 0.5719
epoch: 13, loss: 893.8, accuracy: 0.5763
epoch: 14, loss: 890.01, accuracy: 0.5918
epoch: 15, loss: 887.88, accuracy: 0.5488
epoch: 16, loss: 884.7, accuracy: 0.5877
epoch: 17, loss: 883.28, accuracy: 0.5786
epoch: 18, loss: 877.12, accuracy: 0.5693
epoch: 19, loss: 877.44, accuracy: 0.5676
epoch: 20, loss: 874.2, accuracy: 0.5793
epoch: 21, loss: 868.03, accuracy: 0.5831
epoch: 22, loss: 868.98, accuracy: 0.5883
epoch: 23, loss: 867.97, accuracy: 0.5853
epoch: 24, loss: 866.1, accuracy: 0.577
epoch: 25, loss: 862.8, accuracy: 0.5787
epoch: 26, loss: 865.77, accuracy: 0.5884
epoch: 27, loss: 862.54, accuracy: 0.5785
epoch: 28, loss: 861.6, accuracy: 0.603
epoch: 29, loss: 858.36, accuracy: 0.5872
epoch: 30, loss: 857.4, accuracy: 0.5816
epoch: 31, loss: 855.06, accuracy: 0.5879
epoch: 32, loss: 854.79, accuracy: 0.5982
epoch: 33, loss: 852.36, accuracy: 0.5945
epoch: 34, loss: 852.2, accuracy: 0.5867
epoch: 35, loss: 849.24, accuracy: 0.5909
epoch: 36, loss: 853.31, accuracy: 0.5889
epoch: 37, loss: 847.05, accuracy: 0.5915
epoch: 38, loss: 846.06, accuracy: 0.597
epoch: 39, loss: 846.68, accuracy: 0.5803
epoch: 40, loss: 843.75, accuracy: 0.5966
epoch: 41, loss: 849.8, accuracy: 0.5924
epoch: 42, loss: 844.26, accuracy: 0.5938
epoch: 43, loss: 844.07, accuracy: 0.5999
epoch: 44, loss: 841.51, accuracy: 0.6006
epoch: 45, loss: 843.36, accuracy: 0.5759
epoch: 46, loss: 840.18, accuracy: 0.5968
epoch: 47, loss: 838.21, accuracy: 0.5942
epoch: 48, loss: 838.81, accuracy: 0.5892
epoch: 49, loss: 841.07, accuracy: 0.5909
epoch: 50, loss: 836.8, accuracy: 0.5955
epoch: 51, loss: 834.96, accuracy: 0.5938
epoch: 52, loss: 836.05, accuracy: 0.6042
epoch: 53, loss: 833.09, accuracy: 0.5958
epoch: 54, loss: 834.04, accuracy: 0.5938
epoch: 55, loss: 830.6, accuracy: 0.5917
epoch: 56, loss: 833.98, accuracy: 0.5798
epoch: 57, loss: 834.89, accuracy: 0.5983
epoch: 58, loss: 834.98, accuracy: 0.5971
epoch: 59, loss: 836.14, accuracy: 0.5876
epoch: 60, loss: 832.07, accuracy: 0.5883
Epoch    61: reducing learning rate of group 0 to 5.0000e-03.
epoch: 61, loss: 831.2, accuracy: 0.5983
epoch: 62, loss: 798.09, accuracy: 0.6056
epoch: 63, loss: 797.08, accuracy: 0.6107
epoch: 64, loss: 798.03, accuracy: 0.6124
epoch: 65, loss: 795.34, accuracy: 0.6101
epoch: 66, loss: 793.73, accuracy: 0.6147
epoch: 67, loss: 795.45, accuracy: 0.5972
epoch: 68, loss: 795.94, accuracy: 0.6101
epoch: 69, loss: 792.31, accuracy: 0.6096
epoch: 70, loss: 791.22, accuracy: 0.6073
epoch: 71, loss: 794.53, accuracy: 0.6056
epoch: 72, loss: 793.04, accuracy: 0.6029
epoch: 73, loss: 793.16, accuracy: 0.6042
epoch: 74, loss: 789.21, accuracy: 0.6021
epoch: 75, loss: 760.23, accuracy: 0.6155
epoch: 76, loss: 757.54, accuracy: 0.6178
epoch: 77, loss: 756.13, accuracy: 0.616
epoch: 78, loss: 756.34, accuracy: 0.6196
epoch: 79, loss: 755.46, accuracy: 0.6172
epoch: 80, loss: 756.16, accuracy: 0.6174
epoch: 81, loss: 755.71, accuracy: 0.6177
epoch: 82, loss: 754.26, accuracy: 0.6193
epoch: 83, loss: 754.89, accuracy: 0.6208
epoch: 84, loss: 755.33, accuracy: 0.6159
epoch: 85, loss: 753.24, accuracy: 0.6188
epoch: 86, loss: 754.34, accuracy: 0.6146
epoch: 87, loss: 753.99, accuracy: 0.6203
epoch: 88, loss: 754.85, accuracy: 0.6212
epoch: 89, loss: 754.14, accuracy: 0.62
epoch: 90, loss: 754.47, accuracy: 0.6186
Epoch    91: reducing learning rate of group 0 to 2.5000e-04.
epoch: 91, loss: 754.3, accuracy: 0.6193
epoch: 92, loss: 751.91, accuracy: 0.6212
epoch: 93, loss: 751.95, accuracy: 0.6188
epoch: 94, loss: 750.55, accuracy: 0.6197
epoch: 95, loss: 751.34, accuracy: 0.6199
epoch: 96, loss: 751.53, accuracy: 0.6188
epoch: 97, loss: 750.72, accuracy: 0.6165
epoch: 98, loss: 751.73, accuracy: 0.6203
epoch: 99, loss: 752.83, accuracy: 0.6189
time analysis:
    all 2953.4772 s
    train 2949.7341 s
Accuracy of     0 : 73 %
Accuracy of     1 : 78 %
Accuracy of     2 : 44 %
Accuracy of     3 : 28 %
Accuracy of     4 : 40 %
Accuracy of     5 : 52 %
Accuracy of     6 : 73 %
Accuracy of     7 : 64 %
Accuracy of     8 : 75 %
Accuracy of     9 : 66 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=5, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=5, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]           1,034
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           2,578
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 8,886
DAU params: 3,612
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1229.03, accuracy: 0.4774
epoch: 1, loss: 1086.55, accuracy: 0.5159
epoch: 2, loss: 1047.03, accuracy: 0.5256
epoch: 3, loss: 1034.68, accuracy: 0.5249
epoch: 4, loss: 1010.74, accuracy: 0.5377
epoch: 5, loss: 994.41, accuracy: 0.5404
epoch: 6, loss: 979.86, accuracy: 0.5503
epoch: 7, loss: 962.59, accuracy: 0.5559
epoch: 8, loss: 940.94, accuracy: 0.563
epoch: 9, loss: 930.25, accuracy: 0.5561
epoch: 10, loss: 919.65, accuracy: 0.5576
epoch: 11, loss: 914.86, accuracy: 0.5543
epoch: 12, loss: 912.03, accuracy: 0.5646
epoch: 13, loss: 904.23, accuracy: 0.5539
epoch: 14, loss: 905.42, accuracy: 0.567
epoch: 15, loss: 898.07, accuracy: 0.5747
epoch: 16, loss: 894.3, accuracy: 0.5436
epoch: 17, loss: 895.12, accuracy: 0.5775
epoch: 18, loss: 894.97, accuracy: 0.5764
epoch: 19, loss: 893.15, accuracy: 0.5526
epoch: 20, loss: 883.69, accuracy: 0.5687
epoch: 21, loss: 885.64, accuracy: 0.5778
epoch: 22, loss: 884.01, accuracy: 0.5634
epoch: 23, loss: 881.55, accuracy: 0.5531
epoch: 24, loss: 880.7, accuracy: 0.5581
epoch: 25, loss: 883.67, accuracy: 0.5715
epoch: 26, loss: 878.35, accuracy: 0.569
epoch: 27, loss: 878.76, accuracy: 0.5751
epoch: 28, loss: 874.88, accuracy: 0.5885
epoch: 29, loss: 872.5, accuracy: 0.5749
epoch: 30, loss: 873.18, accuracy: 0.5725
epoch: 31, loss: 872.83, accuracy: 0.5896
epoch: 32, loss: 871.23, accuracy: 0.5856
epoch: 33, loss: 865.89, accuracy: 0.5481
epoch: 34, loss: 866.33, accuracy: 0.5804
epoch: 35, loss: 867.58, accuracy: 0.5876
epoch: 36, loss: 863.4, accuracy: 0.5641
epoch: 37, loss: 860.42, accuracy: 0.5752
epoch: 38, loss: 862.21, accuracy: 0.5964
epoch: 39, loss: 860.79, accuracy: 0.5855
epoch: 40, loss: 858.99, accuracy: 0.5707
epoch: 41, loss: 856.0, accuracy: 0.5848
epoch: 42, loss: 855.59, accuracy: 0.5913
epoch: 43, loss: 849.37, accuracy: 0.583
epoch: 44, loss: 850.4, accuracy: 0.5823
epoch: 45, loss: 851.04, accuracy: 0.5627
epoch: 46, loss: 847.1, accuracy: 0.5957
epoch: 47, loss: 846.26, accuracy: 0.6015
epoch: 48, loss: 842.51, accuracy: 0.5915
epoch: 49, loss: 842.22, accuracy: 0.5883
epoch: 50, loss: 840.71, accuracy: 0.5819
epoch: 51, loss: 843.46, accuracy: 0.5808
epoch: 52, loss: 841.98, accuracy: 0.5876
epoch: 53, loss: 839.45, accuracy: 0.6008
epoch: 54, loss: 836.69, accuracy: 0.595
epoch: 55, loss: 838.39, accuracy: 0.5856
epoch: 56, loss: 831.04, accuracy: 0.5948
epoch: 57, loss: 833.61, accuracy: 0.5906
epoch: 58, loss: 830.66, accuracy: 0.5871
epoch: 59, loss: 829.47, accuracy: 0.5968
epoch: 60, loss: 828.82, accuracy: 0.6
epoch: 61, loss: 833.1, accuracy: 0.589
epoch: 62, loss: 832.04, accuracy: 0.5962
epoch: 63, loss: 828.52, accuracy: 0.6014
epoch: 64, loss: 827.47, accuracy: 0.5983
epoch: 65, loss: 827.77, accuracy: 0.5906
epoch: 66, loss: 830.54, accuracy: 0.598
epoch: 67, loss: 823.68, accuracy: 0.5946
epoch: 68, loss: 824.97, accuracy: 0.6051
epoch: 69, loss: 821.24, accuracy: 0.5957
epoch: 70, loss: 825.72, accuracy: 0.5943
epoch: 71, loss: 820.87, accuracy: 0.6058
epoch: 72, loss: 821.58, accuracy: 0.6033
epoch: 73, loss: 823.43, accuracy: 0.5944
epoch: 74, loss: 821.83, accuracy: 0.579
epoch: 75, loss: 764.37, accuracy: 0.6219
epoch: 76, loss: 757.85, accuracy: 0.6236
epoch: 77, loss: 755.64, accuracy: 0.6242
epoch: 78, loss: 754.03, accuracy: 0.628
epoch: 79, loss: 754.02, accuracy: 0.6243
epoch: 80, loss: 753.4, accuracy: 0.6198
epoch: 81, loss: 751.8, accuracy: 0.6228
epoch: 82, loss: 752.41, accuracy: 0.6238
epoch: 83, loss: 752.16, accuracy: 0.6167
epoch: 84, loss: 751.72, accuracy: 0.6179
epoch: 85, loss: 751.98, accuracy: 0.6242
epoch: 86, loss: 751.77, accuracy: 0.6224
epoch: 87, loss: 750.01, accuracy: 0.6258
epoch: 88, loss: 750.63, accuracy: 0.6236
epoch: 89, loss: 750.28, accuracy: 0.6256
epoch: 90, loss: 750.07, accuracy: 0.6245
epoch: 91, loss: 749.98, accuracy: 0.6215
epoch: 92, loss: 750.93, accuracy: 0.6244
Epoch    93: reducing learning rate of group 0 to 5.0000e-04.
epoch: 93, loss: 749.54, accuracy: 0.6184
epoch: 94, loss: 743.17, accuracy: 0.6246
epoch: 95, loss: 743.29, accuracy: 0.6255
epoch: 96, loss: 746.38, accuracy: 0.6241
epoch: 97, loss: 744.17, accuracy: 0.624
epoch: 98, loss: 744.42, accuracy: 0.6246
epoch: 99, loss: 744.89, accuracy: 0.6253
time analysis:
    all 3545.5797 s
    train 3541.1202 s
Accuracy of     0 : 64 %
Accuracy of     1 : 76 %
Accuracy of     2 : 43 %
Accuracy of     3 : 41 %
Accuracy of     4 : 54 %
Accuracy of     5 : 54 %
Accuracy of     6 : 75 %
Accuracy of     7 : 68 %
Accuracy of     8 : 75 %
Accuracy of     9 : 66 %
```

## CIFAR10, small_fc, DAUConv2dOneMu, units: 6
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dOneMu(in_channels=3, out_channels=64, no_units=6, sigma=0.5)
  (conv2): DAUConv2dOneMu(in_channels=64, out_channels=8, no_units=6, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    DAUConv2dOneMu-1           [-1, 64, 32, 32]           1,228
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
    DAUConv2dOneMu-4            [-1, 8, 16, 16]           3,092
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 9,594
DAU params: 4,320
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1223.73, accuracy: 0.4921
epoch: 1, loss: 1058.12, accuracy: 0.5008
epoch: 2, loss: 1001.68, accuracy: 0.5462
epoch: 3, loss: 966.85, accuracy: 0.5577
epoch: 4, loss: 945.72, accuracy: 0.5797
epoch: 5, loss: 925.47, accuracy: 0.5739
epoch: 6, loss: 908.39, accuracy: 0.5659
epoch: 7, loss: 891.92, accuracy: 0.5961
epoch: 8, loss: 878.51, accuracy: 0.5949
epoch: 9, loss: 862.97, accuracy: 0.5984
epoch: 10, loss: 859.41, accuracy: 0.5756
epoch: 11, loss: 852.06, accuracy: 0.6093
epoch: 12, loss: 845.14, accuracy: 0.6041
epoch: 13, loss: 836.49, accuracy: 0.6109
epoch: 14, loss: 828.72, accuracy: 0.6128
epoch: 15, loss: 824.6, accuracy: 0.6262
epoch: 16, loss: 821.54, accuracy: 0.6121
epoch: 17, loss: 816.72, accuracy: 0.6191
epoch: 18, loss: 815.54, accuracy: 0.614
epoch: 19, loss: 807.71, accuracy: 0.6177
epoch: 20, loss: 808.02, accuracy: 0.6221
epoch: 21, loss: 799.83, accuracy: 0.6199
epoch: 22, loss: 799.61, accuracy: 0.6171
epoch: 23, loss: 797.79, accuracy: 0.6142
epoch: 24, loss: 792.45, accuracy: 0.605
epoch: 25, loss: 789.59, accuracy: 0.6387
epoch: 26, loss: 788.83, accuracy: 0.6187
epoch: 27, loss: 785.47, accuracy: 0.6213
epoch: 28, loss: 781.88, accuracy: 0.6112
epoch: 29, loss: 784.1, accuracy: 0.6271
epoch: 30, loss: 782.06, accuracy: 0.6112
epoch: 31, loss: 778.98, accuracy: 0.6248
epoch: 32, loss: 776.12, accuracy: 0.6363
epoch: 33, loss: 775.2, accuracy: 0.5882
epoch: 34, loss: 775.18, accuracy: 0.6248
epoch: 35, loss: 770.98, accuracy: 0.6262
epoch: 36, loss: 771.76, accuracy: 0.6246
epoch: 37, loss: 765.89, accuracy: 0.6432
epoch: 38, loss: 764.65, accuracy: 0.6374
epoch: 39, loss: 761.86, accuracy: 0.614
epoch: 40, loss: 760.75, accuracy: 0.625
epoch: 41, loss: 758.03, accuracy: 0.6255
epoch: 42, loss: 757.28, accuracy: 0.6337
epoch: 43, loss: 756.58, accuracy: 0.629
epoch: 44, loss: 753.14, accuracy: 0.6493
epoch: 45, loss: 752.76, accuracy: 0.6252
epoch: 46, loss: 753.13, accuracy: 0.6204
epoch: 47, loss: 747.01, accuracy: 0.6445
epoch: 48, loss: 749.55, accuracy: 0.6438
epoch: 49, loss: 746.98, accuracy: 0.6522
epoch: 50, loss: 741.83, accuracy: 0.6424
epoch: 51, loss: 744.42, accuracy: 0.6516
epoch: 52, loss: 739.23, accuracy: 0.6387
epoch: 53, loss: 738.52, accuracy: 0.641
epoch: 54, loss: 739.42, accuracy: 0.6366
epoch: 55, loss: 738.57, accuracy: 0.6475
epoch: 56, loss: 734.14, accuracy: 0.6299
epoch: 57, loss: 733.56, accuracy: 0.6389
epoch: 58, loss: 732.89, accuracy: 0.6558
epoch: 59, loss: 732.5, accuracy: 0.6543
epoch: 60, loss: 732.73, accuracy: 0.6538
epoch: 61, loss: 730.55, accuracy: 0.6431
epoch: 62, loss: 731.17, accuracy: 0.6493
epoch: 63, loss: 730.05, accuracy: 0.6404
epoch: 64, loss: 732.08, accuracy: 0.6389
epoch: 65, loss: 728.79, accuracy: 0.6492
epoch: 66, loss: 726.35, accuracy: 0.6527
epoch: 67, loss: 723.52, accuracy: 0.6407
epoch: 68, loss: 728.65, accuracy: 0.6534
epoch: 69, loss: 723.21, accuracy: 0.6319
epoch: 70, loss: 727.59, accuracy: 0.6482
epoch: 71, loss: 723.38, accuracy: 0.6285
epoch: 72, loss: 721.58, accuracy: 0.6484
epoch: 73, loss: 719.85, accuracy: 0.6545
epoch: 74, loss: 720.96, accuracy: 0.6626
epoch: 75, loss: 661.83, accuracy: 0.6749
epoch: 76, loss: 655.9, accuracy: 0.6761
epoch: 77, loss: 655.3, accuracy: 0.6791
epoch: 78, loss: 653.18, accuracy: 0.6741
epoch: 79, loss: 652.24, accuracy: 0.6779
epoch: 80, loss: 651.43, accuracy: 0.6737
epoch: 81, loss: 653.09, accuracy: 0.6758
epoch: 82, loss: 652.96, accuracy: 0.675
epoch: 83, loss: 649.09, accuracy: 0.6778
epoch: 84, loss: 650.5, accuracy: 0.679
epoch: 85, loss: 650.2, accuracy: 0.6774
epoch: 86, loss: 649.42, accuracy: 0.6754
epoch: 87, loss: 649.24, accuracy: 0.6781
epoch: 88, loss: 648.91, accuracy: 0.6773
Epoch    89: reducing learning rate of group 0 to 5.0000e-04.
epoch: 89, loss: 648.59, accuracy: 0.6742
epoch: 90, loss: 644.41, accuracy: 0.6781
epoch: 91, loss: 643.06, accuracy: 0.678
epoch: 92, loss: 642.88, accuracy: 0.6805
epoch: 93, loss: 643.92, accuracy: 0.6783
epoch: 94, loss: 644.04, accuracy: 0.6791
epoch: 95, loss: 645.79, accuracy: 0.6801
epoch: 96, loss: 644.01, accuracy: 0.6805
epoch: 97, loss: 642.66, accuracy: 0.6778
epoch: 98, loss: 642.74, accuracy: 0.6762
epoch: 99, loss: 643.05, accuracy: 0.6802
time analysis:
    all 4118.7533 s
    train 4113.5344 s
Accuracy of     0 : 75 %
Accuracy of     1 : 76 %
Accuracy of     2 : 54 %
Accuracy of     3 : 42 %
Accuracy of     4 : 54 %
Accuracy of     5 : 64 %
Accuracy of     6 : 71 %
Accuracy of     7 : 65 %
Accuracy of     8 : 75 %
Accuracy of     9 : 76 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 0.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=0.5)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=0.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1230.89, accuracy: 0.4659
epoch: 1, loss: 1109.91, accuracy: 0.4968
epoch: 2, loss: 1089.75, accuracy: 0.5011
epoch: 3, loss: 1079.77, accuracy: 0.5091
epoch: 4, loss: 1067.2, accuracy: 0.5169
epoch: 5, loss: 1063.96, accuracy: 0.5173
epoch: 6, loss: 1061.07, accuracy: 0.5116
epoch: 7, loss: 1056.32, accuracy: 0.5065
epoch: 8, loss: 1049.16, accuracy: 0.5165
epoch: 9, loss: 1045.65, accuracy: 0.51
epoch: 10, loss: 1043.59, accuracy: 0.5168
epoch: 11, loss: 1041.2, accuracy: 0.5217
epoch: 12, loss: 1039.49, accuracy: 0.5235
epoch: 13, loss: 1035.15, accuracy: 0.5261
epoch: 14, loss: 1034.25, accuracy: 0.5169
epoch: 15, loss: 1033.18, accuracy: 0.5196
epoch: 16, loss: 1030.03, accuracy: 0.5147
epoch: 17, loss: 1029.37, accuracy: 0.5252
epoch: 18, loss: 1026.99, accuracy: 0.5338
epoch: 19, loss: 1023.99, accuracy: 0.5173
epoch: 20, loss: 1020.56, accuracy: 0.5165
epoch: 21, loss: 1022.11, accuracy: 0.523
epoch: 22, loss: 1024.45, accuracy: 0.5246
epoch: 23, loss: 1019.08, accuracy: 0.5254
epoch: 24, loss: 1019.42, accuracy: 0.5185
epoch: 25, loss: 1015.86, accuracy: 0.5301
epoch: 26, loss: 1013.47, accuracy: 0.5141
epoch: 27, loss: 1014.16, accuracy: 0.528
epoch: 28, loss: 1015.29, accuracy: 0.5233
epoch: 29, loss: 1013.1, accuracy: 0.5172
epoch: 30, loss: 1012.57, accuracy: 0.5343
epoch: 31, loss: 1007.67, accuracy: 0.5249
epoch: 32, loss: 1006.41, accuracy: 0.5315
epoch: 33, loss: 1007.51, accuracy: 0.5252
epoch: 34, loss: 1004.08, accuracy: 0.5242
epoch: 35, loss: 1008.08, accuracy: 0.5216
epoch: 36, loss: 1004.98, accuracy: 0.5203
epoch: 37, loss: 1001.36, accuracy: 0.5112
epoch: 38, loss: 1002.58, accuracy: 0.5258
epoch: 39, loss: 1004.3, accuracy: 0.5175
epoch: 40, loss: 1001.82, accuracy: 0.5116
epoch: 41, loss: 1001.36, accuracy: 0.5197
epoch: 42, loss: 1000.37, accuracy: 0.5254
epoch: 43, loss: 998.03, accuracy: 0.5153
epoch: 44, loss: 996.0, accuracy: 0.4914
epoch: 45, loss: 995.49, accuracy: 0.5359
epoch: 46, loss: 995.09, accuracy: 0.5288
epoch: 47, loss: 992.79, accuracy: 0.5356
epoch: 48, loss: 993.19, accuracy: 0.5304
epoch: 49, loss: 989.67, accuracy: 0.534
epoch: 50, loss: 988.45, accuracy: 0.5328
epoch: 51, loss: 984.43, accuracy: 0.5283
epoch: 52, loss: 987.59, accuracy: 0.5291
epoch: 53, loss: 990.66, accuracy: 0.5165
epoch: 54, loss: 990.35, accuracy: 0.5318
epoch: 55, loss: 989.28, accuracy: 0.5409
epoch: 56, loss: 985.28, accuracy: 0.5238
Epoch    57: reducing learning rate of group 0 to 5.0000e-03.
epoch: 57, loss: 987.39, accuracy: 0.54
epoch: 58, loss: 957.35, accuracy: 0.5368
epoch: 59, loss: 956.02, accuracy: 0.5431
epoch: 60, loss: 954.92, accuracy: 0.5395
epoch: 61, loss: 954.54, accuracy: 0.5427
epoch: 62, loss: 955.47, accuracy: 0.5456
epoch: 63, loss: 954.72, accuracy: 0.5383
epoch: 64, loss: 955.04, accuracy: 0.5377
epoch: 65, loss: 953.46, accuracy: 0.5459
epoch: 66, loss: 950.82, accuracy: 0.5432
epoch: 67, loss: 952.63, accuracy: 0.5443
epoch: 68, loss: 950.36, accuracy: 0.5437
epoch: 69, loss: 952.87, accuracy: 0.5398
epoch: 70, loss: 951.64, accuracy: 0.5389
epoch: 71, loss: 949.24, accuracy: 0.54
epoch: 72, loss: 950.64, accuracy: 0.5462
epoch: 73, loss: 951.47, accuracy: 0.5361
epoch: 74, loss: 951.69, accuracy: 0.5421
epoch: 75, loss: 921.05, accuracy: 0.5515
epoch: 76, loss: 919.92, accuracy: 0.5526
epoch: 77, loss: 919.33, accuracy: 0.5513
epoch: 78, loss: 917.48, accuracy: 0.5481
epoch: 79, loss: 918.32, accuracy: 0.5526
epoch: 80, loss: 918.16, accuracy: 0.5517
epoch: 81, loss: 916.36, accuracy: 0.5505
epoch: 82, loss: 918.21, accuracy: 0.5509
epoch: 83, loss: 918.47, accuracy: 0.5515
epoch: 84, loss: 916.05, accuracy: 0.554
epoch: 85, loss: 916.32, accuracy: 0.5505
epoch: 86, loss: 917.37, accuracy: 0.5513
Epoch    87: reducing learning rate of group 0 to 2.5000e-04.
epoch: 87, loss: 917.34, accuracy: 0.5531
epoch: 88, loss: 913.8, accuracy: 0.5518
epoch: 89, loss: 914.28, accuracy: 0.5529
epoch: 90, loss: 915.67, accuracy: 0.5513
epoch: 91, loss: 915.54, accuracy: 0.55
epoch: 92, loss: 915.37, accuracy: 0.5529
epoch: 93, loss: 915.32, accuracy: 0.5519
epoch: 94, loss: 914.68, accuracy: 0.5509
epoch: 95, loss: 915.17, accuracy: 0.5518
epoch: 96, loss: 914.49, accuracy: 0.5526
epoch: 97, loss: 913.62, accuracy: 0.5529
Epoch    98: reducing learning rate of group 0 to 1.2500e-04.
epoch: 98, loss: 913.81, accuracy: 0.5511
epoch: 99, loss: 913.93, accuracy: 0.5498
time analysis:
    all 1140.9421 s
    train 1135.0066 s
Accuracy of     0 : 64 %
Accuracy of     1 : 62 %
Accuracy of     2 : 40 %
Accuracy of     3 : 35 %
Accuracy of     4 : 43 %
Accuracy of     5 : 40 %
Accuracy of     6 : 66 %
Accuracy of     7 : 54 %
Accuracy of     8 : 68 %
Accuracy of     9 : 60 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=1)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=1)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1295.15, accuracy: 0.4453
epoch: 1, loss: 1192.94, accuracy: 0.4758
epoch: 2, loss: 1157.51, accuracy: 0.4891
epoch: 3, loss: 1134.75, accuracy: 0.4859
epoch: 4, loss: 1119.73, accuracy: 0.4981
epoch: 5, loss: 1109.07, accuracy: 0.5023
epoch: 6, loss: 1103.3, accuracy: 0.5113
epoch: 7, loss: 1093.81, accuracy: 0.4904
epoch: 8, loss: 1089.91, accuracy: 0.4579
epoch: 9, loss: 1085.01, accuracy: 0.5072
epoch: 10, loss: 1083.29, accuracy: 0.5065
epoch: 11, loss: 1080.45, accuracy: 0.5098
epoch: 12, loss: 1077.28, accuracy: 0.5086
epoch: 13, loss: 1074.99, accuracy: 0.5094
epoch: 14, loss: 1069.3, accuracy: 0.5068
epoch: 15, loss: 1069.06, accuracy: 0.5021
epoch: 16, loss: 1062.16, accuracy: 0.5115
epoch: 17, loss: 1067.34, accuracy: 0.5172
epoch: 18, loss: 1061.59, accuracy: 0.5156
epoch: 19, loss: 1062.05, accuracy: 0.488
epoch: 20, loss: 1061.02, accuracy: 0.5205
epoch: 21, loss: 1058.75, accuracy: 0.4979
epoch: 22, loss: 1057.82, accuracy: 0.4753
epoch: 23, loss: 1055.83, accuracy: 0.5119
epoch: 24, loss: 1056.52, accuracy: 0.4874
epoch: 25, loss: 1054.79, accuracy: 0.5269
epoch: 26, loss: 1049.79, accuracy: 0.517
epoch: 27, loss: 1051.52, accuracy: 0.5146
epoch: 28, loss: 1052.62, accuracy: 0.4974
epoch: 29, loss: 1050.76, accuracy: 0.515
epoch: 30, loss: 1044.6, accuracy: 0.5138
epoch: 31, loss: 1049.77, accuracy: 0.5188
epoch: 32, loss: 1047.41, accuracy: 0.51
epoch: 33, loss: 1043.7, accuracy: 0.5111
epoch: 34, loss: 1046.46, accuracy: 0.5256
epoch: 35, loss: 1044.13, accuracy: 0.4903
epoch: 36, loss: 1043.46, accuracy: 0.5043
epoch: 37, loss: 1042.82, accuracy: 0.5231
epoch: 38, loss: 1040.35, accuracy: 0.4998
epoch: 39, loss: 1038.69, accuracy: 0.5196
epoch: 40, loss: 1040.15, accuracy: 0.5195
epoch: 41, loss: 1036.45, accuracy: 0.502
epoch: 42, loss: 1040.03, accuracy: 0.5175
epoch: 43, loss: 1037.54, accuracy: 0.5189
epoch: 44, loss: 1037.29, accuracy: 0.4964
epoch: 45, loss: 1031.82, accuracy: 0.5134
epoch: 46, loss: 1031.22, accuracy: 0.5216
epoch: 47, loss: 1034.74, accuracy: 0.5225
epoch: 48, loss: 1033.94, accuracy: 0.5252
epoch: 49, loss: 1033.91, accuracy: 0.4852
epoch: 50, loss: 1034.68, accuracy: 0.5228
Epoch    51: reducing learning rate of group 0 to 5.0000e-03.
epoch: 51, loss: 1031.08, accuracy: 0.5225
epoch: 52, loss: 1005.04, accuracy: 0.5244
epoch: 53, loss: 1002.41, accuracy: 0.5277
epoch: 54, loss: 1001.71, accuracy: 0.5326
epoch: 55, loss: 1002.63, accuracy: 0.532
epoch: 56, loss: 1001.16, accuracy: 0.5373
epoch: 57, loss: 999.43, accuracy: 0.5336
epoch: 58, loss: 997.91, accuracy: 0.5349
epoch: 59, loss: 999.06, accuracy: 0.5317
epoch: 60, loss: 997.03, accuracy: 0.5241
epoch: 61, loss: 1000.56, accuracy: 0.5373
epoch: 62, loss: 1000.13, accuracy: 0.5143
epoch: 63, loss: 996.1, accuracy: 0.534
epoch: 64, loss: 998.83, accuracy: 0.5328
epoch: 65, loss: 996.69, accuracy: 0.5319
epoch: 66, loss: 995.67, accuracy: 0.5337
epoch: 67, loss: 996.94, accuracy: 0.5248
epoch: 68, loss: 997.13, accuracy: 0.5314
epoch: 69, loss: 993.59, accuracy: 0.5309
epoch: 70, loss: 997.21, accuracy: 0.5309
epoch: 71, loss: 993.78, accuracy: 0.5378
epoch: 72, loss: 995.51, accuracy: 0.53
epoch: 73, loss: 995.8, accuracy: 0.5312
epoch: 74, loss: 995.07, accuracy: 0.5343
epoch: 75, loss: 967.36, accuracy: 0.5448
epoch: 76, loss: 966.21, accuracy: 0.546
epoch: 77, loss: 964.89, accuracy: 0.5473
epoch: 78, loss: 962.76, accuracy: 0.5449
epoch: 79, loss: 963.04, accuracy: 0.5457
epoch: 80, loss: 963.29, accuracy: 0.5462
epoch: 81, loss: 964.5, accuracy: 0.5486
epoch: 82, loss: 964.52, accuracy: 0.5477
epoch: 83, loss: 965.01, accuracy: 0.5452
Epoch    84: reducing learning rate of group 0 to 2.5000e-04.
epoch: 84, loss: 963.26, accuracy: 0.5447
epoch: 85, loss: 961.53, accuracy: 0.5463
epoch: 86, loss: 960.64, accuracy: 0.5475
epoch: 87, loss: 961.86, accuracy: 0.5506
epoch: 88, loss: 961.0, accuracy: 0.5488
epoch: 89, loss: 960.11, accuracy: 0.5492
epoch: 90, loss: 960.83, accuracy: 0.5468
epoch: 91, loss: 960.59, accuracy: 0.5455
epoch: 92, loss: 961.65, accuracy: 0.5492
epoch: 93, loss: 961.02, accuracy: 0.5458
epoch: 94, loss: 962.65, accuracy: 0.5491
Epoch    95: reducing learning rate of group 0 to 1.2500e-04.
epoch: 95, loss: 960.84, accuracy: 0.546
epoch: 96, loss: 960.11, accuracy: 0.5496
epoch: 97, loss: 960.21, accuracy: 0.5478
epoch: 98, loss: 959.49, accuracy: 0.5462
epoch: 99, loss: 960.73, accuracy: 0.5473
time analysis:
    all 1150.1296 s
    train 1147.8039 s
Accuracy of     0 : 66 %
Accuracy of     1 : 74 %
Accuracy of     2 : 41 %
Accuracy of     3 : 36 %
Accuracy of     4 : 43 %
Accuracy of     5 : 49 %
Accuracy of     6 : 64 %
Accuracy of     7 : 57 %
Accuracy of     8 : 65 %
Accuracy of     9 : 60 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 1.5
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=1.5)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=1.5)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1323.97, accuracy: 0.4445
epoch: 1, loss: 1230.56, accuracy: 0.4561
epoch: 2, loss: 1205.71, accuracy: 0.4555
epoch: 3, loss: 1188.69, accuracy: 0.4653
epoch: 4, loss: 1179.04, accuracy: 0.4729
epoch: 5, loss: 1171.93, accuracy: 0.4756
epoch: 6, loss: 1167.54, accuracy: 0.4658
epoch: 7, loss: 1160.67, accuracy: 0.4747
epoch: 8, loss: 1157.08, accuracy: 0.4778
epoch: 9, loss: 1150.96, accuracy: 0.4703
epoch: 10, loss: 1145.56, accuracy: 0.4862
epoch: 11, loss: 1141.76, accuracy: 0.4785
epoch: 12, loss: 1138.39, accuracy: 0.4865
epoch: 13, loss: 1137.17, accuracy: 0.4648
epoch: 14, loss: 1132.09, accuracy: 0.4775
epoch: 15, loss: 1132.98, accuracy: 0.4902
epoch: 16, loss: 1125.78, accuracy: 0.4877
epoch: 17, loss: 1124.67, accuracy: 0.4911
epoch: 18, loss: 1122.18, accuracy: 0.4759
epoch: 19, loss: 1123.06, accuracy: 0.4836
epoch: 20, loss: 1115.96, accuracy: 0.486
epoch: 21, loss: 1115.5, accuracy: 0.4986
epoch: 22, loss: 1115.59, accuracy: 0.491
epoch: 23, loss: 1112.24, accuracy: 0.4896
epoch: 24, loss: 1110.02, accuracy: 0.4911
epoch: 25, loss: 1110.33, accuracy: 0.4866
epoch: 26, loss: 1108.78, accuracy: 0.4893
epoch: 27, loss: 1108.36, accuracy: 0.4962
epoch: 28, loss: 1108.09, accuracy: 0.4916
epoch: 29, loss: 1103.66, accuracy: 0.4941
epoch: 30, loss: 1103.27, accuracy: 0.4914
epoch: 31, loss: 1102.44, accuracy: 0.4859
epoch: 32, loss: 1099.75, accuracy: 0.4991
epoch: 33, loss: 1098.88, accuracy: 0.4639
epoch: 34, loss: 1096.57, accuracy: 0.48
epoch: 35, loss: 1095.6, accuracy: 0.4983
epoch: 36, loss: 1097.45, accuracy: 0.4851
epoch: 37, loss: 1097.81, accuracy: 0.4942
epoch: 38, loss: 1095.69, accuracy: 0.4985
epoch: 39, loss: 1095.05, accuracy: 0.5001
epoch: 40, loss: 1092.35, accuracy: 0.5084
epoch: 41, loss: 1093.44, accuracy: 0.4981
epoch: 42, loss: 1093.52, accuracy: 0.4951
epoch: 43, loss: 1089.65, accuracy: 0.4945
epoch: 44, loss: 1091.78, accuracy: 0.4976
epoch: 45, loss: 1090.36, accuracy: 0.5072
epoch: 46, loss: 1087.45, accuracy: 0.4815
epoch: 47, loss: 1086.05, accuracy: 0.5003
epoch: 48, loss: 1085.71, accuracy: 0.505
epoch: 49, loss: 1089.6, accuracy: 0.5065
epoch: 50, loss: 1081.88, accuracy: 0.5011
epoch: 51, loss: 1086.88, accuracy: 0.4941
epoch: 52, loss: 1085.41, accuracy: 0.5003
epoch: 53, loss: 1081.47, accuracy: 0.5038
epoch: 54, loss: 1084.3, accuracy: 0.504
epoch: 55, loss: 1082.72, accuracy: 0.5105
epoch: 56, loss: 1079.29, accuracy: 0.4923
epoch: 57, loss: 1081.18, accuracy: 0.4894
epoch: 58, loss: 1079.89, accuracy: 0.5028
epoch: 59, loss: 1082.4, accuracy: 0.5065
epoch: 60, loss: 1078.93, accuracy: 0.5097
epoch: 61, loss: 1076.19, accuracy: 0.4925
epoch: 62, loss: 1075.86, accuracy: 0.5077
epoch: 63, loss: 1076.1, accuracy: 0.4981
epoch: 64, loss: 1073.56, accuracy: 0.5123
epoch: 65, loss: 1073.21, accuracy: 0.5037
epoch: 66, loss: 1075.0, accuracy: 0.4948
epoch: 67, loss: 1071.23, accuracy: 0.5123
epoch: 68, loss: 1069.8, accuracy: 0.5017
epoch: 69, loss: 1068.11, accuracy: 0.5095
epoch: 70, loss: 1067.02, accuracy: 0.5016
epoch: 71, loss: 1069.19, accuracy: 0.5086
epoch: 72, loss: 1067.6, accuracy: 0.5105
epoch: 73, loss: 1070.28, accuracy: 0.5075
epoch: 74, loss: 1069.26, accuracy: 0.5159
epoch: 75, loss: 1018.84, accuracy: 0.5279
epoch: 76, loss: 1014.42, accuracy: 0.5295
epoch: 77, loss: 1014.38, accuracy: 0.5301
epoch: 78, loss: 1011.18, accuracy: 0.5299
epoch: 79, loss: 1013.48, accuracy: 0.5332
epoch: 80, loss: 1012.61, accuracy: 0.534
epoch: 81, loss: 1011.7, accuracy: 0.5289
epoch: 82, loss: 1011.48, accuracy: 0.5317
epoch: 83, loss: 1010.08, accuracy: 0.5334
epoch: 84, loss: 1011.02, accuracy: 0.5336
epoch: 85, loss: 1010.53, accuracy: 0.5337
epoch: 86, loss: 1010.45, accuracy: 0.5322
epoch: 87, loss: 1010.23, accuracy: 0.5318
epoch: 88, loss: 1008.59, accuracy: 0.5325
epoch: 89, loss: 1008.91, accuracy: 0.5324
epoch: 90, loss: 1008.87, accuracy: 0.5344
epoch: 91, loss: 1007.52, accuracy: 0.5297
epoch: 92, loss: 1007.27, accuracy: 0.5283
epoch: 93, loss: 1006.94, accuracy: 0.5338
epoch: 94, loss: 1006.87, accuracy: 0.5318
epoch: 95, loss: 1006.29, accuracy: 0.5333
epoch: 96, loss: 1006.48, accuracy: 0.5344
epoch: 97, loss: 1007.43, accuracy: 0.5331
epoch: 98, loss: 1006.58, accuracy: 0.5348
epoch: 99, loss: 1006.05, accuracy: 0.534
time analysis:
    all 1171.8719 s
    train 1169.5405 s
Accuracy of     0 : 53 %
Accuracy of     1 : 62 %
Accuracy of     2 : 35 %
Accuracy of     3 : 30 %
Accuracy of     4 : 43 %
Accuracy of     5 : 45 %
Accuracy of     6 : 46 %
Accuracy of     7 : 56 %
Accuracy of     8 : 68 %
Accuracy of     9 : 57 %
```

## CIFAR10, small_fc, DAUConv2dZeroMu, units: 1, sigma: 2
```text
Config: {'data_folder': '/home/marko/Desktop/hdd/data', 'out_folder': '/home/marko/Desktop/hdd/out/', 'num_workers': 10}
Device: cuda:2

Network: Net(
  (conv1): DAUConv2dZeroMu(in_channels=3, out_channels=64, no_units=1, sigma=2)
  (conv2): DAUConv2dZeroMu(in_channels=64, out_channels=8, no_units=1, sigma=2)
  (norm1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (norm2): BatchNorm2d(8, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  (fc1): Linear(in_features=512, out_features=10, bias=True)
)
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
   DAUConv2dZeroMu-1           [-1, 64, 32, 32]             256
       BatchNorm2d-2           [-1, 64, 32, 32]             128
         MaxPool2d-3           [-1, 64, 16, 16]               0
   DAUConv2dZeroMu-4            [-1, 8, 16, 16]             520
       BatchNorm2d-5            [-1, 8, 16, 16]              16
         MaxPool2d-6              [-1, 8, 8, 8]               0
            Linear-7                   [-1, 10]           5,130
================================================================
total params: 6,050
DAU params: 776
other params: 5,274
----------------------------------------------------------------
epoch: 0, loss: 1372.24, accuracy: 0.4118
epoch: 1, loss: 1280.82, accuracy: 0.442
epoch: 2, loss: 1250.89, accuracy: 0.4407
epoch: 3, loss: 1237.31, accuracy: 0.4307
epoch: 4, loss: 1224.52, accuracy: 0.4484
epoch: 5, loss: 1216.86, accuracy: 0.4427
epoch: 6, loss: 1204.45, accuracy: 0.4462
epoch: 7, loss: 1197.94, accuracy: 0.4657
epoch: 8, loss: 1192.09, accuracy: 0.4463
epoch: 9, loss: 1185.89, accuracy: 0.4537
epoch: 10, loss: 1179.81, accuracy: 0.463
epoch: 11, loss: 1178.65, accuracy: 0.4791
epoch: 12, loss: 1170.19, accuracy: 0.471
epoch: 13, loss: 1170.5, accuracy: 0.4763
epoch: 14, loss: 1168.39, accuracy: 0.4648
epoch: 15, loss: 1164.03, accuracy: 0.4644
epoch: 16, loss: 1161.56, accuracy: 0.4878
epoch: 17, loss: 1157.98, accuracy: 0.4824
epoch: 18, loss: 1154.84, accuracy: 0.485
epoch: 19, loss: 1154.24, accuracy: 0.4693
epoch: 20, loss: 1154.47, accuracy: 0.464
epoch: 21, loss: 1150.56, accuracy: 0.4821
epoch: 22, loss: 1148.34, accuracy: 0.4766
epoch: 23, loss: 1149.71, accuracy: 0.4876
epoch: 24, loss: 1142.04, accuracy: 0.4822
epoch: 25, loss: 1143.66, accuracy: 0.4711
epoch: 26, loss: 1142.46, accuracy: 0.4776
epoch: 27, loss: 1139.42, accuracy: 0.4768
epoch: 28, loss: 1139.94, accuracy: 0.4843
epoch: 29, loss: 1133.73, accuracy: 0.4799
epoch: 30, loss: 1132.64, accuracy: 0.4849
epoch: 31, loss: 1134.05, accuracy: 0.4775
epoch: 32, loss: 1131.69, accuracy: 0.4785
epoch: 33, loss: 1132.66, accuracy: 0.4981
epoch: 34, loss: 1130.3, accuracy: 0.4779
epoch: 35, loss: 1126.76, accuracy: 0.4795
epoch: 36, loss: 1129.87, accuracy: 0.4983
epoch: 37, loss: 1124.55, accuracy: 0.4846
epoch: 38, loss: 1123.74, accuracy: 0.4832
epoch: 39, loss: 1125.57, accuracy: 0.4756
epoch: 40, loss: 1125.1, accuracy: 0.4919
epoch: 41, loss: 1122.93, accuracy: 0.495
epoch: 42, loss: 1123.92, accuracy: 0.4692
epoch: 43, loss: 1121.99, accuracy: 0.4799
epoch: 44, loss: 1121.02, accuracy: 0.4735
epoch: 45, loss: 1122.93, accuracy: 0.4958
epoch: 46, loss: 1120.22, accuracy: 0.481
epoch: 47, loss: 1117.49, accuracy: 0.4857
epoch: 48, loss: 1116.05, accuracy: 0.4835
epoch: 49, loss: 1117.76, accuracy: 0.483
epoch: 50, loss: 1111.55, accuracy: 0.4942
epoch: 51, loss: 1116.79, accuracy: 0.4867
epoch: 52, loss: 1114.22, accuracy: 0.4836
epoch: 53, loss: 1112.96, accuracy: 0.5004
epoch: 54, loss: 1113.67, accuracy: 0.4752
epoch: 55, loss: 1105.39, accuracy: 0.4991
epoch: 56, loss: 1114.18, accuracy: 0.4853
epoch: 57, loss: 1111.27, accuracy: 0.4944
epoch: 58, loss: 1109.84, accuracy: 0.5085
epoch: 59, loss: 1110.05, accuracy: 0.4706
epoch: 60, loss: 1109.74, accuracy: 0.496
Epoch    61: reducing learning rate of group 0 to 5.0000e-03.
epoch: 61, loss: 1108.2, accuracy: 0.4966
epoch: 62, loss: 1081.18, accuracy: 0.5113
epoch: 63, loss: 1076.92, accuracy: 0.5018
epoch: 64, loss: 1076.56, accuracy: 0.5064
epoch: 65, loss: 1075.77, accuracy: 0.5075
epoch: 66, loss: 1074.11, accuracy: 0.5082
epoch: 67, loss: 1075.21, accuracy: 0.4991
epoch: 68, loss: 1073.9, accuracy: 0.5044
epoch: 69, loss: 1072.23, accuracy: 0.5068
epoch: 70, loss: 1070.19, accuracy: 0.5034
epoch: 71, loss: 1073.38, accuracy: 0.5101
epoch: 72, loss: 1071.26, accuracy: 0.4953
epoch: 73, loss: 1069.44, accuracy: 0.5049
epoch: 74, loss: 1071.51, accuracy: 0.5003
epoch: 75, loss: 1045.09, accuracy: 0.5206
epoch: 76, loss: 1040.8, accuracy: 0.5171
epoch: 77, loss: 1041.07, accuracy: 0.5177
epoch: 78, loss: 1039.56, accuracy: 0.5195
epoch: 79, loss: 1038.8, accuracy: 0.5183
epoch: 80, loss: 1039.84, accuracy: 0.521
epoch: 81, loss: 1038.59, accuracy: 0.5212
epoch: 82, loss: 1040.0, accuracy: 0.5164
epoch: 83, loss: 1040.28, accuracy: 0.5225
Epoch    84: reducing learning rate of group 0 to 2.5000e-04.
epoch: 84, loss: 1039.28, accuracy: 0.517
epoch: 85, loss: 1038.11, accuracy: 0.5175
epoch: 86, loss: 1035.07, accuracy: 0.518
epoch: 87, loss: 1036.93, accuracy: 0.5207
epoch: 88, loss: 1036.05, accuracy: 0.5184
epoch: 89, loss: 1036.47, accuracy: 0.5187
epoch: 90, loss: 1036.25, accuracy: 0.5209
epoch: 91, loss: 1037.22, accuracy: 0.521
epoch: 92, loss: 1036.21, accuracy: 0.5204
epoch: 93, loss: 1037.03, accuracy: 0.5199
epoch: 94, loss: 1037.59, accuracy: 0.5191
Epoch    95: reducing learning rate of group 0 to 1.2500e-04.
epoch: 95, loss: 1036.6, accuracy: 0.5192
epoch: 96, loss: 1034.46, accuracy: 0.5185
epoch: 97, loss: 1035.46, accuracy: 0.5202
epoch: 98, loss: 1035.62, accuracy: 0.5195
epoch: 99, loss: 1032.87, accuracy: 0.5217
time analysis:
    all 1208.5855 s
    train 1206.2394 s
Accuracy of     0 : 62 %
Accuracy of     1 : 58 %
Accuracy of     2 : 40 %
Accuracy of     3 : 28 %
Accuracy of     4 : 38 %
Accuracy of     5 : 47 %
Accuracy of     6 : 62 %
Accuracy of     7 : 67 %
Accuracy of     8 : 63 %
Accuracy of     9 : 60 %
```

